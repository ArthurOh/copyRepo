CREATE TABLE int_newsletter
(
    id serial NOT NULL,
    publish_year varchar(4) NOT NULL,
    publish_month varchar(2) NOT NULL,
    publish_no varchar(10) NOT NULL,
    views int NOT NULL default 0,
    title_int varchar(200) NOT NULL,
    title varchar(200) NOT NULL,
    content_int text NOT NULL,
    content text NOT NULL,
    image varchar(500),
    language varchar(2) NOT NULL,
    state varchar(15) NOT NULL,
    publish_at date NOT NULL default now(),
    create_at timestamp with time zone NOT NULL default now(),
    update_at timestamp with time zone,
    writer int NOT NULL,
    rewriter int,
    CONSTRAINT int_newsletter_pkey PRIMARY KEY (id),
    CONSTRAINT int_newsletter_writer_fkey FOREIGN KEY (writer) REFERENCES int_admin_user (id),
    CONSTRAINT int_newsletter_rewriter_fkey FOREIGN KEY (rewriter) REFERENCES int_admin_user (id)
);