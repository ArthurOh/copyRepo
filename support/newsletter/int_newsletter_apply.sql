CREATE TABLE int_newsletter_apply
(
    id serial NOT NULL,
    name varchar(50) NOT NULL,
    email varchar(255) NOT NULL,
    country varchar(30) NOT NULL,
    apply_language varchar(10) [] NOT NULL,
    state varchar(15) NOT NULL,
    create_at timestamp with time zone NOT NULL default now(),
    language varchar(2),
    CONSTRAINT int_newsletter_apply_pkey PRIMARY KEY (id)
);