drop table int_admin_log;
drop table int_admin_user;

drop table int_brand_category;
drop table int_brand;

drop table int_item;

drop table int_office_info;
drop table int_visual_contents;

drop table int_news_attachment;
drop table int_news;

drop table int_newsletter_apply;
drop table int_newsletter;

drop table int_cookery;
drop table int_ingredient;
drop table int_recipe;
drop table int_recipe_theme;

