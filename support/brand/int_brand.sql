CREATE TABLE int_brand
(
    id serial NOT NULL,
    code character varying(20) NOT NULL,
    title character varying(50) NOT NULL,
    description character varying(500),
    slogan character varying(100),
    point character varying(100),
    logo_img_path character varying(500),
    pc_img_path character varying(500),
    mobile_img_path character varying(500),
    banner_img_path character varying(500),
    relation_site_name character varying(500),
    relation_site_url character varying(1000),
    state character varying(15) NOT NULL,
    publish_at timestamp with time zone NOT NULL default current_timestamp,
    create_at timestamp with time zone NOT NULL default current_timestamp,
    update_at timestamp with time zone,
    writer character varying(50) NOT NULL,
    rewriter character varying(50),
    language character varying(2) NOT NULL,
    CONSTRAINT int_brand_pkey PRIMARY KEY (id)
);