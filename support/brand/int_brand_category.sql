CREATE TABLE int_brand_category
(
    id serial NOT NULL,
    int_brand_id integer NOT NULL,
    code character varying(20) NOT NULL,
    title character varying(50) NOT NULL,
    description character varying(1500),
    ordering int NOT NULL default 0,    
    state character varying(15) NOT NULL,
    create_at timestamp with time zone NOT NULL default current_timestamp,
    update_at timestamp with time zone,
    writer character varying(50) NOT NULL,
    rewriter character varying(50),
    language character varying(2) NOT NULL,
    CONSTRAINT int_brand_category_pkey PRIMARY KEY (id),
    CONSTRAINT int_brand_category_int_brand_id_fkey FOREIGN KEY (int_brand_id) REFERENCES int_brand (id)
);