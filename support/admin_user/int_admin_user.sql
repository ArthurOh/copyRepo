CREATE TABLE int_admin_user
(
  id serial NOT NULL,
  user_id varchar(50) NOT NULL,
  user_pw varchar(500) NOT NULL,
  user_name varchar(20) NOT NULL,
  department varchar(30),
  user_email varchar(255),
  user_tel varchar(255),
  type varchar(30),
  roles varchar(30)[],
  state varchar(15) not null default 'on',
  comments varchar(200),
  create_at timestamp with time zone NOT NULL DEFAULT now(),
  writer integer,
  rewriter integer,
  update_at timestamp with time zone,
  UNIQUE(user_id),
  CONSTRAINT int_admin_user_pkey PRIMARY KEY (id),
  CONSTRAINT int_admin_user_writer_fkey FOREIGN KEY (writer) REFERENCES int_admin_user (id),
  CONSTRAINT int_admin_user_rewriter_fkey FOREIGN KEY (rewriter) REFERENCES int_admin_user (id)
);