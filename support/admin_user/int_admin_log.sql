CREATE TABLE int_admin_log
(
  admin_id character varying(20) NOT NULL,
  log_at timestamp with time zone NOT NULL DEFAULT now(),
  action character varying(1000),
  ip character varying(40)
);