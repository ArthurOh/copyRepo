\i admin_user/int_admin_log.sql
\i admin_user/int_admin_user.sql


\i brand/int_brand_category.sql
\i brand/int_brand.sql

\i item/int_item.sql

\i misc/int_office_info.sql
\i misc/int_visual_contents.sql

\i news/int_news_attachment.sql
\i news/int_news.sql

\i newsletter/int_newsletter_apply.sql
\i newsletter/int_newsletter.sql

\i recipe/int_cookery.sql
\i recipe/int_recipe_theme.sql
\i recipe/int_recipe.sql
\i recipe/int_ingredient.sql