CREATE TABLE int_email_contact_log
(
    id serial NOT NULL,
    subject varchar(255) NOT NULL,
    comment text,
    file1 varchar(255),
    file2 varchar(255),
    file3 varchar(255),
    file4 varchar(255),
    file5 varchar(255),
    name varchar(50),
    email varchar(255),
    country varchar(100),
    create_at timestamp with time zone NOT NULL default now(),
    state varchar(15) NOT NULL default 'wait',
    language varchar(2) NOT NULL,
    CONSTRAINT int_email_contact_log_pkey PRIMARY KEY (id)
);