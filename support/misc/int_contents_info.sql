CREATE TABLE int_contents_info
(
    id serial NOT NULL,
    contents text NOT NULL,
    type varchar(30) NOT NULL,
    create_at timestamp with time zone NOT NULL default now(),
    update_at timestamp with time zone,
    writer integer NOT NULL,
    rewriter integer,
    language character varying(2) NOT NULL,
    CONSTRAINT int_contents_pkey PRIMARY KEY (id)
);