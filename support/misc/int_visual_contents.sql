CREATE TABLE int_visual_contents
(
    id serial NOT NULL,
    type varchar(20) NOT NULL,
    view_type varchar(20),
    title varchar(255) NOT NULL,
    sub_title varchar(255),
    contents text,
    image varchar(255),
    m_image varchar(255),
    url varchar(255),
    video_description varchar(500),
    publish_start_at date,
    publish_end_at date,
    ordering int NOT NULL default 0,
    state varchar(15) NOT NULL,
    create_at timestamp with time zone NOT NULL default now(),
    update_at timestamp with time zone,
    writer varchar(50) NOT NULL,
    rewriter varchar(50),
    views integer not null default 0,
    language varchar(2) NOT NULL,
    CONSTRAINT int_visual_contents_pkey PRIMARY KEY (id)
);