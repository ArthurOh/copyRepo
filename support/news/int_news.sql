CREATE TABLE int_news
(
    id serial NOT NULL,
    title character varying(200) NOT NULL,
    contents text NOT NULL,
    views int NOT NULL default 0,
    tags character varying(200),
    list_img_path character varying(500) NOT NULL,
    state character varying(15) NOT NULL,
    publish_at timestamp with time zone NOT NULL default current_timestamp,
    create_at timestamp with time zone NOT NULL default current_timestamp,
    update_at timestamp with time zone,
    writer character varying(50) NOT NULL,
    rewriter character varying(50),
    language character varying(2) NOT NULL,
    CONSTRAINT int_news_pkey PRIMARY KEY (id)
);