CREATE TABLE int_news_attachment
(
    id serial NOT NULL,
    int_news_id int,
    original_file_name  character varying(50) NOT NULL,
    path character varying(500) NOT NULL,
    CONSTRAINT int_news_attachment_pkey PRIMARY KEY (id),
    CONSTRAINT int_news_attachment_int_news_id_fkey FOREIGN KEY (int_news_id) REFERENCES int_news (id)
);