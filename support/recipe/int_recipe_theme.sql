CREATE TABLE int_recipe_theme
(
  id serial,
  title character varying(50) NOT NULL,
  list_img_path character varying(500) NOT NULL,
  state character varying(15) NOT NULL,
  create_at timestamp with time zone NOT NULL DEFAULT now(),
  update_at timestamp with time zone,
  writer character varying(50) NOT NULL,
  rewriter character varying(50),
  language character varying(2) NOT NULL,
  ordering smallint NOT NULL DEFAULT 0,
  code character varying(20),
  CONSTRAINT int_recipe_theme_pkey PRIMARY KEY (id)
)