CREATE TABLE int_cookery
(
  id serial,
  int_recipe_id integer NOT NULL,
  title character varying(200) NOT NULL,
  directions character varying(200)[] NOT NULL,
  ordering integer DEFAULT 0,
  CONSTRAINT int_cookery_pkey PRIMARY KEY (id),
  CONSTRAINT int_cookery_recipe_id_fkey FOREIGN KEY (int_recipe_id)
      REFERENCES public.int_recipe (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)