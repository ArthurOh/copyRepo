
CREATE TABLE int_ingredient
(
  id serial, -- PK
  int_cookery_id integer, -- 조리법 FK
  type character varying(10), -- 일반, 샘표 제품 구분자
  int_item_id integer, -- 샘표 제품 id FK
  volume character varying(50), -- 제품 용량
  title character varying(100) NOT NULL, -- 재료명
  int_recipe_id integer,
  CONSTRAINT int_ingredient_pkey PRIMARY KEY (id),
  CONSTRAINT int_ingredient_int_cookery_id_fkey FOREIGN KEY (int_cookery_id)
      REFERENCES public.int_cookery (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT int_ingredient_int_item_id_fkey FOREIGN KEY (int_item_id)
      REFERENCES public.int_item (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT int_ingredient_int_recipe_id_fkey FOREIGN KEY (int_recipe_id)
      REFERENCES public.int_recipe (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
COMMENT ON TABLE public.int_ingredient IS '조리법의 재료 정보';
COMMENT ON COLUMN public.int_ingredient.id IS 'PK';
COMMENT ON COLUMN public.int_ingredient.int_cookery_id IS '조리법 FK';
COMMENT ON COLUMN public.int_ingredient.type IS '일반, 샘표 제품 구분자';
COMMENT ON COLUMN public.int_ingredient.int_item_id IS '샘표 제품 id FK';
COMMENT ON COLUMN public.int_ingredient.volume IS '제품 용량';
COMMENT ON COLUMN public.int_ingredient.title IS '재료명';