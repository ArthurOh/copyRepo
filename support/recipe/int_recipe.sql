CREATE TABLE int_recipe
(
  id serial,
  title character varying(50) NOT NULL,
  title_sub character varying(200),
  views integer NOT NULL DEFAULT 0,
  chef character varying(50),
  main_ingredient character varying(200),
  serving character varying(120),
  view_img_path character varying(500) NOT NULL,
  description character varying(2000),
  tip character varying(300),
  state character varying(15) NOT NULL,
  publish_at timestamp with time zone NOT NULL DEFAULT now(),
  create_at timestamp with time zone NOT NULL DEFAULT now(),
  update_at timestamp with time zone,
  writer character varying(50) NOT NULL,
  rewriter character varying(50),
  language character varying(2) NOT NULL,
  recommend integer[],
  int_recipe_theme_id integer[],
  tags character varying(500),
  CONSTRAINT int_recipe_pkey PRIMARY KEY (id)
)