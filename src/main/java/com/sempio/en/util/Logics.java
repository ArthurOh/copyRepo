package com.sempio.en.util;

import javax.servlet.http.HttpSession;

import com.sempio.en.entity.IntAdminUser;

public class Logics {

    public static IntAdminUser adminFromSession(HttpSession session) {
        return (IntAdminUser) session.getAttribute(Const.ADMIN_IN_SESSION_KEY);
    }

    public static void adminToSession(IntAdminUser admin, HttpSession session) {
        session.setAttribute(Const.ADMIN_IN_SESSION_KEY, admin);
    }

    public static boolean isAdminLogin(HttpSession session) {
        IntAdminUser admin = adminFromSession(session);
        return null != admin;
    }
}
