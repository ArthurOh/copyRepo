package com.sempio.en.util;

import org.apache.commons.lang3.StringUtils;

import com.sempio.en.code.Language;

public class LanguageCheck {

    public static boolean languageCheck(String language) {

        boolean result;

        if (StringUtils.equals(Language.en.toString(), language)
                || StringUtils.equals(Language.cn.toString(), language)
                || StringUtils.equals(Language.ru.toString(), language)) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }
}
