package com.sempio.en.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.entity.IntRecipeTheme;

@Repository
public class IntRecipeThemeDao extends DaoBase {
    private final static String[] ThemeState = { "on", "off" };

    public IntRecipeTheme select(int id) {
        return session().get(IntRecipeTheme.class, id);
    }

    public Integer insert(IntRecipeTheme intRecipeTheme) {
        Integer id = ((Number) session().save(intRecipeTheme)).intValue();
        session().flush();
        return id;
    }

    public int update(IntRecipeTheme intRecipeTheme) {
        StringBuilder query = new StringBuilder();
        query.append("update IntRecipeTheme a set ")
             .append("code = :code, ")
             .append("title = :title, ")
             .append("listImgPath = :listImgPath, ")
             .append("state = :state, ")
             .append("updateAt = now(), ")
             .append("rewriter = :rewriter where id = :id");
        return session().createQuery(query.toString())
                        .setParameter("code", intRecipeTheme.getCode())
                        .setParameter("title", intRecipeTheme.getTitle())
                        .setParameter("listImgPath", intRecipeTheme.getListImgPath())
                        .setParameter("state", intRecipeTheme.getState())
                        .setParameter("rewriter", intRecipeTheme.getRewriter())
                        .setParameter("id", intRecipeTheme.getId()).executeUpdate();
    }

    public int update(Integer[] ids, State state, String rewriter) {
        return session().createQuery("update IntRecipeTheme a set a.state = :state, a.rewriter = :rewriter, a.updateAt = now() where id in (:ids)")
                        .setParameter("state", state.name()).setParameter("rewriter", rewriter).setParameterList("ids", ids)
                        .executeUpdate();
    }

    public int updateOrdering(int id, int ordering, Language language) {
        return session().createQuery("update IntRecipeTheme a set a.ordering = :ordering where id = :id and language = :language")
                        .setParameter("ordering", ordering).setParameter("id", id).setParameter("language", language.name()).executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public List<IntRecipeTheme> list(Language language, State state) {
        Criteria q = session().createCriteria(IntRecipeTheme.class);
        q.add(Restrictions.eq("language", language.name()));
        if (state == null) {
            q.add(Restrictions.ne("state", State.deleted.name()));
        } else {
            q.add(Restrictions.eq("state", state.name()));
        }

        return q.addOrder(Order.asc("ordering")).addOrder(Order.desc("id")).list();
    }

    public int count(Language language, State state) {
        Criteria q = session().createCriteria(IntRecipeTheme.class);
        q.add(Restrictions.eq("language", language.name()));
        if (state == null) {
            q.add(Restrictions.ne("state", State.deleted.name()));
        } else {
            q.add(Restrictions.eq("state", state.name()));
        }
        Number num = (Number) q.setProjection(Projections.rowCount()).uniqueResult();
        return num.intValue();
    }

    public IntRecipeTheme selectWithCode(Language language, String recipeTheme) {
        Criteria q = session().createCriteria(IntRecipeTheme.class).add(Restrictions.eq("code", recipeTheme)).add(Restrictions.in("state", ThemeState)).add(Restrictions.eq("language", language.name()));
        return (IntRecipeTheme) q.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public List<IntRecipeTheme> list(Language language, State state, Integer[] ids) {
        Criteria q = session().createCriteria(IntRecipeTheme.class);
        q.add(Restrictions.eq("language", language.name()));
        if (state == null) {
            q.add(Restrictions.ne("state", State.deleted.name()));
        } else {
            q.add(Restrictions.eq("state", state.name()));
        }
        q.add(Restrictions.in("id", ids));
        return q.addOrder(Order.asc("ordering")).addOrder(Order.desc("id")).list();
    }
}
