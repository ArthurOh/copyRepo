package com.sempio.en.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.code.VisualType;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntVisualContents;
import com.sempio.en.util.Pager;

@Repository
public class IntVisualContentsDao extends DaoBase {

    public IntVisualContents select(int id) {
        return (IntVisualContents) session().get(IntVisualContents.class, id);
    }

    public Integer insert(IntVisualContents visualContents) {
        Integer id = ((Number) session().save(visualContents)).intValue();
        session().flush();
        return id;
    }

    public void update(IntVisualContents visualContents) {
        session().update(visualContents);
    }
    
    public void update(VisualType type, IntVisualContents visualContents) {
        update(visualContents);
    }
    
    /* views 컬럼을 제외하기 위해 따로 뺌 */
    public int updatePopup(IntVisualContents visualContents) {
        StringBuilder query = new StringBuilder();
        query.append("update IntVisualContents a set ")
             .append("a.title = :title,")
             .append("a.contents = :contents, ")
             .append("a.publishStartAt = :publishStartAt, ")
             .append("a.publishEndAt = :publishEndAt, ")
             .append("a.state = :state, ")
             .append("a.updateAt = now(), ")
             .append("a.rewriter = :rewriter ")
             .append("where id = :id");
        return session().createQuery(query.toString())
                        .setParameter("title", visualContents.getTitle())
                        .setParameter("contents", visualContents.getContents())
                        .setParameter("publishStartAt", visualContents.getPublishStartAt())
                        .setParameter("publishEndAt", visualContents.getPublishEndAt())
                        .setParameter("state", visualContents.getState())
                        .setParameter("rewriter", visualContents.getRewriter())                        
                        .setParameter("id", visualContents.getId())
                        .executeUpdate();
    }
    
    /* 순서 변경 업데이트 컬럼을 제외하기 위해 따로 뺌 */
    public int updateTop(IntVisualContents visualContents) {
        StringBuilder query = new StringBuilder();
        query.append("update IntVisualContents a set ")
             .append("a.viewType = :viewType, ")
             .append("a.title = :title,")
             .append("a.subTitle = :subTitle, ")
             .append("a.image = :image, ")
             .append("a.mImage = :mImage, ")
             .append("a.publishStartAt = :publishStartAt, ")
             .append("a.publishEndAt = :publishEndAt, ")
             .append("a.url = :url, ")
             .append("a.videoDescription = :videoDescription, ")
             .append("a.state = :state, ")
             .append("a.updateAt = now(), ")
             .append("a.rewriter = :rewriter ")
             .append("where id = :id");
        return session().createQuery(query.toString())
                        .setParameter("viewType", visualContents.getViewType())
                        .setParameter("title", visualContents.getTitle())
                        .setParameter("subTitle", visualContents.getSubTitle())
                        .setParameter("image", visualContents.getImage())
                        .setParameter("mImage", visualContents.getmImage())
                        .setParameter("publishStartAt", visualContents.getPublishStartAt())
                        .setParameter("publishEndAt", visualContents.getPublishEndAt())
                        .setParameter("url", visualContents.getUrl())
                        .setParameter("videoDescription", visualContents.getVideoDescription())
                        .setParameter("state", visualContents.getState())
                        .setParameter("rewriter", visualContents.getRewriter())                        
                        .setParameter("id", visualContents.getId())
                        .executeUpdate();
    }
    
    /* 순서 변경 업데이트 컬럼을 제외하기 위해 따로 뺌 */
    public int updateBottom(IntVisualContents visualContents) {
        StringBuilder query = new StringBuilder();
        query.append("update IntVisualContents a set ")
             .append("a.title = :title,")
             .append("a.subTitle = :subTitle, ")
             .append("a.contents = :contents, ")
             .append("a.image = :image, ")
             .append("a.url = :url, ")
             .append("a.updateAt = now(), ")
             .append("a.rewriter = :rewriter ")
             .append("where id = :id");
        return session().createQuery(query.toString())
                        .setParameter("title", visualContents.getTitle())
                        .setParameter("subTitle", visualContents.getSubTitle())
                        .setParameter("contents", visualContents.getContents())
                        .setParameter("image", visualContents.getImage())
                        .setParameter("url", visualContents.getUrl())
                        .setParameter("rewriter", visualContents.getRewriter())
                        .setParameter("id", visualContents.getId())
                        .executeUpdate();
    }

    public int update(Integer[] ids, State state, IntAdminUser adminUser) {
        return session().createQuery("update IntVisualContents a set a.state = :state, a.rewriter = :rewriter, a.updateAt = now() where id in (:ids)")
                        .setParameter("state", state).setParameter("rewriter", adminUser.getUserId())
                        .setParameterList("ids", ids).executeUpdate();
    }

    public List<IntVisualContents> selectBy(List<Criterion> criterions, List<Order> orders, Pager pager) {
        Criteria q = session().createCriteria(IntVisualContents.class);
        for (Criterion c : criterions) {
            q.add(c);
        }
        if (!CollectionUtils.isEmpty((orders))) {
            for (Order item : orders) {
                q.addOrder(item);
            }
        }
        if (pager != null) {
            q.setFirstResult(pager.getOffset());
            q.setMaxResults(pager.getEntriesPerPage());
        }
        @SuppressWarnings("unchecked")
        List<IntVisualContents> ret = q.list();
        return ret;
    }

    public int countBy(List<Criterion> criterions) {
        Criteria q = session().createCriteria(IntVisualContents.class);
        for (Criterion c : criterions) {
            q.add(c);
        }
        Number num = (Number) q.setProjection(Projections.rowCount()).uniqueResult();
        return num.intValue();
    }
    
    public int updateOrdering(int id, int ordering, Language language) {
        return session().createQuery("update IntVisualContents a set a.ordering = :ordering where id = :id and language = :language")
                        .setParameter("ordering", ordering).setParameter("id", id).setParameter("language", language).executeUpdate();
    }
}
