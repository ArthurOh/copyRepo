package com.sempio.en.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.sempio.en.code.ContentsType;
import com.sempio.en.code.Language;
import com.sempio.en.entity.IntContentsInfo;

@Repository
public class IntContentsInfoDao extends DaoBase {
    
    public IntContentsInfo select(int id) {
        return (IntContentsInfo) session().get(IntContentsInfo.class, id);
    }

    public Integer insert(IntContentsInfo contentsInfo) {
        Integer id = ((Number) session().save(contentsInfo)).intValue();
        session().flush();
        return id;
    }

    public void update(IntContentsInfo contentsInfo) {
        session().update(contentsInfo);
    }

    public IntContentsInfo select(ContentsType contentsType, Language language) {
        Criteria q = session().createCriteria(IntContentsInfo.class);
        q.add(Restrictions.eq("type", contentsType));
        q.add(Restrictions.eq("language", language));
        return (IntContentsInfo) q.addOrder(Order.desc("id")).uniqueResult();
    }
}
