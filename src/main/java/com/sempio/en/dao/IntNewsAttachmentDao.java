package com.sempio.en.dao;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.sempio.en.entity.IntNewsAttachment;

@Repository
public class IntNewsAttachmentDao extends DaoBase {

    public Integer insert(IntNewsAttachment attachment) {
        return ((Number) session().save(attachment)).intValue();
    }

    public void update(Integer[] attachmentid, Integer id) {
        session().createQuery("update IntNewsAttachment set int_news_id = :newsId where id in (:attachmentid)")
                .setParameter("newsId", id)
                .setParameterList("attachmentid", attachmentid)
                .executeUpdate();
    }

    public IntNewsAttachment selectOne(Integer id) {
        return (IntNewsAttachment) session()
                .createCriteria(IntNewsAttachment.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

    public void updateOne(Integer attachmentid, Integer id) {
        session().createQuery("update IntNewsAttachment set intNews = :newsId where id = :attachmentid")
                .setParameter("newsId", id)
                .setParameter("attachmentid", attachmentid)
                .executeUpdate();
    }

}
