package com.sempio.en.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Repository;

import com.sempio.en.code.State;
import com.sempio.en.command.IntNewsletterApplySearchForm;
import com.sempio.en.entity.IntNewsletterApply;
import com.sempio.en.util.Pager;

@Repository
public class IntNewsletterApplyDao extends DaoBase {

    public IntNewsletterApply select(int id) {
        return (IntNewsletterApply) session().get(IntNewsletterApply.class, id);
    }

    public Integer insert(IntNewsletterApply newsletterApply) {
        Integer id = ((Number) session().save(newsletterApply)).intValue();
        session().flush();
        return id;
    }

    public void update(IntNewsletterApply newsletterApply) {
        session().update(newsletterApply);
    }

    public int update(Integer[] ids, State state) {
        return session().createQuery("update IntNewsletterApply a set a.state = :state where id in (:ids)")
                .setParameter("state", state).setParameterList("ids", ids).executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public List<IntNewsletterApply> list(IntNewsletterApplySearchForm form, Pager pager) {
        Criteria q = session().createCriteria(IntNewsletterApply.class);
        form.addRestrictions(q);
        q.addOrder(Order.desc("id"));
        if (pager != null) {
            q.setFirstResult(pager.getOffset()).setMaxResults(pager.getEntriesPerPage());
        }
        return q.list();
    }

    public int count(IntNewsletterApplySearchForm form) {
        Criteria q = session().createCriteria(IntNewsletterApply.class);
        form.addRestrictions(q);
        Number num = (Number) q.setProjection(Projections.rowCount()).uniqueResult();
        return num.intValue();
    }
}
