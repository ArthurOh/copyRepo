package com.sempio.en.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.command.IntNewsletterSearchForm;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntNewsletter;
import com.sempio.en.util.Pager;

@Repository
public class IntNewsletterDao extends DaoBase {

    public IntNewsletter select(int id) {
        return session().get(IntNewsletter.class, id);
    }

    public Integer insert(IntNewsletter newsletter) {
        Integer id = ((Number) session().save(newsletter)).intValue();
        session().flush();
        return id;
    }

    public void update(IntNewsletter intNewsletter) {
        session().update(intNewsletter);
    }

    public int update(Integer[] ids, State state, IntAdminUser adminUser) {
        return session().createQuery("update IntNewsletter a set a.state = :state, a.rewriter = :rewriter, a.updateAt = now() where id in (:ids)")
                        .setParameter("state", state)
                        .setParameter("rewriter", adminUser)
                        .setParameterList("ids", ids).executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public List<IntNewsletter> list(IntNewsletterSearchForm form, Language language, Pager pager) {
        Criteria q = session().createCriteria(IntNewsletter.class);
        q.add(Restrictions.eq("language", language));
        form.addRestrictions(q);
        q.addOrder(Order.desc("publishYear")).addOrder(Order.desc("publishMonth")).addOrder(Order.desc("publishNo"));
        //q.addOrder(Order.desc("id"));
        return q.setFirstResult(pager.getOffset()).setMaxResults(pager.getEntriesPerPage()).list();
    }

    public int count(IntNewsletterSearchForm form, Language language) {
        Criteria q = session().createCriteria(IntNewsletter.class);
        q.add(Restrictions.eq("language", language));
        form.addRestrictions(q);
        Number num = (Number) q.setProjection(Projections.rowCount()).uniqueResult();
        return num.intValue();
    }

    @SuppressWarnings("unchecked")
    public List<IntNewsletter> selectBy(List<Criterion> criterions, List<Order> orders, Pager pager) {
        Criteria q = session().createCriteria(IntNewsletter.class);
        for (Criterion c : criterions) {
            q.add(c);
        }
        if (!CollectionUtils.isEmpty((orders))) {
            for (Order item : orders) {
                q.addOrder(item);
            }
        }
        if (pager != null) {
            q.setFirstResult(pager.getOffset());
            q.setMaxResults(pager.getEntriesPerPage());
        }
        return q.list();
    }

    public int countBy(List<Criterion> criterions) {
        Criteria q = session().createCriteria(IntNewsletter.class);
        for (Criterion c : criterions) {
            q.add(c);
        }
        Number num = (Number) q.setProjection(Projections.rowCount()).uniqueResult();
        return num.intValue();
    }

    // for front search page
    @SuppressWarnings("unchecked")
    public List<IntNewsletter> searchBy(String q, Language language, Pager pager) {
        Criteria query = session().createCriteria(IntNewsletter.class);
        if (StringUtils.isNotEmpty(q)) {
            query.add(Restrictions.or((Restrictions.ilike("title", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("titleInt", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("content", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("contentInt", q, MatchMode.ANYWHERE))));
        }
        query.add(Restrictions.eq("state", State.on));
        query.add(Restrictions.eq("language", language));
        query.add(Restrictions.sqlRestriction("publish_at <= current_date"));
        query.addOrder(Order.desc("publishAt"));
        query.addOrder(Order.desc("id"));

        if (pager != null) {
            query.setFirstResult(pager.getOffset());
            query.setMaxResults(pager.getEntriesPerPage());
        }

        return query.list();
    }

    // for front search page
    public int countBy(String q, Language language) {
        Criteria query = session().createCriteria(IntNewsletter.class);
        if (StringUtils.isNotEmpty(q)) {
            query.add(Restrictions.or((Restrictions.ilike("title", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("titleInt", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("content", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("contentInt", q, MatchMode.ANYWHERE))));
        }
        query.add(Restrictions.eq("state", State.on));
        query.add(Restrictions.eq("language", language));
        query.add(Restrictions.sqlRestriction("publish_at <= current_date"));
        query.setProjection(Projections.rowCount());

        Number num = (Number) query.uniqueResult();
        return num.intValue();
    }

    public int updateViews(int id) {
        Query q = session().createQuery("update IntNewsletter a set a.views = (a.views+1) where id = :id");
        q.setParameter("id", id);
        return q.executeUpdate();
    }

    // for xml
    @SuppressWarnings("unchecked")
    public List<IntNewsletter> listForSitemap() {
        Criteria query = session().createCriteria(IntNewsletter.class)
                .add(Restrictions.eq("language", Language.en))
                .add(Restrictions.eq("state", State.on))
                .addOrder(Order.desc("publishYear"))
                .addOrder(Order.desc("publishMonth"))
                .addOrder(Order.desc("publishNo"));

        return query.list();
    }
}
