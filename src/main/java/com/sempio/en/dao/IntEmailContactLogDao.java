package com.sempio.en.dao;

import org.springframework.stereotype.Repository;

import com.sempio.en.entity.IntEmailContactLog;

@Repository
public class IntEmailContactLogDao extends DaoBase {

    public IntEmailContactLog select(int id) {
        return session().get(IntEmailContactLog.class, id);
    }

    public Integer insert(IntEmailContactLog intEmailContactLog) {
        Integer id = ((Number) session().save(intEmailContactLog)).intValue();
        session().flush();
        return id;
    }

    public void update(IntEmailContactLog intEmailContactLog) {
        session().update(intEmailContactLog);
    }
}
