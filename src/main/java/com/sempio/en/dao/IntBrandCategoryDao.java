package com.sempio.en.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.entity.IntBrandCategory;

@Repository
public class IntBrandCategoryDao extends DaoBase {


    @SuppressWarnings("unchecked")
    public List<IntBrandCategory> selectListForAdmin(Integer brandId) {
        return session().createCriteria(IntBrandCategory.class)
                .add(Restrictions.eq("intBrand.id", brandId))
                .add(Restrictions.ne("state", State.deleted))
                .addOrder(Order.asc("ordering"))
                .addOrder(Order.desc("id"))
                .list();
    }

    public IntBrandCategory selectForAdmin(Integer id) {
        return (IntBrandCategory) session().createCriteria(IntBrandCategory.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

    public Integer insert(IntBrandCategory brandCategory) {
        Integer id = ((Number) session().save(brandCategory)).intValue();
        session().flush();
        return id;
    }

    public IntBrandCategory getBrandCode() {
        return (IntBrandCategory) session().createCriteria(IntBrandCategory.class)
                .addOrder(Order.desc("id"))
                .setMaxResults(1)
                .uniqueResult();
    }

    public IntBrandCategory getbc(Integer id) {
        return session().get(IntBrandCategory.class, id);
    }

    public void update(IntBrandCategory brandCategory) {
        session().update(brandCategory);
    }

    public int updateOrdering(Integer id, Integer ordering, State state, Integer brandId) {
        Query query;

        if (StringUtils.isNotBlank(state.toString())) {
            query = session().createQuery("update IntBrandCategory set ordering = :ordering where id = :id and state = :state and int_brand_id = :brandId")
                .setParameter("state", state);
        } else {
            query = session().createQuery("update IntBrandCategory set ordering = :ordering where id = :id and int_brand_id = :brandId");
        }

        query.setParameter("ordering", ordering)
        .setParameter("brandId", brandId)
        .setParameter("id", id);

        return query.executeUpdate();
    }

    public int update (Integer[] ids, State state) {
        return session().createQuery("update IntBrandCategory set state = :state where id in (:ids)")
                .setParameter("state", state)
                .setParameterList("ids", ids)
                .executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public List<IntBrandCategory> bcList(Integer brandId, String state, Language language) {
        Criteria c = session().createCriteria(IntBrandCategory.class);

        if (StringUtils.isNoneBlank(state)) {
            c.add(Restrictions.eq("state", State.valueOf(state)));
        } else {
            c.add(Restrictions.ne("state", State.deleted));
        }

        c.add(Restrictions.eq("intBrand.id", brandId));
        c.add(Restrictions.eq("language", language));
        return c.addOrder(Order.desc("ordering")).addOrder(Order.asc("id")).list();
    }

    // list for front
    @SuppressWarnings("unchecked")
    public List<IntBrandCategory> listForItem(Integer brandId) {
        return session().createCriteria(IntBrandCategory.class)
                .add(Restrictions.eq("intBrand.id", brandId))
                .add(Restrictions.eq("state", State.on))
                .add(Restrictions.eq("language", Language.en))
                .addOrder(Order.asc("ordering"))
                .addOrder(Order.desc("id"))
                .list();
    }

    // for front
    public IntBrandCategory getLastId(Integer brandId) {
        return (IntBrandCategory) session().createCriteria(IntBrandCategory.class)
                .add(Restrictions.eq("intBrand.id", brandId))
                .add(Restrictions.eq("state", State.on))
                .add(Restrictions.eq("language", Language.en))
                .addOrder(Order.asc("ordering"))
                .addOrder(Order.desc("id"))
                .setMaxResults(1)
                .uniqueResult();
    }
}