package com.sempio.en.dao;

import java.text.ParseException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.command.ItemSearchForm;
import com.sempio.en.entity.IntItem;
import com.sempio.en.util.BaseUtil;
import com.sempio.en.util.Pager;

@Repository
public class IntItemDao extends DaoBase  {

    @SuppressWarnings("unchecked")
    public List<IntItem> listForAdmin(Language language, ItemSearchForm searchForm, int limit, int offset) {
        Criteria queryItem = session().createCriteria(IntItem.class)
                .add(Restrictions.eq("language", language))
                .addOrder(Order.desc("createAt"))
                .addOrder(Order.desc("id"))
                .setMaxResults(limit)
                .setFirstResult(offset);

        queryItem = searchForm.addRestrictions(queryItem);

        return queryItem.list();
    }

    public int countForAdmin(Language language, ItemSearchForm searchForm) {
        Criteria queryItem = session().createCriteria(IntItem.class)
                .add(Restrictions.eq("language", language))
                .setProjection(Projections.rowCount());

        queryItem = searchForm.addRestrictions(queryItem);

        Number nr = (Number) queryItem.uniqueResult();
        return nr.intValue();
    }

    public Integer insert(IntItem item) {
        Integer id = ((Number) session().save(item)).intValue();
        session().flush();

        return id;
    }

    public IntItem getOne(Integer id) {
        return session().get(IntItem.class, id);
    }

    public void update(IntItem item) {
        session().update(item);
    }

    public int update (Integer[] ids, State state) {
        return session().createQuery("update IntItem set state = :state where id in (:ids)")
                .setParameter("state", state)
                .setParameterList("ids", ids)
                .executeUpdate();
    }

    // for excel download
    @SuppressWarnings("unchecked")
    public List<IntItem> listAll(Language language, ItemSearchForm searchForm) {
        Criteria q = session().createCriteria(IntItem.class);

        q.add(Restrictions.ne("state", State.deleted))
        .add(Restrictions.eq("language", language))
        .addOrder(Order.desc("createAt"))
        .addOrder(Order.desc("id"));

        if(StringUtils.isNotEmpty(searchForm.getBrandCategoryId())){
            q.add(Restrictions.eq("intBrandCategory.id", Integer.parseInt(searchForm.getBrandCategoryId())));
        }

        if(StringUtils.isNotEmpty(searchForm.getBrandId())){
            q.createCriteria("intBrandCategory")
                    .add(Restrictions.eq("intBrand.id", Integer.parseInt(searchForm.getBrandId())));
        }

        if(StringUtils.isNotEmpty(searchForm.getState())){
            q.add(Restrictions.eq("state", State.valueOf(searchForm.getState())));
        }

        if(StringUtils.isNotEmpty(searchForm.getQ())){
            q.add(Restrictions.ilike("title", searchForm.getQ(), MatchMode.ANYWHERE));
        }

        return q.list();
    }

    // for front item page
    @SuppressWarnings("unchecked")
    public List<IntItem> listForItem(Integer bcId) {
        Criteria q = session().createCriteria(IntItem.class);
        q.add(Restrictions.eq("intBrandCategory.id", bcId));
        q.add(Restrictions.eq("state", State.on));
        q.addOrder(Order.asc("ordering"));
        q.addOrder(Order.asc("id"));
        return q.list();
    }

    @SuppressWarnings("unchecked")
    public List<Integer> listWithBrandCategory(List<Integer> ids) {
        return  session().createQuery("select a.id from IntItem a where int_brand_category_id in (:ids) and state = :state")
                .setParameter("state", State.on)
                .setParameterList("ids", ids.toArray())
                .list();
    }

    // for front search page
    @SuppressWarnings("unchecked")
    public List<IntItem> searchBy(String q, Language language, Pager pager) throws ParseException {
        Criteria query = session().createCriteria(IntItem.class);
        if (StringUtils.isNotEmpty(q)) {
            query.add(Restrictions.or(
                    (Restrictions.ilike("title", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("pointMsg", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("tags", q, MatchMode.ANYWHERE))
                    ));
        }
        query.add(Restrictions.eq("state", State.on));
        query.add(Restrictions.eq("language", language));
        query.add(Restrictions.le("publishAt", BaseUtil.getToday()));
        query.addOrder(Order.desc("publishAt"));
        query.addOrder(Order.desc("id"));

        if (pager != null) {
            query.setFirstResult(pager.getOffset());
            query.setMaxResults(pager.getEntriesPerPage());
        }

        return query.list();
    }

    // for front search page
    public int countBy(String q, Language language) throws ParseException {
        Criteria query = session().createCriteria(IntItem.class);
        if (StringUtils.isNotEmpty(q)) {
            query.add(Restrictions.or(
                    (Restrictions.ilike("title", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("pointMsg", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("tags", q, MatchMode.ANYWHERE))
                    ));
        }
        query.add(Restrictions.eq("state", State.on));
        query.add(Restrictions.eq("language", language));
        query.add(Restrictions.le("publishAt", BaseUtil.getToday()));
        query.setProjection(Projections.rowCount());

        Number num = (Number) query.uniqueResult();
        return num.intValue();
    }

    public IntItem getOne(Object language, Integer id) {
        Criteria q = session().createCriteria(IntItem.class);

        return (IntItem) q.add(Restrictions.eq("language", language))
                .add(Restrictions.eq("id", id)).uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public List<IntItem> searchByTitle(String q , String language){
        Criteria query = session().createCriteria(IntItem.class);
        if (StringUtils.isNotEmpty(q)) {
            query.add(Restrictions.ilike("title", q, MatchMode.ANYWHERE));
        }
        query.add(Restrictions.eq("language", Language.valueOf(language)));
        return query.list();
    }
}
