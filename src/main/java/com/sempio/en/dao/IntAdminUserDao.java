package com.sempio.en.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.stereotype.Repository;

import com.sempio.en.code.State;
import com.sempio.en.command.IntAdminUserSearchForm;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.util.Pager;

@Repository
public class IntAdminUserDao extends DaoBase {

    public IntAdminUser select(String userId) {
        return (IntAdminUser) session().createCriteria(IntAdminUser.class).add(Restrictions.eq("userId", userId)).uniqueResult();
    }

    public IntAdminUser select(int id) {
        return session().get(IntAdminUser.class, id);
    }

    public Integer insert(IntAdminUser admin) {
        Integer id = ((Number) session().save(admin)).intValue();
        session().flush();
        return id;
    }

    public void update(IntAdminUser admin) {
        session().update(admin);
    }

    public int updateByUser(IntAdminUser admin) {
        StringBuilder query = new StringBuilder();
        query.append("update IntAdminUser a set a.userName = :userName,");
        query.append("a.department = :department,");
        query.append("a.userEmail = :userEmail,");
        query.append("a.userTel = :userTel,");
        query.append("a.rewriter = :rewriter,");
        query.append("a.updateAt = now()");
        if (StringUtils.isNoneBlank(admin.getUserPw())) {
            query.append(", a.userPw = :userPw");
        }
        query.append(" where id = :id");
        Query q = session().createQuery(query.toString()).setParameter("userName", admin.getUserName())
                .setParameter("department", admin.getDepartment())
                .setParameter("userEmail", admin.getUserEmail())
                .setParameter("userTel", admin.getUserTel())
                .setParameter("rewriter", admin.getRewriter())
                .setParameter("id", admin.getId());
        if (StringUtils.isNoneBlank(admin.getUserPw())) {
            q.setParameter("userPw", admin.getUserPw());
        }
        return q.executeUpdate();
    }

    public int update(Integer[] ids, State state, IntAdminUser adminUser) {
        return session().createQuery("update IntAdminUser a set a.state = :state, a.rewriter = :rewriter, a.updateAt = now() where id in (:ids)")
                        .setParameter("state", state)
                        .setParameter("rewriter", adminUser)
                        .setParameterList("ids", ids).executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public List<IntAdminUser> list(IntAdminUserSearchForm form, Pager pager) {
        Criteria q = session().createCriteria(IntAdminUser.class);
        form.addRestrictions(q);
        q.addOrder(Order.desc("id"));
        return q.setFirstResult(pager.getOffset()).setMaxResults(pager.getEntriesPerPage()).list();
    }

    public int count(IntAdminUserSearchForm form) {
        Criteria q = session().createCriteria(IntAdminUser.class);
        form.addRestrictions(q);
        Number num = (Number) q.setProjection(Projections.rowCount()).uniqueResult();
        return num.intValue();
    }

    public List<Object[]> selectLog(String adminId, int limit, int offset) {
        SQLQuery q = session().createSQLQuery("select log_at, ip, action from int_admin_log where admin_id = :adminId order by log_at desc");
        q.addScalar("log_at", TimestampType.INSTANCE)
            .addScalar("ip", StringType.INSTANCE)
            .addScalar("action", StringType.INSTANCE);
        q.setString("adminId", adminId);

        @SuppressWarnings("unchecked")
        List<Object[]> list = q.setMaxResults(limit).setFirstResult(offset).list();
        return list;
    }

    public int countLog(String adminId) {
        SQLQuery q = session().createSQLQuery("select count(*) from int_admin_log where admin_id = :adminId");
        q.setString("adminId", adminId);
        Number nr = (Number) q.uniqueResult();
        return nr.intValue();
    }

    public IntAdminUser selectForAdminById(String adminId) {
        return (IntAdminUser) session().createCriteria(IntAdminUser.class)
                .add(Restrictions.eq("userId", adminId))
                .uniqueResult();
    }

    public void insertLog(String adminId, String ip, String action) {
        session().createSQLQuery("insert into int_admin_log (admin_id, log_at, action, ip) " +
                "values (:adminId, current_timestamp, :action, :ip)")
            .setString("adminId", adminId)
            .setString("action", action)
            .setString("ip", ip)
            .executeUpdate();
    }
}
