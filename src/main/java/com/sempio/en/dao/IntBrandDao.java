package com.sempio.en.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.entity.IntBrand;

@Repository
public class IntBrandDao extends DaoBase {

    @SuppressWarnings("unchecked")
    public List<IntBrand> listForAdmin(Language language, int limit, int offset) {
        Criteria query = session().createCriteria(IntBrand.class);
        query.add(Restrictions.ne("state", State.deleted));
        query.add(Restrictions.eq("language", language));
        query.setMaxResults(limit);
        query.setFirstResult(offset);
        query.addOrder(Order.desc("createAt"));
        query.addOrder(Order.desc("id"));

        return query.list();
    }

    public int countForAdmin(Language language) {
        Criteria query = session().createCriteria(IntBrand.class);
        query.add(Restrictions.ne("state", State.deleted));
        query.add(Restrictions.eq("language", language));
        query.setProjection(Projections.rowCount());

        Number num = (Number) query.uniqueResult();
        return num.intValue();
    }

    public Integer insert(IntBrand brand) {
        Integer id = ((Number) session().save(brand)).intValue();
        session().flush();
        return id;
    }

    public IntBrand selectForAdmin(Integer id) {
        return (IntBrand) session().createCriteria(IntBrand.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

    public int update (Integer[] ids, State state) {
        return session().createQuery("update IntBrand a set a.state = :state where id in (:ids)")
                .setParameter("state", state)
                .setParameterList("ids", ids)
                .executeUpdate();
    }

    public IntBrand getBrand(Integer id) {
        return session().get(IntBrand.class, id);
    }

    public void update(IntBrand brand) {
        session().update(brand);
    }

    @SuppressWarnings("unchecked")
    public List<IntBrand> list(String language) {
        return session().createCriteria(IntBrand.class)
                .add(Restrictions.eq("state", State.on))
                .add(Restrictions.eq("language", Language.valueOf(language)))
                .addOrder(Order.desc("createAt"))
                .addOrder(Order.asc("id"))
                .list();
    }

    @SuppressWarnings("unchecked")
    public List<IntBrand> listAll(Language language) {
        return session().createCriteria(IntBrand.class)
                .add(Restrictions.ne("state", State.deleted))
                .add(Restrictions.eq("language", language))
                .addOrder(Order.desc("createAt"))
                .addOrder(Order.desc("id"))
                .list();
    }

    // list for front
    public IntBrand getOne(String code, Language language) {
        return (IntBrand) session().createCriteria(IntBrand.class)
                .add(Restrictions.eq("code", code))
                .add(Restrictions.eq("state", State.on))
                .add(Restrictions.eq("language", language))
                .addOrder(Order.desc("id"))
                .setMaxResults(1)
                .uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public List<IntBrand> listAll(State state) {
        return session().createCriteria(IntBrand.class)
        .add(Restrictions.eq("state", state))
        .addOrder(Order.asc("id"))
        .list();
    }

    // list for front
    @SuppressWarnings("unchecked")
    public List<IntBrand> listAll(Language language, State state) {
        return session().createCriteria(IntBrand.class)
                .add(Restrictions.eq("state", state))
                .add(Restrictions.eq("language", language))
                .addOrder(Order.asc("id"))
                .list();
    }

    // 브랜드 코드 중복 확인
    public IntBrand checkCode(String code, Language language) {
        return (IntBrand) session().createCriteria(IntBrand.class)
                .add(Restrictions.eq("code", code))
                .add(Restrictions.ne("state", State.deleted))
                .add(Restrictions.eq("language", language))
                .setMaxResults(1)
                .uniqueResult();
    }

    // for xml
    @SuppressWarnings("unchecked")
    public List<IntBrand> listForSitemap() {
        Criteria query = session().createCriteria(IntBrand.class)
                .add(Restrictions.eq("language", Language.en))
                .add(Restrictions.eq("state", State.on))
                .addOrder(Order.desc("publishAt"));

        return query.list();
    }
}
