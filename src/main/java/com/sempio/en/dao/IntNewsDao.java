package com.sempio.en.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.entity.IntNews;
import com.sempio.en.util.Pager;

@Repository
public class IntNewsDao extends DaoBase {

    @SuppressWarnings("unchecked")
    public List<IntNews> listForAdmin(Language language, String q, String qType, int limit, int offset) {
        Criteria query = session().createCriteria(IntNews.class);

        if (StringUtils.isNotEmpty(q)) {
            query.add(Restrictions.ilike(qType, q, MatchMode.ANYWHERE));
        }

        query.add(Restrictions.ne("state", State.deleted));
        query.add(Restrictions.eq("language", language));
        query.setMaxResults(limit);
        query.setFirstResult(offset);
        query.addOrder(Order.desc("publishAt"));
        query.addOrder(Order.desc("id"));

        return query.list();
    }

    public int countForAdmin(Language language, String q, String qType) {
        Criteria query = session().createCriteria(IntNews.class);

        if (StringUtils.isNotEmpty(q)) {
            query.add(Restrictions.ilike(qType, q, MatchMode.ANYWHERE));
        }

        query.add(Restrictions.ne("state", State.deleted));
        query.add(Restrictions.eq("language", language));
        query.setProjection(Projections.rowCount());

        Number num = (Number) query.uniqueResult();
        return num.intValue();
    }

    public Integer insert(IntNews news) {
        Integer id = ((Number) session().save(news)).intValue();
        session().flush();
        return id;
    }

    public IntNews selectForAdmin(Integer id) {
        return (IntNews) session().createCriteria(IntNews.class)
                .setFetchMode("intNewsattachments", FetchMode.JOIN)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

    public IntNews getNews(Integer id) {
        return session().get(IntNews.class, id);
    }

    public void update(IntNews news) {
        session().update(news);
    }

    public int update(Integer[] ids, State state) {
        return session().createQuery("update IntNews set state = :state where id in (:ids)")
                .setParameter("state", state)
                .setParameterList("ids", ids)
                .executeUpdate();
    }

    // for front
    @SuppressWarnings("unchecked")
    public List<IntNews> list(Language language, Pager pager) {
        Criteria q = session().createCriteria(IntNews.class);
        q.add(Restrictions.eq("state", State.on));
        q.add(Restrictions.eq("language", language));
        q.add(Restrictions.sqlRestriction("publish_at <= current_date"));
        q.addOrder(Order.desc("publishAt"));
        q.addOrder(Order.desc("id"));

        if (pager != null) {
            q.setFirstResult(pager.getOffset());
            q.setMaxResults(pager.getEntriesPerPage());
        }

        return q.list();
    }

    // for front
    public int count(Language language) {
        Criteria q = session().createCriteria(IntNews.class);
        q.add(Restrictions.eq("state", State.on));
        q.add(Restrictions.eq("language", language));
        q.add(Restrictions.sqlRestriction("publish_at <= current_date"));
        q.setProjection(Projections.rowCount());

        Number num = (Number) q.uniqueResult();
        return num.intValue();
    }

    // for front
    public IntNews view(Language language, Integer id) {
        return (IntNews) session().createCriteria(IntNews.class)
                .setFetchMode("intNewsattachments", FetchMode.JOIN)
                .add(Restrictions.eq("state", State.on))
                .add(Restrictions.eq("language", language))
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

    // for front (main page)
    @SuppressWarnings("unchecked")
    public List<IntNews> selectMainList(Language language) {
        Criteria q = session().createCriteria(IntNews.class);
        q.add(Restrictions.eq("state", State.on));
        q.add(Restrictions.eq("language", language));
        q.add(Restrictions.sqlRestriction("publish_at <= current_date"));
        q.setMaxResults(3);
        q.addOrder(Order.desc("publishAt"));
        q.addOrder(Order.desc("id"));
        return q.list();
    }

    // for front search page
    @SuppressWarnings("unchecked")
    public List<IntNews> searchBy(String q, Language language, Pager pager) {
        Criteria query = session().createCriteria(IntNews.class);
        if (StringUtils.isNotEmpty(q)) {
            query.add(Restrictions.or(
                    (Restrictions.ilike("title", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("contents", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("tags", q, MatchMode.ANYWHERE))
                    ));
        }
        query.add(Restrictions.eq("state", State.on));
        query.add(Restrictions.eq("language", language));
        query.add(Restrictions.sqlRestriction("publish_at <= current_date"));
        query.addOrder(Order.desc("publishAt"));
        query.addOrder(Order.desc("id"));

        if (pager != null) {
            query.setFirstResult(pager.getOffset());
            query.setMaxResults(pager.getEntriesPerPage());
        }

        return query.list();
    }

    // for front search page
    public int countBy(String q, Language language) {
        Criteria query = session().createCriteria(IntNews.class);
        if (StringUtils.isNotEmpty(q)) {
            query.add(Restrictions.or(
                    (Restrictions.ilike("title", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("contents", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("tags", q, MatchMode.ANYWHERE))
                    ));
        }
        query.add(Restrictions.eq("state", State.on));
        query.add(Restrictions.eq("language", language));
        query.add(Restrictions.sqlRestriction("publish_at <= current_date"));
        query.setProjection(Projections.rowCount());

        Number num = (Number) query.uniqueResult();
        return num.intValue();
    }

    public int updateViews(int id) {
        Query q = session().createQuery("update IntNews a set a.views = (a.views+1) where id = :id");
        q.setParameter("id", id);
        return q.executeUpdate();
    }

    // for xml
    @SuppressWarnings("unchecked")
    public List<IntNews> listForSitemap() {
        Criteria query = session().createCriteria(IntNews.class)
                .add(Restrictions.eq("language", Language.en))
                .add(Restrictions.eq("state", State.on))
                .addOrder(Order.desc("publishAt"));

        return query.list();
    }
}
