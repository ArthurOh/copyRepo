package com.sempio.en.dao;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.type.CustomType;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import com.iropke.common.hibernate.ArrayType;
import com.sempio.en.code.Language;
import com.sempio.en.command.IntRecipeSearchForm;
import com.sempio.en.entity.IntCookery;
import com.sempio.en.entity.IntIngredient;
import com.sempio.en.entity.IntRecipe;
import com.sempio.en.entity.IntRecipeTheme;
import com.sempio.en.util.BaseUtil;
import com.sempio.en.util.DateUtils;
import com.sempio.en.util.Pager;

@Repository
public class IntRecipeDao extends DaoBase {
    private final static String[] intRecipeState = { "on", "off" };

    @SuppressWarnings("unchecked")
    public List<IntRecipe> selectIntRecipeList(IntRecipeSearchForm searchForm, Pager pager, String language) {
        Criteria q = session().createCriteria(IntRecipe.class);
        if (StringUtils.isNotBlank(searchForm.getStateSearch())) {
            q.add(Restrictions.eq("state", searchForm.getStateSearch()));
        }

        if (StringUtils.isNotBlank(searchForm.getqType())) {
            if (StringUtils.equals(searchForm.getqType(), "recipe")) {
                q.add(Restrictions.ilike("title", searchForm.getQ(), MatchMode.ANYWHERE));
            }
        }

        q.add(Restrictions.in("state", intRecipeState))
        .add(Restrictions.eq("language", language))
        .setFirstResult(pager.getOffset())
        .setMaxResults(pager.getEntriesPerPage()).addOrder(Order.desc("id"));

        return q.list();
    }

    @SuppressWarnings("unchecked")
    public List<IntRecipeTheme> selectRecipeThemeAll(String language) {
        Criteria q = session().createCriteria(IntRecipeTheme.class);

        q.add(Restrictions.eq("state", "on")).add(Restrictions.eq("language", language)).addOrder(Order.desc("id"));
        return q.list();
    }

    public IntRecipeTheme selectEnRecipeTheme(Integer id) {
        Criteria q = session().createCriteria(IntRecipeTheme.class);
        q.add(Restrictions.eq("id", id));
        return (IntRecipeTheme) q.uniqueResult();
    }

    public int saveEnRecipe(IntRecipe command) {
        int id = (int) session().save(command);
        session().flush();
        return id;

    }

    public IntRecipe selectOne(Language language, int recipeId) {
        return (IntRecipe) session().createCriteria(IntRecipe.class)
                .add(Restrictions.eq("id", recipeId))
                .add(Restrictions.in("state", intRecipeState))
                .add(Restrictions.eq("language", language.name()))
                .uniqueResult();
    }

    public void enCookerySave(IntCookery intCookery) {
        session().save(intCookery);
        session().flush();
    }

    public IntCookery selectEnCookery(IntRecipe enRecipe, Integer cookeryOrdering) {
        return (IntCookery) session().createCriteria(IntCookery.class).add(Restrictions.eq("IntRecipe", enRecipe)).add(Restrictions.eq("ordering", cookeryOrdering)).uniqueResult();
    }

    public void enIngredientSave(IntIngredient intIngredient) {
        session().save(intIngredient);
        session().flush();
    }

    public int recipeCount(IntRecipeSearchForm searchForm, String language) {
        Criteria q = session().createCriteria(IntRecipe.class);

        if (StringUtils.isNotBlank(searchForm.getStateSearch())) {
            q.add(Restrictions.eq("state", searchForm.getStateSearch()));
        }

        if (StringUtils.isNotBlank(searchForm.getqType())) {
            if (StringUtils.equals(searchForm.getqType(), "recipe")) {
                q.add(Restrictions.like("title", searchForm.getQ(), MatchMode.ANYWHERE));
            }
        }

        q.add(Restrictions.eq("language", language));
        q.add(Restrictions.in("state", intRecipeState)).setProjection(Projections.rowCount());
        Number count = (Number) q.uniqueResult();
        return count.intValue();
    }

    public void IntRecipeUpdate(IntRecipe enRecipe) {
        session().update(enRecipe);
        session().flush();
    }

    public void cookeryDelete(IntRecipe enRecipe) {
        session().createQuery("delete from IntCookery a where a.intRecipe = :intRecipe ").setParameter("intRecipe", enRecipe).executeUpdate();
    }

    public void ingredientDelete(IntRecipe enRecipe) {
        session().createQuery("delete from IntIngredient a where a.intRecipe = :intRecipe ").setParameter("intRecipe", enRecipe).executeUpdate();
    }

    public Integer update(Integer[] ids, String state) {
        return session().createQuery("update IntRecipe set state = :state where id in (:ids)").setParameter("state", state).setParameterList("ids", ids).executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public List<IntRecipe> selectIntRecipeListWithItem(IntRecipeSearchForm searchForm, Pager pager, String language) {
        StringBuilder sql = new StringBuilder();
        String str = "SELECT DISTINCT ON (r.id) r.id, r.title, r.title_sub, r.views"
                + ", r.state, r.publish_at, r.language"
                + " from int_recipe as r inner join int_ingredient as i "
                + " on r.id = i.int_recipe_id"
                + " where r.state in (:state)"
                + " and i.title like '%' ||" + ":q" + "|| '%'"
                + " order by r.id"
                ;

        sql.append(str);

        String query = sql.toString();

        return  session().createSQLQuery(query)
                .addScalar("id", IntegerType.INSTANCE)
                .addScalar("title", StringType.INSTANCE)
                .addScalar("title_sub", StringType.INSTANCE)
                .addScalar("views", IntegerType.INSTANCE)
                .addScalar("state", StringType.INSTANCE)
                .addScalar("publish_at", DateType.INSTANCE)
                .addScalar("language", StringType.INSTANCE)
                .setString("state", searchForm.getStateSearch())
                .setString("q", searchForm.getQ())
                .setResultTransformer(new AliasToBeanResultTransformer(IntRecipe.class)).list();

        /*Criteria q = session().createCriteria(IntIngredient.class);

        q.add(Restrictions.like("title", searchForm.getQ(), MatchMode.END));
        q.setFirstResult(pager.getOffset()).setMaxResults(pager.getEntriesPerPage()).addOrder(Order.desc("intRecipe"));

        List<IntIngredient> ingredientList = q.list();
        List<IntRecipe> recipeList = new ArrayList<IntRecipe>();
        for (IntIngredient item : ingredientList) {
            if(StringUtils.isNotBlank(searchForm.getStateSearch())){
                if(StringUtils.equals(searchForm.getStateSearch(), item.getIntRecipe().getState()) && StringUtils.equals(language, item.getIntRecipe().getLanguage())){
                    recipeList.add(item.getIntRecipe());
                }
            } else if (StringUtils.equals(language, item.getIntRecipe().getLanguage())){
                recipeList.add(item.getIntRecipe());
            }
        }

        recipeList = new ArrayList<IntRecipe>(new HashSet<IntRecipe>(recipeList));
        return recipeList;*/
    }

    @SuppressWarnings("unchecked")
    public List<Integer> selectRecipeIdsWithItem(IntRecipeSearchForm searchForm, List<Integer> itemIds, String language) {
       /* StringBuilder sql = new StringBuilder();
        String str = "SELECT DISTINCT ON (r.id) r.id"
                + " from int_recipe as r inner join int_ingredient as i "
                + " on r.id = i.int_recipe_id"
                + " where r.state in (:state)"
                + " and i.title like '%' ||" + ":q" + "|| '%'"
                + " order by r.id"
                ;

        sql.append(str);*/

        String query = makeSearchRecipeQuery(searchForm, itemIds);

        SQLQuery q = session().createSQLQuery(query);

        q.addScalar("id", IntegerType.INSTANCE);
        if(StringUtils.isNotBlank(searchForm.getStateSearch())){
            q.setString("state", searchForm.getStateSearch());
        }
        if(itemIds.size() > 0 && !itemIds.isEmpty()){
            q.setParameterList("itemIds", itemIds);
        }

        return q.list();
    }

    private String makeSearchRecipeQuery(IntRecipeSearchForm searchForm, List<Integer> itemIds){
        StringBuilder sql = new StringBuilder();
        String str = "SELECT DISTINCT ON (r.id) r.id"
                + " from int_recipe as r inner join int_ingredient as i "
                + " on r.id = i.int_recipe_id"
                ;

        sql.append(str);

        if(StringUtils.isNotBlank(searchForm.getStateSearch())){
            sql.append(" where r.state in (:state)");
        }
        if(itemIds.size() > 0 && !itemIds.isEmpty()){
            sql.append(" and i.int_item_id in (:itemIds)");
        }

        String last = " order by r.id";
        sql.append(last);
        return sql.toString();
    }

    public int recipeCountWithItem(List<Integer> recipeIds, IntRecipeSearchForm searchForm, String language) {
        /*Criteria q = session().createCriteria(IntIngredient.class);

        q.add(Restrictions.like("title", searchForm.getQ(), MatchMode.ANYWHERE));
        List<IntIngredient> ingredientList = q.list();
        List<IntRecipe> recipeList = new ArrayList<IntRecipe>();
        for (IntIngredient item : ingredientList) {
            if(StringUtils.isNotBlank(searchForm.getStateSearch())){
                if(StringUtils.equals(searchForm.getStateSearch(), item.getIntRecipe().getState()) && StringUtils.equals(language, item.getIntRecipe().getLanguage())){
                    recipeList.add(item.getIntRecipe());
                }
            } else if (StringUtils.equals(language, item.getIntRecipe().getLanguage())){
                recipeList.add(item.getIntRecipe());
            }
        }
        recipeList = new ArrayList<IntRecipe>(new HashSet<IntRecipe>(recipeList));

        Number count = recipeList.size();
        return count.intValue();*/

        Criteria query = session().createCriteria(IntRecipe.class);

        query.add(Restrictions.in("state", intRecipeState))
            .add(Restrictions.eq("language", language));
        if(recipeIds != null && !recipeIds.isEmpty()){
            query.add(Restrictions.in("id", recipeIds));
        } else {
            return 0;
        }

        Number num = (Number) query.setProjection(Projections.rowCount()).uniqueResult();

        return num.intValue();
    }

    @SuppressWarnings("unchecked")
    public List<IntRecipe> selectRecommendList(String[] recipeIds) {
        Criteria q = session().createCriteria(IntRecipe.class);

        Integer[] ids = BaseUtil.StringArrToIntArr(recipeIds);

        q.add(Restrictions.in("id", ids));
        return q.list();
    }

    @SuppressWarnings("unchecked")
    public List<IntRecipe> recipeList(Integer id, Pager pager) {
        Integer[] intArray = new Integer[]{id};
        Properties p = new Properties();
        p.setProperty("baseType", "int");
        ArrayType t = new ArrayType();
        t.setParameterValues(p);

        Criteria q = session().createCriteria(IntRecipe.class);
        q.add(Restrictions.sqlRestriction("int_recipe_theme_id @> ?", intArray,  new CustomType(t)));

        Date now = new Date();
        Date Tommorrow = DateUtils.afterDay(now, 0);

        q.add(Restrictions.eq("state", "on"));
        q.add(Restrictions.le("publishAt", Tommorrow));
        q.addOrder(Order.desc("id"));
        q.setFirstResult(pager.getOffset())
        .setMaxResults(pager.getEntriesPerPage());
        return q.list();
    }

    @SuppressWarnings("unchecked")
    public List<IntRecipe> recipeList(Integer id) {
        Integer[] intArray = new Integer[]{id};
        Properties p = new Properties();
        p.setProperty("baseType", "int");
        ArrayType t = new ArrayType();
        t.setParameterValues(p);

        Criteria q = session().createCriteria(IntRecipe.class);
        q.add(Restrictions.sqlRestriction("int_recipe_theme_id @> ?", intArray,  new CustomType(t)));

        Date now = new Date();
        Date Tommorrow = DateUtils.afterDay(now, 0);

        q.add(Restrictions.eq("state", "on"));
        q.add(Restrictions.le("publishAt", Tommorrow));
        q.addOrder(Order.desc("id"));
        return q.list();
    }

    public int recipeCount(Integer id) {
        Integer[] intArray = new Integer[]{id};
        Properties p = new Properties();
        p.setProperty("baseType", "int");
        ArrayType t = new ArrayType();
        t.setParameterValues(p);

        Criteria q = session().createCriteria(IntRecipe.class);
        q.add(Restrictions.sqlRestriction("int_recipe_theme_id @> ?", intArray, new CustomType(t)));

        Date now = new Date();
        Date Tommorrow = DateUtils.afterDay(now, 0);

        q.add(Restrictions.le("publishAt", Tommorrow));
        q.add(Restrictions.eq("state", "on"));
        Number num = (Number) q.setProjection(Projections.rowCount()).uniqueResult();
        return num.intValue();
    }

    @SuppressWarnings("unchecked")
    public List<IntRecipe> recipeList(Integer[] recipeIds ,Language language) {
        Criteria q = session().createCriteria(IntRecipe.class);
        q.add(Restrictions.in("id", recipeIds));
        q.addOrder(Order.desc("id"));
        q.add(Restrictions.eq("language", language.name()));

        return q.list();
    }

    // for front item detail page
    @SuppressWarnings("unchecked")
    public List<IntRecipe> listForItem(Integer[] ids) {
        Criteria q = session().createCriteria(IntRecipe.class);
        q.add(Restrictions.in("id", ids));
        q.setMaxResults(6);
        return q.list();
    }

    @SuppressWarnings("unchecked")
    public List<Object> listWithItem(List<Integer> itemIds) {
        return session().createQuery("select a.intRecipe.id from IntIngredient a where int_item_id in (:ids)")
                .setParameterList("ids", itemIds.toArray())
                .list();
    }

    @SuppressWarnings("unchecked")
    public List<IntRecipe> listWithId(List<Integer> recipeList, Pager pager) {
        Date now = new Date();
        Date Tommorrow = DateUtils.afterDay(now, 0);

        return session().createCriteria(IntRecipe.class)
                .add(Restrictions.in("id", recipeList.toArray()))
                .add(Restrictions.eq("state", "on"))
                .add(Restrictions.le("publishAt", Tommorrow))
                .addOrder(Order.desc("id"))
                .setFirstResult(pager.getOffset())
                .setMaxResults(pager.getEntriesPerPage())
                .list();
    }

    @SuppressWarnings("unchecked")
    public List<IntRecipe> listWithId(List<Integer> recipeList) {
        Date now = new Date();
        Date Tommorrow = DateUtils.afterDay(now, 0);

        return session().createCriteria(IntRecipe.class)
                .add(Restrictions.in("id", recipeList.toArray()))
                .add(Restrictions.eq("state", "on"))
                .add(Restrictions.le("publishAt", Tommorrow))
                .addOrder(Order.desc("id"))
                .list();
    }

    public Integer countWithId(List<Integer> recipeList) {
        Object resultSet = session().createQuery("select count(*) from IntRecipe where id in (:ids) and state = :state and publishAt < now()")
                        .setParameterList("ids", recipeList.toArray())
                        .setParameter("state", "on")
                        .uniqueResult();
        if(resultSet == null){
            return 0;
        }
        Long result = (Long)resultSet;

        return result.intValue();
    }

    // for front search page
    @SuppressWarnings("unchecked")
    public List<IntRecipe> searchBy(String q, String language, Pager pager) throws ParseException {
        Criteria query = session().createCriteria(IntRecipe.class);
        if (StringUtils.isNotEmpty(q)) {
            query.add(Restrictions.or(
                    (Restrictions.ilike("title", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("titleSub", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("description", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("tags", q, MatchMode.ANYWHERE))
                    ));
        }
        query.add(Restrictions.eq("state", "on"));
        query.add(Restrictions.eq("language", language));
        query.add(Restrictions.le("publishAt", BaseUtil.getToday()));
        query.addOrder(Order.desc("publishAt"));
        query.addOrder(Order.desc("id"));

        if (pager != null) {
            query.setFirstResult(pager.getOffset());
            query.setMaxResults(pager.getEntriesPerPage());
        }

        return query.list();
    }

    // for front search page
    public int countBy(String q, String language) throws ParseException {
        Criteria query = session().createCriteria(IntRecipe.class);
        if (StringUtils.isNotEmpty(q)) {
            query.add(Restrictions.or(
                    (Restrictions.ilike("title", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("titleSub", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("description", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("tags", q, MatchMode.ANYWHERE))
                    ));
        }
        query.add(Restrictions.eq("state", "on"));
        query.add(Restrictions.eq("language", language));
        query.add(Restrictions.le("publishAt", BaseUtil.getToday()));
        query.setProjection(Projections.rowCount());

        Number num = (Number) query.uniqueResult();
        return num.intValue();
    }

    public int updateViews(Integer id) {
        Query q = session().createSQLQuery("update int_recipe set views = (views+1) where id = "+id.intValue());
        return q.executeUpdate();
    }

    public int recipeCountForTheme(Integer id) {
        Integer[] intArray = new Integer[]{id};
        Properties p = new Properties();
        p.setProperty("baseType", "int");
        ArrayType t = new ArrayType();
        t.setParameterValues(p);

        Criteria q = session().createCriteria(IntRecipe.class);
        q.add(Restrictions.sqlRestriction("int_recipe_theme_id @> ?", intArray, new CustomType(t)));
        q.add(Restrictions.in("state", intRecipeState));

        Number num = (Number) q.setProjection(Projections.rowCount()).uniqueResult();
        return num.intValue();
    }

    @SuppressWarnings("unchecked")
    public List<IntCookery> searchCookery(String q, String language) {
        Criteria c = session().createCriteria(IntCookery.class);
        String[] qArray = {q};
        if (StringUtils.isNotEmpty(q)) {
            c.add(Restrictions.or(
                    (Restrictions.ilike("title", q, MatchMode.ANYWHERE)),
                    (Restrictions.sqlRestriction("directions @> ?", qArray, new CustomType(new ArrayType())))
                    ));
        }

        return c.list();
    }

    @SuppressWarnings("unchecked")
    public List<IntIngredient> searchIngredient(String q, String language) {
        Criteria c = session().createCriteria(IntIngredient.class);
        if (StringUtils.isNotEmpty(q)) {
            c.add(Restrictions.ilike("title", q,  MatchMode.ANYWHERE));
        }

        return c.list();
    }

    @SuppressWarnings("unchecked")
    public List<IntRecipe> searchBy(String q, String language, Pager pager, List<Integer> ids) throws ParseException {
        Criteria query = session().createCriteria(IntRecipe.class);
        if (StringUtils.isNotEmpty(q) && ids != null && !ids.isEmpty()) {
            query.add(Restrictions.or(
                    (Restrictions.ilike("title", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("titleSub", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("description", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("tags", q, MatchMode.ANYWHERE)),
                    (Restrictions.in("id", ids.toArray()))
                    ));
        } else if (StringUtils.isNotEmpty(q)) {
            query.add(Restrictions.or(
                    (Restrictions.ilike("title", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("titleSub", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("description", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("tags", q, MatchMode.ANYWHERE))
                    ));
        }

        query.add(Restrictions.eq("state", "on"));
        query.add(Restrictions.eq("language", language));
        query.add(Restrictions.le("publishAt", BaseUtil.getToday()));
        query.addOrder(Order.desc("publishAt"));
        query.addOrder(Order.desc("id"));

        if (pager != null) {
            query.setFirstResult(pager.getOffset());
            query.setMaxResults(pager.getEntriesPerPage());
        }

        return query.list();
    }

    public int countBy(String q, String language, List<Integer> ids) throws ParseException {
        Criteria query = session().createCriteria(IntRecipe.class);
        if (StringUtils.isNotEmpty(q) && ids != null && !ids.isEmpty()) {
            query.add(Restrictions.or(
                    (Restrictions.ilike("title", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("titleSub", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("description", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("tags", q, MatchMode.ANYWHERE)),
                    (Restrictions.in("id", ids.toArray()))
                    ));
        } else if (StringUtils.isNotEmpty(q)) {
            query.add(Restrictions.or(
                    (Restrictions.ilike("title", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("titleSub", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("description", q, MatchMode.ANYWHERE)),
                    (Restrictions.ilike("tags", q, MatchMode.ANYWHERE))
                    ));
        }
        query.add(Restrictions.eq("state", "on"));
        query.add(Restrictions.eq("language", language));
        query.add(Restrictions.le("publishAt", BaseUtil.getToday()));
        query.setProjection(Projections.rowCount());

        Number num = (Number) query.uniqueResult();
        return num.intValue();
    }

    @SuppressWarnings("unchecked")
    public List<IntRecipe> selectRecipeListWithIds(List<Integer> recipeIds, String language, Pager pager) {
        Criteria query = session().createCriteria(IntRecipe.class);

        query.add(Restrictions.in("state", intRecipeState))
            .add(Restrictions.eq("language", language));
        if(recipeIds != null && !recipeIds.isEmpty()){
            query.add(Restrictions.in("id", recipeIds));
        } else {
            return null;
        }

        if (pager != null) {
            query.setFirstResult(pager.getOffset());
            query.setMaxResults(pager.getEntriesPerPage());
        }
        return query.list();
    }


}
