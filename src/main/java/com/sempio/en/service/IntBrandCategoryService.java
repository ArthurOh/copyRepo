package com.sempio.en.service;

import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.dao.IntBrandCategoryDao;
import com.sempio.en.entity.IntBrandCategory;

@Service
public class IntBrandCategoryService {

    @Autowired
    private IntBrandCategoryDao intBrandCategoryDao;

    @Transactional
    public List<IntBrandCategory> listForAdmin(Integer brandId) {
        List<IntBrandCategory> brandCategory = intBrandCategoryDao.selectListForAdmin(brandId);
        if (brandCategory != null) {
            for (IntBrandCategory bc : brandCategory) {
                Hibernate.initialize(bc.getIntItemList());
            }
        }
        return brandCategory;
    }

    @Transactional
    public IntBrandCategory load(int id) {
        IntBrandCategory brandCategory = intBrandCategoryDao.selectForAdmin(id);
        if (brandCategory != null) {
            Hibernate.initialize(brandCategory.getIntItemList());
        }
        return brandCategory;
    }

    @Transactional
    public void create(IntBrandCategory brandCategory) {
        intBrandCategoryDao.insert(brandCategory);
    }

    @Transactional
    public IntBrandCategory getBrandCode() {
        return intBrandCategoryDao.getBrandCode();
    }

    @Transactional
    public void updatePost(IntBrandCategory brandCategory) {
        IntBrandCategory loaded = intBrandCategoryDao.getbc(brandCategory.getId());
        loaded.setTitle(brandCategory.getTitle());
        loaded.setDescription(brandCategory.getDescription());
        loaded.setState(brandCategory.getState());
        loaded.setUpdateAt(brandCategory.getUpdateAt());
        loaded.setRewriter(brandCategory.getRewriter());

        intBrandCategoryDao.update(loaded);
    }

    @Transactional
    public void updateLocation(List<Map<String, Object>> list, Integer brandId) {
        for (Map<String, Object> item : list) {
            Integer id = Integer.parseInt(item.get("id").toString());
            Integer orderNum = Integer.parseInt(item.get("orderNum").toString());

            intBrandCategoryDao.updateOrdering(id, orderNum, State.on, brandId);
        }
    }

    @Transactional
    public List<IntBrandCategory> bcList(Integer brandId, String state, Language language) {
        return intBrandCategoryDao.bcList(brandId, state, language);
    }

    // list for front
    @Transactional
    public List<IntBrandCategory> listForItem(Integer brandId) {
        return intBrandCategoryDao.listForItem(brandId);
    }

    // for front
    @Transactional
    public IntBrandCategory getLastId(Integer brandId) {
        return intBrandCategoryDao.getLastId(brandId);
    }
}
