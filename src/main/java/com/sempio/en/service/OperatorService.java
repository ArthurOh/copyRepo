package com.sempio.en.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sempio.en.code.State;
import com.sempio.en.dao.IntAdminUserDao;
import com.sempio.en.dao.IntBrandCategoryDao;
import com.sempio.en.dao.IntBrandDao;
import com.sempio.en.dao.IntItemDao;
import com.sempio.en.dao.IntNewsDao;
import com.sempio.en.dao.IntRecipeDao;
import com.sempio.en.dao.IntRecipeThemeDao;
import com.sempio.en.entity.IntAdminUser;

@Service
public class OperatorService {

    @Autowired
    private IntNewsDao intNewsDao;

    @Autowired
    private IntBrandDao intBrandDao;

    @Autowired
    private IntBrandCategoryDao intBrandCategoryDao;

    @Autowired
    private IntItemDao intItemDao;

    @Autowired
    private IntRecipeDao intRecipeDao;

    @Autowired
    private IntAdminUserDao adminDao;

    @Autowired
    private IntNewsletterService newsletterService;

    @Autowired
    private IntRecipeThemeDao intRecipeThemeDao;

    @Autowired
    private IntVisualContentsService VisualContentsService;

    @Transactional
    public int updateState(Integer[] ids, String state, String type, IntAdminUser rewriter) {
        if (null == ids || 0 == ids.length) {
            return 0;
        }
        Integer result;
        if (StringUtils.equals("news", type)) {
            result = intNewsDao.update(ids, State.valueOf(state));
        } else if (StringUtils.equals("brand", type)) {
            result = intBrandDao.update(ids, State.valueOf(state));
        } else if (StringUtils.equals("brandCategory", type)) {
            result = intBrandCategoryDao.update(ids, State.valueOf(state));
        } else if (StringUtils.equals("item", type)) {
            result = intItemDao.update(ids, State.valueOf(state));
        } else if (StringUtils.equals("recipe", type)) {
            result = intRecipeDao.update(ids, state);
        } else if (StringUtils.equals("adminUser", type)) {
            result = adminDao.update(ids, State.valueOf(state), rewriter);
        } else if (StringUtils.equals("newsletter", type)) {
            result = newsletterService.update(ids, State.valueOf(state), rewriter);
        } else if (StringUtils.equals("newsletter-apply", type)) {
            result = newsletterService.updateApply(ids, State.valueOf(state));
        } else if (StringUtils.equals("recipe-theme", type)) {
            result = intRecipeThemeDao.update(ids, State.valueOf(state), rewriter.getUserId());
        } else if (StringUtils.equals("visual-contents", type)) {
            result = VisualContentsService.update(ids, State.valueOf(state), rewriter);
        } else {
            result = intNewsDao.update(ids, State.valueOf(state));
        }
        return result;
    }
}
