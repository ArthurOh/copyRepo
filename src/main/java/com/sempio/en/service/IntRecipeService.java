package com.sempio.en.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.sempio.en.code.Language;
import com.sempio.en.code.Result;
import com.sempio.en.command.IntCookeryForm;
import com.sempio.en.command.IntIngredientForm;
import com.sempio.en.command.IntRecipeForm;
import com.sempio.en.command.IntRecipeSearchForm;
import com.sempio.en.dao.IntRecipeDao;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntBrand;
import com.sempio.en.entity.IntBrandCategory;
import com.sempio.en.entity.IntCookery;
import com.sempio.en.entity.IntIngredient;
import com.sempio.en.entity.IntItem;
import com.sempio.en.entity.IntRecipe;
import com.sempio.en.entity.IntRecipeTheme;
import com.sempio.en.util.Pager;

@Service
public class IntRecipeService {

    private static Logger log = LoggerFactory.getLogger(IntRecipeService.class);

    @Autowired
    private IntRecipeDao intRecipeDao;

    @Autowired
    private IntRecipeThemeService intRecipeThemeService;

    @Autowired
    private IntBrandService intBrandService;

    @Autowired
    private IntBrandCategoryService intBrandCategoryService;

    @Autowired
    private IntItemService intItemService;

    @Transactional
    public List<IntRecipe> getIntRecipeList(IntRecipeSearchForm searchForm, Pager pager, String language) {
        List<IntRecipe> list = new ArrayList<IntRecipe>();
        if (StringUtils.equals("item", searchForm.getqType()) && StringUtils.isNotBlank(searchForm.getQ())) {
            // 제품 이름으로 제품 ID List를 가져온다.
            List<IntItem> itemList = intItemService.searchByTitle(searchForm.getQ(), language);
            List<Integer> itemIds = new ArrayList<Integer>();
            if(itemList.size() > 0 && !itemList.isEmpty()){
                for(IntItem i : itemList){
                    itemIds.add(i.getId());
                }
            } else {
                return list;
            }
            List<Integer> recipeIds = intRecipeDao.selectRecipeIdsWithItem(searchForm, itemIds, language);
            list = intRecipeDao.selectRecipeListWithIds(recipeIds, language, pager);
        } else {
            list = intRecipeDao.selectIntRecipeList(searchForm, pager, language);
        }

        if (list != null && list.size() > 0) {
            for (IntRecipe intRecipe : list) {
                if (intRecipe.getIntCookeryList() != null && intRecipe.getIntCookeryList().size() > 0) {
                    Hibernate.initialize(intRecipe.getIntCookeryList());
                }
                if (intRecipe.getIntIngredientList() != null && intRecipe.getIntIngredientList().size() > 0) {
                    Hibernate.initialize(intRecipe.getIntIngredientList());
                }
                if (intRecipe.getIntCookeryList() != null && intRecipe.getIntCookeryList().size() > 0) {
                    List<IntCookery> cookeryList = intRecipe.getIntCookeryList();
                    for (IntCookery intCookery : cookeryList) {
                        Hibernate.initialize(intCookery.getIntIngredientList());
                    }
                }
            }
        }

        return list;
    }

    @Transactional
    public int searchCount(IntRecipeSearchForm searchForm, String language) {
        int result = 0;
        if (StringUtils.equals("item", searchForm.getqType()) && StringUtils.isNotBlank(searchForm.getQ())) {
            // 제품 이름으로 제품 ID List를 가져온다.
            List<IntItem> itemList = intItemService.searchByTitle(searchForm.getQ(), language);
            List<Integer> itemIds = new ArrayList<Integer>();
            if(itemList.size() > 0 && !itemList.isEmpty()){
                for(IntItem i : itemList){
                    itemIds.add(i.getId());
                }
            } else {
                return result;
            }
            List<Integer> recipeIds = intRecipeDao.selectRecipeIdsWithItem(searchForm, itemIds, language);
            result = intRecipeDao.recipeCountWithItem(recipeIds, searchForm, language);
        } else {
            result = intRecipeDao.recipeCount(searchForm, language);
        }
        return result;
    }

    @Transactional
    public List<IntRecipeTheme> getRecipeThemeAll(String language) {
        return intRecipeDao.selectRecipeThemeAll(language);
    }

    @Transactional
    public IntRecipeTheme getEnRecipeTheme(Integer id) {
        return intRecipeDao.selectEnRecipeTheme(id);
    }

    @Transactional
    public Result<String> enRecipeRegist(IntRecipeForm form, IntAdminUser loginUser, String language) {

        // 1 레시피 등록
        IntRecipe intRecipe = form.toIntRecipe();
        intRecipe.setWriter(loginUser.getUserId());
        intRecipe.setLanguage(language);
        int recipeId = intRecipeDao.saveEnRecipe(intRecipe);

        if (recipeId == 0) {
            return Result.error();
        }

        // 2 조리법 등록
        List<IntCookeryForm> intCookeryFormList = form.getCookeryList();
        if (CollectionUtils.isEmpty(intCookeryFormList)) {
            return Result.success();
        }

        for (int i = 0; i < intCookeryFormList.size(); i++) {
            IntCookeryForm cookeryForm = intCookeryFormList.get(i);
            IntCookery intCookery = cookeryForm.toIntCookery();
            if (intCookery != null) {
                intCookery.setIntRecipe(intRecipe);
                intCookery.setOrdering(i);
                intCookery.setLanguage(Language.valueOf(language));
                enCookeryRegist(intCookery);

                // 3 재료 등록
                List<IntIngredientForm> ingredientFormList = cookeryForm.getIngredientList();
                if (CollectionUtils.isEmpty(ingredientFormList)) {
                    continue;
                }
                for (IntIngredientForm ingredientForm : ingredientFormList) {
                    IntIngredient intIngredient = ingredientForm.toIntIngredient();
                    if (intIngredient == null) {
                        continue;
                    }
                    intIngredient.setIntRecipe(intRecipe);
                    intIngredient.setIntCookery(intCookery);
                    intIngredient.setLanguage(Language.valueOf(language));
                    if (StringUtils.equals("sempio", intIngredient.getType())) {
                        if (ingredientForm.getItemId() == null || ingredientForm.getItemId().intValue() == 0) {
                            continue;
                        }
                        IntItem intItem = intItemService.getOne(Language.valueOf(language), ingredientForm.getItemId());
                        if (intItem == null) {
                            continue;
                        }
                        intIngredient.setIntItem(intItem);
                    }
                    enIngredientRegist(intIngredient);
                }
            }
        }
        return Result.success();
    }

    //front
    @Transactional
    public IntRecipe selectOne(Language language, int recipeId, boolean b) {
        IntRecipe intRecipe = intRecipeDao.selectOne(language, recipeId);
        if (b) {
            if (intRecipe.getIntIngredientList() != null) {
                Hibernate.initialize(intRecipe.getIntIngredientList());
            }

            if (intRecipe.getIntCookeryList() != null) {
                List<IntCookery> cookeryList = intRecipe.getIntCookeryList();
                for (IntCookery intCookery : cookeryList) {
                    Hibernate.initialize(intCookery.getIntIngredientList());
                }
            }
        }
        return intRecipe;
    }

    @Transactional
    public void enCookeryRegist(IntCookery intCookery) {
        intRecipeDao.enCookerySave(intCookery);
    }

    @Transactional
    public IntCookery getEnCookery(IntRecipe enRecipe, Integer cookeryOrdering) {
        return intRecipeDao.selectEnCookery(enRecipe, cookeryOrdering);
    }

    @Transactional
    public void enIngredientRegist(IntIngredient intIngredient) {
        intRecipeDao.enIngredientSave(intIngredient);
    }

    @Transactional
    public void enRecipeUpdate(IntRecipeForm form, IntAdminUser loginUser, String language) {

        // 1. 레시피 업데이트
        IntRecipe intRecipe = selectOne(Language.valueOf(language), form.getId(), false);
        intRecipe.setIntRecipeTheme(form.getIntRecipeThemeId());
        intRecipe.setTitle(form.getRecipeTitle());
        intRecipe.setTitleSub(form.getTitleSub());
        intRecipe.setChef(form.getChef());
        intRecipe.setMainIngredient(form.getMainIngredient());
        intRecipe.setServing(form.getServing());
        intRecipe.setViewImgPath(form.getFilepath_defaultImage());
        intRecipe.setDescription(form.getDescription());
        intRecipe.setTip(form.getTip());
        intRecipe.setRecommend(form.getRecommend());
        intRecipe.setTags(form.getTags());
        intRecipe.setUpdateAt(new Date());
        intRecipe.setPublishAt(form.getPublishAt());
        intRecipe.setRewriter(loginUser.getUserId());
        intRecipe.setState(form.getState());
        intRecipeDao.IntRecipeUpdate(intRecipe);

        // 2. 레시피의 모든 조리법과 재료리스트를 삭제한다.
        intRecipeDao.ingredientDelete(intRecipe);
        intRecipeDao.cookeryDelete(intRecipe);

        // 3. 조리법 / 재료 등록
        List<IntCookeryForm> intCookeryFormList = form.getCookeryList();
        if (CollectionUtils.isEmpty(intCookeryFormList)) {
            return;
        }

        for (int i = 0; i < intCookeryFormList.size(); i++) {
            IntCookeryForm cookeryForm = intCookeryFormList.get(i);
            IntCookery intCookery = cookeryForm.toIntCookery();
            if (intCookery != null) {
                intCookery.setIntRecipe(intRecipe);
                intCookery.setOrdering(i);
                intCookery.setLanguage(Language.valueOf(language));
                enCookeryRegist(intCookery);

                // 3 재료 등록
                if (!CollectionUtils.isEmpty(cookeryForm.getIngredientList())) {
                    for (IntIngredientForm ingredientForm : cookeryForm.getIngredientList()) {
                        IntIngredient intIngredient = ingredientForm.toIntIngredient();
                        if (intIngredient == null) {
                            continue;
                        }
                        intIngredient.setIntRecipe(intRecipe);
                        intIngredient.setIntCookery(intCookery);
                        intIngredient.setLanguage(Language.valueOf(language));
                        if (StringUtils.equals("sempio", intIngredient.getType())) {
                            if (ingredientForm.getItemId() == null || ingredientForm.getItemId().intValue() == 0) {
                                continue;
                            }
                            IntItem intItem = intItemService.getOne(Language.valueOf(language), ingredientForm.getItemId());
                            if (intItem == null) {
                                continue;
                            }
                            intIngredient.setIntItem(intItem);
                        }
                        enIngredientRegist(intIngredient);
                    }
                }
            }
        }
    }

    @Transactional
    public List<IntRecipe> recipeListWithTheme(Language language, String recipeTheme, Pager pager) {

        IntRecipeTheme item = intRecipeThemeService.selectWithCode(language, recipeTheme);
        if (item == null) {
            return null;
        }

        List<IntRecipe> result;
        result = intRecipeDao.recipeList(item.getId(), pager);
        if (result != null && result.size() > 0) {
            for (IntRecipe intRecipe : result) {
                if (intRecipe.getIntCookeryList() != null && intRecipe.getIntCookeryList().size() > 0) {
                    Hibernate.initialize(intRecipe.getIntCookeryList());
                }
                if (intRecipe.getIntIngredientList() != null && intRecipe.getIntIngredientList().size() > 0) {
                    Hibernate.initialize(intRecipe.getIntIngredientList());
                }
                if (intRecipe.getIntCookeryList() != null && intRecipe.getIntCookeryList().size() > 0) {
                    List<IntCookery> cookeryList = intRecipe.getIntCookeryList();
                    for (IntCookery intCookery : cookeryList) {
                        Hibernate.initialize(intCookery.getIntIngredientList());
                    }
                }
            }
        }

        return result;
    }

    @Transactional
    public int recipeCountWithTheme(Language language, String recipeTheme) {
        IntRecipeTheme item = intRecipeThemeService.selectWithCode(language, recipeTheme);
        if (item == null) {
            return 0;
        }

        return intRecipeDao.recipeCount(item.getId());
    }

    @Transactional
    public List<IntRecipe> selectList(Language language, Integer[] themeIds, boolean b) {
        List<IntRecipe> result = null;

        result = intRecipeDao.recipeList(themeIds, language);
        if (b) {
            for(IntRecipe intRecipe : result){
                if (intRecipe.getIntIngredientList() != null) {
                    Hibernate.initialize(intRecipe.getIntIngredientList());
                }

                if (intRecipe.getIntCookeryList() != null) {
                    List<IntCookery> cookeryList = intRecipe.getIntCookeryList();
                    for (IntCookery intCookery : cookeryList) {
                        Hibernate.initialize(intCookery.getIntIngredientList());
                    }
                }
            }
        }
        return result;
    }

    // for front item detail page
    @Transactional
    public List<IntRecipe> listForItem(Integer[] ids) {
        List<IntRecipe> recipe = intRecipeDao.listForItem(ids);
        if (recipe != null) {
            for (IntRecipe rList : recipe) {
                Hibernate.initialize(rList.getIntIngredientList());
            }
        }
        return recipe;
    }

    @Transactional
    public Integer recipeCountWithBrand(Language language, String code) {
        // 1. Brand Code를 가지고 Brand를 가져온다.
        IntBrand brand = intBrandService.getOne(language, code);
        if (brand == null) {
            return 0;
        }

        // 2. 가져온 Brand ID로 Brand Category List를 가져온다.
        List<IntBrandCategory> brandCategotyList = intBrandCategoryService.bcList(brand.getId(), "on", language);
        if (brandCategotyList.isEmpty()) {
            return 0;
        }
        // 3. Brand Category List를 In 조건으로 가지고 있는 IntItem List를 가져온다.
        List<Integer> ids = new ArrayList<Integer>();
        for (IntBrandCategory item : brandCategotyList) {
            ids.add(item.getId());
        }
        List<Integer> itemList = intItemService.listWithBrandCategory(ids);
        if (itemList.isEmpty()) {
            return 0;
        }

        // 4. IntItem List와 In 조건으로 겹치는 Ingredient List를 가져온다.
        List<Object> recipeList = listWithItem(itemList);

        List<Integer> recipeIntList = new ArrayList<Integer>();
        if (recipeList.isEmpty()) {
            return 0;
        } else {
            for (Object item : recipeList) {
                recipeIntList.add((Integer) item);
            }
        }

        Integer result = 0;
        result = intRecipeDao.countWithId(recipeIntList);

        return result;
    }

    @Transactional
    private List<Object> listWithItem(List<Integer> itemIds) {
        return intRecipeDao.listWithItem(itemIds);
    }

    @Transactional
    public List<IntRecipe> recipeListWithBrand(Language language, String code, Pager pager) {
        // 1. Brand Code를 가지고 Brand를 가져온다.
        IntBrand brand = intBrandService.getOne(language, code);
        if (brand == null) {
            return null;
        }

        // 2. 가져온 Brand ID로 Brand Category List를 가져온다.
        List<IntBrandCategory> brandCategotyList = intBrandCategoryService.bcList(brand.getId(), "on", language);
        if (brandCategotyList.isEmpty()) {
            return null;
        }

        // 3. Brand Category List를 In 조건으로 가지고 있는 IntItem List를 가져온다.
        List<Integer> ids = new ArrayList<Integer>();
        for (IntBrandCategory item : brandCategotyList) {
            ids.add(item.getId());
        }
        List<Integer> itemList = intItemService.listWithBrandCategory(ids);
        if (itemList.isEmpty()) {
            return null;
        }

        // 4. IntItem List와 In 조건으로 겹치는 Ingredient List를 가져온다.
        List<Object> recipeList = listWithItem(itemList);

        List<Integer> recipeIntList = new ArrayList<Integer>();
        if (recipeList.isEmpty()) {
            return null;
        } else {
            for (Object item : recipeList) {
                recipeIntList.add((Integer) item);
            }
        }

        // 7. 가져온 Recipe의 id를 in 조건으로 Select하면 Distinct되고 Ordering된 Recipe List가
        // 나온다. page 적용하자
        List<IntRecipe> result = null;
        result = intRecipeDao.listWithId(recipeIntList, pager);
        for (IntRecipe rList : result) {
            Hibernate.initialize(rList.getIntIngredientList());
        }

        // 8. return
        return result;
    }

    // for front search page
    @Transactional
    public List<IntRecipe> searchBy(String q, String language, Pager pager) throws ParseException {
        List<Integer> ids = new ArrayList<Integer>();
        //q와 일치하는 cookery를 가져오고
        List<IntCookery> cookeryList = intRecipeDao.searchCookery(q, language);
        for(IntCookery item : cookeryList){
            ids.add(item.getIntRecipe().getId());
        }
        //q와 일치하는 ingredient를 가져와서
        List<IntIngredient> ingredientList = intRecipeDao.searchIngredient(q, language);
        for(IntIngredient item : ingredientList){
            ids.add(item.getIntRecipe().getId());
        }
        //해당 list의 아이디로 검색 시키기
        List<IntRecipe> recipe = intRecipeDao.searchBy(q, language, pager, ids);
        if (recipe != null) {
            for (IntRecipe rList : recipe) {
                Hibernate.initialize(rList.getIntIngredientList());
            }
        }
        return recipe;
    }

    // for front search page
    @Transactional
    public int countBy(String q, String language) throws ParseException {
        List<Integer> ids = new ArrayList<Integer>();
        //q와 일치하는 cookery를 가져오고
        List<IntCookery> cookeryList = intRecipeDao.searchCookery(q, language);
        for(IntCookery item : cookeryList){
            ids.add(item.getIntRecipe().getId());
        }
        //q와 일치하는 ingredient를 가져와서
        List<IntIngredient> ingredientList = intRecipeDao.searchIngredient(q, language);
        for(IntIngredient item : ingredientList){
            ids.add(item.getIntRecipe().getId());
        }
        //해당 list의 아이디로 검색 시키기
        return intRecipeDao.countBy(q, language, ids);
    }

    @Transactional
    public int updateViews(Integer id) {
        return intRecipeDao.updateViews(id);
    }

    @Transactional
    public int recipeCountForTheme(Integer id) {
        return intRecipeDao.recipeCountForTheme(id);
    }

    @Transactional
    public List<IntRecipe> listForSitemap(Language language , IntRecipeTheme recipeTheme) {
        IntRecipeTheme item = intRecipeThemeService.selectWithCode(language, recipeTheme.getCode());
        if (item == null) {
            return null;
        }

        List<IntRecipe> result;
        result = intRecipeDao.recipeList(item.getId());

        return result;
    }

    @Transactional
    public List<IntRecipe> listForSitemapWithBrand(Language language, IntBrand intBrand) {
        String code = intBrand.getCode();
        // 1. Brand Code를 가지고 Brand를 가져온다.
        IntBrand brand = intBrandService.getOne(language, code);
        if (brand == null) {
            return null;
        }

        // 2. 가져온 Brand ID로 Brand Category List를 가져온다.
        List<IntBrandCategory> brandCategotyList = intBrandCategoryService.bcList(brand.getId(), "on", language);
        if (brandCategotyList.isEmpty()) {
            return null;
        }

        // 3. Brand Category List를 In 조건으로 가지고 있는 IntItem List를 가져온다.
        List<Integer> ids = new ArrayList<Integer>();
        for (IntBrandCategory item : brandCategotyList) {
            ids.add(item.getId());
        }
        List<Integer> itemList = intItemService.listWithBrandCategory(ids);
        if (itemList.isEmpty()) {
            return null;
        }

        // 4. IntItem List와 In 조건으로 겹치는 Ingredient List를 가져온다.
        List<Object> recipeList = listWithItem(itemList);

        List<Integer> recipeIntList = new ArrayList<Integer>();
        if (recipeList.isEmpty()) {
            return null;
        } else {
            for (Object item : recipeList) {
                recipeIntList.add((Integer) item);
            }
        }

        // 7. 가져온 Recipe의 id를 in 조건으로 Select하면 Distinct되고 Ordering된 Recipe List가 나온다
        List<IntRecipe> result = null;
        result = intRecipeDao.listWithId(recipeIntList);

        // 8. return
        return result;
    }
}
