package com.sempio.en.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sempio.en.dao.IntNewsAttachmentDao;
import com.sempio.en.entity.IntNewsAttachment;



@Service
public class FileService {

    @Autowired
    private IntNewsAttachmentDao newsAttachmentDao;

    @Value("${sempiosite.paths.uploadedFiles}")
    private String basedir;

    @Transactional
    public int createImage(String path) {
    /*    Image img = new Image();
        img.setPath(path);

        return imageDao.insert(img);*/
        return 0;
    }


    @Transactional
    public int createFile(String originalFileName, String path, String extension, String type) {
        int result;
        IntNewsAttachment newsAttachment = new IntNewsAttachment();
        newsAttachment.setOriginalFileName(originalFileName);
        newsAttachment.setPath(path);

        result = newsAttachmentDao.insert(newsAttachment);

        return result;
    }

    @Transactional
    public void updatePostId(Integer[] attachmentid, Integer id) {
        newsAttachmentDao.update(attachmentid, id);
    }

    @Transactional
    public void fileRemove(Integer id, String type) {
        if ("image".equals(type)) {
            //imageDao.updateOne(id, null);
        } else if (StringUtils.equals(type, "file")) {
            newsAttachmentDao.updateOne(id, null);
        }
    }

    @Transactional
    public IntNewsAttachment selectOneNewsAttachment(Integer id) {
        return newsAttachmentDao.selectOne(id);
    }
}
