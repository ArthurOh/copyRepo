package com.sempio.en.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sempio.en.code.ContentsType;
import com.sempio.en.code.Language;
import com.sempio.en.code.Result;
import com.sempio.en.dao.IntContentsInfoDao;
import com.sempio.en.entity.IntContentsInfo;

@Service
public class IntContentsInfoService {

    @Autowired
    private IntContentsInfoDao contentsInfoDao;

    @Transactional
    public IntContentsInfo select(int id) {
        return contentsInfoDao.select(id);
    }

    @Transactional
    public Integer insert(IntContentsInfo contentsInfo) {
        return contentsInfoDao.insert(contentsInfo);
    }

    @Transactional
    public Result<String> update(IntContentsInfo contentsInfo) {
        if (contentsInfo.getId() > 0) {
            IntContentsInfo origin = select(contentsInfo.getId());
            origin.setContents(contentsInfo.getContents());
            origin.setRewriter(contentsInfo.getRewriter());
            origin.setUpdateAt(new Date());
            contentsInfoDao.update(origin);
        } else {
            contentsInfoDao.insert(contentsInfo);
        }
        return Result.success();
    }

    @Transactional
    public IntContentsInfo select(ContentsType contentsType, Language language) {
        return contentsInfoDao.select(contentsType, language);
    }
}
