package com.sempio.en.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sempio.en.dao.IntEmailContactLogDao;
import com.sempio.en.entity.IntEmailContactLog;

@Service
public class IntEmailContactLogService {

    @Autowired
    private IntEmailContactLogDao intEmailContactLogDao;

    @Transactional
    public IntEmailContactLog select(int id) {
        return intEmailContactLogDao.select(id);
    }

    @Transactional
    public Integer insert(IntEmailContactLog intEmailContactLog) {
        return intEmailContactLogDao.insert(intEmailContactLog);
    }

    @Transactional
    public void update(IntEmailContactLog intEmailContactLog) {
        intEmailContactLogDao.update(intEmailContactLog);
    }
}
