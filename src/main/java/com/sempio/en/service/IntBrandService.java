package com.sempio.en.service;

import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.dao.IntBrandDao;
import com.sempio.en.entity.IntBrand;
import com.sempio.en.entity.IntBrandCategory;

@Service
public class IntBrandService {

    @Autowired
    private IntBrandDao intBrandDao;

    @Transactional
    public List<IntBrand> listForAdmin(Language language, int size, int page) {
        List<IntBrand> brand = intBrandDao.listForAdmin(language, size, page * size);
        if (brand != null) {
            for (IntBrand blist : brand) {
                Hibernate.initialize(blist.getIntBrandCategoryList());

                for (IntBrandCategory bclist : blist.getIntBrandCategoryList()) {
                    Hibernate.initialize(bclist.getIntItemList());
                }
            }
        }
        return brand;
    }

    @Transactional
    public int countForAdmin(Language language) {
        return intBrandDao.countForAdmin(language);
    }

    @Transactional
    public Integer create(IntBrand brand) {
        return intBrandDao.insert(brand);
    }

    @Transactional
    public IntBrand load(int id) {
        IntBrand brand = intBrandDao.selectForAdmin(id);
        if (brand != null) {
            /*for (BrandCategory bc : brand.getBrandCategoryList()) {
                Hibernate.initialize(bc.getCurrentItemOption());
                Hibernate.initialize(bc.getCurrentSaleItemOption());
            }*/
        }
        return brand;
    }

    @Transactional
    public void updatePost(IntBrand brand) {
        IntBrand loaded = intBrandDao.getBrand(brand.getId());

        loaded.setTitle(brand.getTitle());
        loaded.setDescription(brand.getDescription());
        loaded.setSlogan(brand.getSlogan());
        loaded.setPoint(brand.getPoint());
        loaded.setPcImgPath(brand.getPcImgPath());
        loaded.setMobileImgPath(brand.getMobileImgPath());
        loaded.setLogoImgPath(brand.getLogoImgPath());
        loaded.setBannerImgPath(brand.getBannerImgPath());
        loaded.setRelationSiteName(brand.getRelationSiteName());
        loaded.setRelationSiteUrl(brand.getRelationSiteUrl());
        loaded.setPublishAt(brand.getPublishAt());
        loaded.setUpdateAt(brand.getUpdateAt());
        loaded.setState(brand.getState());
        loaded.setRewriter(brand.getRewriter());

        intBrandDao.update(loaded);
    }

    @Transactional
    public List<IntBrand> listAllWithCategory(String language) {
        List<IntBrand> brands = intBrandDao.list(language);
        if (brands != null) {
            for (IntBrand brand : brands) {
                Hibernate.initialize(brand.getIntBrandCategoryList());
            }
        }
        return brands;
    }

    @Transactional
    public List<IntBrand> listAll(Language language) {
        return intBrandDao.listAll(language);
    }

    // list for front
    @Transactional
    public IntBrand getOne(Language language, String code) {
        return intBrandDao.getOne(code, language);
    }

    @Transactional
    public List<IntBrand> listAll(State state) {
        List<IntBrand> brand = intBrandDao.listAll(state);
        if (brand != null) {
            for (IntBrand blist : brand) {
                Hibernate.initialize(blist.getIntBrandCategoryList());

                for (IntBrandCategory bclist : blist.getIntBrandCategoryList()) {
                    Hibernate.initialize(bclist.getIntItemList());
                }
            }
        }
        return brand;
    }

    @Transactional
    public List<IntBrand> listAll(Language language, State state) {
        List<IntBrand> brand = intBrandDao.listAll(language, state);
        if (brand != null) {
            for (IntBrand blist : brand) {
                Hibernate.initialize(blist.getIntBrandCategoryList());

                for (IntBrandCategory bclist : blist.getIntBrandCategoryList()) {
                    Hibernate.initialize(bclist.getIntItemList());
                }
            }
        }
        return brand;
    }

    // 브랜드 코드 중복 확인
    @Transactional
    public IntBrand checkCode(String code, Language language) {
        return intBrandDao.checkCode(code, language);
    }

    // for xml
    @Transactional
    public List<IntBrand> listForSitemap() {
        //return intBrandDao.listForSitemap();
        List<IntBrand> brand = intBrandDao.listForSitemap();
        if (brand != null) {
            for (IntBrand blist : brand) {
                Hibernate.initialize(blist.getIntBrandCategoryList());

                for (IntBrandCategory bclist : blist.getIntBrandCategoryList()) {
                    Hibernate.initialize(bclist.getIntItemList());
                }
            }
        }
        return brand;
    }

}
