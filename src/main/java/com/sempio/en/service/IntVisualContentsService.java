package com.sempio.en.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.sempio.en.code.Language;
import com.sempio.en.code.Result;
import com.sempio.en.code.State;
import com.sempio.en.code.VisualType;
import com.sempio.en.command.IntVisualContentsSearchForm;
import com.sempio.en.dao.IntVisualContentsDao;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntVisualContents;
import com.sempio.en.util.Pager;

@Service
public class IntVisualContentsService {
    
    @Autowired
    private IntVisualContentsDao visualContentsDao;
    
    @Transactional
    public IntVisualContents select(int id) {
        return visualContentsDao.select(id);
    }

    @Transactional
    public Integer insert(IntVisualContents visualContents) {
        return visualContentsDao.insert(visualContents);
    }

    @Transactional
    public Result<String> update(IntVisualContents visualContents) {
        VisualType type = visualContents.getType();
        int cnt = 0;
        if (type == VisualType.top) {
            cnt = visualContentsDao.updateTop(visualContents);
        } else if (type == VisualType.bottom) {
            cnt = visualContentsDao.updateBottom(visualContents);
        } else if (type == VisualType.popup) {
            cnt = visualContentsDao.updatePopup(visualContents);
        }
        
        if (cnt > 0) {
            return Result.success();
        }
        return Result.error();
    }

    @Transactional
    public int update(Integer[] ids, State state, IntAdminUser adminUser) {
        return visualContentsDao.update(ids, state, adminUser);
    }
    
    /**
     * 메인 상단배너 조회
     * @param language
     * @param front 프론트 조회 옵션 (true 인 경우 where state = 'on' and publish_start_at <= current_date <= publish_end_at)
     * @return
     */
    @Transactional
    public List<IntVisualContents> selectTopList(Language language, boolean front) {
        List<Criterion> criterions = Lists.<Criterion>newArrayList(
                Restrictions.eq("language", language),
                Restrictions.eq("type", VisualType.top));
        if (front) {
            criterions.add(Restrictions.eq("state", State.on));
            criterions.add(Restrictions.sqlRestriction(StringUtils.join("publish_start_at <= current_date and current_date <= publish_end_at")));
        } else {
            criterions.add(Restrictions.ne("state", State.deleted));
        }
        List<Order> orders = Lists.<Order>newArrayList(Order.asc("ordering"), Order.desc("id"));
        List<IntVisualContents> list = visualContentsDao.selectBy(criterions, orders, null);
        return list.size() > 5? list.subList(0, 5) : list;
    }
    
    /**
     * 메인 하단배너 조회
     * @param language
     * @param front 프론트 조회 옵션 (true 인 경우 where state = 'on')
     * @return
     */
    @Transactional
    public List<IntVisualContents> selectBottomList(Language language, boolean front) {
        List<Criterion> criterions = Lists.<Criterion>newArrayList(
                Restrictions.eq("language", language),
                Restrictions.eq("type", VisualType.bottom));
        if (front) {
            criterions.add(Restrictions.eq("state", State.on));
        } else {
            criterions.add(Restrictions.ne("state", State.deleted));
        }
        List<Order> orders = Lists.<Order>newArrayList(Order.asc("ordering"), Order.desc("id"));
        List<IntVisualContents> list = visualContentsDao.selectBy(criterions, orders, null);
        return list.size() > 3? list.subList(0, 3) : list;
    }
    
    /**
     * 팝업 조회
     * @param language
     * @param front 프론트 조회 옵션 (true 인 경우 where state = 'on' and publish_start_at <= current_date <= publish_end_at)
     * @return
     */
    @Transactional
    public List<IntVisualContents> selectPopupList(Language language, IntVisualContentsSearchForm form, Pager pager, boolean front) {
        List<Criterion> criterions = Lists.<Criterion>newArrayList(
                Restrictions.eq("language", language),
                Restrictions.eq("type", VisualType.popup));
        if (front) {
            criterions.add(Restrictions.eq("state", State.on));
            criterions.add(Restrictions.sqlRestriction(StringUtils.join("publish_start_at <= current_date and current_date <= publish_end_at")));
        } else {
            criterions.add(Restrictions.ne("state", State.deleted));
            if (form != null && StringUtils.isNoneBlank(form.getQ())) {
                if (StringUtils.equals(form.getqType(), "title")) {
                    criterions.add(Restrictions.ilike("title", form.getQ(), MatchMode.START));
                } else if (StringUtils.equals(form.getqType(), "contents")) {
                    criterions.add(Restrictions.ilike("contents", form.getQ(), MatchMode.ANYWHERE));
                }
            }
        }
        List<Order> orders = Lists.<Order>newArrayList(Order.desc("id"));
        List<IntVisualContents> list = visualContentsDao.selectBy(criterions, orders, pager);
        return list.size() > 1 && front ? list.subList(0, 1) : list;
    }
    
    @Transactional
    public int countByPopup(Language language, IntVisualContentsSearchForm form, boolean front) {
        List<Criterion> criterions = Lists.<Criterion>newArrayList(
                Restrictions.eq("language", language),
                Restrictions.eq("type", VisualType.popup));
        if (front) {
            criterions.add(Restrictions.eq("state", State.on));
            criterions.add(Restrictions.sqlRestriction(StringUtils.join("publish_start_at <= current_date and current_date <= publish_end_at")));
        } else {
            criterions.add(Restrictions.ne("state", State.deleted));
            if (form != null && StringUtils.isNoneBlank(form.getQ())) {
                if (StringUtils.equals(form.getqType(), "title")) {
                    criterions.add(Restrictions.ilike("title", form.getQ(), MatchMode.START));
                } else if (StringUtils.equals(form.getqType(), "contents")) {
                    criterions.add(Restrictions.ilike("contents", form.getQ(), MatchMode.ANYWHERE));
                }
            }
        }
        return visualContentsDao.countBy(criterions);
    }
    
    @Transactional
    public void updateOrdering(List<Map<String, Object>> list, Language language) {
        for (Map<String, Object> item : list) {
            int id = Integer.parseInt(item.get("id").toString());
            int ordering = Integer.parseInt(item.get("ordering").toString());
            visualContentsDao.updateOrdering(id, ordering, language);
        }
    }
}
