package com.sempio.en.service;

import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.sempio.en.code.Language;
import com.sempio.en.code.Result;
import com.sempio.en.code.State;
import com.sempio.en.command.IntNewsletterApplySearchForm;
import com.sempio.en.command.IntNewsletterSearchForm;
import com.sempio.en.dao.IntNewsletterApplyDao;
import com.sempio.en.dao.IntNewsletterDao;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntNewsletter;
import com.sempio.en.entity.IntNewsletterApply;
import com.sempio.en.util.Pager;

@Service
public class IntNewsletterService {

    @Autowired
    private IntNewsletterDao newsletterDao;

    @Autowired
    private IntNewsletterApplyDao newsletterApplyDao;

    @Transactional
    public Result<IntNewsletter> select(int id) {
        IntNewsletter newsletter = newsletterDao.select(id);
        if (newsletter == null || newsletter.getState() == State.deleted) {
            return Result.error();
        }
        return Result.success(newsletter);
    }

    @Transactional
    public Integer insert(IntNewsletter newsletter) {
        return newsletterDao.insert(newsletter);
    }

    @Transactional
    public Result<String> update(IntNewsletter newsletter) {

        Result<IntNewsletter> result = select(newsletter.getId());
        if (result.isError()) {
            return Result.error("Invalid access.");
        }

        IntNewsletter origin = result.payload();
        origin.setPublishYear(newsletter.getPublishYear());
        origin.setPublishMonth(newsletter.getPublishMonth());
        origin.setPublishNo(newsletter.getPublishNo());
        origin.setImage(newsletter.getImage());
        origin.setTitle(newsletter.getTitle());
        origin.setTitleInt(newsletter.getTitleInt());
        origin.setContent(newsletter.getContent());
        origin.setContentInt(newsletter.getContentInt());
        origin.setPublishAt(newsletter.getPublishAt());
        origin.setState(newsletter.getState());
        origin.setUpdateAt(new Date());
        origin.setRewriter(newsletter.getRewriter());
        newsletterDao.update(origin);
        return Result.success();
    }

    @Transactional
    public int update(Integer[] ids, State state, IntAdminUser rewriter) {
        return newsletterDao.update(ids, state, rewriter);
    }

    /* 관리자 > 콘텐츠 > 뉴스레터 조회 */
    @Transactional
    public List<IntNewsletter> list(IntNewsletterSearchForm form, Language language, Pager pager) {
        return newsletterDao.list(form, language, pager);
    }

    /* 프론트 > 뉴스레터 조회 */
    @Transactional
    public List<IntNewsletter> list(Language language, Pager pager) {
        List<Criterion> criterions = Lists.newArrayList(
                Restrictions.eq("language", language),
                Restrictions.eq("state", State.on),
                Restrictions.sqlRestriction("publish_at <= current_date"));
        List<Order> orders = Lists.newArrayList(Order.desc("publishYear"), Order.desc("publishMonth"), Order.desc("publishNo"));
        return newsletterDao.selectBy(criterions, orders, pager);
    }

    @Transactional
    public int count(IntNewsletterSearchForm form, Language language) {
        return newsletterDao.count(form, language);
    }

    @Transactional
    public int count(Language language) {
        List<Criterion> criterions = Lists.newArrayList(
                Restrictions.eq("language", language),
                Restrictions.eq("state", State.on),
                Restrictions.sqlRestriction("publish_at <= current_date"));
        return newsletterDao.countBy(criterions);
    }

    @Transactional
    public IntNewsletterApply selectApply(int id) {
        return newsletterApplyDao.select(id);
    }

    @Transactional
    public Integer insertApply(IntNewsletterApply newsletterApply) {
        return newsletterApplyDao.insert(newsletterApply);
    }

    @Transactional
    public Result<String> updateApply(IntNewsletterApply newsletterApply) {
        IntNewsletterApply origin = selectApply(newsletterApply.getId());
        if (origin == null || origin.getState() == State.deleted) {
            return Result.error("Invalid access.");
        }
        origin.setName(newsletterApply.getName());
        origin.setEmail(newsletterApply.getEmail());
        origin.setCountry(newsletterApply.getCountry());
        origin.setApplyLanguage(newsletterApply.getApplyLanguage());
        origin.setLanguage(newsletterApply.getLanguage());
        origin.setState(newsletterApply.getState());
        newsletterApplyDao.update(origin);
        return Result.success();
    }

    @Transactional
    public int updateApply(Integer[] ids, State state) {
        return newsletterApplyDao.update(ids, state);
    }

    @Transactional
    public List<IntNewsletterApply> listApply(IntNewsletterApplySearchForm form, Pager pager) {
        return newsletterApplyDao.list(form, pager);
    }

    @Transactional
    public int countApply(IntNewsletterApplySearchForm form) {
        return newsletterApplyDao.count(form);
    }

    // for front search page
    @Transactional
    public List<IntNewsletter> searchBy(String q, Language language, Pager pager) {
        return newsletterDao.searchBy(q, language, pager);
    }

    // for front search page
    @Transactional
    public int countBy(String q, Language language) {
        return newsletterDao.countBy(q, language);
    }

    @Transactional
    public int updateViews(int id) {
        return newsletterDao.updateViews(id);
    }

    // for xml
    @Transactional
    public List<IntNewsletter> listForSitemap() {
        return newsletterDao.listForSitemap();
    }
}
