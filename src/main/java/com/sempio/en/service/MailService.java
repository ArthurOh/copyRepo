package com.sempio.en.service;

import java.util.Map;

import com.sempio.en.code.MailTemplateType;


public interface MailService {

    public boolean send(String to, String title, MailTemplateType template, Map<String, Object> args);
    public boolean send(String to, String title, String message);
    public boolean send(String to, String from, String title, String msg);
    public boolean send(String to, String toName, String from, String fromName, String title, MailTemplateType template, Map<String, Object> args);
    public boolean send(String to, String toName, String from, String fromName, String title, String message);
    
}
