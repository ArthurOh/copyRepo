package com.sempio.en.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sempio.en.code.Language;
import com.sempio.en.code.Result;
import com.sempio.en.code.State;
import com.sempio.en.dao.IntRecipeThemeDao;
import com.sempio.en.entity.IntRecipeTheme;

@Service
public class IntRecipeThemeService {

    @Autowired
    private IntRecipeThemeDao intRecipeThemeDao;

    @Transactional
    public Result<IntRecipeTheme> select(int id) {
        IntRecipeTheme intRecipeTheme= intRecipeThemeDao.select(id);
        if (intRecipeTheme == null || StringUtils.equals(intRecipeTheme.getState(), State.deleted.name())) {
            return Result.error();
        }
        return Result.success(intRecipeTheme);
    }

    @Transactional
    public Integer insert(IntRecipeTheme intRecipeTheme) {
        return intRecipeThemeDao.insert(intRecipeTheme);
    }

    @Transactional
    public Result<String> update(IntRecipeTheme intRecipeTheme) {
        int cnt = intRecipeThemeDao.update(intRecipeTheme);
        if (cnt == 0) {
            return Result.error("잘못된 접근입니다");
        }
        return Result.success();
    }

    @Transactional
    public int update(Integer[] ids, State state, String rewriter) {
        return intRecipeThemeDao.update(ids, state, rewriter);
    }

    @Transactional
    public List<IntRecipeTheme> list(Language language, State state, Integer[] ids) {
        return intRecipeThemeDao.list(language, state, ids);
    }

    @Transactional
    public List<IntRecipeTheme> list(Language language, State state) {
        return intRecipeThemeDao.list(language, state);
    }

    @Transactional
    public int count(Language language, State state) {
        return intRecipeThemeDao.count(language, state);
    }

    @Transactional
    public void updateOrdering(List<Map<String, Object>> list, Language language) {
        for (Map<String, Object> item : list) {
            int id = Integer.parseInt(item.get("id").toString());
            int ordering = Integer.parseInt(item.get("ordering").toString());
            intRecipeThemeDao.updateOrdering(id, ordering, language);
        }
    }

    @Transactional
    public IntRecipeTheme selectWithCode(Language language, String recipeTheme) {
        return intRecipeThemeDao.selectWithCode(language, recipeTheme);
    }

}
