package com.sempio.en.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

@Component
public class StaticPageHolder {
    @Autowired
    private Environment env;

    private static List<String> staticPages;

    private final String staticPageBase = "staticPages";

    public String getStaticpagebase() {
        return staticPageBase;
    }

    @PostConstruct
    public void gatherTemplate() {
        if (staticPages == null) {
            final List<Path> filesInFolder = Lists.newArrayList();
            String base = env.getProperty("rythm.base-directory");
            String prefix = "file:";
            String postfix = ".rythm";

            if (StringUtils.startsWith(base, prefix)) {
                base = StringUtils.substring(base, base.indexOf(prefix) + prefix.length());
            }
            base = base + staticPageBase;
            try {
                //filesInFolder = Files.walk(Paths.get(base)).filter(Files::isRegularFile).collect(Collectors.toList());
                Files.walkFileTree(Paths.get(base), new FileVisitor<Path>() {
                    @Override
                    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                        return FileVisitResult.CONTINUE;
                    }
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        if (Files.isRegularFile(file)) {
                            filesInFolder.add(file);
                        }
                        return FileVisitResult.CONTINUE;
                    }
                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                        return FileVisitResult.CONTINUE;
                    }
                    @Override
                    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (IOException e) {
            }
            List<String> templatePath = new ArrayList<String>();
            for (Path p : filesInFolder) {
                String aPath = p.toString();
                aPath = StringUtils.replace(aPath, File.separator, "/");
                templatePath.add(StringUtils.substringBetween(aPath, base, postfix));
            }
            staticPages = templatePath;
        }
    }

    public boolean checkPathExists(String path) {
        boolean r = false;
        if (staticPages != null) {
            return staticPages.contains(path);
        }
        return r;
    }
}