package com.sempio.en.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sempio.en.code.Language;
import com.sempio.en.dao.IntNewsAttachmentDao;
import com.sempio.en.dao.IntNewsDao;
import com.sempio.en.entity.IntNews;
import com.sempio.en.util.Pager;

@Service
public class IntNewsService {

    @Autowired
    private IntNewsDao intNewsDao;

    @Autowired
    private IntNewsAttachmentDao intNewsAttachmentDao;

    @Transactional
    public List<IntNews> listForAdmin(Language language, String q, String qType, int size, int page) {
        return intNewsDao.listForAdmin(language, q, qType, size, page * size);
    }

    @Transactional
    public int countForAdmin(Language language, String q, String qType) {
        return intNewsDao.countForAdmin(language, q, qType);
    }

    @Transactional
    public void create(IntNews news, Integer[] attachmentid) {
        Integer newsId = intNewsDao.insert(news);

        if (attachmentid != null) {
            intNewsAttachmentDao.update(attachmentid, newsId);
        }
    }

    @Transactional
    public IntNews load(int id) {
        return intNewsDao.selectForAdmin(id);
    }

    @Transactional
    public void updatePost(IntNews news) {
        IntNews loaded = intNewsDao.getNews(news.getId());
        loaded.setTitle(news.getTitle());
        loaded.setContents(news.getContents());
        loaded.setListImgPath(news.getListImgPath());
        loaded.setPublishAt(news.getPublishAt());
        loaded.setTags(news.getTags());
        loaded.setState(news.getState());
        loaded.setUpdateAt(news.getUpdateAt());
        loaded.setRewriter(news.getRewriter());

        intNewsDao.update(loaded);
    }

    // for front
    @Transactional
    public List<IntNews> list(Language language, Pager pager) {
        return intNewsDao.list(language, pager);
    }

    // for front
    @Transactional
    public int count(Language language) {
        return intNewsDao.count(language);
    }

    // for front
    @Transactional
    public IntNews view(Language language, Integer id) {
        return intNewsDao.view(language, id);
    }

    // for front (main page)
    @Transactional
    public List<IntNews> selectMainList(Language language) {
        return intNewsDao.selectMainList(language);
    }

    // for front search page
    @Transactional
    public List<IntNews> searchBy(String q, Language language, Pager pager) {
        return intNewsDao.searchBy(q, language, pager);
    }

    // for front search page
    @Transactional
    public int countBy(String q, Language language) {
        return intNewsDao.countBy(q, language);
    }

    @Transactional
    public int updateViews(int id) {
        return intNewsDao.updateViews(id);
    }

    // for xml
    @Transactional
    public List<IntNews> listForSitemap() {
        return intNewsDao.listForSitemap();
    }
}
