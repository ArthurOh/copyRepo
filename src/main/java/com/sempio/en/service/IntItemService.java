package com.sempio.en.service;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sempio.en.code.Language;
import com.sempio.en.command.ItemSearchForm;
import com.sempio.en.dao.IntItemDao;
import com.sempio.en.entity.IntItem;
import com.sempio.en.util.Const;
import com.sempio.en.util.Pager;

@Service
public class IntItemService {

    @Autowired
    private IntItemDao intItemDao;

    @Transactional
    public List<IntItem> listForAdmin(Language language, ItemSearchForm searchForm, Pager pager) {
        return intItemDao.listForAdmin(language, searchForm, pager.getEntriesPerPage(), pager.getOffset());
    }

    @Transactional
    public int countForAdmin(Language language, ItemSearchForm searchForm) {
        return intItemDao.countForAdmin(language, searchForm);
    }

    @Transactional
    public Integer create(IntItem item) {
        return intItemDao.insert(item);
    }

    @Transactional
    public IntItem getOne(Language language, Integer id) {
        IntItem item = intItemDao.getOne(language, id);
        return item;
    }

    @Transactional
    public void update(IntItem item) {
        IntItem loaded = intItemDao.getOne(item.getId());

        loaded.setTitle(item.getTitle());
        loaded.setPointMsg(item.getPointMsg());
        loaded.setWrap(item.getWrap());
        loaded.setListImgPath(item.getListImgPath());
        loaded.setViewImgPath(item.getViewImgPath());
        loaded.setRecommendRecipe(item.getRecommendRecipe());
        loaded.setPublishAt(item.getPublishAt());
        loaded.setFeature(item.getFeature());
        loaded.setTags(item.getTags());
        loaded.setIngredient(item.getIngredient());
        loaded.setAllergyInfo(item.getAllergyInfo());
        loaded.setBestBy(item.getBestBy());
        loaded.setCertification(item.getCertification());
        loaded.setServingSize(item.getServingSize());
        loaded.setServingAmount(item.getServingAmount());
        loaded.setFat(item.getFat());
        loaded.setCholesterol(item.getCholesterol());
        loaded.setSodium(item.getSodium());
        loaded.setCarbohydrate(item.getCarbohydrate());
        loaded.setProtein(item.getProtein());
        loaded.setState(item.getState());
        loaded.setOrdering(Const.CONTENT_ORDERING);
        loaded.setUpdateAt(item.getUpdateAt());
        loaded.setLanguage(item.getLanguage());
        loaded.setState(item.getState());
        loaded.setRewriter(item.getRewriter());
        loaded.setIntBrandCategory(item.getIntBrandCategory());
        loaded.setOrdering(item.getOrdering());

        intItemDao.update(loaded);
    }

    // for excel download
    @Transactional
    public List<IntItem> listAll(Language language, ItemSearchForm searchForm) {
        return intItemDao.listAll(language, searchForm);
    }

    // for front item page
    @Transactional
    public List<IntItem> listForItem(Integer bcId) {
        return intItemDao.listForItem(bcId);
    }

    @Transactional
    public List<Integer> listWithBrandCategory(List<Integer> ids) {
        return intItemDao.listWithBrandCategory(ids);
    }

    // for front search page
    @Transactional
    public List<IntItem> searchBy(String q, Language language, Pager pager) throws ParseException {
        return intItemDao.searchBy(q, language, pager);
    }

    // for front search page
    @Transactional
    public int countBy(String q, Language language) throws ParseException {
        return intItemDao.countBy(q, language);
    }

    public List<IntItem> searchByTitle(String q, String language) {
        return intItemDao.searchByTitle(q, language);
    }
}
