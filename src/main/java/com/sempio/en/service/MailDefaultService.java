package com.sempio.en.service;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.rythmengine.Rythm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sempio.en.code.MailTemplateType;
import com.sempio.en.util.SendGrid;
import com.sendgrid.SendGridException;

@Service
public class MailDefaultService implements MailService {

    private static Logger log = LoggerFactory.getLogger(MailDefaultService.class);

    @Value("${email.sendgrid.id}")
    private String id;

    @Value("${email.sendgrid.password}")
    private String password; // API key 대용으로도 사용

    @Value("${email.sender.name}")
    private String defaultSenderName;

    @Value("${email.sender.address}")
    private String defaultSenderAddress;

    @Override
    public boolean send(String to, String title, MailTemplateType template, Map<String, Object> args) {
        return send(to, null, defaultSenderAddress, defaultSenderName, title, template, args);
    }

    @Override
    public boolean send(String to, String title, String message) {
        return send(to, null, defaultSenderAddress, defaultSenderName, title, message);
    }

    @Override
    public boolean send(String to, String from, String title, String msg) {
        return send(to, null, from, defaultSenderName, title, msg);
    }

    @Override
    public boolean send(String to, String toName, String from, String fromName, String title, MailTemplateType template, Map<String, Object> args) {
        String msg = Rythm.render(template.getPath(), args);
        return send(to, toName, from, fromName, title, msg);
    }

    @Override
    public boolean send(String to, String toName, String from, String fromName, String title, String message) {

        log.debug("to = {}, toName = {}, from = {}, fromName = {}, title = {}, message = {}", to, toName, from, fromName, title, message);

        SendGrid sendGrid = getSendGrid();
        SendGrid.Email email = new SendGrid.Email();

        email.addTo(to);
        if (StringUtils.isNoneBlank(toName)) {
            email.addToName(toName);
        }
        if (StringUtils.isNoneBlank(from)) {
            email.setFrom(from);
        } else {
            email.setFrom(defaultSenderAddress);
        }

        if (StringUtils.isNoneBlank(fromName)) {
            email.setFromName(fromName);
        } else {
            email.setFromName(defaultSenderName);
        }
        email.setSubject(title);
        email.setHtml(message);

        SendGrid.Response response = null;
        try {
            response = sendGrid.send(email);
            log.debug("response = {}", response);
            return response.getStatus();

        } catch (SendGridException e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    // 매번 객체를 생성하지않게 할지...추후 고민
    private SendGrid getSendGrid() {
        if (StringUtils.isEmpty(id) || StringUtils.isEmpty(password)) {
            throw new RuntimeException("sendGrid id, password required");
        }
        return new SendGrid(id, password);
    }
}
