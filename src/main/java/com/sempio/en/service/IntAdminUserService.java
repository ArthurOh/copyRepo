package com.sempio.en.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.sempio.en.code.AdminType;
import com.sempio.en.code.Result;
import com.sempio.en.code.State;
import com.sempio.en.command.IntAdminUserSearchForm;
import com.sempio.en.dao.IntAdminUserDao;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.util.BaseUtil;
import com.sempio.en.util.Pager;
import com.sempio.en.view.IntAdminLog;

@Service
public class IntAdminUserService {

    @Autowired
    private IntAdminUserDao adminUserDao;

    @Transactional
    public Result<IntAdminUser> authenticate(String userId, String password) {
        IntAdminUser admin = select(userId);
        if (admin == null || State.on != admin.getState()) {
            return Result.error();
        }
        if (StringUtils.equals(admin.getUserPw(), BaseUtil.getPassword(password))) {
            return Result.success(admin);
        }
        return Result.error();
    }

    @Transactional
    public IntAdminUser select(String userId) {
        return adminUserDao.select(userId);
    }

    @Transactional
    public IntAdminUser select(int id) {
        return adminUserDao.select(id);
    }

    @Transactional
    public Integer insert(IntAdminUser admin) {
        admin.setUserId(admin.getUserId().trim());
        admin.setUserPw(BaseUtil.getPassword(admin.getUserPw().trim()));
        return adminUserDao.insert(admin);
    }

    @Transactional
    public Result<String> update(IntAdminUser admin) {
        IntAdminUser origin = select(admin.getId());
        if (origin == null || origin.getState() == State.deleted) {
            return Result.error("Invalid access.");
        }
        origin.setUserName(admin.getUserName());
        if (StringUtils.isNoneBlank(admin.getUserPw())) {
            origin.setUserPw(BaseUtil.getPassword(admin.getUserPw().trim()));
        }
        origin.setDepartment(admin.getDepartment());
        origin.setUserEmail(admin.getUserEmail());
        origin.setUserTel(admin.getUserTel());
        origin.setType(admin.getType());
        if (origin.getType() == AdminType.integration) {
            origin.setRoles(null);
        } else {
            origin.setRoles(admin.getRoles());
        }
        origin.setState(admin.getState());
        origin.setRewriter(admin.getRewriter());
        origin.setUpdateAt(new Date());
        adminUserDao.update(origin);
        return Result.success();
    }

    @Transactional
    public int updateByUser(IntAdminUser form) {
        IntAdminUser origin = select(form.getId());
        if (origin == null) {
            throw new RuntimeException("not found admin info");
        }
        if (StringUtils.isNoneBlank(form.getUserPw())) {
            String pw = BaseUtil.getPassword(form.getUserPw());
            origin.setUserPw(pw);
        }
        origin.setUserName(form.getUserName());
        origin.setDepartment(form.getDepartment());
        origin.setUserEmail(form.getUserEmail());
        origin.setUserTel(form.getUserTel());
        origin.setRewriter(form.getRewriter());
        return adminUserDao.updateByUser(origin);
    }

    @Transactional
    public int update(Integer[] ids, State state, IntAdminUser adminUser) {
        return adminUserDao.update(ids, state, adminUser);
    }

    @Transactional
    public List<IntAdminUser> list(IntAdminUserSearchForm form, Pager pager) {
        return adminUserDao.list(form, pager);
    }

    @Transactional
    public int count(IntAdminUserSearchForm form) {
        return adminUserDao.count(form);
    }

    @Transactional
    public List<IntAdminLog> listLog(String adminId, int limit, int offset) {
        List<IntAdminLog> logs = Lists.<IntAdminLog>newArrayList();

        List<Object[]> list = adminUserDao.selectLog(adminId, limit, offset);
        for (Object[] arr : list) {
            IntAdminLog log = new IntAdminLog();
            log.setLogAt((Date) arr[0]);
            log.setIp((String) arr[1]);
            log.setAction((String) arr[2]);
            logs.add(log);
        }
        return logs;
    }

    @Transactional
    public int countLog(String sysopid) {
        return adminUserDao.countLog(sysopid);
    }

    @Transactional
    public IntAdminUser load(String adminId) {
        return adminUserDao.selectForAdminById(adminId);
    }

    @Transactional
    public void logAction(String string, String ip, String action) {
        adminUserDao.insertLog(string, ip, action);
    }
}
