package com.sempio.en.code;

import org.apache.commons.lang3.StringUtils;

public enum VisualViewType {
    image, video;
    public static boolean contains(String value) {
        for (VisualViewType item : VisualViewType.values()) {
            if (StringUtils.equals(value, item.name())) {
                return true;
            }
        }
        return false;
    }
}
