package com.sempio.en.code;

import org.apache.commons.lang3.StringUtils;

public enum Language {
    en, cn, ru;

    public static boolean contains(String value) {
        for (Language item : Language.values()) {
            if (StringUtils.equals(value, item.name())) {
                return true;
            }
        }
        return false;
    }
}