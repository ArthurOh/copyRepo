package com.sempio.en.code;

import org.apache.commons.lang3.StringUtils;

public enum VisualType {
    top, bottom, popup;

    public static boolean contains(String value) {
        for (VisualType item : VisualType.values()) {
            if (StringUtils.equals(value, item.name())) {
                return true;
            }
        }
        return false;
    }
}
