package com.sempio.en.code;

import org.apache.commons.lang3.StringUtils;

public enum AdminType {

    integration("통합관리자"), general("일반관리자");

    private String desc;

    private AdminType(String desc) {

        this.desc = desc;
    }
    public String getDesc() {
        return desc;
    }
    public static boolean contains(String value) {
        for (AdminType item : AdminType.values()) {
            if (StringUtils.equals(value, item.name())) {
                return true;
            }
        }
        return false;
    }
}
