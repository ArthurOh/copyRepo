package com.sempio.en.code;

public enum MailTemplateType {
    contact("/mail/contact.rythm");

    private String path;

    private MailTemplateType(String path) {
        this.path = path;
    }

    public String getPath() {
        return this.path;
    }
}
