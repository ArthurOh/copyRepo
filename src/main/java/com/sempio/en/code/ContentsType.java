package com.sempio.en.code;

import org.apache.commons.lang3.StringUtils;

public enum ContentsType {
    contact, network;
    
    public static boolean contains(String value) {
        for (ContentsType item : ContentsType.values()) {
            if (StringUtils.equals(value, item.name())) {
                return true;
            }
        }
        return false;
    }
}