package com.sempio.en.code;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

public enum AdminRoles {
    en_contents, cn_contents, ru_contents, // 콘텐츠 관리
    en_display, cn_display, ru_display, // 전시 관리
    en_product, cn_product, ru_product, // 제품 관리
    en_operation, cn_operation, ru_operation, // 운영 관리
    integration;

    public String getValue() {
        return this.name().toLowerCase();
    }

    public static AdminRoles findByName(String name) {
        try {
            return AdminRoles.valueOf(name);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
    
    public static final List<AdminRoles> enGroup = ImmutableList.of(en_contents, en_display, en_product, en_operation);
    public static final List<AdminRoles> cnGroup = ImmutableList.of(cn_contents, cn_display, cn_product, cn_operation);
    public static final List<AdminRoles> ruGroup = ImmutableList.of(ru_contents, ru_display, ru_product, ru_operation);

    public static List<AdminRoles> findByLanguage(Language value) {
        if (Language.en == value) {
            return enGroup;
        } else if (Language.cn == value) {
            return cnGroup;
        } else if (Language.ru == value) {
            return ruGroup;
        }
        return null;   
    }
    
    public static List<AdminRoles> toList(String [] roles) {
        List<AdminRoles> list = Lists.newArrayList();
        for (String item : roles) {
            AdminRoles role = findByName(item);
            if (role != null) {
                list.add(role);
            }
        }
        return list;
    }
}
