package com.sempio.en.entity;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.AdminType;
import com.sempio.en.code.Language;
import com.sempio.en.code.State;

@Entity
@Table(name = "int_admin_user")
public class IntAdminUser {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_admin_user")
	@SequenceGenerator(name = "seq_int_admin_user", sequenceName = "int_admin_user_id_seq", allocationSize = 1)
	private Integer id;

	@Column(name = "user_id")
	private String userId;

	@Column(name = "user_pw")
	private String userPw;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "department")
    private String department;

	@Column(name = "user_email")
    private String userEmail;

    @Column(name = "user_tel")
    private String userTel;

    @Enumerated(EnumType.STRING)
    @Column
    private AdminType type;

    @Type(type = "com.iropke.common.hibernate.ArrayType")
    private String[] roles;

    @Enumerated(EnumType.STRING)
    @Column
    private State state;

	@Column(name = "comments")
	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;

	@ManyToOne
    @JoinColumn(name = "writer", updatable = false)
	private IntAdminUser writer;

	@ManyToOne
    @JoinColumn(name = "rewriter", insertable = false)
	private IntAdminUser rewriter;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_at", insertable = false)
	private Date updateAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPw() {
        return userPw;
    }

    public void setUserPw(String userPw) {
        this.userPw = userPw;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserTel() {
        return userTel;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel;
    }

    public AdminType getType() {
        return type;
    }

    public void setType(AdminType type) {
        this.type = type;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public IntAdminUser getWriter() {
        return writer;
    }

    public void setWriter(IntAdminUser writer) {
        this.writer = writer;
    }

    public IntAdminUser getRewriter() {
        return rewriter;
    }

    public void setRewriter(IntAdminUser rewriter) {
        this.rewriter = rewriter;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    /* 통합관리자 유무 */
    public boolean isSuperAdmin() {
        return getType() == AdminType.integration;
	}

    /* 권한 체크 */
	public boolean hasPermission(String role) {
        if (this.isSuperAdmin()) {
            return true;
        }
        return null != this.roles && Arrays.asList(this.roles).contains(role);
	}

	/* 담당국가 정보 리턴 */
	public List<Language> getLanguageInfo() {
	    if (getRoles() == null) {
	        return null;
	    }
	    List<Language> result = Lists.newArrayList();
	    List<AdminRoles> currentRoles = AdminRoles.toList(getRoles());
	    if (CollectionUtils.containsAny(AdminRoles.enGroup, currentRoles)) {
	        result.add(Language.en);
	    }
	    if (CollectionUtils.containsAny(AdminRoles.cnGroup, currentRoles)) {
            result.add(Language.cn);
        }
	    if (CollectionUtils.containsAny(AdminRoles.ruGroup, currentRoles)) {
            result.add(Language.ru);
        }
	    return result;
	}
	
    public List<AdminRoles> getAdminRoles() {
        if (getRoles() != null) {
            return AdminRoles.toList(getRoles());    
        }
        return null;
    }
}
