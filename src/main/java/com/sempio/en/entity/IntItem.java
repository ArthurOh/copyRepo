package com.sempio.en.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;

@Entity
@Table(name = "int_item")
public class IntItem {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_item")
    @SequenceGenerator(name = "seq_int_item", sequenceName = "int_item_id_seq", allocationSize = 1)
    private Integer id;

    @ManyToOne
    @JoinColumn(name="int_brand_category_id")
    private IntBrandCategory intBrandCategory;

    @Column
    private String title;

    @Column(name="point_msg")
    private String pointMsg;

    @Column
    private String wrap;

    @Column(name="list_img_path")
    private String listImgPath;

    @Column(name="view_img_path")
    private String viewImgPath;

    @Column(name="recommend_recipe")
    private String recommendRecipe;

    @Column
    private String feature;

    @Column
    private String tags;

    @Column
    private String ingredient;

    @Column(name="allergy_info")
    private String allergyInfo;

    @Column(name="best_by")
    private String bestBy;

    @Column
    private String certification;

    @Column(name="serving_size")
    private String servingSize;

    @Column(name="serving_amount")
    private String servingAmount;

    @Column
    private String fat;

    @Column
    private String cholesterol;

    @Column
    private String sodium;

    @Column
    private String carbohydrate;

    @Column
    private String protein;

    @Column
    private Integer ordering;

    @Column
    @Enumerated(EnumType.STRING)
    private State state;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "publish_at")
    private Date publishAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_at")
    private Date createAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_at")
    private Date updateAt;

    @Column
    private String writer;

    @Column
    private String rewriter;

    @Column
    @Enumerated(EnumType.STRING)
    private Language language;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IntBrandCategory getIntBrandCategory() {
        return intBrandCategory;
    }

    public void setIntBrandCategory(IntBrandCategory intBrandCategory) {
        this.intBrandCategory = intBrandCategory;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPointMsg() {
        return pointMsg;
    }

    public void setPointMsg(String pointMsg) {
        this.pointMsg = pointMsg;
    }

    public String getWrap() {
        return wrap;
    }

    public void setWrap(String wrap) {
        this.wrap = wrap;
    }

    public String getListImgPath() {
        return listImgPath;
    }

    public void setListImgPath(String listImgPath) {
        this.listImgPath = listImgPath;
    }

    public String getViewImgPath() {
        return viewImgPath;
    }

    public void setViewImgPath(String viewImgPath) {
        this.viewImgPath = viewImgPath;
    }

    public String getRecommendRecipe() {
        return recommendRecipe;
    }

    public void setRecommendRecipe(String recommendRecipe) {
        this.recommendRecipe = recommendRecipe;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public String getAllergyInfo() {
        return allergyInfo;
    }

    public void setAllergyInfo(String allergyInfo) {
        this.allergyInfo = allergyInfo;
    }

    public String getBestBy() {
        return bestBy;
    }

    public void setBestBy(String bestBy) {
        this.bestBy = bestBy;
    }

    public String getCertification() {
        return certification;
    }

    public void setCertification(String certification) {
        this.certification = certification;
    }

    public String getServingSize() {
        return servingSize;
    }

    public void setServingSize(String servingSize) {
        this.servingSize = servingSize;
    }

    public String getServingAmount() {
        return servingAmount;
    }

    public void setServingAmount(String servingAmount) {
        this.servingAmount = servingAmount;
    }

    public String getFat() {
        return fat;
    }

    public void setFat(String fat) {
        this.fat = fat;
    }

    public String getCholesterol() {
        return cholesterol;
    }

    public void setCholesterol(String cholesterol) {
        this.cholesterol = cholesterol;
    }

    public String getSodium() {
        return sodium;
    }

    public void setSodium(String sodium) {
        this.sodium = sodium;
    }

    public String getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(String carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    public String getProtein() {
        return protein;
    }

    public void setProtein(String protein) {
        this.protein = protein;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getPublishAt() {
        return publishAt;
    }

    public void setPublishAt(Date publishAt) {
        this.publishAt = publishAt;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getRewriter() {
        return rewriter;
    }

    public void setRewriter(String rewriter) {
        this.rewriter = rewriter;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
