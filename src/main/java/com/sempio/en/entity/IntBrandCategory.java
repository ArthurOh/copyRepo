package com.sempio.en.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;

@Entity
@Table(name = "int_brand_category")
public class IntBrandCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_brand_category")
    @SequenceGenerator(name = "seq_int_brand_category", sequenceName = "int_brand_category_id_seq", allocationSize = 1)
    private Integer id;

    @ManyToOne
    @JoinColumn(name="int_brand_id")
    private IntBrand intBrand;

    @Column
    private String code;

    @Column
    private String title;

    @Column
    private String description;

    @Column
    private Integer ordering;

    @Column
    @Enumerated(EnumType.STRING)
    private State state;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_at")
    private Date createAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_at")
    private Date updateAt;

    @Column
    private String writer;

    @Column
    private String rewriter;

    @Column
    @Enumerated(EnumType.STRING)
    private Language language;

    @OneToMany
    @JoinColumn(name = "int_brand_category_id")
    private List<IntItem> intItemList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IntBrand getIntBrand() {
        return intBrand;
    }

    public void setIntBrand(IntBrand intBrand) {
        this.intBrand = intBrand;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getRewriter() {
        return rewriter;
    }

    public void setRewriter(String rewriter) {
        this.rewriter = rewriter;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public List<IntItem> getIntItemList() {
        return intItemList;
    }

    public void setIntItemList(List<IntItem> intItemList) {
        this.intItemList = intItemList;
    }
}
