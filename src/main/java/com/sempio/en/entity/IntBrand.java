package com.sempio.en.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OrderBy;
import org.hibernate.annotations.Type;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;

@Entity
@Table(name = "int_brand")
public class IntBrand {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_brand")
    @SequenceGenerator(name = "seq_int_brand", sequenceName = "int_brand_id_seq", allocationSize = 1)
    private Integer id;

    @Column
    private String code;

    @Column
    private String title;

    @Column
    private String description;

    @Column
    private String slogan;

    @Column
    private String point;

    @Column(name="logo_img_path")
    private String logoImgPath;

    @Column(name="pc_img_path")
    private String pcImgPath;

    @Column(name="mobile_img_path")
    private String mobileImgPath;

    @Column(name="banner_img_path")
    private String bannerImgPath;

    @Column(name = "relation_site_name")
    @Type(type = "com.iropke.common.hibernate.ArrayType")
    private String[] relationSiteName;

    @Column(name = "relation_site_url")
    @Type(type = "com.iropke.common.hibernate.ArrayType")
    private String[] relationSiteUrl;

    @Column
    @Enumerated(EnumType.STRING)
    private State state;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "publish_at")
    private Date publishAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_at")
    private Date createAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_at")
    private Date updateAt;

    @Column
    private String writer;

    @Column
    private String rewriter;

    @Column
    @Enumerated(EnumType.STRING)
    private Language language;

    @OneToMany
    @OrderBy(clause = "ordering asc")
    @JoinColumn(name = "int_brand_id")
    private List<IntBrandCategory> intBrandCategoryList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getLogoImgPath() {
        return logoImgPath;
    }

    public void setLogoImgPath(String logoImgPath) {
        this.logoImgPath = logoImgPath;
    }

    public String getPcImgPath() {
        return pcImgPath;
    }

    public void setPcImgPath(String pcImgPath) {
        this.pcImgPath = pcImgPath;
    }

    public String getMobileImgPath() {
        return mobileImgPath;
    }

    public void setMobileImgPath(String mobileImgPath) {
        this.mobileImgPath = mobileImgPath;
    }

    public String getBannerImgPath() {
        return bannerImgPath;
    }

    public void setBannerImgPath(String bannerImgPath) {
        this.bannerImgPath = bannerImgPath;
    }

    public String[] getRelationSiteName() {
        return relationSiteName;
    }

    public void setRelationSiteName(String[] relationSiteName) {
        this.relationSiteName = relationSiteName;
    }

    public String[] getRelationSiteUrl() {
        return relationSiteUrl;
    }

    public void setRelationSiteUrl(String[] relationSiteUrl) {
        this.relationSiteUrl = relationSiteUrl;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getPublishAt() {
        return publishAt;
    }

    public void setPublishAt(Date publishAt) {
        this.publishAt = publishAt;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getRewriter() {
        return rewriter;
    }

    public void setRewriter(String rewriter) {
        this.rewriter = rewriter;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public List<IntBrandCategory> getIntBrandCategoryList() {
        return intBrandCategoryList;
    }

    public void setIntBrandCategoryList(List<IntBrandCategory> intBrandCategoryList) {
        this.intBrandCategoryList = intBrandCategoryList;
    }
}