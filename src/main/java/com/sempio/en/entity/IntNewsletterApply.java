package com.sempio.en.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;

@Entity
@Table(name = "int_newsletter_apply")
public class IntNewsletterApply {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_newsletter_apply")
    @SequenceGenerator(name = "seq_int_newsletter_apply", sequenceName = "int_newsletter_apply_id_seq", allocationSize = 1)
    private int id;

    @Column
    private String name;

    @Column
    private String email;

    @Column
    private String country;

    @Column(name = "apply_language")
    @Type(type = "com.iropke.common.hibernate.ArrayType")
    private String[] applyLanguage;

    @Column
    @Enumerated(EnumType.STRING)
    private State state;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_at", insertable = false, updatable = false)
    private Date createAt;

    @Column
    @Enumerated(EnumType.STRING)
    private Language language;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String[] getApplyLanguage() {
        return applyLanguage;
    }

    public void setApplyLanguage(String[] applyLanguage) {
        this.applyLanguage = applyLanguage;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

}
