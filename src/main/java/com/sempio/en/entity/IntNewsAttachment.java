package com.sempio.en.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "int_news_attachment")
public class IntNewsAttachment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_news_attachment")
    @SequenceGenerator(name = "seq_int_news_attachment", sequenceName = "int_news_attachment_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "original_file_name")
    private String originalFileName;

    @Column
    private String path;

    @ManyToOne
    @JoinColumn(name = "int_news_id")
    private IntNews intNews;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public IntNews getIntNews() {
        return intNews;
    }

    public void setIntNews(IntNews intNews) {
        this.intNews = intNews;
    }
}
