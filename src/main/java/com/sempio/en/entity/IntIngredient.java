package com.sempio.en.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.sempio.en.code.Language;

@Entity
@Table(name = "int_ingredient")
public class IntIngredient {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_ingredient")
    @SequenceGenerator(name = "seq_int_ingredient", sequenceName = "int_ingredient_id_seq", allocationSize = 1)
    private Integer id;

    @ManyToOne
    @JoinColumn(name="int_cookery_id")
    private IntCookery intCookery;

    @Column
    private String type;

    @ManyToOne
    @JoinColumn(name="int_item_id")
    private IntItem IntItem;

    @Column
    private String volume;

    @Column
    private String title;

    @ManyToOne
    @JoinColumn(name="int_recipe_id")
    private IntRecipe intRecipe;

    @Column
    @Enumerated(EnumType.STRING)
    private Language language;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IntCookery getIntCookery() {
        return intCookery;
    }

    public void setIntCookery(IntCookery intCookery) {
        this.intCookery = intCookery;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public IntRecipe getIntRecipe() {
        return intRecipe;
    }

    public void setIntRecipe(IntRecipe intRecipe) {
        this.intRecipe = intRecipe;
    }

    public IntItem getIntItem() {
        return IntItem;
    }

    public void setIntItem(IntItem intItem) {
        IntItem = intItem;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

}
