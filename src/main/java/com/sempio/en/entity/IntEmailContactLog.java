package com.sempio.en.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;

@Entity
@Table(name = "int_email_contact_log")
public class IntEmailContactLog {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_email_contact_log")
    @SequenceGenerator(name = "seq_int_email_contact_log", sequenceName = "int_email_contact_log_id_seq", allocationSize = 1)
    private int id;

    @Column
    private String subject;

    @Column
    private String comment;

    @Column
    private String file1;
   
    @Column
    private String file2;
    
    @Column
    private String file3;
   
    @Column
    private String file4;
    
    @Column
    private String file5;

    @Column
    private String name;

    @Column
    private String email;

    @Column
    private String country;
    @Temporal(TemporalType.TIMESTAMP)

    @Column(name = "create_at", insertable = false, updatable = false)
    private Date createAt;

    @Column
    @Enumerated(EnumType.STRING)
    private State state;

    @Column
    @Enumerated(EnumType.STRING)
    private Language language;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getFile1() {
        return file1;
    }

    public void setFile1(String file1) {
        this.file1 = file1;
    }

    public String getFile2() {
        return file2;
    }

    public void setFile2(String file2) {
        this.file2 = file2;
    }

    public String getFile3() {
        return file3;
    }

    public void setFile3(String file3) {
        this.file3 = file3;
    }

    public String getFile4() {
        return file4;
    }

    public void setFile4(String file4) {
        this.file4 = file4;
    }

    public String getFile5() {
        return file5;
    }

    public void setFile5(String file5) {
        this.file5 = file5;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
