package com.sempio.en.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.code.VisualType;
import com.sempio.en.code.VisualViewType;

@Entity
@Table(name = "int_visual_contents")
public class IntVisualContents {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_visual_contents")
    @SequenceGenerator(name = "seq_int_visual_contents", sequenceName = "int_visual_contents_id_seq", allocationSize = 1)
    private int id;

    @Column
    @Enumerated(EnumType.STRING)
    private VisualType type;

    @Column(name = "view_type")
    @Enumerated(EnumType.STRING)
    private VisualViewType viewType;

    @Column
    private String title;

    @Column(name = "sub_title")
    private String subTitle;

    @Column
    private String contents;

    @Column
    private String image;

    @Column(name = "m_image")
    private String mImage;

    @Column
    private String url;

    @Column(name = "video_description")
    private String videoDescription;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "publish_start_at")
    private Date publishStartAt;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "publish_end_at")
    private Date publishEndAt;

    @Column
    private int ordering;

    @Column
    @Enumerated(EnumType.STRING)
    private State state;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_at", insertable = false, updatable = false)
    private Date createAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_at", insertable = false)
    private Date updateAt;

    @Column
    private String writer;

    @Column
    private String rewriter;

    @Column
    @Enumerated(EnumType.STRING)
    private Language language;
    
    @Column(insertable = false)
    private int views;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public VisualType getType() {
        return type;
    }

    public void setType(VisualType type) {
        this.type = type;
    }

    public VisualViewType getViewType() {
        return viewType;
    }

    public void setViewType(VisualViewType viewType) {
        this.viewType = viewType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getmImage() {
        return mImage;
    }

    public void setmImage(String mImage) {
        this.mImage = mImage;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public Date getPublishStartAt() {
        return publishStartAt;
    }

    public void setPublishStartAt(Date publishStartAt) {
        this.publishStartAt = publishStartAt;
    }

    public Date getPublishEndAt() {
        return publishEndAt;
    }

    public void setPublishEndAt(Date publishEndAt) {
        this.publishEndAt = publishEndAt;
    }

    public int getOrdering() {
        return ordering;
    }

    public void setOrdering(int ordering) {
        this.ordering = ordering;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getRewriter() {
        return rewriter;
    }

    public void setRewriter(String rewriter) {
        this.rewriter = rewriter;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }
}
