package com.sempio.en.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "int_recipe")
public class IntRecipe {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_recipe")
    @SequenceGenerator(name = "seq_int_recipe", sequenceName = "int_recipe_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "int_recipe_theme_id")
    @Type(type = "com.iropke.common.hibernate.ArrayType", parameters = { @Parameter(name = "baseType", value = "Integer") })
    private Integer[] intRecipeTheme;

    @Column
    private String title;

    @Column(name = "title_sub")
    private String titleSub;

    @Column(updatable = false)
    private Integer views;

    @Column
    private String chef;

    @Column(name = "main_ingredient")
    private String mainIngredient;

    @Column
    private String serving;

    @Column(name = "view_img_path")
    private String viewImgPath;

    @Column
    private String description;

    @Column
    private String tip;

    @Column
    @Type(type = "com.iropke.common.hibernate.ArrayType", parameters = {@Parameter(name = "baseType", value = "Integer") })
    private Integer[] recommend;

    @Column
    private String tags;

    @Column
    private String state;

    @Column(name = "publish_at")
    private Date publishAt;

    @Column(name = "create_at", insertable = false, updatable = false)
    private Date createAt;

    @Column(name = "update_at", insertable = false)
    private Date updateAt;

    @Column(updatable = false)
    private String writer;

    @Column(insertable = false)
    private String rewriter;

    @Column(updatable = false)
    private String language;

    @OneToMany
    @OrderBy(clause = "id asc")
    @JoinColumn(name = "int_recipe_id", insertable = false, updatable = false)
    private List<IntCookery> intCookeryList;

    @OneToMany
    @JoinColumn(name = "int_recipe_id", insertable = false, updatable = false)
    private List<IntIngredient> intIngredientList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer[] getIntRecipeTheme() {
        return intRecipeTheme;
    }

    public void setIntRecipeTheme(Integer[] intRecipeTheme) {
        this.intRecipeTheme = intRecipeTheme;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleSub() {
        return titleSub;
    }

    public void setTitleSub(String titleSub) {
        this.titleSub = titleSub;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public String getChef() {
        return chef;
    }

    public void setChef(String chef) {
        this.chef = chef;
    }

    public String getMainIngredient() {
        return mainIngredient;
    }

    public void setMainIngredient(String mainIngredient) {
        this.mainIngredient = mainIngredient;
    }

    public String getServing() {
        return serving;
    }

    public void setServing(String serving) {
        this.serving = serving;
    }

    public String getViewImgPath() {
        return viewImgPath;
    }

    public void setViewImgPath(String viewImgPath) {
        this.viewImgPath = viewImgPath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Integer[] getRecommend() {
        return recommend;
    }

    public void setRecommend(Integer[] recommend) {
        this.recommend = recommend;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getPublishAt() {
        return publishAt;
    }

    public void setPublishAt(Date publishAt) {
        this.publishAt = publishAt;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAT) {
        this.updateAt = updateAT;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getRewriter() {
        return rewriter;
    }

    public void setRewriter(String rewriter) {
        this.rewriter = rewriter;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<IntCookery> getIntCookeryList() {
        return intCookeryList;
    }

    public void setIntCookeryList(List<IntCookery> intCookeryList) {
        this.intCookeryList = intCookeryList;
    }

    public List<IntIngredient> getIntIngredientList() {
        return intIngredientList;
    }

    public void setIntIngredientList(List<IntIngredient> intIngredientList) {
        this.intIngredientList = intIngredientList;
    }

}
