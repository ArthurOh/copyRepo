package com.sempio.en.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;

@Entity
@Table(name = "int_news")
public class IntNews {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_news")
    @SequenceGenerator(name = "seq_int_news", sequenceName = "int_news_id_seq", allocationSize = 1)
    private Integer id;

    @Column
    private String title;

    @Column
    private String contents;

    @Column
    private int views;

    @Column
    private String tags;

    @Column(name="list_img_path")
    private String listImgPath;

    @Column
    @Enumerated(EnumType.STRING)
    private State state;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "publish_at")
    private Date publishAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_at")
    private Date createAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_at")
    private Date updateAt;

    @Column
    private String writer;

    @Column
    private String rewriter;

    @Column
    @Enumerated(EnumType.STRING)
    private Language language;

    @OneToMany
    @JoinColumn(name = "int_news_id")
    private List<IntNewsAttachment> intNewsattachments;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getListImgPath() {
        return listImgPath;
    }

    public void setListImgPath(String listImgPath) {
        this.listImgPath = listImgPath;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getPublishAt() {
        return publishAt;
    }

    public void setPublishAt(Date publishAt) {
        this.publishAt = publishAt;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getRewriter() {
        return rewriter;
    }

    public void setRewriter(String rewriter) {
        this.rewriter = rewriter;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public List<IntNewsAttachment> getIntNewsattachments() {
        return intNewsattachments;
    }

    public void setIntNewsattachments(List<IntNewsAttachment> intNewsattachments) {
        this.intNewsattachments = intNewsattachments;
    }

}
