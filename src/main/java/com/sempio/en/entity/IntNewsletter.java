package com.sempio.en.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;

@Entity
@Table(name = "int_newsletter")
public class IntNewsletter {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_newsletter")
    @SequenceGenerator(name = "seq_int_newsletter", sequenceName = "int_newsletter_id_seq", allocationSize = 1)
    private int id;

    @Column(name = "publish_year")
    private String publishYear;

    @Column(name = "publish_month")
    private String publishMonth;

    @Column(name = "publish_no")
    private String publishNo;

    @Column
    private int views;

    @Column(name = "title_int")
    private String titleInt;

    @Column
    private String title;

    @Column(name = "content_int")
    private String contentInt;

    @Column
    private String content;

    @Column
    private String image;

    @Column
    @Enumerated(EnumType.STRING)
    private Language language;

    @Column
    @Enumerated(EnumType.STRING)
    private State state;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "publish_at")
    private Date publishAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_at", insertable = false, updatable = false)
    private Date createAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_at", insertable = false)
    private Date updateAt;

    @ManyToOne
    @JoinColumn(name = "writer", updatable = false)
    private IntAdminUser writer;

    @ManyToOne
    @JoinColumn(name = "rewriter", insertable = false)
    private IntAdminUser rewriter;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(String publishYear) {
        this.publishYear = publishYear;
    }

    public String getPublishMonth() {
        return publishMonth;
    }

    public void setPublishMonth(String publishMonth) {
        this.publishMonth = publishMonth;
    }

    public String getPublishNo() {
        return publishNo;
    }

    public void setPublishNo(String publishNo) {
        this.publishNo = publishNo;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public String getTitleInt() {
        return titleInt;
    }

    public void setTitleInt(String titleInt) {
        this.titleInt = titleInt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContentInt() {
        return contentInt;
    }

    public void setContentInt(String contentInt) {
        this.contentInt = contentInt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getPublishAt() {
        return publishAt;
    }

    public void setPublishAt(Date publishAt) {
        this.publishAt = publishAt;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public IntAdminUser getWriter() {
        return writer;
    }

    public void setWriter(IntAdminUser writer) {
        this.writer = writer;
    }

    public IntAdminUser getRewriter() {
        return rewriter;
    }

    public void setRewriter(IntAdminUser rewriter) {
        this.rewriter = rewriter;
    }
}
