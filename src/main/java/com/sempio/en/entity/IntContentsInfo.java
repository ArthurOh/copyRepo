package com.sempio.en.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sempio.en.code.ContentsType;
import com.sempio.en.code.Language;

@Entity
@Table(name = "int_contents_info")
public class IntContentsInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_contents_info")
    @SequenceGenerator(name = "seq_int_contents_info", sequenceName = "int_contents_info_id_seq", allocationSize = 1)
    private int id;

    @Column
    private String contents;

    @Column
    @Enumerated(EnumType.STRING)
    private ContentsType type;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_at", insertable = false, updatable = false)
    private Date createAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_at", insertable = false)
    private Date updateAt;

    @ManyToOne
    @JoinColumn(name = "writer", updatable = false)
    private IntAdminUser writer;

    @ManyToOne
    @JoinColumn(name = "rewriter", insertable = false)
    private IntAdminUser rewriter;

    @Column
    @Enumerated(EnumType.STRING)
    private Language language;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public ContentsType getType() {
        return type;
    }

    public void setType(ContentsType type) {
        this.type = type;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public IntAdminUser getWriter() {
        return writer;
    }

    public void setWriter(IntAdminUser writer) {
        this.writer = writer;
    }

    public IntAdminUser getRewriter() {
        return rewriter;
    }

    public void setRewriter(IntAdminUser rewriter) {
        this.rewriter = rewriter;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
