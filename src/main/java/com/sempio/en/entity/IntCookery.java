package com.sempio.en.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;
import org.hibernate.annotations.Type;

import com.sempio.en.code.Language;

@Entity
@Table(name = "int_cookery")
public class IntCookery {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_int_cookery")
    @SequenceGenerator(name = "seq_int_cookery", sequenceName = "int_cookery_id_seq", allocationSize = 1)
    private Integer id;

    @ManyToOne
    @JoinColumn(name="int_recipe_id", updatable = false)
    private IntRecipe intRecipe;

    @Column
    private String title;

    @Column
    @Type(type = "com.iropke.common.hibernate.ArrayType")
    private String[] directions;

    @Column
    private Integer ordering;

    @Column
    @Enumerated(EnumType.STRING)
    private Language language;

    @OneToMany
    @OrderBy(clause = "id asc")
    @JoinColumn(name = "int_cookery_id")
    private List<IntIngredient> intIngredientList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IntRecipe getIntRecipe() {
        return intRecipe;
    }

    public void setIntRecipe(IntRecipe intRecipe) {
        this.intRecipe = intRecipe;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getDirections() {
        return directions;
    }

    public void setDirections(String[] directions) {
        this.directions = directions;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public List<IntIngredient> getIntIngredientList() {
        return intIngredientList;
    }

    public void setIntIngredientList(List<IntIngredient> intIngredientList) {
        this.intIngredientList = intIngredientList;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

}
