package com.sempio.en.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sempio.en.code.Language;
import com.sempio.en.code.Result;
import com.sempio.en.code.State;
import com.sempio.en.entity.IntNewsletter;
import com.sempio.en.entity.IntNewsletterApply;
import com.sempio.en.service.IntNewsletterService;
import com.sempio.en.util.Pager;
import com.sempio.en.util.Paging;
import com.sempio.en.util.Paging.Builder;
import com.sempio.en.util.ViewMessage;

@RequestMapping(value = "/newsletter")
@Controller
public class NewsletterController {

    @Autowired
    private IntNewsletterService newsletterService;

    @RequestMapping(value = "/list")
    public String list(@RequestParam(value = "size", required = false, defaultValue = "6") int size,
                       RedirectAttributes attrs,
                       @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                       Model model) {
        return list(page, size, attrs, model);
    }

    @RequestMapping(value = "/list/{page}")
    public String list(@PathVariable int page,
                       @RequestParam(value = "size", required = false, defaultValue = "6") int size,
                       RedirectAttributes attrs,
                       Model model) {
        Pager pager = new Pager(page, size);

        List<IntNewsletter> list = newsletterService.list(Language.en, pager);
        int count = newsletterService.count(Language.en);

        Builder builder = Paging.builder();
        Paging paging = builder.currentPage(page)
               .entriesPerPage(size)
               .totalCount(count)
               .link("/newsletter/list").build();

        model.addAttribute("list", list);
        model.addAttribute("count", count);
        model.addAttribute("paging", paging);
        return "newsletter/list";
    }

    @RequestMapping(value = "/view/{id}")
    public String view(@PathVariable int id,
                       @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                       RedirectAttributes attrs,
                       Model model) {
        Result<IntNewsletter> result = newsletterService.select(id);
        if (result.isError()) {
            ViewMessage.error().message("Incorrect access").register(attrs);
            return "redirect:/newsletter/list";
        }
        
        newsletterService.updateViews(id);
        
        model.addAttribute("item", result.payload());
        model.addAttribute("page", page);
        return "newsletter/view";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register() {
        return "newsletter/register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(IntNewsletterApply form, RedirectAttributes attrs) {
        form.setLanguage(Language.en);
        form.setState(State.on);
        newsletterService.insertApply(form);
        ViewMessage.success().message("application is complete").register(attrs);
        return "redirect:/newsletter/register";
    }
}
