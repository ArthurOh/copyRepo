package com.sempio.en.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sempio.en.code.Language;
import com.sempio.en.entity.IntNews;
import com.sempio.en.entity.IntVisualContents;
import com.sempio.en.service.IntNewsService;
import com.sempio.en.service.IntVisualContentsService;

@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    private IntVisualContentsService visualContentsService;

    @Autowired
    private IntNewsService intNewsService;

    @RequestMapping("")
    public String home(Model model) {

        // 팝업조회 1개
        List<IntVisualContents> popupList = visualContentsService.selectPopupList(Language.en, null, null, true);
        
        // 상단 배너 0~5개 조회
        List<IntVisualContents> topList = visualContentsService.selectTopList(Language.en, true);

        // 가운데 뉴스 롤링
        List<IntNews> newsList = intNewsService.selectMainList(Language.en);

        // 하단 배너 3개 조회
        List<IntVisualContents> bottomList = visualContentsService.selectBottomList(Language.en, true);

        model.addAttribute("topList", topList);
        model.addAttribute("newsList", newsList);
        model.addAttribute("bottomList", bottomList);
        model.addAttribute("popupList", popupList);
        return "/index";
    }
}
