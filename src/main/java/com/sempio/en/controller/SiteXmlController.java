package com.sempio.en.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.entity.IntBrand;
import com.sempio.en.entity.IntBrandCategory;
import com.sempio.en.entity.IntItem;
import com.sempio.en.entity.IntNews;
import com.sempio.en.entity.IntNewsletter;
import com.sempio.en.entity.IntRecipe;
import com.sempio.en.entity.IntRecipeTheme;
import com.sempio.en.service.IntBrandService;
import com.sempio.en.service.IntNewsService;
import com.sempio.en.service.IntNewsletterService;
import com.sempio.en.service.IntRecipeService;
import com.sempio.en.service.IntRecipeThemeService;
import com.sempio.en.util.BaseUtil;

@Controller
@RequestMapping("/misc")
public class SiteXmlController {
    private static Logger log = LoggerFactory.getLogger(SiteXmlController.class);

    @Autowired
    private IntBrandService intBrandService;

    @Autowired
    private IntNewsService intNewsService;

    @Autowired
    private IntNewsletterService intNewsletterService;

    @Autowired
    private IntRecipeService intRecipeService;

    @Autowired
    private IntRecipeThemeService intRecipeThemeService;

    @RequestMapping(value="/sitemap.xml")
    public void sitemapXml(HttpServletResponse response, Model model) throws IOException{
        response.setContentType("text/xml; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        out.println("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

        String mainUrl = "http://en.sempio.com";
        String subUrl = null;

        // all pages :
        // product (brand, item view),
        // recipes (index, theme, list, view),
        // company, contact us, news (list, view),
        // newsletter (list, join),
        // sitemap

        BaseUtil.makeXmlCode("", response);

        BaseUtil.makeXmlCode("/recipe", response);

        BaseUtil.makeXmlCode("/company/sempio", response);
        BaseUtil.makeXmlCode("/company/history", response);
        BaseUtil.makeXmlCode("/company/network", response);
        BaseUtil.makeXmlCode("/company/news", response);

        BaseUtil.makeXmlCode("/contact/info", response);
        BaseUtil.makeXmlCode("/contact/email", response);

        BaseUtil.makeXmlCode("/misc/sitemap", response);

        BaseUtil.makeXmlCode("/newsletter/list", response);
        BaseUtil.makeXmlCode("/newsletter/register", response);
        // html page en

        // product - brand & item
        String itemUrl = null;
        List<IntBrand> intBrand = intBrandService.listForSitemap();
        for (IntBrand brandList : intBrand) {
            subUrl = "/product/" + brandList.getCode();

            // brand page
            out.println("<url>");

            out.print("<loc>");
            out.print(mainUrl);
            out.print(subUrl);
            out.println("</loc>");

            out.print("<lastmod>");
            out.print(sdf.format(brandList.getPublishAt()));
            out.println("</lastmod>");

            out.println("<changefreq>daily</changefreq>");
            out.println("</url>");

            // item page
            itemUrl = "/product/" + brandList.getCode() +"/view/";

            for (IntBrandCategory brandCategory : brandList.getIntBrandCategoryList()) {
                for (IntItem item : brandCategory.getIntItemList()) {
                    out.println("<url>");

                    out.print("<loc>");
                    out.print(mainUrl);
                    out.print(itemUrl + item.getId());
                    out.println("</loc>");

                    out.print("<lastmod>");
                    out.print(sdf.format(brandList.getPublishAt()));
                    out.println("</lastmod>");

                    out.println("<changefreq>daily</changefreq>");
                    out.println("</url>");
                }
            }
        }

        // recipe theme page & list page & view page
        // to-do : make recipe xml

        // /recipe/list/theme/{code}/{pageNum}
        // /recipe/list/theme/{code}/view/{id}
        List<IntRecipeTheme> intRecipeThemeList = intRecipeThemeService.list(Language.en, State.on);
        for(IntRecipeTheme item : intRecipeThemeList){
            subUrl = "/recipe/list/theme/" + item.getCode();
            out.println("<url>");

            out.print("<loc>");
            out.print(mainUrl);
            out.print(subUrl);
            out.println("</loc>");

            out.print("<lastmod>");
            out.print(sdf.format(item.getCreateAt()));
            out.println("</lastmod>");

            out.println("<changefreq>daily</changefreq>");
            out.println("</url>");

            List<IntRecipe> intRecipeList = intRecipeService.listForSitemap(Language.en, item);
            if(intRecipeList.size() > 0 && intRecipeList != null){
                for(IntRecipe intRecipe : intRecipeList){

                    itemUrl = "/view/" + intRecipe.getId();
                    out.println("<url>");

                    out.print("<loc>");
                    out.print(mainUrl);
                    out.print(subUrl + itemUrl);
                    out.println("</loc>");

                    out.print("<lastmod>");
                    out.print(sdf.format(intRecipe.getPublishAt()));
                    out.println("</lastmod>");

                    out.println("<changefreq>daily</changefreq>");
                    out.println("</url>");
                }

            }
        }

        // /recipe/list/product/{code}/{pageNum}
        // /recipe/list/product/{code}/view/{id}
        for(IntBrand item : intBrand){
            subUrl = "/recipe/list/theme/" + item.getCode();
            out.println("<url>");

            out.print("<loc>");
            out.print(mainUrl);
            out.print(subUrl);
            out.println("</loc>");

            out.print("<lastmod>");
            out.print(sdf.format(item.getPublishAt()));
            out.println("</lastmod>");

            out.println("<changefreq>daily</changefreq>");
            out.println("</url>");
            List<IntRecipe> intRecipeList = intRecipeService.listForSitemapWithBrand(Language.en, item);
            if(intRecipeList != null && intRecipeList.size() > 0 ){
                for(IntRecipe intRecipe : intRecipeList){
                    subUrl = "/recipe/list/theme/" + item.getCode();
                    itemUrl = "/view/" + intRecipe.getId();
                    out.println("<url>");

                    out.print("<loc>");
                    out.print(mainUrl);
                    out.print(subUrl + itemUrl);
                    out.println("</loc>");

                    out.print("<lastmod>");
                    out.print(sdf.format(intRecipe.getPublishAt()));
                    out.println("</lastmod>");

                    out.println("<changefreq>daily</changefreq>");
                    out.println("</url>");
                }
            }
        }

        // news view page
        List<IntNews> intNews = intNewsService.listForSitemap();
        for (IntNews newsList : intNews) {
            subUrl = "/news/view/" + newsList.getId();
            out.println("<url>");

            out.print("<loc>");
            out.print(mainUrl);
            out.print(subUrl);
            out.println("</loc>");

            out.print("<lastmod>");
            out.print(sdf.format(newsList.getPublishAt()));
            out.println("</lastmod>");

            out.println("<changefreq>daily</changefreq>");
            out.println("</url>");
        }

        // newsletter view page
        List<IntNewsletter> intNewsletter = intNewsletterService.listForSitemap();
        for (IntNewsletter newsletterList : intNewsletter) {
            subUrl = "/newsletter/view/" + newsletterList.getId();
            out.println("<url>");

            out.print("<loc>");
            out.print(mainUrl);
            out.print(subUrl);
            out.println("</loc>");

            out.print("<lastmod>");
            out.print(sdf.format(newsletterList.getPublishAt()));
            out.println("</lastmod>");

            out.println("<changefreq>daily</changefreq>");
            out.println("</url>");
        }
        out.println("</urlset>");

        response.flushBuffer();
    }
}
