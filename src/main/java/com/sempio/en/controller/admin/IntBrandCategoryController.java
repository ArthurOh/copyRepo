
package com.sempio.en.controller.admin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntBrand;
import com.sempio.en.entity.IntBrandCategory;
import com.sempio.en.entity.IntItem;
import com.sempio.en.interceptor.AdminRole;
import com.sempio.en.service.IntAdminUserService;
import com.sempio.en.service.IntBrandCategoryService;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.Logics;
import com.sempio.en.util.ViewMessage;

@Controller
@AdminRole({AdminRoles.en_product, AdminRoles.cn_product, AdminRoles.ru_product})
@RequestMapping("/admin/{language}/brand/category")
public class IntBrandCategoryController {
    private static Logger log = LoggerFactory.getLogger(IntBrandCategoryController.class);

    @Autowired
    private IntBrandCategoryService intBrandCategoryService;

    @Autowired
    private IntAdminUserService adminUserService;

    @RequestMapping(value = "/{id}")
    public String categoryList(@PathVariable("language") String language,
            @PathVariable("id") Integer brandId, RedirectAttributes attrs, Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        List<IntBrandCategory> brandCategory = intBrandCategoryService.listForAdmin(brandId);
        model.addAttribute("brandCategory", brandCategory);
        model.addAttribute("brandId", brandId);

        model.addAttribute("language", language);
        return "/admin/brand_category/list";
    }

    // 쓰기
    @RequestMapping(value = "/create/{id}")
    public String categoryCreate(@PathVariable("language") String language,
            @PathVariable("id") Integer brandId, RedirectAttributes attrs, Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        model.addAttribute("pageType", "create");
        model.addAttribute("brandId", brandId);
        model.addAttribute("language", language);

        return "/admin/brand_category/edit";
    }

    // 쓰기 등록
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String categoryCreateAction(@PathVariable("language") String language,
            @RequestParam(value = "brand_id") Integer brandId,
            @RequestParam(value = "brand_category_title") String title,
            @RequestParam(value = "description") String description,
            @RequestParam(value = "state") String state,
            HttpSession session, RedirectAttributes attrs, HttpServletRequest request) {

        String categoryCode = makeBrandCode("category");
        long now = System.currentTimeMillis();
        Date createAt = new Date(now);

        IntAdminUser admin = Logics.adminFromSession(session);
        String adminId =  admin.getUserId();

        IntBrandCategory brandCategory = new IntBrandCategory();
        brandCategory.setCode(categoryCode);
        brandCategory.setTitle(title);
        brandCategory.setDescription(description);
        brandCategory.setOrdering(0);
        brandCategory.setCreateAt(createAt);
        brandCategory.setWriter(adminId);
        brandCategory.setState(State.valueOf(state));
        brandCategory.setLanguage(Language.valueOf(language));

        IntBrand brand = new IntBrand();
        brand.setId(brandId);
        brandCategory.setIntBrand(brand);

        log.debug("input value = {}", brandCategory);

        intBrandCategoryService.create(brandCategory);

        ViewMessage.success().message("It is registered.").register(attrs);

        adminUserService.logAction(adminId, HttpUtils.ip(request), "("+ language +") create brand category: " + title + " / " + admin.getId());

        return "redirect:/admin/"+ language +"/brand/category/"+brandId;
    }

    // 수정
    @RequestMapping(value = "/edit/{brandId}/{id}", method = RequestMethod.GET)
    public String categoryEditView(@PathVariable("language") String language,
            @PathVariable("brandId") Integer brandId, @PathVariable("id") Integer id, RedirectAttributes attrs, Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        IntBrandCategory brandCategory = intBrandCategoryService.load(id);
        model.addAttribute("brandCategory", brandCategory);

        Integer itemCount = 0;
        for (IntItem item : brandCategory.getIntItemList()) {
            if (item.getState() != State.deleted) {
                itemCount = itemCount + 1;
            }
        }
        model.addAttribute("itemCount", itemCount);

        model.addAttribute("pageType", "edit");
        model.addAttribute("brandId", brandId);
        model.addAttribute("language", language);

        return "/admin/brand_category/edit";
    }

    // 수정 등록
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String categoryEditModify(@PathVariable("language") String language,
            @PathVariable("id") Integer id,
            @RequestParam(value = "brand_id") String brandId,
            @RequestParam(value = "brand_category_title") String title,
            @RequestParam(value = "description") String description,
            @RequestParam(value = "state") String state,
            HttpSession session, RedirectAttributes attrs, HttpServletRequest request) throws Exception {

        long now = System.currentTimeMillis();
        Date updateAt = new Date(now);

        IntAdminUser admin = Logics.adminFromSession(session);
        String adminId =  admin.getUserId();

        IntBrandCategory brandCategory = new IntBrandCategory();
        brandCategory.setId(id);
        brandCategory.setTitle(title);
        brandCategory.setDescription(description);
        brandCategory.setUpdateAt(updateAt);
        brandCategory.setRewriter(adminId);
        brandCategory.setState(State.valueOf(state));

        log.debug("input value = {}", brandCategory);

        intBrandCategoryService.updatePost(brandCategory);

        ViewMessage.success().message("Changes have been reflected.").register(attrs);

        adminUserService.logAction(adminId, HttpUtils.ip(request), "("+ language +") modify brand category : " + id + " / " + admin.getId());

        return "redirect:/admin/"+ language +"/brand/category/edit/" + brandId+ "/" + id ;
    }

    // 브랜드 카테고리 - 순서 변경
    @RequestMapping(value = "/orderNum", method = RequestMethod.POST)
    public String location(@PathVariable("language") String language,
            @RequestParam(value = "brand_id") Integer brandId,
            @RequestParam(name="ordersData", required = true) String ordersData, RedirectAttributes attrs,
            HttpSession session, HttpServletRequest request) throws Exception {

        IntAdminUser admin = Logics.adminFromSession(session);
        String adminId =  admin.getUserId();

        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper(factory);
        TypeReference<List<Map<String, Object>>> typeRef = new TypeReference<List<Map<String, Object>>>(){};
        List<Map<String, Object>> updateOrders = mapper.readValue(ordersData, typeRef);

        intBrandCategoryService.updateLocation(updateOrders, brandId);

        ViewMessage.success().message("The order is changed.").register(attrs);
        adminUserService.logAction(adminId, HttpUtils.ip(request), "("+ language +") modify brand category orders: " + brandId + " / " + admin.getId());

        return "redirect:/admin/"+ language +"/brand/category/"+brandId;
    }

    // 브랜드 코드 생성
    private String makeBrandCode(String type) {
        // 카테고리 코드 EX) SPBC201611160001 / SPBCI(Sem Pio Brand Category International) , 년월일, 번호순차증가
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String codeCreateAt = formatter.format(date);
        String baseCategoryCode = "SPBCI".concat(codeCreateAt.toString());

        String code = null;
        IntBrandCategory lastBrandCategory = intBrandCategoryService.getBrandCode();

        String lastCode, nextCode;
        Integer lastCodeNum, NextCodeNum;

        if (lastBrandCategory == null) {
            code = baseCategoryCode.concat("0001");    // 0001 부터 시작
        } else {
            lastCode = StringUtils.right(lastBrandCategory.getCode(), 4);
            lastCodeNum = Integer.parseInt(lastCode);
            NextCodeNum =  lastCodeNum + 1;
            nextCode = StringUtils.right("000" + NextCodeNum.toString(), 4);
            code = baseCategoryCode.concat(nextCode);
        }

        return code;
    }

    // 브랜드 카테고리 리스트
    @RequestMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> bcList(
            @PathVariable("language") String language,
            @RequestParam(value = "brand_id", required = false) Integer brandId,
            @RequestParam(value = "state", required = false) String state) {

        Map<String, Object> result = new HashMap<String, Object>();
        List<Map<String, Object>> brandCategoryList = new ArrayList<Map<String, Object>>();
        result.put("list", brandCategoryList);

        List<IntBrandCategory> list = null;
        list = intBrandCategoryService.bcList(brandId, state, Language.valueOf(language));

        for (IntBrandCategory bcList : list){
            Map<String, Object> BrandCategory = new HashMap<String, Object>();
            BrandCategory.put("id", bcList.getId());
            BrandCategory.put("brandCategoryTitle", bcList.getTitle());
            BrandCategory.put("state", bcList.getState());

            brandCategoryList.add(BrandCategory);
        }

        return result;
    }
}
