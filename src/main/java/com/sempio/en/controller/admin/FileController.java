package com.sempio.en.controller.admin;

import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sempio.en.entity.IntNewsAttachment;
import com.sempio.en.service.FileService;
import com.sempio.en.util.BaseUtil;
import com.sempio.en.util.Const;

@Controller
@RequestMapping({"/admin/fileuploads", "/fileuploads"})
public class FileController {
    private static final Logger log = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FileService fileService;

    @Value("${sempiosite.paths.uploadedFiles}")
    private String basedir;

    private final String fileResultFile = "\"filepath\":\"%s\"";
    private final String fileResultRename = "\"filerename\":\"%s\"";
    private final String fileResultOrgname = "\"fileorgname\":\"%s\"";
    private final String fileResultID = "\"fileid\":\"%s\"";
    private final String fileResultText = "{\"filenowrite\":\"%s\"}";

    // 이미지 세로사이즈도 사용할 경우 @RequestParam("imageHeight") int imageHeight 추가
    @RequestMapping(value = "/upload/images", method = RequestMethod.POST)
    @ResponseBody
    public void uploadImages(
    		@RequestParam(required = false) MultipartFile uploadfileImage,
            @RequestParam(value = "imageWidth", defaultValue = "0") int imageWidth,
            @RequestParam(value = "imageHeight", defaultValue = "0") int imageHeight,
            @RequestParam(value = "imageType", defaultValue = "no") String imageType,
            @RequestParam(value = "CKEditorFuncNum", required = false, defaultValue = "") String CKEditorFuncNum,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

    	// ckeditor 이미지 업로드를 위해 임시 처리...
    	if (uploadfileImage == null || uploadfileImage.isEmpty()) {
    		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
    		uploadfileImage = multipartRequest.getFile("upload");
    	}

        if (!uploadfileImage.isEmpty()) {
            try {
                String fileOrginalname = uploadfileImage.getOriginalFilename();
                String fileRename = getFileRename(fileOrginalname);

                String makeRandomDir1 = (BaseUtil.getRandomDir()).toString();
                String makeRandomDir2 = (BaseUtil.getRandomDir()).toString();

                String saveDirectory = Paths.get(basedir, Const.IMAGE_PREFIX_PATH, makeRandomDir1, makeRandomDir2).toString();
                String returnFilePath = Paths.get("/"+Const.IMAGE_PREFIX_PATH, makeRandomDir1, makeRandomDir2, fileRename).toString();

                String realFilepath = Paths.get(saveDirectory, fileRename).toString();

                makeFolder(saveDirectory);

                uploadFiles(realFilepath, uploadfileImage);

                // 이미지 가로x세로 크기 확인
                Image uploaded = new ImageIcon(realFilepath).getImage();
                int realImgWidth = uploaded.getWidth(null);
                int realImgHeight = uploaded.getHeight(null); //이미지 세로 크기도 사용할 경우 활성화

                // 사이즈가 안맞으면 이미지 resize
                Boolean resultResize = true;
                if (imageWidth > 0 && imageHeight > 0) {
                	if (realImgWidth != imageWidth || realImgHeight != imageHeight) {
                        try {
                            Thumbnails.of(new File(realFilepath)).size(imageWidth, imageHeight).keepAspectRatio(false).toFile(new File(realFilepath));
                            resultResize = true;
                        } catch (Exception e) {
                        	log.error("ERROR", e);
                            resultResize = false;
                            response.getWriter().write(String.format(fileResultText, "F"));
                        }
                    }
                }

                if (resultResize) {
                    int imageID = 0;
                    if (imageType.equals("otherImage")) {
                        imageID = fileService.createImage(returnFilePath);
                    }

                    // front ckeditor
                    if (StringUtils.isNoneBlank(CKEditorFuncNum)) {
                    	response.getWriter().write("<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction("+CKEditorFuncNum+", '"+returnFilePath.replace("\\", "\\\\")+"', '이미지가 업로드 되었습니다.');</script>");
                    	return;
                    }

                    String returnPath = (String.format(fileResultFile, returnFilePath.replace("\\", "\\\\"), "T"));
                    String returnRename = (String.format(fileResultRename, fileRename, "T"));
                    String returnImageID = (String.format(fileResultID, imageID, "T"));
                    String returnInfo = "{" + returnPath + "," + returnRename + "," + returnImageID + "}";

                    response.getWriter().write(returnInfo);
                }
            } catch (Exception e) {
                log.error("ERROR", e);
            }
        }
    }

    @RequestMapping(value = "/upload/files", method = RequestMethod.POST)
    @ResponseBody
    public void uploadFiles(@RequestParam MultipartFile uploadfileFile,
                            @RequestParam(value = "noInsert", required = false, defaultValue = "false") boolean noInsert,
                            HttpServletResponse response) {
        if (!uploadfileFile.isEmpty()) {
            try {
                String fileOrginalname = uploadfileFile.getOriginalFilename();
                String fileExtension = getFileIndexOf(fileOrginalname);
                String fileRename = getFileRename(fileOrginalname);

                String makeRandomDir1 = (BaseUtil.getRandomDir()).toString();
                String makeRandomDir2 = (BaseUtil.getRandomDir()).toString();

                String saveDirectory = Paths.get(basedir, Const.FILE_PREFIX_PATH, makeRandomDir1, makeRandomDir2).toString();
                String realFilepath = Paths.get(saveDirectory, fileRename).toString();
                String returnFilePath = Paths.get("/"+Const.FILE_PREFIX_PATH, makeRandomDir1, makeRandomDir2, fileRename).toString();

                makeFolder(saveDirectory);
                uploadFiles(realFilepath, uploadfileFile);

                int fileID = 0;
                if (!noInsert) {
                    fileID = fileService.createFile(fileOrginalname.toString(), returnFilePath, fileExtension.replace(".", "").toString(), null);
                }
                String returnPath = (String.format(fileResultFile, returnFilePath.replace("\\", "\\\\"), "T"));
                String returnRename = (String.format(fileResultRename, fileRename, "T"));
                String returnOrgname = (String.format(fileResultOrgname, fileOrginalname, "T"));
                String returnImageID = (String.format(fileResultID, fileID, "T"));
                String returnInfo = "{" + returnPath + "," + returnRename + "," + returnOrgname + "," + returnImageID + "}";
                response.getWriter().write(returnInfo);

            } catch (Exception e) {
                log.error("ERROR", e);
            }
        }
    }

    private String getFileIndexOf(String filename) {
        String fileExtension = filename.substring(filename.lastIndexOf("."));
        return fileExtension;
    }

    private String getFileRename(String filename) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String today = formatter.format(new Timestamp(new java.util.Date().getTime()));

        String fileExtension = getFileIndexOf(filename);
        String fileRename = today + UUID.randomUUID().toString() + fileExtension;

        return fileRename;
    }

    private void makeFolder(String directory) {
        File dFile = new File(directory);

        if (!dFile.exists() || !dFile.isDirectory()) {
            boolean result = dFile.mkdirs();
            log.debug("mkdir {} directory   : {{}}", directory, result);
        }
    }

    private void uploadFiles(String filepath, MultipartFile uploadfileFile) {
        try {
            uploadfileFile.transferTo(new File(filepath));
        } catch (Exception e) {
            log.error("ERROR", e);
        }
    }

    @RequestMapping(value = "/filedownload/{fileid}/{postid}")
    @ResponseBody
    public void getFile(@PathVariable("fileid") Integer fileid, @PathVariable("postid") Integer postid, HttpServletResponse response, HttpServletRequest request,
            RedirectAttributes attrs) throws UnsupportedEncodingException {
        IntNewsAttachment attachment = fileService.selectOneNewsAttachment(fileid);

        String fileName = attachment.getOriginalFileName();
        String path = attachment.getPath();
        Path realPath = Paths.get(basedir, path);

        String header = BaseUtil.getBrowser(request);
        if (header.contains("MSIE")) {
            String docName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-Disposition", "attachment;filename=" + docName + ";");
        } else {
            String docName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
        }

        response.setHeader("Content-Type", "application/octet-stream");
        response.setHeader("Content-Transfer-Encoding", "binary;");
        response.setHeader("Pragma", "no-cache;");
        response.setHeader("Expires", "-1;");

        try {
            FileCopyUtils.copy(new FileInputStream(realPath.toString()), response.getOutputStream());
            response.flushBuffer();
        } catch (Exception e) {
        	log.error("ERROR", e);
        }
    }

    @RequestMapping(value = "/fileremove/{filetype}", method = RequestMethod.POST)
    @ResponseBody
    public String editFileRemove(@PathVariable("filetype") String filetype, @RequestParam(value = "fileid") int fileid,
            @RequestParam(value = "postid") int postid, RedirectAttributes attrs) {
        String result = null;
        try {
            fileService.fileRemove(fileid, filetype);
            result = "T";
        } catch (Exception e) {
            result = "F";
            log.error("ERROR", e);
        }
        return String.format("{\"result\":\"%s\"}", result);
    }
}
