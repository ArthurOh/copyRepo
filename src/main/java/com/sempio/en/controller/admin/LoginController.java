package com.sempio.en.controller.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sempio.en.code.Result;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.service.IntAdminUserService;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.Logics;
import com.sempio.en.util.ViewMessage;

@Controller
@RequestMapping(value = "/admin")
public class LoginController {

    @Autowired
    private IntAdminUserService adminService;

    @Autowired
    private IntAdminUserService adminUserService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(@RequestParam(value = "r", required = false) String r, Model model) {
        if (StringUtils.isNotBlank(r)) {
            model.addAttribute("r", r);
        }
        return "admin/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@RequestParam("userId") String userId,
                        @RequestParam("userPw") String userPw,
                        @RequestParam(value = "r", required = false) String r,
                        HttpSession session,
                        RedirectAttributes attrs,
                        HttpServletRequest request) {
        if (StringUtils.isBlank(userId) || StringUtils.isBlank(userPw)) {
            ViewMessage.error().message("Please enter your username and password.").register(attrs);
            return "redirect:/admin/login?r=" + StringUtils.defaultString(r);
        }
        Result<IntAdminUser> result = adminService.authenticate(userId, userPw);
        if (result.isError()) {
            ViewMessage.error().message("Sorry, there is no such ID or password. Please check your ID or password.").register(attrs);
            return "redirect:/admin/login?r=" + StringUtils.defaultString(r);
        }
        Logics.adminToSession(result.payload(), session);
        adminUserService.logAction(userId, HttpUtils.ip(request), "login");

        return StringUtils.isBlank(r) ? "redirect:/admin" : "redirect:" + r;
    }

    @RequestMapping("/logout")
    public String logout(HttpSession session, RedirectAttributes attrs) {
        session.invalidate();
        ViewMessage.success().message("Signed out").register(attrs);
        return "redirect:/admin/login";
    }
}
