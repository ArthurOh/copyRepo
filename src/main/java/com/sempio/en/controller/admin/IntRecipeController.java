package com.sempio.en.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.Language;
import com.sempio.en.command.IntRecipeForm;
import com.sempio.en.command.IntRecipeSearchForm;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntRecipe;
import com.sempio.en.entity.IntRecipeTheme;
import com.sempio.en.interceptor.AdminRole;
import com.sempio.en.service.IntAdminUserService;
import com.sempio.en.service.IntRecipeService;
import com.sempio.en.util.Const;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.LanguageCheck;
import com.sempio.en.util.Logics;
import com.sempio.en.util.Pager;
import com.sempio.en.util.Paging;
import com.sempio.en.util.Paging.Builder;
import com.sempio.en.util.ViewMessage;

@Controller
@AdminRole({ AdminRoles.en_contents, AdminRoles.cn_contents, AdminRoles.ru_contents })
@RequestMapping("/admin/{language}/recipe")
public class IntRecipeController {
    private static Logger log = LoggerFactory.getLogger(IntRecipeController.class);

    @Autowired
    private IntRecipeService intRecipeService;

    @Autowired
    private IntAdminUserService adminUserService;

    @RequestMapping("/list")
    public String recipe(@PathVariable("language") String language,
                         @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                         IntRecipeSearchForm searchForm,
                         RedirectAttributes attrs,
                         Model model) {
        return recipeList(language, page, searchForm, attrs, model);
    }

    @RequestMapping("/list/{page}")
    public String recipeList(@PathVariable("language") String language,
                             @PathVariable("page") int page,
                             IntRecipeSearchForm searchForm,
                             RedirectAttributes attrs,
                             Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }
        Pager pager = new Pager(page, Const.ADMIN_LIST_SIZE);

        List<IntRecipe> intRecipeList = intRecipeService.getIntRecipeList(searchForm, pager, language);
        int count = intRecipeService.searchCount(searchForm, language);

        Builder builder = Paging.builder().currentPage(page)
                                .entriesPerPage(Const.ADMIN_LIST_SIZE)
                                .totalCount(count)
                                .link("/admin/" + language + "/recipe/list");

        model.addAttribute("paging", searchForm.addQueryParams(builder).build());
        model.addAttribute("intRecipeList", intRecipeList);
        model.addAttribute("searchForm", searchForm);
        model.addAttribute("language", language);
        return "/admin/recipe/list";
    }

    // 레시피 - 등록페이지
    @RequestMapping(value = "/create")
    public String recipeCreate(@PathVariable("language") String language,
                               IntRecipeSearchForm searchForm, RedirectAttributes attrs, Model model,
                               HttpSession session, HttpServletRequest request) {

        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        List<IntRecipeTheme> intRecipeThemeList = intRecipeService.getRecipeThemeAll(language);

        model.addAttribute("intRecipeThemeList", intRecipeThemeList);
        model.addAttribute("pageType", "create");
        model.addAttribute("language", language);
        model.addAttribute("searchForm", searchForm);
        return "/admin/recipe/edit";
    }

    // 레시피 - 저장하기
    @RequestMapping(value = "/regist", method = RequestMethod.POST)
    public String recipeRegistAction(@PathVariable("language") String language,
                                     IntRecipeForm intRecipeForm,
                                     HttpSession session, HttpServletRequest request,
                                     RedirectAttributes attrs,
                                     Model model) {

        IntAdminUser admin = Logics.adminFromSession(session);
        String adminId =  admin.getUserId();

        if (!LanguageCheck.languageCheck(language)) {
            model.addAttribute("info", "The wrong approach.");
            return "/admin/error";
        }

        if(intRecipeForm.getTags().length() > 200){
            ViewMessage.error().message("You can insert up to 200 letters for recipe tag.").register(attrs);
            return "redirect:/admin/"+ language +"/recipe/create";
        }
        IntAdminUser loginUser = Logics.adminFromSession(session);
        intRecipeService.enRecipeRegist(intRecipeForm, loginUser, language);

        adminUserService.logAction(adminId, HttpUtils.ip(request),"("+language+")"+ "create recipe: " + intRecipeForm.getRecipeTitle() + " / " + admin.getId());

        ViewMessage.success().message("It is registered").register(attrs);
        return "redirect:/admin/"+ language +"/recipe/list/0";
    }

    // 레시피 - 수정 페이지
    @RequestMapping("/edit/{id}")
    public String recipeEdit(@PathVariable("language") String language, @PathVariable("id") int id,
            @RequestParam(value = "page", defaultValue = "0", required = false) int page,
            IntRecipeSearchForm searchForm, Model model) {
        if (!LanguageCheck.languageCheck(language)) {
            model.addAttribute("info", "The wrong approach.");
            return "/admin/error";
        }
        IntRecipe intRecipe = intRecipeService.selectOne(Language.valueOf(language), id, true);
        List<IntRecipeTheme> intRecipeThemeList = intRecipeService.getRecipeThemeAll(language);

        model.addAttribute("pageType", "edit");
        model.addAttribute("page", page);
        model.addAttribute("searchForm", searchForm);
        model.addAttribute("intRecipe", intRecipe);
        model.addAttribute("language", language);
        model.addAttribute("intRecipeThemeList", intRecipeThemeList);

        return "/admin/recipe/edit";
    }

    // 레시피 - 수정하기
    @RequestMapping(value = "/regist/{id}", method = RequestMethod.POST)
    public String recipeEditAction(@PathVariable("language") String language,
                                   @PathVariable("id") int id,
                                   IntRecipeForm form,
                                   HttpSession session, HttpServletRequest request,
                                   RedirectAttributes attrs,
                                   Model model) {
        if (!Language.contains(language) || id == 0) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        if(form.getTags().length() > 200){
            ViewMessage.error().message("You can insert up to 200 letters for recipe tag.").register(attrs);
            return "redirect:/admin/"+ language +"/recipe/edit/{id}";
        }
        form.setId(id);
        IntAdminUser loginUser = Logics.adminFromSession(session);
        String adminId = loginUser.getUserId();
        intRecipeService.enRecipeUpdate(form, loginUser, language);

        ViewMessage.success().message("Changes have been reflected.").register(attrs);
        adminUserService.logAction(adminId, HttpUtils.ip(request),"("+language+")"+ "modify recipe: " + form.getRecipeTitle() + " / " + loginUser.getId());

        attrs.addFlashAttribute("id", id);
        return "redirect:/admin/"+ language +"/recipe/edit/{id}";
    }

    // 레시피 - 팝업
    @AdminRole({ AdminRoles.en_contents, AdminRoles.cn_contents, AdminRoles.ru_contents, AdminRoles.en_product, AdminRoles.cn_product, AdminRoles.ru_product })
    @RequestMapping("/search")
    public String recipeSearchList(@PathVariable("language") String language, Model model) {
        if (!LanguageCheck.languageCheck(language)) {
            model.addAttribute("info", "The wrong approach.");
            return "/admin/error";
        }
        return "redirect:/admin/"+language+"/recipe/search/0";
    }

    // 레시피 - 팝업 검색
    @AdminRole({ AdminRoles.en_contents, AdminRoles.cn_contents, AdminRoles.ru_contents, AdminRoles.en_product, AdminRoles.cn_product, AdminRoles.ru_product })
    @RequestMapping("/search/{page}")
    public String recipeSearch(@PathVariable("language") String language, @PathVariable("page") int page,
            IntRecipeSearchForm searchForm, Model model) {
        if (!LanguageCheck.languageCheck(language)) {
            model.addAttribute("info", "The wrong approach.");
            return "/admin/error";
        }
        // searchForm에서 값을 가져와서 객체를 생성
        int count = intRecipeService.searchCount(searchForm, language);

        // 이제 searchForm의 값으로 Recipe List를 가져와야 겠다.
        // 사실 Recipe에는 item에 관한 정보는 없고 조리법에 대한 정보만 있다.
        // 그 조리법에 대한 정보에는 재료에 대한 정보가 있다.
        // 그 재료(item)과 Brand, BrandCategory, Item에 관한 정보를 일치하는 것을 찾아야 한다.
        // 연관관계가 매우 중요하다.
        Pager pager = new Pager(page, Const.ADMIN_LIST_SIZE);
        String link = "/admin/" + language + "/recipe/search/";
        Builder builder = Paging.builder();
        builder.currentPage(page).entriesPerPage(Const.ADMIN_LIST_SIZE).totalCount(count).link(link);

        List<IntRecipe> intRecipeList = intRecipeService.getIntRecipeList(searchForm, pager, language);

        model.addAttribute("paging", searchForm.addQueryParams(builder).build());
        model.addAttribute("intRecipeList", intRecipeList);
        model.addAttribute("searchForm", searchForm);
        model.addAttribute("language", language);

        return "/admin/recipe/search";
    }

    // 추천 레시피 ajax
    @AdminRole({ AdminRoles.en_contents, AdminRoles.cn_contents, AdminRoles.ru_contents, AdminRoles.en_product,
            AdminRoles.cn_product, AdminRoles.ru_product })
    @RequestMapping(value = "/recommend/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getRecommend(@PathVariable("language") String language, @PathVariable("id") Integer id,
            Model model) {
        IntRecipe intRecipe = intRecipeService.selectOne(Language.valueOf(language), id, false);

        Map<String, Object> jsonObject = new HashMap<String, Object>();

        jsonObject.put("state", intRecipe.getState());
        jsonObject.put("id", intRecipe.getId());
        jsonObject.put("title", intRecipe.getTitle());

        return jsonObject;
    }

}
