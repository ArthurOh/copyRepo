package com.sempio.en.controller.admin;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Maps;
import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.Result;
import com.sempio.en.code.State;
import com.sempio.en.command.IntAdminUserSearchForm;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.interceptor.AdminRole;
import com.sempio.en.service.IntAdminUserService;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.Logics;
import com.sempio.en.util.Pager;
import com.sempio.en.util.Paging;
import com.sempio.en.util.Paging.Builder;
import com.sempio.en.util.ViewMessage;
import com.sempio.en.view.IntAdminLog;

@Controller
@RequestMapping("/admin")
public class IntAdminController {

    @Autowired
    private IntAdminUserService adminUserService;

    @RequestMapping("")
    public String main(Model model){
        return "/admin/index";
    }

    @AdminRole(AdminRoles.integration)
    @RequestMapping({"/user/list", "/user"})
    public String userList(@RequestParam(value = "size", required = false, defaultValue = "10") int size,
                           @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                            IntAdminUserSearchForm form,
                            Model model) {
        return userList(page, form, size, model);
    }

    @RequestMapping(value = "/mypage", method = RequestMethod.GET)
    public String mypage(HttpSession session, Model model) {
        IntAdminUser loginUser = Logics.adminFromSession(session);
        IntAdminUser item = adminUserService.select(loginUser.getUserId());
        model.addAttribute("item", item);
        return "/admin/mypage";
    }

    @RequestMapping(value = "/mypage", method = RequestMethod.POST)
    public String mypage(IntAdminUser form, HttpSession session, RedirectAttributes attrs, Model model) {
        IntAdminUser user = Logics.adminFromSession(session);
        form.setRewriter(user);
        adminUserService.updateByUser(form);
        ViewMessage.success().message("Changes have been reflected.").register(attrs);
        return "redirect:/admin/mypage";
    }

    /**
     * 관리자 관리
     * @param page
     * @param form
     * @param size
     * @param model
     * @return
     */
    @AdminRole(AdminRoles.integration)
    @RequestMapping("/user/list/{page}")
    public String userList(@PathVariable int page,
                            IntAdminUserSearchForm form,
                            @RequestParam(value = "size", required = false, defaultValue = "10") int size,
                            Model model) {
        Pager pager = new Pager(page, size);
        List<IntAdminUser> list = adminUserService.list(form, pager);
        int count = adminUserService.count(form);

        Builder builder = Paging.builder();
        form.addQueryParams(builder);
        Paging paging = builder.currentPage(page)
               .entriesPerPage(size)
               .totalCount(count)
               .link("/admin/user/list/").build();

        model.addAttribute("form", form);
        model.addAttribute("list", list);
        model.addAttribute("count", count);
        model.addAttribute("paging", paging);
        return "admin/user/list";
    }

    /**
     * 관리자 상세
     * @param id
     * @param attrs
     * @param model
     * @return
     */
    @AdminRole(AdminRoles.integration)
    @RequestMapping(value = "/user/edit/{id}", method = RequestMethod.GET)
    public String userEdit(@PathVariable int id,
                           @RequestParam(required = false, value = "qs") String qs,
                           RedirectAttributes attrs,
                           Model model) {
        IntAdminUser item = adminUserService.select(id);
        if (item == null || item.getState() == State.deleted) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin/user/list";
        }
        model.addAttribute("item", item);
        model.addAttribute("qs", qs);
        return "admin/user/edit";
    }

    /**
     * 관리자 수정
     * @param id
     * @param attrs
     * @param model
     * @return
     */
    @AdminRole(AdminRoles.integration)
    @RequestMapping(value = "/user/edit/{id}", method = RequestMethod.POST)
    public String userEdit(@PathVariable int id,
                           IntAdminUser admin,
                           @RequestParam(required = false, value = "qs") String qs,
                           HttpSession session,
                           RedirectAttributes attrs,
                           HttpServletRequest request) {
        IntAdminUser loginUser = Logics.adminFromSession(session);
        admin.setRewriter(loginUser);

        Result<String> result = adminUserService.update(admin);
        if (result.isError()) {
            ViewMessage.error().message(result.payload()).register(attrs);
            return "redirect:/admin/user/list";
        }
        ViewMessage.success().message("Changes have been reflected.").register(attrs);
        adminUserService.logAction(loginUser.getUserId(), HttpUtils.ip(request), "modify admin user : " + admin.getId());

        attrs.addAttribute("id", id);
        attrs.addAttribute("qs", qs);

        return "redirect:/admin/user/edit/{id}?qs={qs}";
    }

    @AdminRole(AdminRoles.integration)
    @RequestMapping(value = "/user/edit", method = RequestMethod.GET)
    public String adminWrite(@RequestParam(required = false, value = "qs") String qs, Model model) {
        model.addAttribute("qs", qs);
        return "admin/user/edit";
    }

    @AdminRole(AdminRoles.integration)
    @RequestMapping(value = "/user/edit", method = RequestMethod.POST)
    public String adminWrite(IntAdminUser admin,
                             HttpSession session,
                             RedirectAttributes attrs,
                             HttpServletRequest request) {
        IntAdminUser loginUser = Logics.adminFromSession(session);
        admin.setWriter(loginUser);

        Integer id = adminUserService.insert(admin);
        if (id == null || id.intValue() < 1) {
            ViewMessage.error().message("Registration failed.").register(attrs);
            return "redirect:/admin/user/edit";
        }
        ViewMessage.success().message("It is registered.").register(attrs);

        adminUserService.logAction(loginUser.getUserId(), HttpUtils.ip(request), "create admin user : " + admin.getId());
        return "redirect:/admin/user/list";
    }

    @AdminRole(AdminRoles.integration)
    @RequestMapping(value = "/user/check/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> checkId(@PathVariable String userId) {
        Map<String, Object> result = Maps.newHashMap();
        result.put("cnt", 0);
        IntAdminUser item = adminUserService.select(userId);
        if (item != null) {
            result.put("cnt", 1);
        }
        return result;
    }

    @AdminRole(AdminRoles.integration)
    @RequestMapping("/user/log/{id}")
    public String coupon(@PathVariable("id") String id) {
        return "redirect:/admin/user/log/" + id + "/0";
    }

    public static final int LOG_LIST_SIZE = 40;
    @AdminRole(AdminRoles.integration)
    @RequestMapping("/user/log/{id}/{p}")
    public String logList(@PathVariable("id") String id, @PathVariable("p") int page, Model model) {

        IntAdminUser admin = adminUserService.select(Integer.parseInt(id));

        List<IntAdminLog> list = adminUserService.listLog(admin.getUserId(), LOG_LIST_SIZE, page * LOG_LIST_SIZE);
        model.addAttribute("list", list);

        int count = adminUserService.countLog(id);
        Paging paging = Paging.builder()
                .currentPage(page)
                .entriesPerPage(LOG_LIST_SIZE)
                .totalCount(count)
                .link("/admin/user/log/" + id + "/")
                .build();
        model.addAttribute("paging", paging);

        model.addAttribute("admin", admin);

        return "admin/user/log";
    }
}
