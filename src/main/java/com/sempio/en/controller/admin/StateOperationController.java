package com.sempio.en.controller.admin;

import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriUtils;

import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.service.IntAdminUserService;
import com.sempio.en.service.OperatorService;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.Logics;
import com.sempio.en.util.ViewMessage;

@Controller
@RequestMapping("/admin/op")
public class StateOperationController {

	private static Logger log = LoggerFactory.getLogger(StateOperationController.class);

    @Autowired
    private OperatorService operatorService;

    @Autowired
    private IntAdminUserService adminUserService;

    private static final String OPERATION_REQUEST_MAPPING = "/admin/op";

    // 삭제 from 목록
    @RequestMapping("/**/delete")
    public String delete(
    		@RequestParam("ids") Integer[] ids,
    		@RequestParam("type") String type,
    		RedirectAttributes attrs,
    		HttpServletRequest request,
    		@RequestParam (required = false, name="queryString") String queryString) throws UnsupportedEncodingException {

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        String menu = getMenu(path, "delete");
        //log.debug(type + " delete ids = {}", (Object) ids);

        if (StringUtils.isNoneBlank(queryString)) {
        	menu = StringUtils.join(menu, "?", UriUtils.encodeQuery(queryString, "utf-8"));
        }

        IntAdminUser rewriter = Logics.adminFromSession(request.getSession());
        int num = operatorService.updateState(ids, "deleted", type, rewriter);

        String uri = request.getRequestURI();
        Pattern p = Pattern.compile("/admin/(en|cn|ru)/");
        Matcher m = p.matcher(uri);
        String language = "";
        if (m.find()) {
            language = StringUtils.join("(", m.group(1), ") ");
        }
        IntAdminUser loginUser = Logics.adminFromSession(request.getSession());
        adminUserService.logAction(loginUser.getUserId(), HttpUtils.ip(request), StringUtils.join(language,"delete ", type , " : ", StringUtils.join(ids, ",")));

        ViewMessage.success().message("The selected contents  are deleted. (" + num + ") contents").register(attrs);


        return "redirect:" + menu;
    }


    // 삭제 from 상세
    @RequestMapping("/**/delete/{id}/{type}")
    public String deleteOne(
    		@PathVariable("id") Integer[] id,
    		@PathVariable("type") String type,
    		RedirectAttributes attrs, HttpServletRequest request,
    		@RequestParam (required = false, name="queryString")String queryString) throws Exception {

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        String menu = getMenu(path, "delete");
        log.debug("post delete ids = {}", (Object) id);

        if (StringUtils.isNoneBlank(queryString)) {
        	menu = StringUtils.join(menu, "?", UriUtils.encodeQuery(queryString, "utf-8"));
        }

        IntAdminUser rewriter = Logics.adminFromSession(request.getSession());
        operatorService.updateState(id, "deleted", type, rewriter);

        String uri = request.getRequestURI();
        Pattern p = Pattern.compile("/admin/(en|cn|ru)/");
        Matcher m = p.matcher(uri);
        String language = "";
        if (m.find()) {
            language = StringUtils.join("(", m.group(1), ") ");
        }
        IntAdminUser loginUser = Logics.adminFromSession(request.getSession());
        adminUserService.logAction(loginUser.getUserId(), HttpUtils.ip(request), StringUtils.join(language,"delete ", type , " : ", String.valueOf(id)));

        ViewMessage.success().message("The selected contents are deleted. (1 contents)").register(attrs);

        return "redirect:" + menu;
    }

    // 상태(state) 변경 from 목록
    @RequestMapping("/**/{stateTo:on|off}")
    public String updateStateInService(
    		@RequestParam(value = "ids") Integer[] ids,
            @RequestParam(value = "type") String type,
            @PathVariable("stateTo") String stateTo,
            RedirectAttributes attrs, HttpServletRequest request,
            @RequestParam (required = false, name="queryString")String queryString) throws UnsupportedEncodingException {

    	String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        String menu = getMenu(path, "/"+stateTo);

        if (StringUtils.isNoneBlank(queryString)) {
        	menu = StringUtils.join(menu, "?", UriUtils.encodeQuery(queryString, "utf-8"));
        }

        String typeValue = null;
        String returnType = null;
        if (StringUtils.equals("on", stateTo)) {
            typeValue = "on";
            returnType = "Disclose";
        } else {
            typeValue = "off";
            returnType = "Undisclose";
        }

        IntAdminUser rewriter = Logics.adminFromSession(request.getSession());

        int num = operatorService.updateState(ids, typeValue, type, rewriter);

        String uri = request.getRequestURI();
        Pattern p = Pattern.compile("/admin/(en|cn|ru)/");
        Matcher m = p.matcher(uri);
        String language = "";
        if (m.find()) {
            language = StringUtils.join("(", m.group(1), ") ");
        }
        IntAdminUser loginUser = Logics.adminFromSession(request.getSession());
        adminUserService.logAction(loginUser.getUserId(), HttpUtils.ip(request), StringUtils.join(language,"update state ", stateTo ," ", type , " : ", StringUtils.join(ids, ",")));

        ViewMessage.success().message("The status of selected contents  has been changed to " + returnType + ". (" + num + ") contents").register(attrs);

        return "redirect:" + menu;
    }

    private String getMenu(String path, String state) {
        return path.substring(OPERATION_REQUEST_MAPPING.length(), path.indexOf(state));
    }

    public OperatorService getOperatorService() {
        return operatorService;
    }

    public void setOperatorService(OperatorService operatorService) {
        this.operatorService = operatorService;
    }
}
