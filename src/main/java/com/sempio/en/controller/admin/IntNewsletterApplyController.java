package com.sempio.en.controller.admin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.Result;
import com.sempio.en.code.State;
import com.sempio.en.command.IntNewsletterApplySearchForm;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntNewsletterApply;
import com.sempio.en.interceptor.AdminRole;
import com.sempio.en.service.IntAdminUserService;
import com.sempio.en.service.IntNewsletterService;
import com.sempio.en.util.BaseUtil;
import com.sempio.en.util.Const;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.Logics;
import com.sempio.en.util.Pager;
import com.sempio.en.util.Paging;
import com.sempio.en.util.Paging.Builder;
import com.sempio.en.util.ViewMessage;

@Controller
@AdminRole({ AdminRoles.en_operation, AdminRoles.cn_operation, AdminRoles.ru_operation })
@RequestMapping("/admin/newsletter-apply")
public class IntNewsletterApplyController {

    private static Logger log = LoggerFactory.getLogger(IntNewsletterApplyController.class);

    @Autowired
    private IntNewsletterService newsletterService;

    @Autowired
    private IntAdminUserService adminUserService;

    @Value("${sempiosite.paths.uploadedFiles}")
    private String basedir;

    @RequestMapping(value = "/list")
    public String list(IntNewsletterApplySearchForm form,
                      @RequestParam(value = "size", required = false, defaultValue = "10") int size,
                      @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                      RedirectAttributes attrs,
                      Model model) {
        return list(page, form, size, attrs, model);
    }

    @RequestMapping(value = "/list/{page}")
    public String list(@PathVariable int page,
                       IntNewsletterApplySearchForm form,
                       @RequestParam(value = "size", required = false, defaultValue = "10") int size,
                       RedirectAttributes attrs,
                       Model model) {

        Pager pager = new Pager(page, size);
        List<IntNewsletterApply> list = newsletterService.listApply(form, pager);
        int count = newsletterService.countApply(form);

        Builder builder = Paging.builder();
        form.addQueryParams(builder);
        Paging paging = builder.currentPage(page)
               .entriesPerPage(size)
               .totalCount(count)
               .link("/admin/newsletter-apply/list/").build();

        model.addAttribute("form", form);
        model.addAttribute("list", list);
        model.addAttribute("count", count);
        model.addAttribute("paging", paging);
        return "admin/newsletter_apply/list";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable int id,
                       @RequestParam(required = false, value = "qs") String qs,
                       RedirectAttributes attrs,
                       Model model) {
        IntNewsletterApply item = newsletterService.selectApply(id);
        if (item == null || item.getState() == State.deleted) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin/newsletter-apply/list";
        }
        model.addAttribute("item", item);
        model.addAttribute("qs", qs);
        return "admin/newsletter_apply/edit";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String edit(@PathVariable int id,
                       IntNewsletterApply form,
                       @RequestParam(required = false, value = "qs") String qs,
                       HttpServletRequest request,
                       RedirectAttributes attrs,
                       Model model) {

        Result<String> result = newsletterService.updateApply(form);
        if (result.isError()) {
            ViewMessage.error().message(result.payload()).register(attrs);
            return "redirect:/admin/newsletter-apply/list";
        }

        ViewMessage.success().message("Changes have been reflected.").register(attrs);
        attrs.addAttribute("id", id);
        if (StringUtils.isNoneBlank(qs)) {
            attrs.addAttribute("qs", qs);
            return "redirect:/admin/newsletter-apply/edit/{id}?qs={qs}";
        }

        IntAdminUser loginUser = Logics.adminFromSession(request.getSession());
        adminUserService.logAction(loginUser.getUserId(), HttpUtils.ip(request), StringUtils.join("modify newsletter-apply : ", String.valueOf(id) , " / ", form.getName()));

        return "redirect:/admin/newsletter-apply/edit/{id}";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String write(@RequestParam(required = false, value = "qs") String qs, Model model) {
        model.addAttribute("qs", qs);
        return "admin/newsletter_apply/edit";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String write(IntNewsletterApply form,
                        HttpServletRequest request,
                        RedirectAttributes attrs) {
        newsletterService.insertApply(form);
        ViewMessage.success().message("Registered.").register(attrs);

        IntAdminUser loginUser = Logics.adminFromSession(request.getSession());
        adminUserService.logAction(loginUser.getUserId(), HttpUtils.ip(request), StringUtils.join("create newsletter-apply : ", String.valueOf(form.getId()), " / ",form.getName()));

        return "redirect:/admin/newsletter-apply/list";
    }

    @RequestMapping(value = "/excel", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> excel(IntNewsletterApplySearchForm form) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("code", "fail");

        List<IntNewsletterApply> list = newsletterService.listApply(form, null);
        if (CollectionUtils.isEmpty(list)) {
            result.put("message", "내역이 없습니다");
            return result;
        }

        Workbook book = null;
        FileOutputStream out = null;

        // 파일명 생성
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String today = formatter.format(new Timestamp(new java.util.Date().getTime()));
        String prefix = "newsletter" + File.separator;
        String fileName = "apply_" + today + ".xlsx";
        try {
            book = new SXSSFWorkbook();
            Sheet sheet1 = book.createSheet("newsletter-apply_" + today.substring(2,8));

            sheet1.setColumnWidth(0, 1450); // No
            sheet1.setColumnWidth(1, 2725); // Name
            sheet1.setColumnWidth(2, 10000); // Email
            sheet1.setColumnWidth(3, 10000); // Language
            sheet1.setColumnWidth(4, 7000); // Country
            sheet1.setColumnWidth(5, 2725); // 유입경로
            sheet1.setColumnWidth(6, 7000); // 신청일
            sheet1.setColumnWidth(7, 2725); // 사용여부

            Row row = sheet1.createRow(2);

            // th 폰트
            Font thFont = book.createFont();
            thFont.setFontName("맑은 고딕");
            thFont.setBold(true);
            thFont.setFontHeightInPoints((short) 10);

            // th 스타일
            CellStyle thStyle = book.createCellStyle();
            thStyle.setBorderBottom(CellStyle.BORDER_THIN);
            thStyle.setBorderTop(CellStyle.BORDER_THIN);
            thStyle.setBorderLeft(CellStyle.BORDER_THIN);
            thStyle.setBorderRight(CellStyle.BORDER_THIN);
            thStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            thStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            thStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            thStyle.setAlignment(CellStyle.ALIGN_CENTER);
            thStyle.setFont(thFont);

            row.setHeightInPoints(27);

            Cell cell = row.createCell(0);
            cell.setCellStyle(thStyle);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue("No");

            cell = row.createCell(1);
            cell.setCellStyle(thStyle);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue("Name");

            cell = row.createCell(2);
            cell.setCellStyle(thStyle);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue("Email");

            cell = row.createCell(3);
            cell.setCellStyle(thStyle);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue("Language");

            cell = row.createCell(4);
            cell.setCellStyle(thStyle);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue("Country");

            cell = row.createCell(5);
            cell.setCellStyle(thStyle);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue("Inflow route");

            cell = row.createCell(6);
            cell.setCellStyle(thStyle);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue("Application date");

            cell = row.createCell(7);
            cell.setCellStyle(thStyle);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue("Use (y/n) - In use/Not in use");


            // 공통 폰트
            Font font = book.createFont();
            font.setFontName("맑은 고딕");
            font.setFontHeightInPoints((short) 9);

            // 문자열 스타일
            CellStyle strStyle = book.createCellStyle();
            strStyle.setBorderBottom(CellStyle.BORDER_THIN);
            strStyle.setBorderTop(CellStyle.BORDER_THIN);
            strStyle.setBorderLeft(CellStyle.BORDER_THIN);
            strStyle.setBorderRight(CellStyle.BORDER_THIN);
            strStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            strStyle.setAlignment(CellStyle.ALIGN_CENTER);
            strStyle.setWrapText(true);
            strStyle.setFont(font);

            // 내용 스타일
            CellStyle textStyle = book.createCellStyle();
            textStyle.setBorderBottom(CellStyle.BORDER_THIN);
            textStyle.setBorderTop(CellStyle.BORDER_THIN);
            textStyle.setBorderLeft(CellStyle.BORDER_THIN);
            textStyle.setBorderRight(CellStyle.BORDER_THIN);
            textStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            textStyle.setWrapText(true);
            textStyle.setFont(font);

            // 날짜 스타일
            DataFormat format = book.createDataFormat();
            CellStyle dateStyle = book.createCellStyle();
            dateStyle.setBorderBottom(CellStyle.BORDER_THIN);
            dateStyle.setBorderTop(CellStyle.BORDER_THIN);
            dateStyle.setBorderLeft(CellStyle.BORDER_THIN);
            dateStyle.setBorderRight(CellStyle.BORDER_THIN);
            dateStyle.setDataFormat(format.getFormat("yyyy.mm.dd hh:mm"));
            dateStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            dateStyle.setAlignment(CellStyle.ALIGN_CENTER);
            dateStyle.setFont(font);

            for (int i = 0; i < list.size(); i++) {

                IntNewsletterApply item = list.get(i);
                row = sheet1.createRow(i + 3);

                cell = row.createCell(0);
                cell.setCellStyle(strStyle);
                cell.setCellValue(list.size() - i);

                cell = row.createCell(1);
                cell.setCellStyle(strStyle);
                cell.setCellValue(item.getName());

                cell = row.createCell(2);
                cell.setCellStyle(strStyle);
                cell.setCellValue(item.getEmail());

                cell = row.createCell(3);
                cell.setCellStyle(strStyle);
                List<String> applyLanguage = Lists.newArrayList();
                for (String value : item.getApplyLanguage()) {
                    if (StringUtils.equals("en", value)) {
                        applyLanguage.add("English");
                    } else if (StringUtils.equals("kr", value)) {
                        applyLanguage.add("Korean");
                    }
                }
                cell.setCellValue(StringUtils.join(applyLanguage, ", "));

                cell = row.createCell(4);
                cell.setCellStyle(strStyle);
                cell.setCellValue(item.getCountry());

                cell = row.createCell(5);
                cell.setCellStyle(strStyle);
                String language = "";
                if (item.getLanguage() != null) {
                    language = item.getLanguage().name().toUpperCase();
                }
                cell.setCellValue(language);

                cell = row.createCell(6);
                cell.setCellStyle(dateStyle);
                cell.setCellValue(item.getCreateAt());

                cell = row.createCell(7);
                cell.setCellStyle(strStyle);
                if (item.getState() == State.on) {
                    cell.setCellValue("사용");
                } else if (item.getState() == State.off) {
                    cell.setCellValue("미사용");
                }
            }


            BaseUtil.makeFolder(Paths.get(basedir, Const.FILE_PREFIX_PATH) + File.separator + prefix);
            File xlsFile = new File(Paths.get(basedir, Const.FILE_PREFIX_PATH) + File.separator + prefix + File.separator + fileName);
            out = new FileOutputStream(xlsFile);

            book.write(out);
            book.close();

            result.put("code", "success");
            result.put("path", prefix + File.separator + fileName);

        } catch (Exception e) {
            log.error("ERROR", e);
            result.put("code", "fail");
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    log.error("ERROR", e);
                }
            }
            if (book != null) {
                ((SXSSFWorkbook)book).dispose();
            }
        }
        return result;
    }
}
