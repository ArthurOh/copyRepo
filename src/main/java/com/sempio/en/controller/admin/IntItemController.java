package com.sempio.en.controller.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Maps;
import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.command.ItemSearchForm;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntBrand;
import com.sempio.en.entity.IntBrandCategory;
import com.sempio.en.entity.IntItem;
import com.sempio.en.interceptor.AdminRole;
import com.sempio.en.service.IntAdminUserService;
import com.sempio.en.service.IntBrandService;
import com.sempio.en.service.IntItemService;
import com.sempio.en.util.Const;
import com.sempio.en.util.DateUtils;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.LanguageCheck;
import com.sempio.en.util.Logics;
import com.sempio.en.util.Pager;
import com.sempio.en.util.Paging;
import com.sempio.en.util.Paging.Builder;
import com.sempio.en.util.ViewMessage;
import com.sempio.en.view.ItemXlsView;


@Controller
@AdminRole({AdminRoles.en_product, AdminRoles.cn_product, AdminRoles.ru_product})
@RequestMapping("/admin/{language}/item")
public class IntItemController {
    private static Logger log = LoggerFactory.getLogger(IntNewsController.class);

    @Autowired
    private IntItemService intItemService;

    @Autowired
    private IntBrandService intBrandService;

    @Autowired
    private IntAdminUserService adminUserService;

    @RequestMapping(value = {"", "/list"})
    public String index(@PathVariable("language") String language,
            ItemSearchForm searchForm, Model model) {
        return list(language, 0, searchForm, model);
    }

    // 리스트
    @RequestMapping(value="/list/{page}")
    public String list(@PathVariable("language") String language, @PathVariable("page") int page,
            ItemSearchForm searchForm, Model model) {

        if (!LanguageCheck.languageCheck(language)) {
            model.addAttribute("info", "The wrong approach.");
            return "/admin/error";
        }

        Pager pager = new Pager(page, Const.ADMIN_LIST_SIZE);
        int count = 0;
        List<IntItem> item = intItemService.listForAdmin(Language.valueOf(language), searchForm, pager);
        count = intItemService.countForAdmin(Language.valueOf(language), searchForm);

        model.addAttribute("item", item);
        model.addAttribute("count", count);

        Builder pagingBuilder = Paging.builder()
                .currentPage(page)
                .totalCount(count)
                .entriesPerPage(Const.ADMIN_LIST_SIZE)
                .link("/admin/"+ language +"/item/list");

        model.addAttribute("paging", searchForm.addQueryParams(pagingBuilder).build());

        List<IntBrand> brand = intBrandService.listAll(Language.valueOf(language));
        model.addAttribute("brand", brand);

        List<IntBrand> brandList = intBrandService.listAllWithCategory(language);
        model.addAttribute("brandList", brandList);
        model.addAttribute("searchForm", searchForm);
        model.addAttribute("language", language);

        return "/admin/item/list";
    }

    // 쓰기
    @RequestMapping(value = "/create")
    public String create(@PathVariable("language") String language, Model model) {

        //브랜드 리스트
        List<IntBrand> brand = intBrandService.listAll(Language.valueOf(language));
        model.addAttribute("brand", brand);

        model.addAttribute("pageType", "create");
        model.addAttribute("q", null);
        model.addAttribute("page", null);
        model.addAttribute("language", language);

        return "/admin/item/edit";
    }

    // 쓰기 등록
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String createAction(@PathVariable("language") String language,
            @RequestParam(value = "brand_category") Integer brandCategoryId,
            @RequestParam(value = "item_title") String title,
            @RequestParam(value = "point_msg", defaultValue = "") String pointMsg,
            @RequestParam(value = "wrap", defaultValue = "") String wrap,
            @RequestParam(value = "filepath_thumbnail", defaultValue = "") String listImg,
            @RequestParam(value = "filepath_defaultImage", defaultValue = "") String viewImg,
            @RequestParam(value = "recommend_recipe", defaultValue = "") String recommendRecipe,
            @RequestParam(value = "publish_at", defaultValue = "") String publishAt,
            @RequestParam(value = "feature", defaultValue = "") String feature,
            @RequestParam(value = "tags", defaultValue = "") String tags,
            @RequestParam(value = "ingredient", defaultValue = "") String ingredient,
            @RequestParam(value = "allergy_innfo", defaultValue = "") String allergyInfo,
            @RequestParam(value = "best_by", defaultValue = "") String bestBy,
            @RequestParam(value = "certification", defaultValue = "") String certification,
            @RequestParam(value = "serving_size", defaultValue = "") String servingSize,
            @RequestParam(value = "serving_amount", defaultValue = "") String servingAmount,
            @RequestParam(value = "fat", defaultValue = "") String fat,
            @RequestParam(value = "cholesterol", defaultValue = "") String cholesterol,
            @RequestParam(value = "sodium", defaultValue = "") String sodium,
            @RequestParam(value = "carbohydrate", defaultValue = "") String carbohydrate,
            @RequestParam(value = "protein", defaultValue = "") String protein,
            @RequestParam(value = "state", defaultValue = "") String state,
            HttpSession session, RedirectAttributes attrs, HttpServletRequest request) throws ParseException {

        long now = System.currentTimeMillis();
        Date createAt = new Date(now);

        IntAdminUser admin = Logics.adminFromSession(session);
        String adminId =  admin.getUserId();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date pAt = sdf.parse(publishAt + " 00:00:00");

        if (tags.length() > 200) {
            ViewMessage.error().message("You can insert up to 200 letters for Tag.").register(attrs);
            return "redirect:/admin/"+ language +"/item";
        }

        if (wrap.length() > 200) {
            ViewMessage.error().message("You can insert up to 200 letters for Package size.").register(attrs);
            return "redirect:/admin/"+ language +"/item";
        }

        IntItem item = new IntItem();
        item.setTitle(title);
        item.setPointMsg(pointMsg);
        item.setWrap(wrap);
        item.setListImgPath(listImg);
        item.setViewImgPath(viewImg);
        item.setRecommendRecipe(recommendRecipe);
        item.setPublishAt(pAt);
        item.setFeature(feature);
        item.setTags(tags);
        item.setIngredient(ingredient);
        item.setAllergyInfo(allergyInfo);
        item.setBestBy(bestBy);
        item.setCertification(certification);
        item.setServingSize(servingSize);
        item.setServingAmount(servingAmount);
        item.setFat(fat);
        item.setCholesterol(cholesterol);
        item.setSodium(sodium);
        item.setCarbohydrate(carbohydrate);
        item.setProtein(protein);
        item.setState(State.valueOf(state));
        item.setOrdering(Const.CONTENT_ORDERING);
        item.setCreateAt(createAt);
        item.setWriter(adminId);
        item.setLanguage(Language.valueOf(language));

        IntBrandCategory bc = new IntBrandCategory();
        bc.setId(brandCategoryId);
        item.setIntBrandCategory(bc);

        log.debug("input value = {}", item);

        intItemService.create(item);

        ViewMessage.success().message("It is registered.").register(attrs);

        adminUserService.logAction(adminId, HttpUtils.ip(request), "("+ language +") create item : " + title + " / " + admin.getId());

        return "redirect:/admin/"+ language +"/item";
    }

    // 수정
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editView(@PathVariable("language") String language,
            @PathVariable("id") Integer id,
            @RequestParam(value = "page", defaultValue = "0") int page, Model model) {

        IntItem item = intItemService.getOne(Language.valueOf(language), id);
        model.addAttribute("item", item);

        List<IntBrand> brand = intBrandService.listAll(Language.valueOf(language));
        model.addAttribute("brand", brand);

        model.addAttribute("pageType", "edit");
        model.addAttribute("page", page);
        model.addAttribute("language", language);

        String[] certification = item.getCertification().split(",");
        model.addAttribute("certification", certification);

        return "/admin/item/edit";
    }

    // 수정 등록
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String editModify(@PathVariable("language") String language,
            @PathVariable("id") Integer id,
            @RequestParam(value = "brand_category") Integer brandCategoryId,
            @RequestParam(value = "brand") String brand,
            @RequestParam(value = "item_title") String title,
            @RequestParam(value = "point_msg", defaultValue = "") String pointMsg,
            @RequestParam(value = "wrap", defaultValue = "") String wrap,
            @RequestParam(value = "filepath_thumbnail", defaultValue = "") String listImg,
            @RequestParam(value = "filepath_defaultImage", defaultValue = "") String viewImg,
            @RequestParam(value = "recommend_recipe", defaultValue = "") String recommendRecipe,
            @RequestParam(value = "publish_at", defaultValue = "") String publishAt,
            @RequestParam(value = "feature", defaultValue = "") String feature,
            @RequestParam(value = "tags", defaultValue = "") String tags,
            @RequestParam(value = "ingredient", defaultValue = "") String ingredient,
            @RequestParam(value = "allergy_innfo", defaultValue = "") String allergyInfo,
            @RequestParam(value = "best_by", defaultValue = "") String bestBy,
            @RequestParam(value = "certification", defaultValue = "") String certification,
            @RequestParam(value = "serving_size", defaultValue = "") String servingSize,
            @RequestParam(value = "serving_amount", defaultValue = "") String servingAmount,
            @RequestParam(value = "fat", defaultValue = "") String fat,
            @RequestParam(value = "cholesterol", defaultValue = "") String cholesterol,
            @RequestParam(value = "sodium", defaultValue = "") String sodium,
            @RequestParam(value = "carbohydrate", defaultValue = "") String carbohydrate,
            @RequestParam(value = "protein", defaultValue = "") String protein,
            @RequestParam(value = "state", defaultValue = "") String state,
            @RequestParam(value = "q", defaultValue = "") String q,
            @RequestParam(value = "page", defaultValue = "0") int page,
            HttpSession session, RedirectAttributes attrs, HttpServletRequest request) throws Exception {

        long now = System.currentTimeMillis();
        Date updateAt = new Date(now);

        IntAdminUser admin = Logics.adminFromSession(session);
        String adminId =  admin.getUserId();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date pAt = sdf.parse(publishAt + " 00:00:00");

        IntItem beforeItem = intItemService.getOne(Language.valueOf(language), id);

        if (tags.length() > 200) {
            ViewMessage.error().message("You can insert up to 200 letters for Tag.").register(attrs);
            return "redirect:/admin/"+ language +"/item/edit/" + id;
        }

        if (wrap.length() > 200) {
            ViewMessage.error().message("You can insert up to 200 letters for Package size.").register(attrs);
            return "redirect:/admin/"+ language +"/item/edit/" + id;
        }

        IntItem item = new IntItem();
        item.setId(id);
        item.setTitle(title);
        item.setPointMsg(pointMsg);
        item.setWrap(wrap);
        item.setListImgPath(listImg);
        item.setViewImgPath(viewImg);
        item.setRecommendRecipe(recommendRecipe);
        item.setPublishAt(pAt);
        item.setFeature(feature);
        item.setTags(tags);
        item.setIngredient(ingredient);
        item.setAllergyInfo(allergyInfo);
        item.setBestBy(bestBy);
        item.setCertification(certification);
        item.setServingSize(servingSize);
        item.setServingAmount(servingAmount);
        item.setFat(fat);
        item.setCholesterol(cholesterol);
        item.setSodium(sodium);
        item.setCarbohydrate(carbohydrate);
        item.setProtein(protein);
        item.setState(State.valueOf(state));
        item.setOrdering(Const.CONTENT_ORDERING);
        item.setUpdateAt(updateAt);
        item.setRewriter(adminId);
        item.setLanguage(Language.valueOf(language));
        item.setOrdering(beforeItem.getOrdering());

        IntBrandCategory bc = new IntBrandCategory();
        bc.setId(brandCategoryId);
        item.setIntBrandCategory(bc);

        log.debug("input value = {}", item.getOrdering());

        intItemService.update(item);

        ViewMessage.success().message("Changes have been reflected.").register(attrs);

        adminUserService.logAction(adminId, HttpUtils.ip(request), "("+ language +") modify item : " + id + " / " + admin.getId());

        //return "redirect:/admin/"+ language +"/item/edit/" + id + StringUtils.join("?", UriUtils.encodeQuery(queryString.toString(), "utf-8"));
        return "redirect:/admin/"+ language +"/item/edit/" + id;
    }

    // for excel download
    @RequestMapping("/excel")
    @ResponseBody
    public ModelAndView itemExcelDownload(ItemSearchForm searchForm, @PathVariable("language") String language) {
        Map<String, Object> model = Maps.newHashMap();
        String filename = language + "_item_" + DateUtils.format(new Date(), "yyyyMMddHHmm") + ".xls";
        model.put("filename", filename);

        List<IntItem> item = intItemService.listAll(Language.valueOf(language), searchForm);
        model.put("itemList", item);

        return new ModelAndView(new ItemXlsView(), model);
    }

    //Sempio Item popup
    @AdminRole({AdminRoles.en_product, AdminRoles.cn_product, AdminRoles.ru_product, AdminRoles.en_contents, AdminRoles.cn_contents, AdminRoles.ru_contents})
    @RequestMapping("/search")
    public String itemSearchList(@PathVariable("language") String language, Model model){
        if (!LanguageCheck.languageCheck(language)) {
            model.addAttribute("info", "The wrong approach.");
            return "/admin/error";
        }
        return "redirect:/admin/"+language+"/item/search/0";
    }

    //Sempio Item popup -검색
    @AdminRole({AdminRoles.en_product, AdminRoles.cn_product, AdminRoles.ru_product, AdminRoles.en_contents, AdminRoles.cn_contents, AdminRoles.ru_contents})
    @RequestMapping("/search/{page}")
    public String itemSearch(@PathVariable("language") String language, @PathVariable("page") int page,
            ItemSearchForm searchForm, Model model){
        if (!LanguageCheck.languageCheck(language)) {
            model.addAttribute("info", "The wrong approach.");
            return "/admin/error";
        }

        List<IntBrand> brandList = intBrandService.listAllWithCategory(language);

        Pager pager = new Pager(page, Const.ADMIN_LIST_SIZE);
        List<IntItem> itemList = intItemService.listForAdmin(Language.valueOf(language), searchForm, pager);

        int count = 0;
        count = intItemService.countForAdmin(Language.valueOf(language), searchForm);

        String link = "/admin/"+ language + "/item/search";

        Builder builder = Paging.builder();
        builder.currentPage(page)
        .entriesPerPage(Const.ADMIN_LIST_SIZE)
        .totalCount(count)
        .link(link);

        model.addAttribute("paging", searchForm.addQueryParams(builder).build());
        model.addAttribute("searchForm", searchForm);
        model.addAttribute("intItemList", itemList);
        model.addAttribute("intBrandList", brandList);
        model.addAttribute("language", language);

        return "/admin/item/search";
    }
}
