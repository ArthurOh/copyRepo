package com.sempio.en.controller.admin;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.Language;
import com.sempio.en.code.Result;
import com.sempio.en.code.State;
import com.sempio.en.code.VisualType;
import com.sempio.en.command.IntVisualContentsSearchForm;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntVisualContents;
import com.sempio.en.interceptor.AdminRole;
import com.sempio.en.service.IntAdminUserService;
import com.sempio.en.service.IntVisualContentsService;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.JsonConverter;
import com.sempio.en.util.Logics;
import com.sempio.en.util.Pager;
import com.sempio.en.util.Paging;
import com.sempio.en.util.Paging.Builder;
import com.sempio.en.util.ViewMessage;

@Controller
@AdminRole({ AdminRoles.en_display, AdminRoles.cn_display, AdminRoles.ru_display })
@RequestMapping("/admin/{language}/visual-contents/")
public class IntVisualContentsController {

    @Autowired
    private IntVisualContentsService visualContentsService;

    @Autowired
    private IntAdminUserService adminUserService;

    /**
     * 상단배너/하단배너 조회
     * @param language en|cn|ru
     * @param type top|bottom|popup
     * @param attrs
     * @param model
     * @return
     */
    @RequestMapping(value = "/{type:top|bottom}/list")
    public String list(@PathVariable String language,
                       @PathVariable String type,
                       RedirectAttributes attrs,
                       Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        Language currentLanguage = Language.valueOf(language);
        VisualType currentType = VisualType.valueOf(type);

        List<IntVisualContents> list = null;
        if (currentType == VisualType.top) {
            list = visualContentsService.selectTopList(currentLanguage, false);
        } else if (currentType == VisualType.bottom) {
            list = visualContentsService.selectBottomList(currentLanguage, false);
        }

        model.addAttribute("list", list);
        model.addAttribute("language", language);
        return "admin/visual_contents/" + type + "_list";
    }

    @RequestMapping(value = "/popup/list")
    public String popupList(@PathVariable String language,
                            IntVisualContentsSearchForm form,
                            @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                            @RequestParam(value = "size", required = false, defaultValue = "10") int size,
                            RedirectAttributes attrs,
                            Model model) {
        return popupList(language, page, size, form, attrs, model);
    }

    @RequestMapping(value = "/popup/list/{page}")
    public String popupList(@PathVariable String language,
                            @PathVariable int page,
                            @RequestParam(value = "size", required = false, defaultValue = "10") int size,
                            IntVisualContentsSearchForm form,
                            RedirectAttributes attrs,
                            Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        Language currentLanguage = Language.valueOf(language);

        Pager pager = new Pager(page, size);

        List<IntVisualContents> list = visualContentsService.selectPopupList(currentLanguage, form, pager, false);
        int count = visualContentsService.countByPopup(currentLanguage, form, false);

        Builder builder = Paging.builder();
        form.addQueryParams(builder);
        Paging paging = builder.currentPage(page)
               .entriesPerPage(size)
               .totalCount(count)
               .link("/admin/"+language+"/visual-contents/popup/list/").build();

        model.addAttribute("list", list);
        model.addAttribute("paging", paging);
        model.addAttribute("language", language);
        model.addAttribute("form", form);
        return "admin/visual_contents/popup_list";
    }

    /**
     * 상단배너/하단배너/팝업 상세
     * @param language en|cn|ru
     * @param type top|bottom|popup
     * @param id
     * @param attrs
     * @param model
     * @return
     */
    @RequestMapping(value = "/{type:top|bottom|popup}/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable String language,
                       @PathVariable String type,
                       @PathVariable int id,
                       @RequestParam(required = false, value = "qs") String qs,
                       RedirectAttributes attrs,
                       Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        IntVisualContents item = visualContentsService.select(id);
        if (item == null || item.getState() == State.deleted) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            attrs.addFlashAttribute("language", language);
            attrs.addFlashAttribute("type", type);
            return "redirect:/admin/{language}/visual-contents/{type}/list";
        }

        model.addAttribute("item", item);
        model.addAttribute("language", language);
        model.addAttribute("qs", qs);
        return "admin/visual_contents/" + type + "_edit";
    }

    /**
     * 상단배너/하단배너/팝업 수정
     * @param language en|cn|ru
     * @param type top|bottom|popup
     * @param id
     * @param attrs
     * @param model
     * @return
     */
    @RequestMapping(value = "/{type:top|bottom|popup}/edit/{id}", method = RequestMethod.POST)
    public String edit(@PathVariable String language,
                       @PathVariable String type,
                       @PathVariable int id,
                       @RequestParam(required = false, value = "qs") String qs,
                       IntVisualContents form,
                       RedirectAttributes attrs,
                       HttpServletRequest request,
                       Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        IntAdminUser loginUser = Logics.adminFromSession(request.getSession());
        form.setRewriter(loginUser.getUserId());
        form.setUpdateAt(new Date());
        form.setId(id);

        Result<String> result = visualContentsService.update(form);

        attrs.addFlashAttribute("language", language);
        attrs.addFlashAttribute("type", type);

        if (result.isError()) {
            ViewMessage.error().message("Editing failed.").register(attrs);
            return "redirect:/admin/{language}/visual-contents/{type}/list";
        }

        adminUserService.logAction(loginUser.getUserId(), HttpUtils.ip(request), StringUtils.join("(",language,") ","modify visual-contents-", type, " : ", String.valueOf(id)," / ", form.getTitle()));

        ViewMessage.success().message("Changes have been reflected").register(attrs);
        if (StringUtils.equals(type, VisualType.popup.name())) {
            attrs.addFlashAttribute("id", id);
            if (StringUtils.isNoneBlank(qs)) {
                attrs.addAttribute("qs", qs);
                return "redirect:/admin/{language}/visual-contents/{type}/edit/{id}?qs={qs}";
            }
            return "redirect:/admin/{language}/visual-contents/{type}/edit/{id}";
        }

        return "redirect:/admin/{language}/visual-contents/{type}/list";
    }

    @RequestMapping(value = "/popup/edit", method = RequestMethod.GET)
    public String edit(@PathVariable String language,
                       RedirectAttributes attrs,
                       @RequestParam(required = false, value = "qs") String qs,
                       Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }
        model.addAttribute("qs", qs);
        return "admin/visual_contents/popup_edit";
    }

    @RequestMapping(value = "/popup/edit", method = RequestMethod.POST)
    public String edit(@PathVariable String language,
                       IntVisualContents form,
                       RedirectAttributes attrs,
                       HttpServletRequest request,
                       Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        IntAdminUser loginUser = Logics.adminFromSession(request.getSession());
        form.setWriter(loginUser.getUserId());
        form.setCreateAt(new Date());
        form.setType(VisualType.popup);

        visualContentsService.insert(form);

        adminUserService.logAction(loginUser.getUserId(), HttpUtils.ip(request), StringUtils.join("(",language,") ","create visual-contents-popup : ", String.valueOf(form.getId()), " / ", form.getTitle()));

        ViewMessage.success().message("Registered.").register(attrs);
        return "redirect:/admin/{language}/visual-contents/popup/list/0";
    }


    /**
     * 순서 변경
     * @param language en|cn|ru
     * @param type top|bottom
     * @param data json string
     * @param attrs
     * @return
     */
    @RequestMapping(value = "/{type:top|bottom}/ordering", method = RequestMethod.POST)
    public String updateOrderNum(@PathVariable String language,
                                 @PathVariable String type,
                                 @RequestParam(name="data", required = true) String data,
                                 RedirectAttributes attrs) {

        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        List<Map<String, Object>> updateOrders = JsonConverter.toList(data);
        visualContentsService.updateOrdering(updateOrders, Language.valueOf(language));

        ViewMessage.success().message("The order is changed.").register(attrs);

        attrs.addFlashAttribute("language", language);
        attrs.addFlashAttribute("type", type);
        return "redirect:/admin/{language}/visual-contents/{type}/list";
    }
}
