package com.sempio.en.controller.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.Language;
import com.sempio.en.code.Result;
import com.sempio.en.command.IntNewsletterSearchForm;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntNewsletter;
import com.sempio.en.interceptor.AdminRole;
import com.sempio.en.service.IntAdminUserService;
import com.sempio.en.service.IntNewsletterService;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.Logics;
import com.sempio.en.util.Pager;
import com.sempio.en.util.Paging;
import com.sempio.en.util.Paging.Builder;
import com.sempio.en.util.ViewMessage;

@Controller
@AdminRole({AdminRoles.en_contents, AdminRoles.cn_contents, AdminRoles.ru_contents})
@RequestMapping("/admin/{language}/newsletter")
public class IntNewsletterController {

    private static Logger log = LoggerFactory.getLogger(IntNewsletterController.class);

    @Autowired
    private IntNewsletterService newsletterService;

    @Autowired
    private IntAdminUserService adminUserService;

    @RequestMapping(value = "/list")
    public String list(@PathVariable String language,
                      IntNewsletterSearchForm form,
                      @RequestParam(value = "size", required = false, defaultValue = "10") int size,
                      @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                      RedirectAttributes attrs,
                      Model model) {
        return list(language, page, form, size, attrs, model);
    }

    /**
     * 뉴스레터 조회
     * @param language (en|cn|ru)
     * @param page
     * @param form
     * @param size
     * @param attrs
     * @param model
     * @return
     */
    @RequestMapping(value = "/list/{page}")
    public String list(@PathVariable String language,
                       @PathVariable int page,
                       IntNewsletterSearchForm form,
                       @RequestParam(value = "size", required = false, defaultValue = "10") int size,
                       RedirectAttributes attrs,
                       Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }
        Language languageType = Language.valueOf(language);

        Pager pager = new Pager(page, size);
        List<IntNewsletter> list = newsletterService.list(form, languageType, pager);
        int count = newsletterService.count(form, languageType);

        Builder builder = Paging.builder();
        form.addQueryParams(builder);
        Paging paging = builder.currentPage(page)
               .entriesPerPage(size)
               .totalCount(count)
               .link("/admin/"+language+"/newsletter/list/").build();

        model.addAttribute("form", form);
        model.addAttribute("list", list);
        model.addAttribute("count", count);
        model.addAttribute("paging", paging);
        model.addAttribute("language", language);
        return "admin/newsletter/list";
    }

    /**
     * 뉴스레터 상세
     * @param language (en|cn|ru)
     * @param id
     * @param attrs
     * @param model
     * @return
     */
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable String language,
                       @PathVariable int id,
                       @RequestParam(required = false, value = "qs") String qs,
                       RedirectAttributes attrs,
                       Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }
        Result<IntNewsletter> result = newsletterService.select(id);
        if (result.isError()) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin/"+language+"/newsletter/list";
        }
        model.addAttribute("item", result.payload());
        model.addAttribute("language", language);
        model.addAttribute("qs", qs);
        return "admin/newsletter/edit";
    }

    /**
     * 뉴스레터 수정
     * @param language (en|cn|ru)
     * @param id
     * @param form
     * @param attrs
     * @param model
     * @return
     */
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String edit(@PathVariable String language,
                       @PathVariable int id,
                       IntNewsletter form,
                       @RequestParam(required = false, value = "qs") String qs,
                       HttpServletRequest request,
                       RedirectAttributes attrs,
                       Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        IntAdminUser loginUser = Logics.adminFromSession(request.getSession());
        form.setRewriter(loginUser);

        Result<String> result = newsletterService.update(form);
        if (result.isError()) {
            ViewMessage.error().message(result.payload()).register(attrs);
            return "redirect:/admin/newsletter/list";
        }

        ViewMessage.success().message("Changes have been reflected.").register(attrs);
        attrs.addAttribute("id", id);
        attrs.addAttribute("language", language);

        adminUserService.logAction(loginUser.getUserId(), HttpUtils.ip(request), StringUtils.join("(",language,") ", "modify newsletter : ", String.valueOf(id)," / ", form.getTitleInt()));

        if (StringUtils.isNoneBlank(qs)) {
            attrs.addAttribute("qs", qs);
            return "redirect:/admin/{language}/newsletter/edit/{id}?qs={qs}";
        }

        return "redirect:/admin/{language}/newsletter/edit/{id}";
    }

    /**
     * 뉴스레터 등록 폼
     * @param language (en|cn|ru)
     * @param attrs
     * @param model
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String write(@PathVariable String language,
                        @RequestParam(required = false, value = "qs") String qs,
                        RedirectAttributes attrs,
                        Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }
        model.addAttribute("language", language);
        model.addAttribute("qs", qs);
        return "admin/newsletter/edit";
    }

    /**
     * 뉴스레터 등록
     * @param language (en|cn|ru)
     * @param form
     * @param session
     * @param attrs
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String write(@PathVariable String language, IntNewsletter form, HttpServletRequest request, RedirectAttributes attrs) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }
        IntAdminUser loginUser = Logics.adminFromSession(request.getSession());
        form.setWriter(loginUser);

        newsletterService.insert(form);
        ViewMessage.success().message(" It is registered.").register(attrs);

        adminUserService.logAction(loginUser.getUserId(), HttpUtils.ip(request), StringUtils.join("(",language,") ","create newsletter : ", String.valueOf(form.getId())," / ",form.getTitleInt()));

        attrs.addFlashAttribute("language", language);
        return "redirect:/admin/{language}/newsletter/list";

    }
}
