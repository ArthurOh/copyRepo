
package com.sempio.en.controller.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriUtils;

import com.google.common.collect.Maps;
import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntBrand;
import com.sempio.en.interceptor.AdminRole;
import com.sempio.en.service.IntAdminUserService;
import com.sempio.en.service.IntBrandCategoryService;
import com.sempio.en.service.IntBrandService;
import com.sempio.en.util.Const;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.Logics;
import com.sempio.en.util.Paging;
import com.sempio.en.util.Paging.Builder;
import com.sempio.en.util.ViewMessage;

@Controller
@AdminRole({AdminRoles.en_product, AdminRoles.cn_product, AdminRoles.ru_product})
@RequestMapping("/admin/{language}/brand")
public class IntBrandController {
    private static Logger log = LoggerFactory.getLogger(IntBrandController.class);

    @Autowired
    private IntBrandService intBrandService;

    @Autowired
    private IntBrandCategoryService intBrandCategoryService;

    @Autowired
    private IntAdminUserService adminUserService;

    @RequestMapping(value = {"", "/list"})
    public String index(@PathVariable("language") String language, RedirectAttributes attrs, Model model) {
        return list(language, 0, attrs, model);
    }

    // 리스트
    @RequestMapping(value="/list/{page}")
    public String list(@PathVariable("language") String language,
            @PathVariable("page") int page, RedirectAttributes attrs, Model model) {

        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        int count = 0;
        List<IntBrand> brand = intBrandService.listForAdmin(Language.valueOf(language), Const.ADMIN_LIST_SIZE, page);
        count = intBrandService.countForAdmin(Language.valueOf(language));

        model.addAttribute("brand", brand);
        model.addAttribute("count", count);

        Builder pagingBuilder = Paging.builder()
                .currentPage(page)
                .totalCount(count)
                .entriesPerPage(Const.ADMIN_LIST_SIZE)
                .link("/admin/"+ language +"/brand/list");

        model.addAttribute("paging", pagingBuilder.build());

        model.addAttribute("language", language);

        return "/admin/brand/list";
    }

    // 쓰기
    @RequestMapping(value = "/create")
    public String create(@PathVariable("language") String language, RedirectAttributes attrs, Model model) {

        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        model.addAttribute("pageType", "create");
        model.addAttribute("language", language);

        return "/admin/brand/edit";
    }

    // 쓰기 등록
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String createAction(@PathVariable("language") String language,
            @RequestParam(value = "brand_code") String code,
            @RequestParam(value = "brand_title") String title,
            @RequestParam(value = "brand_description") String description,
            @RequestParam(value = "brand_slogan") String slogan,
            @RequestParam(value = "point") String point,
            @RequestParam(value = "filepath_thumbnail", defaultValue = "") String logoImg,
            @RequestParam(value = "filepath_defaultImage", defaultValue = "") String pcImg,
            @RequestParam(value = "filepath_mobileImage", defaultValue = "") String MobileImg,
            @RequestParam(value = "filepath_banner", defaultValue = "") String bannerImg,
            @RequestParam(value = "relation_site_name") String[] relationSiteName,
            @RequestParam(value = "relation_site_url") String[] relationSiteUrl,
            @RequestParam(value = "publishAt") String publishDate,
            @RequestParam(value = "state") String state,
            HttpSession session, HttpServletRequest request, RedirectAttributes attrs) throws ParseException {

        long now = System.currentTimeMillis();
        Date createAt = new Date(now);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date publishAt = formatter.parse(publishDate);

        IntAdminUser admin = Logics.adminFromSession(session);
        String adminId =  admin.getUserId();

        for (int i=0; i<relationSiteUrl.length; i++){
            if (StringUtils.isBlank(relationSiteUrl[i])) {
                relationSiteName[i] = null;
                relationSiteUrl[i] = null;
            }
            i = i + 1;
        }

        IntBrand brand = new IntBrand();
        brand.setCode(code);
        brand.setTitle(title);
        brand.setDescription(description);
        brand.setSlogan(slogan);
        brand.setPoint(point);
        brand.setPcImgPath(pcImg);
        brand.setMobileImgPath(MobileImg);
        brand.setLogoImgPath(logoImg);
        brand.setBannerImgPath(bannerImg);
        brand.setRelationSiteName(relationSiteName);
        brand.setRelationSiteUrl(relationSiteUrl);
        brand.setPublishAt(publishAt);
        brand.setCreateAt(createAt);
        brand.setWriter(adminId);
        brand.setState(State.valueOf(state));
        brand.setLanguage(Language.valueOf(language));

        log.debug("input value = {}", brand);

        Integer id = intBrandService.create(brand);

        ViewMessage.success().message("It is registered.").register(attrs);

        adminUserService.logAction(adminId, HttpUtils.ip(request), "("+ language +") create brand : " + title + " / " + admin.getId());

        return "redirect:/admin/"+ language +"/brand/category/"+id;
    }

    // 수정
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editView(@PathVariable("language") String language,
            @PathVariable("id") Integer id,
            @RequestParam(value = "page", defaultValue = "0") int page, RedirectAttributes attrs, Model model) {

        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        IntBrand brand = intBrandService.load(id);
        model.addAttribute("brand", brand);
        model.addAttribute("pageType", "edit");
        model.addAttribute("page", page);
        model.addAttribute("language", language);

        if (brand.getRelationSiteName() != null) {
            Integer relationLength = brand.getRelationSiteName().length;
            model.addAttribute("relationLength", relationLength);

            String[] relationSiteName = brand.getRelationSiteName();
            String[] relationSiteUrl = brand.getRelationSiteUrl();
            model.addAttribute("relationSiteName", relationSiteName);
            model.addAttribute("relationSiteUrl", relationSiteUrl);
        }
        return "/admin/brand/edit";
    }

    // 수정 등록
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String editModify(@PathVariable("language") String language,
            @PathVariable("id") Integer id,
            @RequestParam(value = "brand_title") String title,
            @RequestParam(value = "brand_description") String description,
            @RequestParam(value = "brand_slogan") String slogan,
            @RequestParam(value = "point") String point,
            @RequestParam(value = "filepath_thumbnail", defaultValue = "") String logoImg,
            @RequestParam(value = "filepath_defaultImage", defaultValue = "") String pcImg,
            @RequestParam(value = "filepath_mobileImage", defaultValue = "") String MobileImg,
            @RequestParam(value = "filepath_banner", defaultValue = "") String bannerImg,
            @RequestParam(value = "relation_site_name") String[] relationSiteName,
            @RequestParam(value = "relation_site_url") String[] relationSiteUrl,
            @RequestParam(value = "publishAt") String publishDate,
            @RequestParam(value = "state") String state,
            @RequestParam(value = "page", defaultValue = "0") int page,
            HttpSession session, RedirectAttributes attrs, HttpServletRequest request) throws Exception {

        long now = System.currentTimeMillis();
        Date updateAt = new Date(now);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date publishAt = formatter.parse(publishDate);

        IntAdminUser admin = Logics.adminFromSession(session);
        String adminId =  admin.getUserId();

        for (int i=0; i<relationSiteUrl.length; i++){
            if (StringUtils.isBlank(relationSiteUrl[i])) {
                relationSiteName[i] = null;
                relationSiteUrl[i] = null;
            }
            i = i + 1;
        }

        IntBrand brand = new IntBrand();
        brand.setId(id);
        brand.setTitle(title);
        brand.setDescription(description);
        brand.setSlogan(slogan);
        brand.setPoint(point);
        brand.setPcImgPath(pcImg);
        brand.setMobileImgPath(MobileImg);
        brand.setLogoImgPath(logoImg);
        brand.setBannerImgPath(bannerImg);
        brand.setRelationSiteName(relationSiteName);
        brand.setRelationSiteUrl(relationSiteUrl);
        brand.setPublishAt(publishAt);
        brand.setUpdateAt(updateAt);
        brand.setRewriter(adminId);
        brand.setState(State.valueOf(state));

        log.debug("input value = {}", brand);

        intBrandService.updatePost(brand);

        ViewMessage.success().message("Changes have been reflected.").register(attrs);

        adminUserService.logAction(adminId, HttpUtils.ip(request), "("+ language +") modify brand : " + id + " / " + admin.getId());

        StringBuilder queryString = new StringBuilder();

        queryString.append("&page=").append(page);

        return "redirect:/admin/"+ language +"/brand/edit/" + id + StringUtils.join("?", UriUtils.encodeQuery(queryString.toString(), "utf-8"));
    }

    // 브랜드 코드 중복 확인
    @RequestMapping(value = "/checkCode/{code}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> checkCode(@PathVariable("language") String language, @PathVariable String code) {
        Map<String, Object> result = Maps.newHashMap();
        result.put("cnt", 0);
        IntBrand brand = intBrandService.checkCode(code, Language.valueOf(language));
        if (brand != null) {
            result.put("cnt", 1);
        }
        return result;
    }
}
