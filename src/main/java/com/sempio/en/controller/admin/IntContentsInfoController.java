package com.sempio.en.controller.admin;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.ContentsType;
import com.sempio.en.code.Language;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntContentsInfo;
import com.sempio.en.interceptor.AdminRole;
import com.sempio.en.service.IntAdminUserService;
import com.sempio.en.service.IntContentsInfoService;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.Logics;
import com.sempio.en.util.ViewMessage;

@Controller
@RequestMapping("/admin/{language}/contents-info")
public class IntContentsInfoController {

    @Autowired
    private IntContentsInfoService contentsInfoService;

    @Autowired
    private IntAdminUserService adminUserService;

    @AdminRole({ AdminRoles.en_contents, AdminRoles.cn_contents, AdminRoles.ru_contents })
    @RequestMapping(value = "/{type}/edit", method = RequestMethod.GET)
    public String edit(@PathVariable String language, @PathVariable String type, RedirectAttributes attrs, Model model) {

        if (!Language.contains(language) || !ContentsType.contains(type)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        Language currentLanguage = Language.valueOf(language);
        ContentsType currentType = ContentsType.valueOf(type);

        IntContentsInfo item = contentsInfoService.select(currentType, currentLanguage);
        if (item != null) {
            model.addAttribute("item", item);
        }
        model.addAttribute("language", currentLanguage);
        model.addAttribute("type", currentType);
        return "admin/contents_info/edit";
    }

    @AdminRole({ AdminRoles.en_contents, AdminRoles.cn_contents, AdminRoles.ru_contents })
    @RequestMapping(value = "/{type}/edit", method = RequestMethod.POST)
    public String edit(@PathVariable String language,
                       @PathVariable String type,
                       IntContentsInfo form,
                       HttpServletRequest request,
                       RedirectAttributes attrs) {

        if (!Language.contains(language) || !ContentsType.contains(type)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        IntAdminUser loginUser = Logics.adminFromSession(request.getSession());
        if (form.getId() > 0) {
            form.setRewriter(loginUser);
        } else {
            form.setWriter(loginUser);
        }

        contentsInfoService.update(form);

        adminUserService.logAction(loginUser.getUserId(), HttpUtils.ip(request), StringUtils.join("(",language,") ","modify contents-info-", type, " : ", String.valueOf(form.getId())));

        ViewMessage.success().message("Changes have been reflected.").register(attrs);
        attrs.addFlashAttribute("language", language);
        attrs.addFlashAttribute("type", type);
        return "redirect:/admin/{language}/contents-info/{type}/edit";
    }
}
