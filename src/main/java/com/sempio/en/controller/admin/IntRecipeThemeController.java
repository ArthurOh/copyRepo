package com.sempio.en.controller.admin;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Maps;
import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.Language;
import com.sempio.en.code.Result;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntRecipeTheme;
import com.sempio.en.interceptor.AdminRole;
import com.sempio.en.service.IntAdminUserService;
import com.sempio.en.service.IntRecipeService;
import com.sempio.en.service.IntRecipeThemeService;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.JsonConverter;
import com.sempio.en.util.Logics;
import com.sempio.en.util.ViewMessage;

@Controller
@AdminRole({AdminRoles.en_display, AdminRoles.cn_display, AdminRoles.ru_display})
@RequestMapping("/admin/{language}/recipe-theme")
public class IntRecipeThemeController {

    @Autowired
    private IntRecipeThemeService intRecipeThemeService;

    @Autowired
    private IntRecipeService intRecipeService;

    @Autowired
    private IntAdminUserService adminUserService;

    @RequestMapping(value = "/list")
    public String list(@PathVariable String language, RedirectAttributes attrs, Model model) {

        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }
        Language languageType = Language.valueOf(language);

        List<IntRecipeTheme> list = intRecipeThemeService.list(languageType, null);
        int count = intRecipeThemeService.count(languageType, null);

        //레시피테마와 연관된 등록한 레시피 수(on,off 갯수)
        for(IntRecipeTheme item : list){
            int recipeCount = intRecipeService.recipeCountForTheme(item.getId());
            item.setRecipeCount(recipeCount);
        }

        model.addAttribute("list", list);
        model.addAttribute("count", count);
        model.addAttribute("language", language);
        return "admin/recipe_theme/list";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable String language,
                       @PathVariable int id,
                       RedirectAttributes attrs,
                       Model model) {

        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }
        Result<IntRecipeTheme> result = intRecipeThemeService.select(id);
        if (result.isError()) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            attrs.addFlashAttribute("language", language);
            return "redirect:/admin/{language}/recipe-theme/list";
        }
        int recipeCount = intRecipeService.recipeCountForTheme(result.payload().getId());
        result.payload().setRecipeCount(recipeCount);

        model.addAttribute("item", result.payload());
        model.addAttribute("language", language);
        return "admin/recipe_theme/edit";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String edit(@PathVariable String language,
                       @PathVariable int id,
                       IntRecipeTheme form,
                       HttpSession session, HttpServletRequest request,
                       RedirectAttributes attrs,
                       Model model) {

        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        IntAdminUser loginUser = Logics.adminFromSession(session);
        String adminId = loginUser.getUserId();
        form.setRewriter(loginUser.getUserId());

        Result<String> result = intRecipeThemeService.update(form);

        if (result.isError()) {
            ViewMessage.error().message(result.payload()).register(attrs);
            attrs.addFlashAttribute("language", language);
            return "redirect:/admin/{language}/recipe-theme/list";
        }

        ViewMessage.success().message("Changes have been reflected.").register(attrs);
        adminUserService.logAction(adminId, HttpUtils.ip(request),"("+language+")"+ "modify recipe theme : " + form.getTitle() + " / " + loginUser.getId());
        attrs.addAttribute("language", language);
        attrs.addAttribute("id", id);
        return "redirect:/admin/{language}/recipe-theme/edit/{id}";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String write(@PathVariable String language,
                        RedirectAttributes attrs,
                        Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }
        model.addAttribute("language", language);
        return "admin/recipe_theme/edit";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String edit(@PathVariable String language,
                       IntRecipeTheme form,
                       HttpSession session, HttpServletRequest request,
                       RedirectAttributes attrs,
                       Model model) {

        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        IntAdminUser loginUser = Logics.adminFromSession(session);
        String adminId = loginUser.getUserId();
        form.setWriter(loginUser.getUserId());

        intRecipeThemeService.insert(form);

        ViewMessage.success().message("Registered.").register(attrs);
        adminUserService.logAction(adminId, HttpUtils.ip(request),"("+language+")"+ "create recipe theme : " + form.getTitle() + " / " + loginUser.getId());

        attrs.addAttribute("language", language);
        return "redirect:/admin/{language}/recipe-theme/list";
    }

    @RequestMapping(value = "/ordering", method = RequestMethod.POST)
    public String updateProgramOrderNum(@PathVariable String language,
                                        @RequestParam(name="data", required = true) String data,
                                        RedirectAttributes attrs) {

        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        List<Map<String, Object>> updateOrders = JsonConverter.toList(data);
        intRecipeThemeService.updateOrdering(updateOrders, Language.valueOf(language));

        ViewMessage.success().message("The order has changed.").register(attrs);

        attrs.addFlashAttribute("language", language);
        return "redirect:/admin/{language}/recipe-theme/list";
    }

    @RequestMapping(value = "/check/{code}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> checkId(@PathVariable String code, @PathVariable String language) {
        Map<String, Object> result = Maps.newHashMap();
        result.put("cnt", 0);
        IntRecipeTheme item = intRecipeThemeService.selectWithCode(Language.valueOf(language), code);
        if (item != null) {
            result.put("cnt", 1);
        }
        return result;
    }
}
