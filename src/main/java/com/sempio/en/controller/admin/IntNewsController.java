package com.sempio.en.controller.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriUtils;

import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.entity.IntNews;
import com.sempio.en.interceptor.AdminRole;
import com.sempio.en.service.FileService;
import com.sempio.en.service.IntAdminUserService;
import com.sempio.en.service.IntNewsService;
import com.sempio.en.util.Const;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.Logics;
import com.sempio.en.util.Paging;
import com.sempio.en.util.Paging.Builder;
import com.sempio.en.util.ViewMessage;

@Controller
@AdminRole({AdminRoles.en_contents, AdminRoles.cn_contents, AdminRoles.ru_contents})
@RequestMapping("/admin/{language}/news")
public class IntNewsController {
    private static Logger log = LoggerFactory.getLogger(IntNewsController.class);

    @Autowired
    private IntNewsService intNewsService;

    @Autowired
    private FileService fileService;

    @Autowired
    private IntAdminUserService adminUserService;

    @RequestMapping(value = {"", "/list"})
    public String index(@PathVariable("language") String language, RedirectAttributes attrs, Model model) {
        return list(language, 0, null, null, attrs, model);
    }

    // 리스트
    @RequestMapping(value = "/list/{page}")
    public String list(@PathVariable("language") String language,
            @PathVariable("page") int page,
            @RequestParam(value = "q", defaultValue = "") String q,
            @RequestParam(value = "qType", defaultValue = "") String qType, RedirectAttributes attrs, Model model) {

        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        if(StringUtils.isNotBlank(q)){
            q = q.trim();
        }

        int count = 0;
        List<IntNews> news = intNewsService.listForAdmin(Language.valueOf(language), q, qType, Const.ADMIN_LIST_SIZE, page);
        count = intNewsService.countForAdmin(Language.valueOf(language), q, qType);

        model.addAttribute("news", news);
        model.addAttribute("count", count);

        model.addAttribute("q", q);
        model.addAttribute("qType", qType);

        Builder pagingBuilder = Paging.builder()
                .currentPage(page)
                .totalCount(count)
                .entriesPerPage(Const.ADMIN_LIST_SIZE)
                //.param("p")
                .link("/admin/"+ language +"/news/list");

        model.addAttribute("paging", pagingBuilder.build());
        model.addAttribute("language", language);

        return "/admin/news/list";
    }

    // 쓰기
    @RequestMapping(value = "/create")
    public String create(@PathVariable("language") String language, RedirectAttributes attrs, Model model) {
        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        model.addAttribute("pageType", "create");
        model.addAttribute("language", language);
        return "/admin/news/edit";

    }

    // 쓰기 등록
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String createAction(@PathVariable("language") String language,
            @RequestParam(value = "title") String title,
            @RequestParam(value = "contents") String contents,
            @RequestParam(value = "filepath_thumbnail", defaultValue = "") String listImg,
            @RequestParam(value = "publishAt") String publishDate,
            @RequestParam(value = "tags") String tags, @RequestParam(value = "state") State state,
            @RequestParam(value = "fileid_file", required = false) Integer[] attachmentid,
            HttpSession session,  RedirectAttributes attrs, HttpServletRequest request) throws ParseException {

        if(tags.length() > 200){
            ViewMessage.error().message("You can insert up to 200 letters for tag.").register(attrs);
            return "redirect:/admin/"+ language +"/news/create";
        }

        IntAdminUser admin = Logics.adminFromSession(session);
        String adminId =  admin.getUserId();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date publishAt = formatter.parse(publishDate);

        long now = System.currentTimeMillis();
        Date lastModifiedAt = new Date(now);

        IntNews news = new IntNews();
        news.setTitle(title);
        news.setContents(contents);
        news.setPublishAt(publishAt);
        news.setCreateAt(lastModifiedAt);
        news.setTags(tags);
        news.setListImgPath(listImg);
        news.setState(state);
        news.setWriter(adminId);
        news.setLanguage(Language.valueOf(language));

        log.debug("input value = {}", news);

        intNewsService.create(news, attachmentid);

        adminUserService.logAction(adminId, HttpUtils.ip(request), "("+ language +") create news : " + title + " / " + admin.getId());

        return "redirect:/admin/"+ language +"/news";
    }

    // 수정
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editView(@PathVariable("language") String language,
            @PathVariable("id") Integer id,
            @RequestParam(value = "q", defaultValue = "") String q,
            @RequestParam(value = "qType", defaultValue = "") String qType,
            @RequestParam(value = "page", defaultValue = "0") int page, RedirectAttributes attrs, Model model) {

        if (!Language.contains(language)) {
            ViewMessage.error().message("Invalid access.").register(attrs);
            return "redirect:/admin";
        }

        IntNews news = intNewsService.load(id);

        model.addAttribute("news", news);
        model.addAttribute("pageType", "edit");

        model.addAttribute("q", q);
        model.addAttribute("qType", qType);
        model.addAttribute("page", page);

        model.addAttribute("language", language);

        return "/admin/news/edit";
    }

    // 수정 등록
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String editModify(@PathVariable("language") String language,
            @PathVariable("id") Integer id,
            @RequestParam(value = "title") String title,
            @RequestParam(value = "contents") String contents,
            @RequestParam(value = "filepath_thumbnail", defaultValue = "") String listImg,
            @RequestParam(value = "publishAt") String publishDate,
            @RequestParam(value = "tags") String tags,
            @RequestParam(value = "state") State state,
            @RequestParam(value = "fileid_file", defaultValue = "") Integer[] attachmentid,
            @RequestParam(value = "q", defaultValue = "") String q,
            @RequestParam(value = "qType", defaultValue = "") String qType,
            @RequestParam(value = "page", defaultValue = "0") int page,
            RedirectAttributes attrs, HttpSession session, HttpServletRequest request) throws Exception {

        IntAdminUser admin = Logics.adminFromSession(session);
        String adminId =  admin.getUserId();

        if(tags.length() > 200){
            ViewMessage.error().message("You can insert up to 200 letters for tag.").register(attrs);
            return "redirect:/admin/"+ language +"/news/edit/" + id;
        }

        long now = System.currentTimeMillis();
        Date updateAt = new Date(now);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date publishAt = formatter.parse(publishDate);

        IntNews news = new IntNews();
        news.setId(id);
        news.setTitle(title);
        news.setContents(contents);
        news.setListImgPath(listImg);
        news.setPublishAt(publishAt);
        news.setTags(tags);
        news.setState(state);
        news.setUpdateAt(updateAt);
        news.setRewriter(adminId);

        log.debug("input value = {}", news);

        intNewsService.updatePost(news);

        if (attachmentid.length > 0) {
            fileService.updatePostId(attachmentid, id);
        }

        ViewMessage.success().message("Your changes have been successfully fixed").register(attrs);

        StringBuilder queryString = new StringBuilder();
        queryString.append("q=").append(q);
        queryString.append("&qType=").append(qType);
        queryString.append("&page=").append(page);

        adminUserService.logAction(adminId, HttpUtils.ip(request), "("+ language +") modify news : " + id + " / " + admin.getId());

        return "redirect:/admin/"+ language +"/news/edit/" + id + StringUtils.join("?", UriUtils.encodeQuery(queryString.toString(), "utf-8"));
    }
}
