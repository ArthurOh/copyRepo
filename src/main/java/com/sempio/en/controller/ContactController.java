package com.sempio.en.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Maps;
import com.sempio.en.code.ContentsType;
import com.sempio.en.code.Language;
import com.sempio.en.code.MailTemplateType;
import com.sempio.en.code.State;
import com.sempio.en.entity.IntContentsInfo;
import com.sempio.en.entity.IntEmailContactLog;
import com.sempio.en.service.IntContentsInfoService;
import com.sempio.en.service.IntEmailContactLogService;
import com.sempio.en.service.MailDefaultService;
import com.sempio.en.util.HttpUtils;
import com.sempio.en.util.ViewMessage;

@Controller
@RequestMapping("/contact")
public class ContactController {

    @Autowired
    private IntContentsInfoService contentsInfoService;

    @Autowired
    private IntEmailContactLogService emailContactLogService;

    @Autowired
    private MailDefaultService mailService;

    @Autowired
    private Environment environment;

    @RequestMapping(value = "/info")
    public String info(Model model) {
        IntContentsInfo item = contentsInfoService.select(ContentsType.contact, Language.en);
        model.addAttribute("item", item);
        return "contact/info";
    }

    @RequestMapping(value = "/send-email", method = RequestMethod.POST)
    public String email(IntEmailContactLog form,
                        HttpServletRequest request,
                        RedirectAttributes attrs) {
        Map<String, Object> args = Maps.newHashMap();
        args.put("item", form);
        args.put("site", HttpUtils.getDoamin(request));

        String to = environment.getProperty("email.contactus.en");
        if (StringUtils.isBlank(to)) {
            boolean isProd = environment.acceptsProfiles("prod");
            if (isProd) {
                to = "intl@sempio.com";
            } else {
                to = "christina@iropke.com";
            }
        }
        boolean result = mailService.send(to, "[sempio] E-mail Contact Form", MailTemplateType.contact, args);
        if (result) {
            form.setState(State.end);
            form.setLanguage(Language.en);
            int id = emailContactLogService.insert(form);
            if (id > 0) {
                ViewMessage.success().message("Your inquiry is submitted. We will send you an email as soon as possible.").register(attrs);
                return "redirect:/contact/email";
            }
        }
        ViewMessage.error().message("Application failed.").register(attrs);
        return "redirect:/contact/email";
    }
}
