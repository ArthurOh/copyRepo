package com.sempio.en.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import com.sempio.en.service.StaticPageHolder;


@Controller
public class StaticPageController implements org.springframework.web.servlet.mvc.Controller {
    @Autowired
    private StaticPageHolder staticPageHolder;

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String p1 = request.getServletPath();
        String p2 = StringUtils.defaultString(request.getPathInfo(), "");
        String path = p1 + p2;

        if (!staticPageHolder.checkPathExists(path)) {
            // 404
            response.sendError(404);
            return null;
        }

        String templateName = "/" + staticPageHolder.getStaticpagebase() + path;
        ModelAndView mav = new ModelAndView(templateName);
        return mav;

    }
}
