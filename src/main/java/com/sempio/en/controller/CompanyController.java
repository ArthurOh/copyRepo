package com.sempio.en.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sempio.en.code.ContentsType;
import com.sempio.en.code.Language;
import com.sempio.en.entity.IntContentsInfo;
import com.sempio.en.service.IntContentsInfoService;

@Controller
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private IntContentsInfoService contentsInfoService;
    
    @RequestMapping(value = "/network")
    public String network(Model model) {
        IntContentsInfo item = contentsInfoService.select(ContentsType.network, Language.en);
        model.addAttribute("item", item);
        return "company/network";
    }
}
