package com.sempio.en.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sempio.en.code.Language;
import com.sempio.en.entity.IntBrand;
import com.sempio.en.entity.IntBrandCategory;
import com.sempio.en.entity.IntItem;
import com.sempio.en.entity.IntRecipe;
import com.sempio.en.service.IntBrandCategoryService;
import com.sempio.en.service.IntBrandService;
import com.sempio.en.service.IntItemService;
import com.sempio.en.service.IntRecipeService;
import com.sempio.en.util.ViewMessage;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private IntBrandService intBrandService;

    @Autowired
    private IntBrandCategoryService intBrandCategoryService;

    @Autowired
    private IntItemService intItemService;

    @Autowired
    private IntRecipeService intRecipeService;

    @RequestMapping("/{code}")
    public String index(@PathVariable("code") String code, RedirectAttributes attrs,  Model model){
        IntBrand brand = intBrandService.getOne(Language.en, code);
        if (brand == null) {
            ViewMessage.success().message("Invalid access.").register(attrs);
            return "redirect:/";
        }
        model.addAttribute("brand", brand);

        List<IntBrandCategory> brandCategoryList = intBrandCategoryService.listForItem(brand.getId());
        model.addAttribute("brandCategoryList", brandCategoryList);

        Integer brandCategoryId = null;
        if (brandCategoryList.size() > 0) {
            brandCategoryId = intBrandCategoryService.getLastId(brand.getId()).getId();
            if (brandCategoryId != null) {
                List<IntItem> item = intItemService.listForItem(brandCategoryId);
                model.addAttribute("item", item);
            }

            IntBrandCategory bc = intBrandCategoryService.load(brandCategoryId);
            String bcTitle = bc.getTitle();
            String bcDescription = bc.getDescription();
            model.addAttribute("bcTitle", bcTitle);
            model.addAttribute("bcDescription", bcDescription);
        }

        if (brand.getRelationSiteName() != null) {
            Integer relationLength = brand.getRelationSiteName().length;
            model.addAttribute("relationLength", relationLength);

            String[] relationSiteName = brand.getRelationSiteName();
            String[] relationSiteUrl = brand.getRelationSiteUrl();
            model.addAttribute("relationSiteName", relationSiteName);
            model.addAttribute("relationSiteUrl", relationSiteUrl);
        }

        model.addAttribute("code", code);

        return "/product/index";
    }

    @RequestMapping("{code}/{id}")
    public String indexApi(@PathVariable("code") String code, @PathVariable("id") Integer brandCategoryId,  RedirectAttributes attrs,Model model) {
        IntBrand brand = intBrandService.getOne(Language.en, code);
        if (brand == null) {
            ViewMessage.success().message("Invalid access.").register(attrs);
            return "redirect:/";
        }

        List<IntBrandCategory> brandCategoryList = intBrandCategoryService.listForItem(brand.getId());
        model.addAttribute("brandCategoryList", brandCategoryList);

        IntBrandCategory bc = intBrandCategoryService.load(brandCategoryId);
        String bcTitle = bc.getTitle();
        String bcDescription = bc.getDescription();
        model.addAttribute("bcTitle", bcTitle);
        model.addAttribute("bcDescription", bcDescription);

        List<IntItem> item = intItemService.listForItem(brandCategoryId);
        model.addAttribute("item", item);

        model.addAttribute("code", code);

        return "/product/list";
    }


    @RequestMapping("/{code}/view/{id}")
    public String view(@PathVariable("code") String code, @PathVariable("id") Integer id, RedirectAttributes attrs, Model model){
        IntBrand brand = intBrandService.getOne(Language.en, code);
        if (brand == null) {
            ViewMessage.success().message("Invalid access.").register(attrs);
            return "redirect:/";
        }
        //제품 정보
        IntItem item = intItemService.getOne(Language.en, id);
        model.addAttribute("item", item);

        // 배너 정보
        String brandBannerImg = item.getIntBrandCategory().getIntBrand().getBannerImgPath();
        model.addAttribute("brandBannerImg", brandBannerImg);

        // gnb type
        String gnbCode = item.getIntBrandCategory().getIntBrand().getCode();
        model.addAttribute("gnbCode", gnbCode);

        // 레시피 정보
        if (item.getRecommendRecipe() != null) {
            String[] ids = StringUtils.split(item.getRecommendRecipe(), ",");
            Integer[] recipeId = new Integer[ids.length];
            for (int i=0; i<ids.length; i++) {
                recipeId[i] = Integer.parseInt(ids[i]);
            }

            if (recipeId.length > 0) {
                List<IntRecipe> recipe = intRecipeService.listForItem(recipeId);
                model.addAttribute("recipe", recipe);
            }
        }

        return "/product/view";
    }
}
