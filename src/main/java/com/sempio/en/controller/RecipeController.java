package com.sempio.en.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.entity.IntBrand;
import com.sempio.en.entity.IntRecipe;
import com.sempio.en.entity.IntRecipeTheme;
import com.sempio.en.service.IntBrandService;
import com.sempio.en.service.IntRecipeService;
import com.sempio.en.service.IntRecipeThemeService;
import com.sempio.en.util.Const;
import com.sempio.en.util.Pager;
import com.sempio.en.util.Paging;
import com.sempio.en.util.Paging.Builder;
import com.sempio.en.util.ViewMessage;

@Controller
@RequestMapping("/recipe")
public class RecipeController {

    @Autowired
    private IntRecipeService intRecipeService;

    @Autowired
    private IntRecipeThemeService intRecipeThemeService;

    @Autowired
    private IntBrandService intBrandService;

    @RequestMapping({"","/index"})
    public String index(Model model){

        List<IntRecipeTheme> intRecipeThemeList = intRecipeThemeService.list(Language.en, State.on);
        List<IntBrand> intBrandList = intBrandService.listAll(Language.en, State.on);

        model.addAttribute("intRecipeThemeList", intRecipeThemeList);
        model.addAttribute("intBrandList", intBrandList);

        return "/recipe/index";
    }

    @RequestMapping("/list/{type}/{recipeTheme}")
    public String list(@PathVariable("type") String type,
            @PathVariable("recipeTheme") String recipeTheme, RedirectAttributes attrs, Model model ){

        return themeList(model, attrs, type, recipeTheme, 0);
    }


    @RequestMapping("/list/{type}/{recipeTheme}/{page}")
    public String themeList(Model model,
            RedirectAttributes attrs,
            @PathVariable("type") String type,
            @PathVariable("recipeTheme") String recipeTheme,
            @PathVariable("page") Integer page){

        List<IntRecipe> intRecipeList = null;
        int count =0;
        Pager pager = new Pager(page, Const.ADMIN_LIST_SIZE);

        String link = "/recipe/list/";
        link += type + "/" + recipeTheme;
        String nav = "0";

        IntRecipeTheme theme = intRecipeThemeService.selectWithCode(Language.en, recipeTheme);
        IntBrand brand = null;
        if(StringUtils.equals(type, "theme")){
            count = intRecipeService.recipeCountWithTheme(Language.en, recipeTheme);
            intRecipeList = intRecipeService.recipeListWithTheme(Language.en, recipeTheme, pager);

            if(StringUtils.equals("vegetarian", recipeTheme)){
                nav = "4";
            } else if(StringUtils.equals("meat", recipeTheme)){
                nav = "2";
            } else if(StringUtils.equals("seafood", recipeTheme)){
                nav = "3";
            } else if(StringUtils.equals("influencer", recipeTheme)){
                nav = "5";
            } else {
                nav = "";
            }
        } else if(StringUtils.equals(type, "product")) {
            brand = intBrandService.getOne(Language.en, recipeTheme);
            intRecipeList = intRecipeService.recipeListWithBrand(Language.en, recipeTheme, pager);
            count = intRecipeService.recipeCountWithBrand(Language.en, recipeTheme);
        } else {
            ViewMessage.success().message("Invalid access.").register(attrs);
            return "redirect:/recipe";
        }

        Builder builder = Paging.builder();
        builder.currentPage(page)
        .entriesPerPage(Const.ADMIN_LIST_SIZE)
        .totalCount(count)
        .link(link);

        model.addAttribute("intRecipeList", intRecipeList);
        model.addAttribute("paging", builder.build());
        model.addAttribute("type", type);
        model.addAttribute("nav", nav);
        model.addAttribute("theme", theme);
        model.addAttribute("brand", brand);

        return "/recipe/list";
    }

    @RequestMapping("/{type:product|theme}/{value}/view/{id}")
    public String view(@PathVariable("type") String type, @PathVariable("value") String value, @PathVariable("id") Integer id, Model model){

        model.addAttribute("type", type);
        model.addAttribute("typeValue", value);
        return view(id, model);
    }

    @RequestMapping("/view/{id}")
    public String view(@PathVariable("id") Integer id, Model model){

        IntRecipe intRecipe = intRecipeService.selectOne(Language.en, id, true);

        //테마 리스트 찾기
        Integer[] ids = intRecipe.getIntRecipeTheme();
        List<IntRecipeTheme> themeList = intRecipeThemeService.list(Language.en, State.on, ids);

        //추천 레시피 리스트 찾기
        List<IntRecipe> recommendList = null;
        Integer[] recipeIds = intRecipe.getRecommend();
        if(recipeIds != null && recipeIds.length >0){
            recommendList = intRecipeService.selectList(Language.en, recipeIds, true);
        }

        //조회수 증가
        intRecipeService.updateViews(id);
        model.addAttribute("intRecipe", intRecipe);
        model.addAttribute("themeList", themeList);
        model.addAttribute("recommendList", recommendList);
        return "/recipe/detail";
    }

}
