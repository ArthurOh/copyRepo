package com.sempio.en.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sempio.en.code.Language;
import com.sempio.en.entity.IntItem;
import com.sempio.en.entity.IntNews;
import com.sempio.en.entity.IntNewsletter;
import com.sempio.en.entity.IntRecipe;
import com.sempio.en.service.IntItemService;
import com.sempio.en.service.IntNewsService;
import com.sempio.en.service.IntNewsletterService;
import com.sempio.en.service.IntRecipeService;
import com.sempio.en.util.Const;
import com.sempio.en.util.Pager;
import com.sempio.en.util.Paging;
import com.sempio.en.util.Paging.Builder;

@Controller
@RequestMapping("/search")
public class SearchController {

    @Autowired
    IntItemService intItemService;

    @Autowired
    IntRecipeService intRecipeService;

    @Autowired
    IntNewsService intNewsService;

    @Autowired
    IntNewsletterService intNewsletterService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index(@RequestParam(value = "q") String q, Model model) throws ParseException{
        q = q.trim();
        model.addAttribute("q", q);

        // Product 4개
        List<IntItem> itemList = intItemService.searchBy(q, Language.en, new Pager(0, 4));
        int itemCount = intItemService.countBy(q, Language.en);
        model.addAttribute("itemList", itemList);
        model.addAttribute("itemCount", itemCount);

        // Recipes 3개
        List<IntRecipe> recipeList = intRecipeService.searchBy(q, Language.en.toString(), new Pager(0,3));
        int recipeCount = intRecipeService.countBy(q, Language.en.toString());
        model.addAttribute("recipeList", recipeList);
        model.addAttribute("recipeCount", recipeCount);

        // News 3개
        List<IntNews> newsList = intNewsService.searchBy(q, Language.en, new Pager(0,3));
        int newsCount = intNewsService.countBy(q, Language.en);
        model.addAttribute("newsList", newsList);
        model.addAttribute("newsCount", newsCount);

        // NewsLetter 4개
        List<IntNewsletter> newsletterList = intNewsletterService.searchBy(q, Language.en, new Pager(0,4));
        int newsletterCount = intNewsletterService.countBy(q, Language.en);
        model.addAttribute("newsletterList", newsletterList);
        model.addAttribute("newsletterCount", newsletterCount);

        int totalCount = itemCount + recipeCount + newsCount + newsletterCount;
        model.addAttribute("totalCount", totalCount);

        return "/search/index";
    }

    @RequestMapping(value = "/{category}")
    public String datail(@PathVariable(value = "category") String category,
            @RequestParam(value = "q") String q, Model model) throws ParseException {
        return detail(category, 0, q, model);
    }

    @RequestMapping(value = "/{category}/{page}")
    public String detail(@PathVariable(value = "category") String category,
            @PathVariable("page") int page, @RequestParam(value = "q") String q, Model model) throws ParseException {

        List<IntItem> itemList = null;
        List<IntRecipe> recipeList = null;
        List<IntNews> newsList = null;
        List<IntNewsletter> newsletterList = null;

        int itemCount = intItemService.countBy(q, Language.en);
        int recipeCount = intRecipeService.countBy(q, Language.en.toString());
        int newsCount = intNewsService.countBy(q, Language.en);
        int newsletterCount = intNewsletterService.countBy(q, Language.en);

        Builder builder = Paging.builder();
        Paging paging = null;

        switch (category) {
        case "product":
            itemList = intItemService.searchBy(q, Language.en, new Pager(page, Const.FRONT_LIST_SIZE));

            paging = builder.currentPage(page)
                    .entriesPerPage(Const.FRONT_LIST_SIZE)
                    .totalCount(itemCount)
                    .link("/search/"+ category+"/")
                    .query("q", q).build();

            break;
        case "recipe":
            recipeList = intRecipeService.searchBy(q, Language.en.toString(), new Pager(page, Const.FRONT_LIST_SIZE));

            paging = builder.currentPage(page)
                    .entriesPerPage(Const.FRONT_LIST_SIZE)
                    .totalCount(recipeCount)
                    .link("/search/"+ category+"/")
                    .query("q", q).build();

            break;
        case "news":
            newsList = intNewsService.searchBy(q, Language.en, new Pager(page, Const.FRONT_LIST_SIZE));

            paging = builder.currentPage(page)
                    .entriesPerPage(Const.FRONT_LIST_SIZE)
                    .totalCount(newsCount)
                    .link("/search/"+ category+"/")
                    .query("q", q).build();

            break;
        case "newsletter":
            newsletterList = intNewsletterService.searchBy(q, Language.en, new Pager(page, Const.FRONT_LIST_SIZE));

            paging = builder.currentPage(page)
                    .entriesPerPage(Const.FRONT_LIST_SIZE)
                    .totalCount(newsletterCount)
                    .link("/search/"+ category+"/")
                    .query("q", q).build();
            break;
        }

        model.addAttribute("itemList", itemList);
        model.addAttribute("itemCount", itemCount);
        model.addAttribute("recipeList", recipeList);
        model.addAttribute("recipeCount", recipeCount);
        model.addAttribute("newsList", newsList);
        model.addAttribute("newsCount", newsCount);
        model.addAttribute("newsletterList", newsletterList);
        model.addAttribute("newsletterCount", newsletterCount);

        int totalCount = itemCount + recipeCount + newsCount + newsletterCount;
        model.addAttribute("totalCount", totalCount);

        model.addAttribute("q", q);
        model.addAttribute("category", category);
        model.addAttribute("paging", paging);

        return "/search/detail";
    }
}
