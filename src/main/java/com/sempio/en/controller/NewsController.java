package com.sempio.en.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sempio.en.code.Language;
import com.sempio.en.entity.IntNews;
import com.sempio.en.service.IntNewsService;
import com.sempio.en.util.Pager;
import com.sempio.en.util.Paging;
import com.sempio.en.util.Paging.Builder;

@Controller
@RequestMapping("/company/news")
public class NewsController {

    @Autowired
    IntNewsService intNewsService;

    @RequestMapping("")
    public String index(Model model){
        return list(0, 6, model);
    }

    @RequestMapping("/{page}")
    public String list(@PathVariable int page,
            @RequestParam(value = "size", required = false, defaultValue = "6") int size, Model model) {
        Pager pager = new Pager(page, size);

        List<IntNews> news = intNewsService.list(Language.en, pager);
        int count = intNewsService.count(Language.en);

        Builder builder = Paging.builder();
        Paging paging = builder.currentPage(page)
               .entriesPerPage(size)
               .totalCount(count)
               .link("/company/news").build();

        model.addAttribute("news", news);
        model.addAttribute("count", count);
        model.addAttribute("paging", paging);

        return "/company/news/list";
    }

    @RequestMapping("/view/{id}")
    public String view(@PathVariable int id, Model model) {
        IntNews news = intNewsService.view(Language.en, id);
        model.addAttribute("news", news);

        if (news.getTags() != null) {
            String[] tags = news.getTags().split(",");
            model.addAttribute("tags", tags);
        }
        intNewsService.updateViews(id);
        return "/company/news/view";
    }
}
