package com.sempio.en.config;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.AbstractHandlerMapping;

import com.ctlok.springframework.web.servlet.view.rythm.RythmConfigurator;
import com.ctlok.springframework.web.servlet.view.rythm.RythmViewResolver;
import com.sempio.en.controller.StaticPageController;
import com.sempio.en.interceptor.AdminLoginCheckInterceptor;
import com.sempio.en.service.StaticPageHolder;

@Configuration
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter {
    @Autowired
    private Environment env;

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
    }

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        registry.viewResolver(viewResolver());
    }

    @Bean
    public RythmConfigurator rythmConfigurator() {
        RythmConfigurator configurator = new RythmConfigurator();

        String base = env.getProperty("rythm.base-directory");
        if (!StringUtils.endsWith(base, "/")) {
            base = base + "/";
        }
        configurator.setRootDirectory(base);
        configurator.setMode(env.getProperty("rythm.mode"));
        return configurator;
    }

    @Bean
    public RythmViewResolver viewResolver() {
        RythmViewResolver resolver = new RythmViewResolver(rythmConfigurator());
        resolver.setSuffix(".rythm");
        return resolver;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(adminLoginCheckInterceptor())
                .addPathPatterns("/admin", "/admin/**")
                .excludePathPatterns("/admin/static", "/admin/static/**");
    }
    
    @Bean
    public AdminLoginCheckInterceptor adminLoginCheckInterceptor() {
        return new AdminLoginCheckInterceptor();
    }

    @Bean
    public ServerProperties getServerProperties() {
        return new ServerCustomization();
    }
    
    @Bean
    public HandlerMapping staticPageHanderMapping() {
        StaticPageHandlerMapping mapping = new StaticPageHandlerMapping();
        mapping.setOrder(Integer.MAX_VALUE - 5);
        return mapping;
    }

    public static class StaticPageHandlerMapping extends AbstractHandlerMapping {
        @Autowired
        private StaticPageHolder staticPageHolder;

        @Override
        protected Object getHandlerInternal(HttpServletRequest request) throws Exception {
            String p1 = request.getServletPath();
            String p2 = StringUtils.defaultString(request.getPathInfo(), "");
            String path = p1 + p2;

            if (staticPageHolder.checkPathExists(path)) {
                return getApplicationContext().getBean(StaticPageController.class);
            }
            return null;
        }

    }
}
