package com.sempio.en.view;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.sempio.en.code.State;
import com.sempio.en.entity.IntItem;

public class ItemXlsView extends AbstractXlsView {
    @SuppressWarnings("unchecked")
    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String filename = (String) model.get("filename");
        List<IntItem> itemList =  (List<IntItem>) model.get("itemList");

        Sheet worksheet = workbook.createSheet();
        worksheet.setDefaultColumnWidth(8);

        Row row = worksheet.createRow(0);
        row.createCell(0).setCellValue("브랜드");
        row.createCell(1).setCellValue("카테고리");
        row.createCell(2).setCellValue("제품명");
        row.createCell(3).setCellValue("공개여부");

        int idx = 1;
        String discountText = null;
        String stateText = null;
        String saleStateText = null;
        for (IntItem item : itemList) {
            if (State.on.equals(item.getIntBrandCategory().getIntBrand().getState())) {
                if (State.on.equals(item.getState())) {
                    stateText = "공개";
                } else if (State.off.equals(item.getState())) {
                    stateText = "비공개";
                }

                row = worksheet.createRow(idx);
                row.createCell(0).setCellValue(item.getIntBrandCategory().getIntBrand().getTitle());
                row.createCell(1).setCellValue(item.getIntBrandCategory().getTitle());
                row.createCell(2).setCellValue(item.getTitle());
                row.createCell(3).setCellValue(stateText);

                int start = idx;
                int end = idx - 1;
                if (end > start) {
                    for (int c = 0; c < 8; c++) {
                        worksheet.addMergedRegion(new CellRangeAddress(start, end, c, c));
                    }
                }
                idx++;
            }
        }

        response.setContentType("application/msexcel");
        response.setHeader("Content-Disposition", "Attachment; filename=" + filename);
    }
}
