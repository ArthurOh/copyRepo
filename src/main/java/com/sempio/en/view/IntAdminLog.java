package com.sempio.en.view;

import java.util.Date;

public class IntAdminLog {
    private Date logAt;
    private String ip;
    private String action;

    public Date getLogAt() {
        return logAt;
    }
    public void setLogAt(Date logAt) {
        this.logAt = logAt;
    }
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
}
