package com.sempio.en.interceptor;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.iropke.common.exception.PermissionException;
import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.Language;
import com.sempio.en.controller.admin.LoginController;
import com.sempio.en.entity.IntAdminUser;
import com.sempio.en.util.Logics;

public class AdminLoginCheckInterceptor extends HandlerInterceptorAdapter {
    private static Logger log = LoggerFactory.getLogger(AdminLoginCheckInterceptor.class);

    @SuppressWarnings("unchecked")
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.debug("handle = {}", handler);

        if (handler instanceof HandlerMethod) {
            HandlerMethod hm = (HandlerMethod) handler;
            if (hm.getBeanType() == LoginController.class) {
                return true;
            }
            HttpSession session = request.getSession();
            IntAdminUser admin = Logics.adminFromSession(session);

            if (null == admin) {
                StringBuilder sb = new StringBuilder();
                sb.append(request.getServletPath());
                String p = request.getPathInfo();
                if (StringUtils.isNotBlank(p)) {
                    sb.append(p);
                }
                response.sendRedirect("/admin/login?r=" + sb.toString());
                return false;
            }

            // 1. 슈퍼관리자 유무 체크 (슈퍼관리자는 통과)
            if (admin.isSuperAdmin()) {
                return true;
            }
            
            // 2. 접근 주소의 메뉴 접근 권한 확인
            // - (1) 메소드의 AdminRoles 정보 확인
            // - (2) 메소드의 AdminRoles 정보가 없는 경우 컨트롤러의 AdminRole 정보 확인
            // - (3) AdminRoles 정보가 없는 경우 통과
            AdminRole adminRole = hm.getMethodAnnotation(AdminRole.class);
            if (adminRole == null) {
                adminRole = hm.getBeanType().getAnnotation(AdminRole.class);
            }
            if (null == adminRole || adminRole.value() == null) {
                return true;
            }
           
            final AdminRoles[] value = adminRole.value();
            List<AdminRoles> roles = null;
            String uri = request.getRequestURI();
            Pattern p = Pattern.compile("/admin/(en|cn|ru)/");
            Matcher m = p.matcher(uri);
            if (m.find()) {
                String language = m.group(1);
                roles = Lists.<AdminRoles> newArrayList(Iterables.<AdminRoles> filter(AdminRoles.findByLanguage(Language.valueOf(language)), new Predicate<AdminRoles>() {
                    @Override
                    public boolean apply(AdminRoles input) {
                        return ArrayUtils.contains(value, input);
                    }
                }));
            } else {
                roles = CollectionUtils.arrayToList(value);
            }
            log.debug("handler {} requires roles = {}", handler, roles);
            if (CollectionUtils.containsAny(roles, admin.getAdminRoles())) {
                return true;
            }
            throw new PermissionException("admin has not required permission");
        }
        return true;
    }
}
