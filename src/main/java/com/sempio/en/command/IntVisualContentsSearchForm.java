package com.sempio.en.command;

import org.apache.commons.lang3.StringUtils;

import com.sempio.en.util.Paging;

public class IntVisualContentsSearchForm {

    private String q;
    private String qType;

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q.trim();
    }

    public String getqType() {
        return qType;
    }

    public void setqType(String qType) {
        this.qType = qType;
    }

    public Paging.Builder addQueryParams(Paging.Builder builder) {
        if (StringUtils.isNoneBlank(getQ())) {
            builder.query("q", getQ());
        }
        if (StringUtils.isNoneBlank(getqType())) {
            builder.query("qType", getqType());
        }
        return builder;
    }

    public String queryString() {
        return queryString(0);
    }

    public String queryString(int page) {
        StringBuilder q = new StringBuilder();
        q.append("page=").append(String.valueOf(page));
        if (StringUtils.isNoneBlank(getQ())) {
            q.append("&q=").append(getQ());
        }
        if (StringUtils.isNoneBlank(getqType())) {
            q.append("&qType=").append(getqType());
        }
        return q.toString();
    }
}
