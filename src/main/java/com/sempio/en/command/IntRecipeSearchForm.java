package com.sempio.en.command;

import org.apache.commons.lang3.StringUtils;

import com.sempio.en.util.Paging;

public class IntRecipeSearchForm {
    private String stateSearch;
    private String qType;
    private String q;

    public String getStateSearch() {
        return stateSearch;
    }

    public void setStateSearch(String stateSearch) {
        this.stateSearch = stateSearch;
    }

    public String getqType() {
        return qType;
    }

    public void setqType(String qType) {
        this.qType = qType;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q.trim();
    }

    public Paging.Builder addQueryParams(Paging.Builder builder) {

        if (StringUtils.isNotBlank(getStateSearch())) {
            builder.query("stateSearch", getStateSearch());
        }
        if (StringUtils.isNotBlank(getqType())) {
            builder.query("qType", getqType());
        }
        if (StringUtils.isNotBlank(getQ())) {
            builder.query("q", getQ());
        }

        return builder;
    }

    public String getQueryString() {
        return "&stateSearch=" + StringUtils.defaultIfEmpty(stateSearch, "") + "&qType=" + StringUtils.defaultIfEmpty(qType, "")
                + "&q=" + StringUtils.defaultIfEmpty(q, "");
    }

}
