package com.sempio.en.command;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.sempio.en.entity.IntIngredient;
import com.sempio.en.entity.IntItem;

public class IntIngredientForm {
    private String type;
    private String volume;
    private String title;
    private Integer itemId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public IntIngredient toIntIngredient() {
        if (StringUtils.isNoneBlank(getTitle())) {
            IntIngredient item = new IntIngredient();
            item.setTitle(getTitle());
            item.setType(getType());
            item.setVolume(getVolume());
            return item;
        }
        return null;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
