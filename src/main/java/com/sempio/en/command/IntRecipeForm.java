package com.sempio.en.command;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import com.sempio.en.entity.IntRecipe;

public class IntRecipeForm {
    private Integer id;
    private Integer[] intRecipeThemeId;
    private String recipeTitle;
    private String titleSub;
    private String chef;
    private String mainIngredient;
    private String serving;
    private String filepath_thumbnail;
    private String filepath_defaultImage;
    private String description;
    private String tip;
    private Integer[] recommend;
    private String tags;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date publishAt;
    private String state;
    private List<IntCookeryForm> cookeryList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer[] getIntRecipeThemeId() {
        return intRecipeThemeId;
    }

    public void setIntRecipeThemeId(Integer[] intRecipeThemeId) {
        this.intRecipeThemeId = intRecipeThemeId;
    }

    public String getRecipeTitle() {
        return recipeTitle;
    }

    public void setRecipeTitle(String recipeTitle) {
        this.recipeTitle = recipeTitle;
    }

    public String getTitleSub() {
        return titleSub;
    }

    public void setTitleSub(String titleSub) {
        this.titleSub = titleSub;
    }

    public String getChef() {
        return chef;
    }

    public void setChef(String chef) {
        this.chef = chef;
    }

    public String getMainIngredient() {
        return mainIngredient;
    }

    public void setMainIngredient(String mainIngredient) {
        this.mainIngredient = mainIngredient;
    }

    public String getServing() {
        return serving;
    }

    public void setServing(String serving) {
        this.serving = serving;
    }

    public String getFilepath_thumbnail() {
        return filepath_thumbnail;
    }

    public void setFilepath_thumbnail(String filepath_thumbnail) {
        this.filepath_thumbnail = filepath_thumbnail;
    }

    public String getFilepath_defaultImage() {
        return filepath_defaultImage;
    }

    public void setFilepath_defaultImage(String filepath_defaultImage) {
        this.filepath_defaultImage = filepath_defaultImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Date getPublishAt() {
        return publishAt;
    }

    public void setPublishAt(Date publishAt) {
        this.publishAt = publishAt;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer[] getRecommend() {
        return recommend;
    }

    public void setRecommend(Integer[] recommend) {
        this.recommend = recommend;
    }

    public List<IntCookeryForm> getCookeryList() {
        return cookeryList;
    }

    public void setCookeryList(List<IntCookeryForm> cookeryList) {
        this.cookeryList = cookeryList;
    }

    public IntRecipe toIntRecipe() {
        IntRecipe item = new IntRecipe();
        item.setIntRecipeTheme(getIntRecipeThemeId());
        item.setTitle(getRecipeTitle());
        item.setTitleSub(getTitleSub());
        item.setViews(0);
        item.setChef(getChef());
        item.setMainIngredient(getMainIngredient());
        item.setServing(getServing());
        item.setViewImgPath(getFilepath_defaultImage());
        item.setDescription(getDescription());
        item.setTip(getTip());
        item.setRecommend(getRecommend());
        item.setTags(getTags());
        item.setPublishAt(getPublishAt());
        item.setState(getState());
        return item;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
