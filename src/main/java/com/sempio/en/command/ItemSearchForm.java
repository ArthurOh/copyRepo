package com.sempio.en.command;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.sempio.en.code.State;
import com.sempio.en.util.Paging;

public class ItemSearchForm {

    private String brandId;
    private String brandCategoryId;
    private String state;
    private String q;

    public String getBrandId() {
        return brandId;
    }
    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }
    public String getBrandCategoryId() {
        return brandCategoryId;
    }
    public void setBrandCategoryId(String brandCategoryId) {
        this.brandCategoryId = brandCategoryId;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getQ() {
        return q;
    }
    public void setQ(String q) {
        this.q = q.trim();
    }

    public Criteria addRestrictions(Criteria q) {

        State state = State.contains(getState())? State.valueOf(getState())  : null;

        Integer brandId = null;
        Integer brandCategoryId = null;

        if (StringUtils.isNoneBlank(getBrandId())) {
            try {
                brandId = Integer.valueOf(getBrandId());
            } catch (Exception e) {
            } // 에러무시
        }

        if (StringUtils.isNoneBlank(getBrandCategoryId())) {
            try {
                brandCategoryId = Integer.valueOf(getBrandCategoryId());
            } catch (Exception e) {
            } // 에러무시
        }

        if (brandCategoryId != null && brandCategoryId.intValue() > 0) {
            //q.createCriteria("intBrandCategory").add(Restrictions.eq("id", brandCategoryId));
            Criteria qBc = q.createCriteria("intBrandCategory");
            qBc.add(Restrictions.eq("id", brandCategoryId));
            qBc.createCriteria("intBrand")
                .add(Restrictions.eq("state", State.on));

        } else if (brandId != null && brandId.intValue() > 0) {
            //q.createCriteria("intBrandCategory").add(Restrictions.eq("intBrand.id", brandId));
            Criteria qBc = q.createCriteria("intBrandCategory");
            qBc.add(Restrictions.eq("intBrand.id", brandId));
            qBc.createCriteria("intBrand")
                .add(Restrictions.eq("state", State.on));

        } else {
            Criteria qBc = q.createCriteria("intBrandCategory");
            qBc.createCriteria("intBrand")
                .add(Restrictions.eq("state", State.on));
        }

        if (null != state) {
            q.add(Restrictions.eq("state", state));
        } else {
            q.add(Restrictions.ne("state", State.deleted));
        }

        if (StringUtils.isNoneBlank(getQ())) {
            q.add(Restrictions.ilike("title", getQ(), MatchMode.ANYWHERE));
        }
        return q;
    }

    public String queryString() {
        StringBuilder queryString = new StringBuilder();
        queryString.append("brandId=").append(StringUtils.defaultString(getBrandId(), "")).append("&");
        queryString.append("brandCategoryId=").append(StringUtils.defaultString(getBrandCategoryId(), "")).append("&");
        queryString.append("state=").append(StringUtils.defaultString(getState(), "")).append("&");
        queryString.append("q=").append(StringUtils.defaultString(getQ(), "")).append("&");
        return queryString.toString();
    }

    public Paging.Builder addQueryParams(Paging.Builder builder) {
        if (StringUtils.isNotBlank(getBrandId())) {
            builder.query("brandId", getBrandId());
        }
        if (StringUtils.isNotBlank(getBrandCategoryId())) {
            builder.query("brandCategoryId", getBrandCategoryId());
        }
        if (StringUtils.isNotBlank(getState())) {
            builder.query("state", getState());
        }
        if (StringUtils.isNotBlank(getQ())) {
            builder.query("q", getQ());
        }
        return builder;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
