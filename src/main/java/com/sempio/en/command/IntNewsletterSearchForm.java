package com.sempio.en.command;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.sempio.en.code.State;
import com.sempio.en.util.Paging;

public class IntNewsletterSearchForm {

    private String state;
    private String q;
    private String qType;

    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getQ() {
        return q;
    }
    public void setQ(String q) {
        this.q = q.trim();
    }
    public String getqType() {
        return qType;
    }
    public void setqType(String qType) {
        this.qType = qType;
    }

    public Criteria addRestrictions(Criteria q) {
        if (getState() != null && State.contains(getState())) {
            q.add(Restrictions.eq("state", State.valueOf(getState())));
        } else {
            q.add(Restrictions.ne("state", State.deleted));
        }
        if (StringUtils.isNoneBlank(getQ())) {
            if (StringUtils.equals("title", getqType())) {
                q.add(Restrictions.ilike("titleInt", getQ(), MatchMode.START));
            } else if (StringUtils.equals("content", getqType())) {
                q.add(Restrictions.ilike("contentInt", getQ(), MatchMode.ANYWHERE));
            }
        }
        return q;
    }

    public Paging.Builder addQueryParams(Paging.Builder builder) {
        if (getState() != null) {
            builder.query("state", getState());
        }
        if (StringUtils.isNoneBlank(getQ())) {
            builder.query("q", getQ());
        }
        if (StringUtils.isNoneBlank(getqType())) {
            builder.query("qType", getqType());
        }
        return builder;
    }

    public String queryString() {
        return queryString(0);
    }

    public String queryString(int page) {
        StringBuilder q = new StringBuilder();
        q.append("page=").append(String.valueOf(page));
        if (StringUtils.isNoneBlank(getState())) {
            q.append("&state=").append(getState());
        }
        if (StringUtils.isNoneBlank(getQ())) {
            q.append("&q=").append(getQ());
        }
        if (StringUtils.isNoneBlank(getqType())) {
            q.append("&qType=").append(getqType());
        }
        return q.toString();
    }
}
