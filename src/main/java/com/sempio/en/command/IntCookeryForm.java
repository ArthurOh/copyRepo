package com.sempio.en.command;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.sempio.en.entity.IntCookery;

public class IntCookeryForm {
    private String title;
    private String[] directions;
    private Integer ordering;
    private List<IntIngredientForm> ingredientList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getDirections() {
        return directions;
    }

    public void setDirections(String[] directions) {
        this.directions = directions;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public List<IntIngredientForm> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(List<IntIngredientForm> ingredientList) {
        this.ingredientList = ingredientList;
    }

    public IntCookery toIntCookery() {
        if (StringUtils.isNotEmpty(getTitle())) {
            IntCookery item = new IntCookery();
            item.setTitle(getTitle());
            item.setDirections(getDirections());
            //item.setOrdering(getOrdering());
            return item;
        }
        return null;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
