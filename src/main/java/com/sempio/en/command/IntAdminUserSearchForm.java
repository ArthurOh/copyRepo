package com.sempio.en.command;


import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.sempio.en.code.AdminRoles;
import com.sempio.en.code.AdminType;
import com.sempio.en.code.Language;
import com.sempio.en.code.State;
import com.sempio.en.util.Paging;

public class IntAdminUserSearchForm {

    private String type;
    private String state;
    private String language;
    private String q;
    private String qType;

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getLanguage() {
        return language;
    }
    public void setLanguage(String language) {
        this.language = language;
    }
    public String getQ() {
        return q;
    }
    public void setQ(String q) {
        this.q = q.trim();
    }
    public String getqType() {
        return qType;
    }
    public void setqType(String qType) {
        this.qType = qType;
    }

    public Criteria addRestrictions(Criteria q) {
        if (StringUtils.isNoneBlank(getType()) && AdminType.contains(getType())) {
            q.add(Restrictions.eq("type", AdminType.valueOf(getType())));
        }
        if (StringUtils.isNoneBlank(getState()) && State.contains(getState())) {
            q.add(Restrictions.eq("state", State.valueOf(getState())));
        } else {
            q.add(Restrictions.ne("state", State.deleted));
        }
        if (StringUtils.isNoneBlank(getQ())) {
            if (StringUtils.equals("userId", getqType())) {
                q.add(Restrictions.ilike("userId", getQ(), MatchMode.START));
            } else if (StringUtils.equals("userName", getqType())) {
                q.add(Restrictions.ilike("userName", getQ(), MatchMode.START));
            } else if (StringUtils.equals("userEmail", getqType())) {
                q.add(Restrictions.ilike("userEmail", getQ(), MatchMode.START));
            }
        }
        if (StringUtils.isNoneBlank(getLanguage())) {
            String value = null;
            if (StringUtils.equals("en", getLanguage())) {
                value = StringUtils.join(AdminRoles.findByLanguage(Language.en), ",");
            } else if (StringUtils.equals("cn", getLanguage())) {
                value = StringUtils.join(AdminRoles.findByLanguage(Language.cn), ",");
            } else if (StringUtils.equals("ru", getLanguage())) {
                value = StringUtils.join(AdminRoles.findByLanguage(Language.ru), ",");
            }
            if (StringUtils.isNoneBlank(value)) {
                q.add(Restrictions.sqlRestriction(StringUtils.join("( {alias}.roles && '{",value,"}' OR {alias}.type = 'integration' )")));
            }
        }
        return q;
    }

    public Paging.Builder addQueryParams(Paging.Builder builder) {
        if (StringUtils.isNoneBlank(getType())) {
            builder.query("type", getType());
        }
        if (StringUtils.isNoneBlank(getState())) {
            builder.query("state", getState());
        }
        if (StringUtils.isNoneBlank(getQ())) {
            builder.query("q", getQ());
        }
        if (StringUtils.isNoneBlank(getqType())) {
            builder.query("qType", getqType());
        }
        if (StringUtils.isNoneBlank(getLanguage())) {
            builder.query("language", getLanguage());
        }
        return builder;
    }

    public String queryString(int page) {
        StringBuilder q = new StringBuilder();

        q.append("page=").append(String.valueOf(page));
        if (StringUtils.isNoneBlank(getType())) {
            q.append("&type=").append(getType());
        }
        if (StringUtils.isNoneBlank(getState())) {
            q.append("&state=").append(getState());
        }
        if (StringUtils.isNoneBlank(getQ())) {
            q.append("&q=").append(getQ());
        }
        if (StringUtils.isNoneBlank(getqType())) {
            q.append("&qType=").append(getqType());
        }
        if (StringUtils.isNoneBlank(getLanguage())) {
            q.append("&language=").append(getLanguage());
        }
        String queryString = q.toString();
        return queryString;
    }

    public String queryString() {
        return queryString(0);
    }
}
