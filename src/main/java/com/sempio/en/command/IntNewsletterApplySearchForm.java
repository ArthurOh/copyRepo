package com.sempio.en.command;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.sempio.en.code.State;
import com.sempio.en.util.Paging;

public class IntNewsletterApplySearchForm {

    private String applyLanguageType;
    private String state;
    private String q;
    private String qType;

    public String getApplyLanguageType() {
        return applyLanguageType;
    }

    public void setApplyLanguageType(String applyLanguageType) {
        this.applyLanguageType = applyLanguageType;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q.trim();
    }

    public String getqType() {
        return qType;
    }

    public void setqType(String qType) {
        this.qType = qType;
    }

    public Criteria addRestrictions(Criteria q) {
        if (StringUtils.isNoneBlank(getApplyLanguageType())) {
            if (StringUtils.equals("en", getApplyLanguageType())) {
                q.add(Restrictions.sqlRestriction(StringUtils.join("{alias}.apply_language && '{en}'")));
            } else if (StringUtils.equals("kr", getApplyLanguageType())) {
                q.add(Restrictions.sqlRestriction(StringUtils.join("{alias}.apply_language && '{kr}'")));
            } else if (StringUtils.equals("en_kr", getApplyLanguageType())) {
                q.add(Restrictions.sqlRestriction(StringUtils.join("{alias}.apply_language @> '{kr, en}'")));
            }
        }
        if (getState() != null && State.contains(getState())) {
            q.add(Restrictions.eq("state", State.valueOf(getState())));
        } else {
            q.add(Restrictions.ne("state", State.deleted));
        }
        if (StringUtils.isNoneBlank(getQ())) {
            if (StringUtils.equals("name", getqType())) {
                q.add(Restrictions.ilike("name", getQ(), MatchMode.START));
            } else if (StringUtils.equals("email", getqType())) {
                q.add(Restrictions.ilike("email", getQ(), MatchMode.START));
            }
        }
        return q;
    }

    public Paging.Builder addQueryParams(Paging.Builder builder) {
        if (StringUtils.isNoneBlank(getApplyLanguageType())) {
            builder.query("applyLanguageType", getApplyLanguageType());
        }
        if (StringUtils.isNoneBlank(getState())) {
            builder.query("state", getState());
        }
        if (StringUtils.isNoneBlank(getQ())) {
            builder.query("q", getQ());
        }
        if (StringUtils.isNoneBlank(getqType())) {
            builder.query("qType", getqType());
        }
        return builder;
    }

    public String queryString() {
        return queryString(0);
    }

    public String queryString(int page) {
        StringBuilder q = new StringBuilder();
        q.append("page=").append(String.valueOf(page));
        if (StringUtils.isNoneBlank(getApplyLanguageType())) {
            q.append("&applyLanguageType=").append(getApplyLanguageType());
        }
        if (StringUtils.isNoneBlank(getState())) {
            q.append("&state=").append(getState());
        }
        if (StringUtils.isNoneBlank(getQ())) {
            q.append("&q=").append(getQ());
        }
        if (StringUtils.isNoneBlank(getqType())) {
            q.append("&qType=").append(getqType());
        }
        return q.toString();
    }
}
