window.bindFunctions = window.bindFunctions || {};

function displayError(msg, title) {
    toastr.error(msg, title, { closeButton: true, timeOut: 0 });
}

function displaySuccess(msg, title) {
    toastr.success(msg, title, { closeButton: true, timeOut: 5000 });
}

function displayInfo(msg, title) {
    toastr.info(msg, title, { closeButton: true, timeOut: 5000 });
}

function registerBindFunction(name, func) {
    window.bindFunctions[name] = func;
}

$(function() {
    'use strict';
    
    registerBindFunction("toggleCheck", function() {
        var targetName = $(this).data('targetName') || 'ids';
        $(this).parents('FORM').find('[name="' + targetName + '"]').
                prop('checked', $(this).is(':checked')).trigger('change');
    });
    
    registerBindFunction("validateChecks", function(evt) {
        var targetName = $(this).data('targetName') || 'ids';
        if ($('input[name="' + targetName + '"]', this).filter(':checked').get().length == 0) {
            displayError("You have not selected anything")
            evt.preventDefault();
            return;
        }
    });
    
    $('button[data-form-action], input[data-form-action').filter('[type!="button"]').click(function() {
        $(this.form).attr('action', $(this).data('formAction'));
    });
    
    $('[data-bind-func]').each(function(idx, e) {
        var funcName = $(e).data('bindFunc');
        var func = window.bindFunctions[funcName];
        if (! func) {
            console.log('funcName=' + funcName + " is not defined");
            return;
        }
        if (e.tagName == 'FORM') {
            $(e).submit(func);
        } else if (e.tagName == 'A' || e.type == 'submit' || e.type == 'button') {
            $(e).click(func);
        } else if (e.type && (e.type == 'select-one' || e.type == 'select-multiple' || e.type == 'checkbox' || e.type == 'radio')) {
            $(e).change(func);
        } else {
            console.log("unsupported for " + e);
        }
    });

    $('.sp-danger-action').confirmation({
        title: function() { return $(this).data('title') || 'You cannot undo the deletion. Do you want to delete it?'; },
        singleton: true,
        popout: true
    });
    
    // 링크(유튜브영상코드, 채용-지원서링크) 입력시 링크 확인 기능
    $(".gdo-link-check").click(function(){
        var url = "";
        var videoUrl = $('input:text[name=video_url]').val();

        if (videoUrl == "" || videoUrl == undefined ) {
            var recruitUrl = $('input:text[name=recruit_url]').val();
            if (recruitUrl == "" || recruitUrl == undefined){
                url = "";
            } else {
                url = recruitUrl;
            }
        } else {
            url = videoUrl;
        }

        if (url == "" || url == undefined) {
            alert('Please enter a URL.');
        } else {
            if (url.substring(0, 4) == "http") {
                window.open(url);
            } else {
                alert('Please enter the full URL starting with http: //.');
            }
        }
    });

    $(".sp-form-validate").validate({
    	errorElement: 'div',
        submitHandler: function(){
            var status = true;

            var thumbnail = $('input[name="filepath_thumbnail"]').val();
            var isThumbnail = $('#jquery-upload-image_thumbnail').hasClass('sp-form-thumbnail');

	        if( (thumbnail == undefined || thumbnail == "")&& isThumbnail ){
                if(!$('.sp-form-thumbnail').hasClass("sp-validate-error")){
                    $('.sp-form-thumbnail').addClass("sp-validate-error");
                    $('.sp-form-thumbnail').after('<span class="sp-validate-error-msg">Image is required information.</span>');
                }
                status = false;
            }

	        var defaultImage = $('input[name="filepath_defaultImage"]').val();
	        var isDefaultImage = $('#jquery-upload-image_default').hasClass('sp-form-defaultImage');

	        if( (defaultImage == undefined || defaultImage == "")&& isDefaultImage ){
                if(!$('.sp-form-defaultImage').hasClass("sp-validate-error")){
                    $('.sp-form-defaultImage').addClass("sp-validate-error");
                    $('.sp-form-defaultImage').after('<span class="sp-validate-error-msg">Image is required information.</span>');
                }
                status = false;
            }

	        var mobileImage = $('input[name="filepath_mobileImage"]').val();
	        var isMobileImage = $('#jquery-upload-image-mobile').hasClass('sp-form-mobileImage');

	        if( (mobileImage == undefined || mobileImage == "")&& isMobileImage ){
                if(!$('.sp-form-mobileImage').hasClass("sp-validate-error")){
                    $('.sp-form-mobileImage').addClass("sp-validate-error");
                    $('.sp-form-mobileImage').after('<span class="sp-validate-error-msg">Image is required information.</span>');
                }
                status = false;
            }

	        var bannerImage = $('input[name="filepath_banner"]').val();
	        var isBannerImage = $('#jquery-upload-image-banner').hasClass('sp-form-banner');

	        if( (bannerImage == undefined || bannerImage == "")&& isBannerImage ){
                if(!$('.sp-form-banner').hasClass("sp-validate-error")){
                    $('.sp-form-banner').addClass("sp-validate-error");
                    $('.sp-form-banner').after('<span class="sp-validate-error-msg">Image is required information.</span>');
                }
                status = false;
            }

	        var otherImage1 = $('input[name="filepath_image01"]').val();
	        var otherImage2 = $('input[name="filepath_image02"]').val();
	        var otherImage3 = $('input[name="filepath_image03"]').val();

	        var isOtherImage1 = $('#upload-other-image1').hasClass('sp-ex-form-other-image1');
	        var isOtherImage2 = $('#upload-other-image2').hasClass('sp-ex-form-other-image2');
	        var isOtherImage3 = $('#upload-other-image3').hasClass('sp-ex-form-other-image3');

	        if( (otherImage1 == undefined || otherImage1 == "")&& isOtherImage1 ){
	            if(!$('.sp-ex-form-other-image1').hasClass("sp-validate-error")){
	                $('.sp-ex-form-other-image1').addClass("sp-validate-error");
	                $('.sp-ex-form-other-image1').after('<span class="sp-validate-error-msg">Icon Image is required information.</span>');
	            }
	            status = false;
	        }

	        if( (otherImage2 == undefined || otherImage2 == "")&& isOtherImage2 ){
	            if(!$('.sp-ex-form-other-image2').hasClass("sp-validate-error")){
	                $('.sp-ex-form-other-image2').addClass("sp-validate-error");
	                $('.sp-ex-form-other-image2').after('<span class="sp-validate-error-msg">Icon Image is required information.</span>');
	            }
	            status = false;
	        }

	        if( (otherImage3 == undefined || otherImage3 == "")&& isOtherImage3 ){
	            if(!$('.sp-ex-form-other-image3').hasClass("sp-validate-error")){
	                $('.sp-ex-form-other-image3').addClass("sp-validate-error");
	                $('.sp-ex-form-other-image3').after('<span class="sp-validate-error-msg">Icon Image is required information.</span>');
	            }
	            status = false;
	        }

	        var videoUrl = $('input[name="video_url"]').val();
            var isVideoUrl = $('#video_url').hasClass('sp-form-video-url');

            if (isVideoUrl){
                if ((videoUrl.substring(0, 4) != "http")){
                    $('.sp-form-video-url').addClass("gdo-validate-error");
                    $('.sp-form-video-url').after('<span class="sp-validate-error-msg">Video URLs can only be registered as full URLs starting with http: //.</span>');
                    status = false;
                }
            }

            var linkUrl = $('input[name="link_url"]').val();
            var islinkUrl = $('#link_url').hasClass('sp-form-link-url');

            if (islinkUrl){
                if ((linkUrl.substring(0, 4) != "http")){
                    $('.sp-form-link-url').addClass("gdo-validate-error");
                    $('.sp-form-link-url').after('<span class="sp-validate-error-msg">Link address URLs can only be registered with full URLs beginning with http: //.</span>');
                    status = false;
                }
            }

            var wrap = $('input[name="wrap"]').val();
            var isWrap = $('#wrap').hasClass('sp-form-wrap');

            if( (wrap == undefined || wrap == "")&& isWrap ){
                if(!$('.sp-form-wrap').hasClass("sp-validate-error")){
                    $('.sp-form-wrap').addClass("sp-validate-error");
                    $('.sp-form-wrap').after('<span class="sp-validate-error-msg">Capacity is required.</span>');
                }
                status = false;
            }

            var startAt = $('input[name="startAt"]').val();
            var isStartAt = $('#startAt').hasClass('sp-startAt');

            var endAt = $('input[name="endAt"]').val();
            var isEndAt = $('#endAt').hasClass('sp-endAt');

            if (isStartAt && isEndAt){
                startAt = startAt.split('-');
                endAt = endAt.split('-');

                startAt = parseInt(startAt[0] + startAt[1] + startAt[2]);
                endAt = parseInt(endAt[0] + endAt[1] + endAt[2]);

                if ( (endAt - startAt) < 0){
                    $('.sp-startAt').addClass("sp-validate-error");                    
                    $('.sp-date-msg').after('<span class="sp-validate-error-msg">Start date can not be later than end date.</span>');
                    status = false;
                }
            }

            var brand = $('select[name="brand"]').val();
            var isBrand = $('#brand').hasClass('sp-form-brand');

            if (isBrand) {
                if (brand == "") {
                    $('.sp-form-brand').addClass("gdo-validate-error");
                    $('.sp-form-brand').after('<span class="sp-validate-error-msg">Brand selection is required.</span>');
                    status = false;
                }
            }

            var brandCategory = $('select[name="brand_category"]').val();
            var isBrandCategory = $('#brand_category').hasClass('sp-form-brandcategory');

            if (isBrandCategory) {
                if (brandCategory == "") {
                    $('.sp-form-brandcategory').addClass("gdo-validate-error");
                    $('.sp-form-brandcategory').after('<span class="sp-validate-error-msg">Category selection is required.</span>');
                    status = false;
                }
            }

            var isCkeditor = $('#ckeditor').hasClass('ckeditor');
            if (isCkeditor) {
                var c_cnt = $.trim(CKEDITOR.instances.ckeditor.getData()).length;
                if (c_cnt === 0) {
	                if(!$('.contents').hasClass("sp-validate-error")){
                        $('.contents').addClass("sp-validate-error");
                        $('.contents-error-msg').after('<span class="sp-validate-error-msg">Comment is required information.</span>');
                     }
	                 status = false;
                }
            }

            var isBrandCodeCheck = $('#brand_code_check').hasClass('brand_code_check');
            if (isBrandCodeCheck) {
                var brandCodeCheck = $('input[name="brand_code_check"]').val();

                if (brandCodeCheck == "false" || brandCodeCheck == "") {
                    $('.sp-error-msg-brandcode').remove();
                    $('.sp-form-brand-code').after('<span class="sp-validate-error-msg sp-error-msg-brandcode">Verification of brand code duplication is required.</span>');
                    status = false;
                }
            }

            var isCertification = $('#certification-list').hasClass('certification-list');
            if (isCertification) {
                $("#certification").is(":checked") ;
                $("input:checkbox[id='certification']").is(":checked") ;
                var _certification_count = $("#certification:checked").length ;

                if (_certification_count > 3) {
                    $('.sp-error-msg-certification').remove();
                    $('.certification-list').after('<span class="sp-validate-error-msg sp-error-msg-certification">Only three can be selected of certification.</span>');
                    status = false;
                }
            }

            if (status){
                return true;
            } else {
                return false;
            }

        },
        rules: {
            title: {
                maxlength : 100
            },
            subtitle: {
                maxlength : 500
            },
            item_title: {
                maxlength : 100
            },
            point_msg: {
                maxlength : 500
            },
            brand_code: {
                maxlength : 20
            },
            brand_title: {
                maxlength : 50
            },
            description: {
                maxlength : 1000
            },
            brand_slogan: {
                minlength : 5,
                maxlength : 500
            },
            brand_description: {
                maxlength : 200
            },
            brand_category_title: {
                maxlength : 50
            },
            feature: {
                maxlength : 2000
            },
            wrap: {
                maxlength : 200
            },
            serving_size: {
                maxlength : 200
            },
            serving_amount: {
                maxlength : 200
            },
            fat: {
                maxlength : 200
            },
            cholesterol: {
                maxlength : 200
            },
            sodium: {
                maxlength : 200
            },
            carbohydrate: {
                maxlength : 500
            },
            protein: {
                maxlength : 200
            },
            tags: {
            	maxlength : 200
            },
            ingredient: {
            	maxlength : 2000
            },
            allergy_innfo: {
            	maxlength : 2000
            },
            best_by: {
            	maxlength : 100
            }
        },
        messages: {
            title: {
                required : "Please enter the subject.",
                maxlength : "You can insert up to {0} letters for subject."
            },
            subtitle: {
                required : "Subtitle are required.",
                maxlength : "Subtitle can only be less than {0} characters."
            },
            item_title: {
                required : "Product Name  is required information.",
                maxlength : "You can insert up to {0} letters for Product Name."
            },
            video_url: {
                required : "Video URL is required."
            },
            link_url: {
                required : "Banner link URL is required."
            },
            title_keyword : {
                required : "Keyword titles are required."
            },
            title_sub : {
                required : "Subtitles are required."
            },
            content_url :{
                required : "URL Required."
            },
            point_msg :{
                required : "One-point messages are required.",
                maxlength : "You can insert up to {0} letters for One point Messeage."
            },
            brand_code: {
                required : "Brand Code is required information. <br>",
                maxlength : "You can insert up to {0} letters for Brand Code."
            },
            brand_title: {
                required : "Brand Name is required information.",
                maxlength : "You can insert up to {0} letters for Brand Name."
            },
            description: {
                maxlength : "You can insert up to {0} letters for Category Description"
            },
            brand_slogan: {
                required : "Slogan is required information.",
                minlength : "The brand slogan is at least {0} characters long.",
                maxlength : "Brand slogans are limited to {0} characters."
            },
            brand_description: {
                maxlength : "You can insert up to {0} letters for Brand Description."
            },
            brand_category_title: {
               required : "Category Name is required information. <br>",
               maxlength : "You can insert up to {0} letters for Category Name"
            },
            feature: {
                required : "Product Feature is required information.",
                maxlength : "You can insert up to {0} letters for Product features."
            },
            wrap: {
                required : "Capacity is a required field."
            },
            serving_size: {
                maxlength : "You can insert up to {0} letters for Serving Size."
            },
            serving_amount: {
                maxlength : "You can insert up to {0} letters for Amount Per Serving."
            },
            fat: {
                maxlength : "You can insert up to {0} letters for Total Fat."
            },
            cholesterol: {
                maxlength : "You can insert up to {0} letters for Cholesterol."
            },
            sodium: {
                maxlength : "You can insert up to {0} letters for Sodium."
            },
            carbohydrate: {
                maxlength : "You can insert up to {0} letters for Total Carbohydrate."
            },
            protein: {
                maxlength : "You can insert up to {0} letters for Protein."
            },
            tags: {
                maxlength : "Tags can only be less than {0} characters long."
            },
            ingredient: {
            	maxlength : "Ingredient can only be less than {0} characters."
            },
            allergy_innfo: {
            	maxlength :"Allergy_innfo can only be less than {0} characters long."
            },
            best_by: {
                maxlength :"You can insert up to {0} letters for Expiration Date."
            }
        }
    });

    /** 
     *    2016.08.12 summernote > ckeditor 에디터 변경
     *    ex) ckeditor 클래스명이 붙은 대상 적용 (textarea에 클래스명 표기)
     **/
    if ($(".ckeditor").length > 0 && window.CKEDITOR != undefined) {
        // 이미지 업로드 설정
        CKEDITOR.config.filebrowserUploadUrl = '/admin/fileuploads/upload/images?imageWidth=0&imageHeight=0&imageType=no';
        CKEDITOR.config.height = "300px";
    }

});

/* jquery ui datepicker options set */
$(function(){
    if (typeof $.datepicker == 'object') {
        $.datepicker.setDefaults({
            dateFormat : 'yy-mm-dd',
            monthNamesShort : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            dayNamesMin : ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'],
            changeMonth : true,
            changeYear : true,
            showMonthAfterYear : true
        });
    }
});

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();