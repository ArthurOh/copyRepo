var validation_config = {};

validation_config.rules = 
{
    title: {                
        maxlength : 200             
    },
    subTitle: {             
        maxlength : 500             
    }
};

validation_config.messages = 
{
    title: {
        required : "제목은 필수 입력 사항입니다.",
        maxlength : "제목은 {0}글자 이하만 가능합니다."
    },
    subTitle: {
        required : "부제목은 필수 입력 사항입니다.",
        maxlength : "부제목은 {0}글자 이하만 가능합니다."
    },
    link: {
        required : "링크URL은 필수 입력 사항입니다."
        
    },
    name01: {
        required : "이름은 필수 입력 사항입니다."
    },
    name02: {
        required : "이름은 필수 입력 사항입니다."
    },
    name03: {
        required : "이름은 필수 입력 사항입니다."
    },
    result01: {
        required : "성과는 필수 입력 사항입니다."
    },
    result02: {
        required : "성과는 필수 입력 사항입니다."
    },
    result03: {
        required : "성과는 필수 입력 사항입니다."
    },
    figure01: {
        required : "수치는 필수 입력 사항입니다."

    },
    figure02: {
        required : "수치는 필수 입력 사항입니다."

    },
    figure03: {
        required : "수치는 필수 입력 사항입니다."

    },
    description: {
    	required : "설명은 필수 입력 사항입니다."
    },
    location: {
    	required : "장소는 필수 입력 사항입니다."
    },
    authorInfo: {
    	required : "참여 작가는 필수 입력 사항입니다."
    }
}