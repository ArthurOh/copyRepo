/*
    # 사용법 
*/                  

var ExPagination = function(options) {
    	this._init(options);
    	this.show();
} 

var ExPagination = function(options) {
	this._init(options);
	this.show();
};

ExPagination.prototype = {
	_init: function(options) {
		if (this.options == undefined) {
			this.options = $.extend({
	            totalCount: 0, // 조건에 맞는 전체 건수
	            currentPage: 0, // 현재 페이지
	            entriesPerPage: 10, // 한페이지에 보여질 엔트리 숫자
	            pagesPerPageGroup: 10, // 페이징 그룹에 보일 페이징 갯수
	            firstPageInCurrentPageGroup: 0,
	            f_link: function(page){},
	            f_obj: undefined,
	            target: 'body' // 태그 생성 후  pagination 클래스를 찾아 태그를 넣어줄 때 해당 클래스를 찾는 기준이 되는 root element (ex. id인 경우 #id 클래스인 경우 .classname)
	        }, options);
		} else {
			this.options = $.extend(this.options, options);
		}
		
		this.options.firstPageInCurrentPageGroup = parseInt(parseInt(this.options.currentPage / this.options.pagesPerPageGroup) * this.options.pagesPerPageGroup);
	},
	show: function() {
		
		var _object = this;
	    var totalCount = this.getTotalCount();
	    var currentPage = this.getCurrentPage() + 1;
	    var lastPage = this.getLastPage() + 1;
	    var prePage = this.getPrevious();
	    
	    function bindLink($el) {
	    	$el.find("a").on("click", function(event) {
	    		
	    		if (_object.options.f_obj != undefined) {
		    		_object.options.f_link.call(_object.options.f_obj, $(this).data("page"));
		    	} else if (typeof _object.options.f_link == "function") {
		    		//_object.options.f_link.call(_object, $(this).data("page"));
		    		_object.options.f_link($(this).data("page"));
		    	} else if (typeof window[_object.options.f_link] == "function") {
		    		window[_object.options.f_link].call(_object, $(this).data("page"));
		    	}
	    		event.preventDefault();
	    	});
	    }
	    
	    var _html = $('<ul class="pagination"></ul>')
	    	, $prev, $next, _list = [];
	
	    if (this.hasPrevious()) {
	    	$prev = $('<li class="previous"><a href="#prev">&lt;</a></li>');
	    	$prev.find("a").data("page", this.getPrevious());
	    } else {
	    	$prev = $('<li class="previous"><span>&lt;</span></li>');
	    }
	    _list.push($prev);
	    
	    $.each(this.getPages(), function(i, data){
	        var printPage = data + 1
	        	,$item;
	        
	        if ( data == _object.getCurrentPage()) {
	        	$item = $('<li class="active"><span>'+printPage+'</span></li>');
	        } else {
	        	$item = $('<li><a href="#page'+printPage+'">'+printPage+'</a></li>');
	        	$item.find("a").data("page", data);
	        }
	        
	        _list.push($item);
	    });
	
	    if (this.hasNext()) {
	    	$next = $('<li class="next"><a href="#next">&gt;</a></li>');
	    	$next.find("a").data("page", _object.getNext());
	    } else {
	    	$next = $('<li class="next"><span>&gt;</span></li>');
	    }
	    _list.push($next);
	   
	    $.each(_list, function(i,$el){
	    	bindLink($el);
	    	_html.append($el);
	    });
	    
	    var origin = $(this.options.target).find(".pagination");
	    origin.before(_html);
	    origin.remove();
		
	}
		
	,getLastPage: function() {
	    if (0 == this.options.totalCount) {
	        return 0;
	    }
	    return parseInt((this.options.totalCount - 1) / this.options.entriesPerPage);
	},
	getPages: function() {
	    var first = this.options.firstPageInCurrentPageGroup;
	    var last = first + this.options.pagesPerPageGroup - 1;
	    if (last > this.getLastPage()) {
	        last = this.getLastPage();
	    }
	    var _pages = [];
	    for (var i=first; i<=last; i++) {
	        _pages.push(i);
	    }
	    return _pages;
	},
	hasPrevious: function() {
	    return (this.options.currentPage >= this.options.pagesPerPageGroup);
	},
	getPrevious: function() {
	    return this.options.firstPageInCurrentPageGroup - 1;
	},
	getPrevPage: function() {
	    return this.options.currentPage - 1;
	},
	hasNext: function() {
	    return (this.options.firstPageInCurrentPageGroup + this.options.pagesPerPageGroup) <= this.getLastPage();
	},
	getNext: function() {
	    return this.options.firstPageInCurrentPageGroup + this.options.pagesPerPageGroup;
	},
	getNextPage: function() {
	    return this.options.currentPage + 1;
	},
	getCurrentPage: function() {
	    return this.options.currentPage;
	},
	getTotalCount: function() {
	    return this.options.totalCount;
	},
}