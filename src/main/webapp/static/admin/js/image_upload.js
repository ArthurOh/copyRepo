/*
    # 사용법 

        1. 적용 대상
        	<input type=file class="img-upload-ex" >
        	target class "img-upload-ex" 
        	
        2. 옵션
            
			data-width : 가로 리사이즈 크기
            
            data-height : 세로 리사이즈 크기
            
            data-insert : 값이 "Y"면  FILE TABLE에 INSERT 후 fileid 값 리턴 
            (현재 적용안됨)

            data-callback : 값이 있으면 해당 callback 함수를 호출
                
                ex. 
                    CASE 1) data-callback="test1"
					미리정의된 함수의 이름을 스트링으로 넘겨준다.
                    	
                    CASE 2) data-callback=function(){}
					함수를 넘겨준다.

                                                     호출된 함수의 첫번째 인자로 넘어오는 데이터 형식(json)은 아래와 같다.
                    {   
                        filepath: "\image\GZ\HN\20160609215246889b23d57ec-79c8-48b7-95ce-7323ddd07495.jpg", 
                        filerename: "20160609215246889b23d57ec-79c8-48b7-95ce-7323ddd07495.jpg", 
                        fileid: "0"
                    }
		
		3. 기타
		
			동적으로 추가되는 태그에 적용하고 싶은 경우 아래와 같이 호출해준다.
			
			ExFileupload(target element);
*/                

$(document).ready(function() {
	Fileupload = function(target) {
		$(target).fileupload({
	        type : 'POST',
	        url : '/admin/fileuploads/upload/images',
	        dataType: 'json',
	        add: function(e, data) {
	            var uploadFile = data.files[0];
	            data.paramName = "uploadfileImage";
	            var isValid = true;
	            var imageWidth = $(this).data("width"),
	            	imageHeight = $(this).data("height"),
	            	imageType = $(this).data("insertYN") == 'Y'? 'other-image' : undefined;

	            if (!(/(png|jpe?g|png|gif|bmp)$/i).test(uploadFile.name)) {
	                alert('Please select appropriate files that meet the attachment requirements ');
	                isValid = false;
	            } else if (uploadFile.size > 5000000) {
	                alert('You can register images up to 5MB');
	                isValid = false;
	            }
	           
	            if (isValid) {
	                //이미지 세로크기 사용할 경우 추가 : {name:'imageHeight', value:imageHeight}
	                //data.formData = [{name:'uploadfile', value:uploadFile}, {name:'imageWidth', value:imageWidth}];
	                var fd = [];
	                if (imageWidth != undefined) {
	                    fd.push({name:'imageWidth', value:imageWidth});
	                } else {
	                    fd.push({name:'imageWidth', value:0});
	                }
	                if (imageHeight != undefined) {
	                    fd.push({name:'imageHeight', value:imageHeight});
	                } else {
	                    fd.push({name:'imageHeight', value:0});
	                }
	                if (imageType != undefined) {
	                    fd.push({name:'imageType', value:imageType});
	                } else {
	                    fd.push({name:'imageType', value:'no'});
	                }
	                data.formData = fd;
	                data.submit();
	            }
	        }, 
	        done: function(e, data) {            
	            if (data.result.filenowrite == "F") {
	                alert('Invalid file. Please select a different image');
	            } else {
	            	var callback = $(this).data('callback');
	            	if (typeof callback !== 'function') {
	            		callback = window[callback];
	            	}
	            	if (typeof callback == 'function') {
                        callback.call(this, data.result);
                    }
	            }
	        }, fail: function(e, data) {
	            alert('An error occurred during server communication. (img-upload)');
	        }
	    });
	}
	$('.img-upload-ex').each(function(){
		Fileupload(this);
    });
});
