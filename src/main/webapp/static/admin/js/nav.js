/*
 * nav(첫번째 인자, 두번째 인자)
 *
 *  언어코드는 url을 분석하여 처리
 *  /admin/{language}/..
 *  
 *  첫번째 인자(string) 
 * 	 - 콘텐츠관리: 'content'
 * 	 - 전시관리: 'display'
 *   - 제품관리: 'product'
 *   - 운영관리: 'operation'
 *  
 *  두번째 인자(string)
 *   - News: 'new'
 *   - Recipe: 'recipe'
 *   - Newsletter: 'newsletter'
 *   - Contact Us: 'contact'
 *   - Global Network: 'network'
 *   - 상단 배너 관리: 'top'
 *   - 하단 배너 관리: 'bottom'
 *   - 팝업 관리: 'popup'
 *   - 레시피 테마 관리: 'recipe-theme'
 *   - 제품 브랜드 관리: 'brand'
 *   - 제품 관리: 'item'
 *   - 관리자 관리: 'admin-user'
 *   - 뉴스레터 신청자 관리: 'newsletter-apply' 
 **/
function type(data) {
   return Object.prototype.toString.call(data).slice(8,-1).toLowerCase();
}

function nav(menu, menu_item) {
	var pathname = document.location.pathname,
		_pattern = /^\/admin\/(en|cn|ru)(\/.*)?$/,
		_result = _pattern.exec(pathname),
		language = '';
	//console.log(_result);
	if (_result !== undefined && _result !== null && _result.length > 1) {
		language = _result[1]+'-';
	}
	if (type(menu) !== 'string') return;
	var $sidebar = $('aside section ul.sidebar-menu');
	
	if ($sidebar.length === 1) {
		$sidebar.find('.active').removeClass('active');
		
		var $menu = $sidebar.find('.menu-'+menu);
		if ($menu.length === 0) return;
		$menu.addClass('active');
		
		if (type(menu_item) !== 'string') return;
		var $item = $menu.find('.menu-item-'+language+menu_item);
		if ($item.length === 0) return;
		$item.addClass('active');
		
		var $parent = $item.closest('ul');
		if (!$parent.hasClass('.menu-open')) {
			$parent.closest('li').addClass('active');
		}
	}
}