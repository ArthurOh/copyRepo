$(document).ready(function() {
    var imageWidthThumbnail = $('#image-width-thumbnail').val();
    var imageHeightThumbnail = $('#image-height-thumbnail').val();   //이미지 세로크기 사용할 경우 활성화
    
    var imageWidthDefaultImage = $('#image-width-defaultImage').val();
    var imageHeightDefaultImage = $('#image-height-defaultImage').val();

    var imageWidthMobileImage = $('#image-width-mobileImage').val();
    var imageHeightMobileImage = $('#image-height-mobileImage').val();
    
    var imageWidthOther = $('#image-width-other').val();
    var imageHeightOther = $('#image-height-other').val();
    
    var imageWidthBanner = $('#image-width-banner').val();
    var imageHeightBanner = $('#image-height-banner').val();

    $('.img-upload').fileupload({
        type : 'POST',
        url : '/admin/fileuploads/upload/images',
        dataType: 'json',
        add: function(e, data) {
            var imageAddChecked = parseInt($('#image-add-checked').val());    //이미지 업로드 갯수 지정 - 고정    
            var imageAddCount = parseInt($('#image-add-count').val());        //이미지 업로드 갯수 카운트 - 증가,감소

            var uploadFile = data.files[0];
            var isValid = true;
            
            if (($(this).data('image-type')) == "other-image") {    //일반 이미지 (다수개) 업로드 일때
                if ( !isNaN(imageAddChecked) && !isNaN(imageAddCount) ){
                    if (imageAddChecked >= imageAddCount){    // 고정값 > 카운트값
                        imageAddCount =  imageAddCount + 1;
                        $('#image-add-count').val(imageAddCount);
                        isValid = true;
                    } else {
                        alert('You can register up to ' + imageAddChecked + ' images ');
                        isValid =  false;
                    }
                }
            }

            if (!(/png|jpe?g|png|gif/i).test(uploadFile.name)) {
                if (uploadFile.size > 5000000) { // 5mb
                    alert('Please select appropriate files that meet image uploading requirements.');
                    isValid = false;
                } else {
                    imageAddCount =  imageAddCount - 1;
                    $('#image-add-count').val(imageAddCount);

                    alert('Please select appropriate files that meet the attachment requirements');
                    isValid = false;
                }
            } else {
                if (uploadFile.size > 5000000) { // 5mb
                    alert('You can register images up to 5MB');
                    isValid = false;
                }
            }

            if (isValid) {
                //이미지 세로크기 사용할 경우 추가 : {name:'imageHeight', value:imageHeight}
                //data.formData = [{name:'uploadfile', value:uploadFile}, {name:'imageWidth', value:imageWidth}];
                if (($(this).data('image-type')) == "thumbnail-image") {
                    imageWidth = imageWidthThumbnail;
                    imageHeight = imageHeightThumbnail;
                    imageType = "thumbnail";
                } else if (($(this).data('image-type')) == "default-image") {
                	imageWidth = imageWidthDefaultImage;
                    imageHeight = imageHeightDefaultImage;
                    imageType = "defaultImage";
                } else if (($(this).data('image-type')) == "mobile-image") {
                	imageWidth = imageWidthMobileImage;
                    imageHeight = imageHeightMobileImage;
                    imageType = "mobileImage";
                } else if (($(this).data('image-type')) == "banner-image") {
                    imageWidth = imageWidthBanner;
                    imageHeight = imageHeightBanner;
                    imageType = "bannerImage";
                } else if (($(this).data('image-type')) == "other-image") {
                    imageWidth = imageWidthOther;
                    imageHeight = imageHeightOther;
                    imageType = "otherImage";
                }
                data.formData = [{name:'imageWidth', value:imageWidth}, {name:'imageHeight', value:imageHeight}, {name:'imageType', value:imageType}];
                data.submit();
            }
        }, progressall: function(e,data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-image').text('upload ' + progress + '%' );
        }, done: function(e, data) {            
            if (data.result.filenowrite == "F") {
                $('#progress-image').text('');
                var imageAddCount = parseInt($('#image-add-count').val());
                imageAddCount =  imageAddCount - 1;
                $('#image-add-count').val(imageAddCount);
                alert('Invalid file. Please select a different image.');
            } else {
                var filepath = data.result.filepath;
                var fileRename = data.result.filerename;
                var fileID = data.result.fileid;

                if (($(this).data('image-type')) == "thumbnail-image") {
                    //브로슈어 썸네일 등록일때 class명 변경을 위한 조치
                    var lastLocation = location.pathname;
                    var resultLocation = lastLocation.indexOf("/pr/brochure");

                    $('#result-thumbnail').empty();
                    $('#fileinfo-thumbnail').empty();

                    if (resultLocation >= 0) {
                        $('<img class="brochure-thumbnail"/>').attr('src', filepath).appendTo($("#result-thumbnail"));
                    } else {
                        $('<img class="thumbnail"/>').attr('src', filepath).appendTo($("#result-thumbnail"));
                    }
                    $('<input name="filepath_thumbnail" type="hidden" />').attr('value', filepath).appendTo($("#fileinfo-thumbnail"));
                    $('<input name="filename_thumbnail" type="hidden" />').attr('value', fileRename).appendTo($("#fileinfo-thumbnail"));
                    
                } else if (($(this).data('image-type')) == "default-image") {
                	$('#result-defaultImage').empty();
                    $('#fileinfo-defaultImage').empty();
                    
                	$('<img class="thumbnail"/>').attr('src', filepath).appendTo($("#result-defaultImage"));
                	$('<input name="filepath_defaultImage" type="hidden" />').attr('value', filepath).appendTo($("#fileinfo-defaultImage"));
                    $('<input name="filename_defaultImage" type="hidden" />').attr('value', fileRename).appendTo($("#fileinfo-defaultImage"));
                } else if (($(this).data('image-type')) == "mobile-image") {
                	$('#result-mobileImage').empty();
                    $('#fileinfo-mobileImage').empty();
                    
                	$('<img class="thumbnail"/>').attr('src', filepath).appendTo($("#result-mobileImage"));
                	$('<input name="filepath_mobileImage" type="hidden" />').attr('value', filepath).appendTo($("#fileinfo-mobileImage"));
                    $('<input name="filename_mobileImage" type="hidden" />').attr('value', fileRename).appendTo($("#fileinfo-mobileImage"));

                } else if (($(this).data('image-type')) == "banner-image") {
                    $('#result-banner').empty();
                    $('#fileinfo-banner').empty();

                    $('<img class="thumbnail"/>').attr('src', filepath).appendTo($("#result-banner"));
                    $('<input name="filepath_banner" type="hidden" />').attr('value', filepath).appendTo($("#fileinfo-banner"));
                    $('<input name="filename_banner" type="hidden" />').attr('value', fileRename).appendTo($("#fileinfo-banner"));

                } else {
                    $('<p class="fa-remove text-left gdo-image-remove image-info'+ fileID +'" id="image-info'+ +fileID +'">Image delete</p>').appendTo($('#result-OtherImage'));
                    
                    $('<img class="thumbnail"/>').attr('src', filepath).appendTo($('#image-info'+fileID));
                    $('<input name="fileid_image" type="hidden" />').attr('value', fileID).appendTo($('#image-info'+fileID));
                    $('<input name="filepath_image" type="hidden" />').attr('value', filepath).appendTo($('#image-info'+fileID));
                    $('<input name="filename_image" type="hidden" />').attr('value', fileRename).appendTo($('#image-info'+fileID));
                }
               
                e.preventDefault();
            }
        }, fail: function(e, data) {
            alert('An error occurred during server communication. (img-upload)');
            foo = data;
        }
    });

    $('#jquery-upload-file').fileupload({
        type : 'POST',
        url : '/admin/fileuploads/upload/files',
        dataType: 'json',
        add: function(e, data) {
            var fileAddChecked = parseInt($('#file-add-checked').val());    //첨부파일 업로드 갯수 지정 - 고정    
            var fileAddCount = parseInt($('#file-add-count').val());        //첨부파일 업로드 갯수 카운트 - 증가,감소

            var uploadFile = data.files[0];
            var isValid = true;
            
            if ( !isNaN(fileAddChecked) && !isNaN(fileAddCount) ){
                if (fileAddChecked >= fileAddCount){    // 고정값 > 카운트값
                    fileAddCount =  fileAddCount + 1;
                    $('#file-add-count').val(fileAddCount);
                    isValid = true;
                } else {
                    alert('You can register up to ' + fileAddChecked + ' attached files ');
                    isValid =  false;
                }
            }

            if (!(/ppt|pptx|pdf|zip|doc|docx|xls|xlsx/i).test(uploadFile.name)) {
                if (uploadFile.size > 5000000) { // 5mb
                    alert('Please select appropriate files that meet attached file uploading requirements.');
                    isValid = false;
                } else {
                    alert('Attachable file formats are.pdf, .ppt, .pptx, .doc, .docx, .xls, .xlsx, .zip.');
                    isValid = false;
                }
            } else {
                if (uploadFile.size > 5000000) { // 5mb
                    alert('You can register 5MB or less per file.');
                    isValid = false;
                }
            }

            if (isValid) {
                data.submit();
            }
        }, progressall: function(e,data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-file').text('upload ' + progress + '%' );
        }, done: function(e, data) {
            var filepath=data.result.filepath;
            var fileRename=data.result.filerename;
            var fileOrgname=data.result.fileorgname;
            var fileID = data.result.fileid;

            $('<div class="sp-createfile-remove file-info'+ fileID +'" id="file-info'+ +fileID +'"><i class="fa fa-fw fa-remove"></i></div>').appendTo($('#result-file'));
            $('<p class="file-info">'+ fileOrgname +'</p>').appendTo($('#file-info'+fileID));
            $('<input name="filename_file" type="hidden" />').attr('value', fileRename).appendTo($('#file-info'+fileID));
            $('<input name="filepath_file" type="hidden" />').attr('value', filepath).appendTo($('#file-info'+fileID));
            $('<input name="fileid_file" type="hidden" />').attr('value', fileID).appendTo($('#file-info'+fileID));

            //$('#file-add-count').val(fileAddCount);
            e.preventDefault();
        }, fail: function(e, data) {
            alert('An error occurred during server communication. (jquery-upload-file)');
            fileAddCount =  fileAddCount - 1;
            $('#file-add-count').val(fileAddCount);
            foo = data;
        }
    });

    // create page - 이미지 삭제
    $("#result-image").on("click", ".gdo-image-remove", function () {
        if (confirm("You cannot undo the deletion. Do you want to delete it?") == true) {
            var imageAddCount = parseInt($('#image-add-count').val()); //이미지 업로드 갯수 카운트 - 감소
            imageAddCount = imageAddCount - 1;

            $('#image-add-count').val(imageAddCount);
            $(this).empty();
            $(this).remove();
            $("#fileinfo-image").empty();
        } else {
            return;
        }
    });
    
    // create page - 파일 삭제
    $("#result-file").on("click", ".sp-createfile-remove", function () {
        if (confirm("You cannot undo the deletion. Do you want to delete it?") == true) {
            var fileAddCount = parseInt($('#file-add-count').val()); //첨부파일 업로드 갯수 카운트 - 감소
            fileAddCount = fileAddCount - 1;

            $('#file-add-count').val(fileAddCount);
            $(this).empty();
            $(this).remove();
        } else {
            return;
        }
    });
});

//edit page - 첨부파일, 이미지 삭제
function fileremove(fileID, postID, type){
    var fileID = fileID;
    var postID = postID;
    var actionUrl = "/admin/fileuploads/fileremove/"+type;

    if (type == "file"){
        var fileAddCount = parseInt($('#file-add-count').val()); //첨부파일 업로드 갯수 카운트 - 감소
        fileAddCount = fileAddCount - 1;
    } else if (type == "image") {
        var imageAddCount = parseInt($('#image-add-count').val()); //이미지 업로드 갯수 카운트 - 감소
        imageAddCount = imageAddCount - 1;
    }

    $.ajax({
        type: "POST",
        url: actionUrl,
        data: {'fileid' : fileID, 'postid' : postID},
        dataType:"json",
        success: function(data){
            var result = data.result;
            if( result == 'F' ){
                alert("The file is not deleted.");
            } else {
                alert("The file is deleted.")
                $(".sp-fileinfo-"+fileID).empty();
                $(".fileinfo-" + type + "-" + fileID).empty();

                if (type == "file"){
                    $('#file-add-count').val(fileAddCount);
                } else if (type == "image") {
                    $('#image-add-count').val(imageAddCount);
                }
            }
        },
        error : function(){
            alert("An error occurred during server communication.");
        }
    })
}

// remove for Thumbnail
$('.sp-thumbnail-remove').click( function() {
	$('#result-thumbnail').empty();
	$('#fileinfo-thumbnail').empty();
	$('.sp-thumbnail-remove').remove();
})

// remove for Default Image 
$('.sp-defaultImage-remove').click( function() {
	$('#result-defaultImage').empty();
	$('#fileinfo-defaultImage').empty();
	$('.sp-defaultImage-remove').remove();
})

// remove for Mobile Image
$('.sp-mobileImage-remove').click( function() {
	$('#result-mobileImage').empty();
	$('#fileinfo-mobileImage').empty();
	$('.sp-mobileImage-remove').remove();
})

// remove for Other Image
$('.sp-otherImage-remove').click( function() {
	$('#result-otherImage').empty();
	$('#fileinfo-otherImage').empty();
	$('.sp-otherImage-remove').remove();
})

// remove for banner Image
$('.sp-bannerImage-remove').click( function() {
	$('#result-banner').empty();
	$('#fileinfo-banner').empty();
	$('.sp-bannerImage-remove').remove();
})