/**
 * Requirejs module sample
 */
define(['ui', 'jquery'], function(IG, $) {
    'use strict';

    function Pagination(options) {
        this.options = $.extend({
            current: 1,
            perPage: 6,
            items: '.item',
            control: '.pagination',
            onShow: $.noop
        }, options);

        this.$control = $(this.options.control);
        this.$first = this.$control.find('.page-first');
        this.$prev = this.$control.find('.page-prev');
        this.$next = this.$control.find('.page-next');
        this.$last = this.$control.find('.page-last');
        this.$pages = this.$control.find('.page-wrap.l-desktop');
        this._$page = $('<a href="#" class="page"></a>');

        this.$items = $(this.options.items);

        this.small = {
            $current: this.$control.find('.page-current'),
            $total: this.$control.find('.page-total')
        };

        this._current = this.options.current;
        this._total = this.$items.length;
        this._pages = Math.ceil(this._total / this.options.perPage);

        this._init();
    }

    Pagination.prototype = {
        _init: function() {
            var self = this;

            function onClick(event) {
                event.preventDefault();

                var $this = $(this),
                    page = $this.data('page');

                if(this._current === page) {
                    return;
                }

                self.show(page);
            }

            if(this._pages <= 1) {
                this.$control.hide();
                return;
            }

            this._setControl();

            this._$page
                .add(this.$first)
                .add(this.$last)
                .add(this.$prev)
                .add(this.$next)
                .off('click').on('click', onClick);

            this._buildPages();
            this.show(this._current);
        },
        _setControl: function() {
            this.$first.data('page', 1);
            this.$last.data('page', this._pages);
            this.$prev.data('page', Math.max(1, this._current - 1));
            this.$next.data('page', Math.min(this._pages, this._current + 1));
        },
        reset: function(page) {
            if(page) {
                this._current = page;
            }

            this._$page
                .add(this.$first)
                .add(this.$last)
                .add(this.$prev)
                .add(this.$next)
                .off('click');

            this._init();
        },
        _buildPages: function() {
            this.$pages.empty();

            for(var i = 1; i <= this._pages; i++) {
                this._$page
                    .clone(true)
                    .text(i)
                    .attr('href', '#page' + i)
                    .addClass('page-' + i)
                    .data('page', i)
                    .appendTo(this.$pages);
            }

            this.$page = this.$pages.find('.page');
            this.small.$total.text(this._pages);
            this.$control.show();
        },
        show: function(page) {
            var start = (page - 1) * this.options.perPage,
                end = start + this.options.perPage;

            this.$prev.data('page', Math.max(1, page - 1));
            this.$next.data('page', Math.min(this._pages, page + 1));

            this.$page.removeClass('is-current').eq(page - 1).addClass('is-current');
            this.small.$current.text(page);
            this.$items.hide().slice(start, end).show();

            this._current = page;

            this.options.onShow();
        }
    };

    return Pagination;
});