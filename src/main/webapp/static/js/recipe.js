/**
 * 제품 상세
 */
define(['ui', 'jquery', 'gsap', 'slick'], function(IG, $) {
    'use strict';

    // cover arrow 애니메이션
    $(function(){
        // $('.research-visual-video').ytiframe();

        var $cover = $('.visual-cover'),
            $continue = $cover.find('.cover-continue'),
            $continueBtn = $continue.find('.cover-continue-btn'),

            continueTl = new TimelineMax({
                paused: true,
                repeat: -1,
                repeatDelay: 1.2
            });

        // 계속하기 화살표
        continueTl
            .to($continueBtn, 0.2, { yPercent: -20 })
            .to($continueBtn, 0.2, { yPercent: 20, ease: Power1.easeOut })
            .to($continueBtn, 0.2, { yPercent: 0 });

        continueTl.play();

        $continueBtn.on('click', function(event) {
            var offset = IG.headerHeight + $('.project-header').outerHeight();
            $('html, body').animate({ scrollTop: offset }, 700, 'easeInOutCubic');
            event.preventDefault();
        });
    });
    
    // Recipes 슬라이더
    $(function() {
        var $section = $('.carousel'),
            slickOpts = {
                product: {
                    responsive: [
                        {
                            breakpoint: 400,
                            settings: {
                                slidesToShow: 2
                            }
                        }, {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 4
                            }
                        }, {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 6
                            }
                        }
                    ]
                }
            };

        $section.each(function() {
            var $this = $(this),
                $list = $this.find('.carousel-list'),
                prev = $this.find('.carousel-prev').remove()[0].outerHTML,
                next = $this.find('.carousel-next').remove()[0].outerHTML,
                type = $this.attr('data-type');

            if(!$list.children().length) {
                return true;
            }

            $list.slick($.extend({
                slidesToShow: 1,
                prevArrow: prev,
                nextArrow: next,
                easing: 'easeOutQuart',
                cssEase: 'cubic-bezier(0.15, 0.8, 0.5, 1)',
                mobileFirst: true
            }, slickOpts[type]));
        });
    });
});