define(['ui', 'jquery'], function(IG,$){

	$(document).ready(function(){
		var $itemResult = $('#product-list');
		
		$(".sp-brand-category").click(function(e){			
			$.ajax({
				url : $(this).data('action-url'),
				type : "GET",
				data : $(this).serialize(),
	            dataType : "text",
	            success : function(data) {
	            	$itemResult.html(data);
	            },
	            error : function() {
	                console.log("error");
	            }
			});
			$(".sp-brand-category").parents().removeClass("is-current");
			$(this).parent("#bc-list").addClass("is-current");
		});
	});

});