define(['jquery', 'lib/jquery.iframe-transport', 'fileupload'], function($){
	var Fileupload = function(target) {
	    $(target).fileupload({
	        type : 'POST',
	        url : '/fileuploads/upload/files?noInsert=true',
	        dataType: 'json',
	        add: function(e, data) {
	        	
	        	// data-limited=y 값으로 첨부파일 개수 제한 확인 (해당 속성값은 페이지단에서 확인하여 넣어주고 빼준다.)
	        	//
	        	var _limited = $(this).data('limited');
	        	if (_limited === 'y') {
	        		alert('You have exceeded the maximum number of attachment uploads.');
	        		return;
	        	}
	        	
	            var uploadFile = data.files[0];
	            data.paramName = "uploadfileFile";
	            var isValid = true;
	            if (!(/(jpg|jpge|gif|bmp|png|pdf|docx|xlsx|pptx)$/i).test(uploadFile.name)) {
	                alert('Attachable file formats are jpg, jpeg, gif, bmp, png, pdf, docx, xlsx, pptx.');
	                isValid = false;
	            } else if (uploadFile.size > 5000000) { // 5mb
                    alert('You can register up to 5 attachments, 5MB or less per file.');
                    isValid = false;
	            }
	            if (isValid) {
	                data.submit();
	            }
	        }, progressall: function(e,data) {
	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            $('#progress-file').text('upload ' + progress + '%' );
	        }, done: function(e, data) {
        		var callback = $(this).data('callback');
        		if (typeof callback !== 'function' ) {
        			callback = window[callback];
        		}
        		if (typeof callback === 'function') {
        			callback.call(this, data.result);
        		}
	        }, fail: function(e, data) {
	            alert('An error occurred during server communication.');
	        }
	    });
	}

	$(document).ready(function() {
	    $('.file-upload-ex').each(function(){
	    	Fileupload(this);
	    });
	});
	return Fileupload;
});