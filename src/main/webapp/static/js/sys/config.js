// ui팀에서 생성한  config.js 와 충돌을 피하기 위해 별도 분리(syslab 사용)   
require.paths['validate'] = 'lib/jquery.validate.min';
require.paths['fileupload'] = 'lib/jquery.fileupload';
require.paths['jquery.ui.widget'] = 'lib/jquery-ui.min';

require.shim['validate'] = {deps: ['jquery']};
require.shim['fileupload'] = {deps: ['jquery', 'lib/jquery.iframe-transport']};
require.shim['jquery.ui.widget'] = {deps: ['jquery']};
require.shim['lib/jquery.iframe-transport'] = {deps: ['jquery']};
