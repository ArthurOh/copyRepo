/**
 * 제품 리스트
 */
define(['ui', 'jquery', 'masonry', 'drawborder'], function(IG, $, Masonry) {

    $(function() {
        //links rollover effect
        $('.block').drawBorder();
    });

    require(['jquery-bridget/jquery.bridget'], function(jQueryBridget) {
        jQueryBridget( 'masonry', Masonry, $ );

        var msnList = $('#recipe-list').masonry({
            itemSelector: '.item',
            gutterWidth: 0,
            isAnimated: true,
            animationOptions: {
                duration: 300,
                queue: false
            }
        });

        msnList.imagesLoaded().progress(function() {
            msnList.masonry('layout');
            IG.contentHeight = $(document).height();
        });
    });

});