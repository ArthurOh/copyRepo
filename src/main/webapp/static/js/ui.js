/**
 * 전체 페이지 공통 실행 모듈
 */
define(function(require) {
    'use strict';

    void 0;

    // load modules
    var IG              =   require('global'),
        legacy          =   require('ui/legacy'),
        FastClick       =   require('fastclick'),
        SwitchSelect    =   require('ui/switchselect'),
        placeHolder     =   require('ui/placeholder'),
        ytiframe        =   require('ytiframe'),
        selectbox       =   require('ui/selectbox'),
        simpletab       =   require('simpletab'),
        accordion       =   require('ui/accordion'),
        modal           =   require('modal'),
        Notify          =   require('ui/notify'),
        rating          =   require('rating');

    /**
     * 본문 바로가기 링크 작동
     */
    function skipNav() {
        IG.$main.on('focusout', function() {
            IG.$main.removeAttr('tabindex');
        });
        $('#skipnav').on('click', function(event) {
            IG.$main
                .attr('tabindex', '-1')
                .focus();

            event.preventDefault();
        });
    }

    /**
     * 내용 처음으로 가기
     */
    function goToTop() {
        var $topBtn = $('#go-to-top');

        // 네이버 앱으로 접속 시 앱에서 제공하는 UI와 겹치기 때문에 해당 기능 삭제
        if(IG.isNaver) {
            $topBtn.remove();
            return;
        }

        // iOS 모바일 사파리에서 뷰포트 하단 영역 터치 시
        // 브라우저 도구모음이 우선으로 표시되는 동작 때문에 버튼 위치를 수정
        if(IG.ua.browser.name === 'Mobile Safari') {
            $topBtn.addClass('go-to-top-safari');
        }

        // 하단에 패널 존재할 때 버튼 위치를 수정
        if($('.panel').length) {
            $topBtn.addClass('go-to-top-panel');
        }

        IG.$win.on('scroll', function() {
            $topBtn.toggleClass('is-active', IG.scrollTop >= 300);
        });

        $topBtn.on('click', function(event) {
            $('html, body').animate({ scrollTop: 0 }, 500, 'easeOutExpo', function() {
                IG.$main.attr('tabindex', '-1').focus();
            });
            event.preventDefault();
        });
    }

    /**
     * footer 링크 조정
     */
    function setFooterLinks() {
        if(IG.isLegacyIE) {
            return;
        }

        // 관련사이트 링크 설정
        var $familySites = IG.$footer.find('#familysite .selectbox-option a');

        if(IG.isMobile) {
            $familySites.each(function() {
                var $this = $(this),
                    href = $this.data('mobile-href');

                if(href) {
                    $this.attr('href', href);
                }
            });
        }
    }

    function fixChkbox($target) {
        var $labels = $target.find('label');

        $labels.on('click', function() {
            var $this = $(this),
                $control = $this.prevAll('input').eq(0);

            $control.prop('checked', true);
            $labels.removeClass('is-checked');
            $this.addClass('is-checked');
        });

        $target.find('input').nextAll('label').eq(0).addClass('is-checked');
    }

    /**
     * 전체 페이지 공통 실행
     */
    $(function() {
        // 디버그용 모듈 로드
        void 0;

        // 메뉴 작동
        IG.Nav  = require('ui/nav');

        window.IG = IG;

        // FastClick 적용
        FastClick.attach(document.body);
        void 0;

        // 통합검색을 제외한 입력칸에 placeholder polyfill 적용
        $('input[placeholder], textarea[placeholder]').not('.global-search-query').placeholder();

        // UI 작동 함수 실행
        IG.Nav.init();
        skipNav();
        goToTop();
        setFooterLinks();
        legacy();

        // Youtube player 작동
        if(IG.isMobile) {
            $('.yt-player').ytiframe();
        } else {
            $('.yt-player').on('click', function(event) {
                $(this).ytiframe({
                    autoplay: true
                });

                event.preventDefault();
            });
        }

        // 커스텀 select box 작동
        selectbox();
        new SwitchSelect($('.switch-select'));

        // 커스텀 라디오 및 체크박스 fix
        if(IG.isLegacyIE) {
            $('.checkbox-fix').each(function() {
                fixChkbox($(this));
            });
        }

        // Modal 플러그인 기본값 설정
        $.modal.defaults = {
            escapeClose: false,
            clickClose: false,
            closeText: '닫기',
            closeClass: '',
            showClose: true,
            modalClass: 'modal',
            spinnerHtml: null,
            showSpinner: true,
            fadeDuration: 200,
            fadeDelay: 0
        };

        // footer > contact accordion
        /*
        var contact_accordion = null;

        IG.UI.switchLayout('switch_accordion', function() {
            return IG.size < IG.BP_SMALL;
        }, function(state) {
            if(state) {
                if ( contact_accordion === null ) {
                    contact_accordion = $('.contact-accordion').accordion({
                        titleSelector:  '.button',
                        contSelector:   '.inner'
                    });
                }
            } else {
                if ( contact_accordion !== null ) {
                    contact_accordion.destroy();
                    contact_accordion = null;
                }
            }
        });
        */

        // rating 모듈 동작
        $('.rating').rating();

        // 알림 모듈 초기화
        IG.notify = new Notify();
    });

    // 전역 객체 리턴
    return IG;
});