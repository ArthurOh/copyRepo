/**
 * Contact Us
 */
define(['ui', 'jquery', 'ui/selectbox', 'gsap', 'sticky', 'scrollto', 'simpletab'], function(IG, $, selectbox) {
    'use strict';

    $(function() {
        var $lnb = $('.lnb'),
            $lnbWrap = $('.lnb-wrap'),
            $lnbList = $('.lnb-list'),
            $section = $('.membership-target-section'),
            $selected = $lnb.find('.selectbox-option > li').eq(IG.Nav.current[1]),
            $lnbTarget = $('.main .l-content'),

            startOffset = 155,

            currentSection, prevSection;

        selectbox('.lnb-select');

        // LNB 현재 메뉴 표시
        $selected.addClass('is-current');
        $lnb.find('.selector-text').text($selected.text());

        // LNB 고정
        IG.$win.on({
            'scroll': setLnbNav,
            'resize': setLnbNav,
            'load': setLnbNav
        });

        setLnbNav();
        setTimeout(setLnbNav, 100);

        function setLnbNav() {
            if ( $lnb.length == 0 ) return;

            if ( IG.size < IG.BP_LARGE ) {
                $lnb.css({
                    position: '',
                    top: ''
                });

                return;
            }

            var lnbWrapTop = $lnbWrap.offset().top,
                lnbHeight = $lnb.height(),
                contentHeight = $lnbTarget.outerHeight(),
                contentRange = $lnbTarget.offset().top + contentHeight;

            if(lnbWrapTop - IG.scrollTop <= startOffset) {
                if((IG.scrollTop + startOffset + lnbHeight) < contentRange) {
                    $lnb.css({
                        position: 'fixed',
                        top: startOffset
                    });
                } else {
                    $lnb.css({
                        position: 'absolute',
                        top: (contentRange) - (lnbWrapTop + lnbHeight)
                    });
                }
            } else {
                $lnb.css({
                    position: '',
                    top: ''
                });
            }
        }

        // process
        $('.process-info').accordion({
            titleSelector:  '.q',
            contSelector:   '.a'
        });

        // 콘텐츠 탭 처리
        $('#detail-tabs').simpleTab({
            activeClass: 'is-current',
            scroll: true,
            scrollOffset: -80
        });

        // faq
        $('.faq-list').accordion({
            titleSelector:  '.q',
            contSelector:   '.a'
        });

        //membership
        var membership_accordion = null;

        IG.UI.switchLayout('switch_accordion', function() {
            return IG.size < IG.BP_SMALL;
        }, function(state) {
            if(state) {
                if ( membership_accordion === null ) {
                    membership_accordion = $('.membership-accordion').accordion({
                        activeClass: 'is-current',
                        titleSelector:  '.class-item-title',
                        contSelector:   '.class-item-inner'
                    });
                }
            } else {
                if ( membership_accordion !== null ) {
                    membership_accordion.destroy();
                    membership_accordion = null;
                }
            }
        });

        // LNB 링크
        $lnbList.find('a').on('click', function(event) {
            var $this = $(this),
                href = $this.attr('href'),
                $target = $(href);

            if(href.indexOf('#') === -1) {
                return;
            }

            setSectionPos($target, true);

            event.preventDefault();
        });

        // 색션 위치지정
        function setSectionPos($target, isAnimate) {
            void 0;

            var targetHeight,
                targetTop = $target.offset().top;

            // target의 offset이 없을 때는 실행 하지 않음
            if(isNaN(targetTop)) {
                return;
            }

            // 위치이동 효과 지정
            if(isAnimate) {
                setPageMove(targetTop, IG.headerHeight);
            } else {
                $('html, body').scrollTop(targetTop - IG.headerHeight);
            }
        }

        IG.$win.on("scroll", function() {
            getCurrentSection();
        });

        // 현재 색션 찾기
        function getCurrentSection() {

            $section.each(function(i) {
                var $this = $(this);

                if( $this.offset().top + $this.height() - (IG.scrollTop + 40) > 0) {
                    currentSection = i;

                    // console.log( ($this.offset().top + $this.height() +'-'+ (IG.scrollTop)) +'='+ ($this.offset().top + $this.height() - (IG.scrollTop)) );

                    return false;
                }else if(i + 1 === $section.length) {
                    currentSection = i;
                }
            });

            // 섹션 변경 시 이벤트 실행
            if(prevSection != currentSection) {
                changeSection(currentSection);
                prevSection = currentSection;
            }
        }

        // 현재 색션 LNB 활성화
        function changeSection(current) {
            void 0;

            $lnbList.find('li').removeClass("is-current");
            $lnbList.find('li').eq(current).addClass("is-current");
        }

        //위치 이동
        function setPageMove(scrollTarget, correction) {
            void 0;

            TweenMax.to(IG.$win, 1.8, {
                scrollTo: {
                    y: Math.max(0, scrollTarget - correction),
                    autoKill: true
                },
                ease: Power4.easeInOut,
                overwrite: 'preexisting'
            });
        }
    });

    $(function() {
        var $tabs = $('#info-premise');

        // 콘텐츠 탭 처리
        $tabs.simpleTab({
            activeClass: 'is-current',
            scroll: true,
            scrollOffset: -80
        });
    });

    /**
     * 파일 첨부
     */
    $(function() {
        var $attach = $('#attach');

        if(!$attach.length) {
            return;
        }

        var $control = $attach.find('.attach-control'),
            $msg = $attach.find('.attach-msg'),
            defaultMsg = $msg.text(),
            getFile;

        getFile = (function() {
            // Filelist를 지원하지 않을 경우
            if(typeof $control[0].files !== 'object') {
                return function() {
                    var val = $control.val();

                    if(!val) {
                        return false;
                    }

                    return $control.val().replace(/^.*[\\\/]/, '');
                }
            }

            return function() {
                var files = $control[0].files;

                if(!files.length) {
                    return false;
                }

                return files[0].name;
            }
        })();

        $control.on('change', function() {
            var filename = getFile();

            $msg.text(filename ? filename : defaultMsg);
        });
    })
});