/**
 * 제품 페이지
 */
define(['ui', 'jquery', 'underscore', 'gsap', 'slick', 'drawborder', 'simpletab'], function(IG, $, _, gsap) {

    // cover arrow 애니메이션
    $(function(){
        // $('.research-visual-video').ytiframe();

        var $cover = $('.visual-cover'),
            $continue = $cover.find('.cover-continue'),
            $continueBtn = $continue.find('.cover-continue-btn'),

            continueTl = new TimelineMax({
                paused: true,
                repeat: -1,
                repeatDelay: 1.2
            });

        // 계속하기 화살표
        continueTl
            .to($continueBtn, 0.2, { yPercent: -20 })
            .to($continueBtn, 0.2, { yPercent: 20, ease: Power1.easeOut })
            .to($continueBtn, 0.2, { yPercent: 0 });

        continueTl.play();

        $continueBtn.on('click', function(event) {
            var offset = IG.headerHeight + $('.project-header').outerHeight();
            $('html, body').animate({ scrollTop: offset }, 700, 'easeInOutCubic');
            event.preventDefault();
        });
    });

    function init() {
        var $tabs = $('#detail-tabs');

        // 콘텐츠 탭 처리
        $tabs.simpleTab({
            activeClass: 'is-current',
            scroll: true,
            scrollOffset: -80,
            onChange: function($target) {
                var $slider = $target.find('.slick-initialized');

                if(!$slider.length) {
                    return;
                }

                $slider.slick('setPosition');
                setPaging($slider.slick('slickCurrentSlide') + 1, $slider.find('.slick-slide').length);
            }
        });
    }

    $(function() {
        // links rollover effect
        $('.block').drawBorder();

        init();
    });

});
