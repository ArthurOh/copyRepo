/**
 * 샘표 소개 메뉴 공통
 */
define(['ui', 'jquery', 'gsap', 'scrollto'], function(IG, $) {
    'use strict';

    var $content = $('.main.about'),
        $sections = $content.find('.history-section'),
        $nav = $content.find('.nav'),
        $navLink,
        $continueBtn = $content.find('.cover-continue-btn'),

        currentSection,
        prevSection;

    function About() {
        this.actions = {};
        this.onChangeSection = $.noop;
    }

    About.prototype = {
        /**
         * 페이지 네비게이션 초기화
         */
        nav: function() {
            var self = this,
                $navList = $nav.find('.nav-list'),
                $item = $navList.find('.item').remove().clone(),
                appendTmr;

            function align() {
                $navList.css('margin-top', (-$navList.outerHeight() / 2)|0);
            }

            function init() {
                $navLink = $navList.find('.item-link');

                $navLink.on('click', function(event) {
                    var $this = $(this),
                        href = $this.attr('href');

                    self.gotoSection(href);

                    if(IG.isMobile) {
                        $this.addClass('is-hover');

                        setTimeout(function() {
                            $this.removeClass('is-hover');
                        }, 1500);
                    }

                    event.preventDefault();
                });

                self.setNavItem(currentSection);

                if(!IG.isLegacyIE) {
                    TweenMax.from($navList, 1, {
                        autoAlpha: 0,
                        xPercent: 100,
                        ease: Power4.easeOut
                    });
                } else {
                    $navList.find('.item-link').append('<span class="dot"></span>')
                }

                IG.$win.on('resize.about.nav', align());
                align();
            }

            // 섹션에 따라 아이템 만들기
            $sections.each(function() {
                var $this = $(this),
                    $_item = $item.clone(),
                    id = $this.attr('id'),
                    title = $this.data('title') || $this.find('h1, h2').eq(0).text();

                $_item
                    .find('.item-link')
                    .attr('href', '#' + id)
                    .find('.item-label')
                    .text(title);

                $_item.appendTo($navList);
            });

            // 아이템 만들어지면 초기화 후 보여줌
            appendTmr = setInterval(function() {
                if($navList.find('.item').length === $sections.length) {
                    clearTimeout(appendTmr);
                    init();
                }
            }, 1);
        },

        gotoSection: function(target) {
            var $target = $(target),
                scrollTarget = $target.offset().top - IG.headerHeight,

                targetHeight,
                windowHeight;

            // 섹션 이동을 중앙으로 정렬 시
            if($target.is('[data-nav-center]')) {
                windowHeight = IG.height - IG.headerHeight;
                targetHeight = $target.outerHeight(true);

                // 섹션 크기가 윈도우 크기보다 높으면 정렬하지 않음
                if(windowHeight > targetHeight) {
                    scrollTarget -= (windowHeight - targetHeight) / 2;
                }
            }

            if($target.is('[data-offset]')) {
                scrollTarget += parseInt($target.attr('data-offset'));
            }

            TweenMax.to(IG.$win, 1.8, {
                scrollTo: {
                    y: Math.max(0, scrollTarget),
                    autoKill: true
                },
                ease: Power4.easeInOut,
                overwrite: 'preexisting'
            });
        },

        /**
         * 네비게이션에 현재 섹션 표시
         * @param {number} current
         */
        setNavItem: function(current) {
            // 네비게이션 초기화가 안된 경우 종료
            if(!$navLink || !$navLink.length) {
                return;
            }

            // 섹션 네비게이션에 현재 섹션 표시
            $navLink
                .removeClass('is-current')
                .eq(current)
                .addClass('is-current');
        },

        /**
         * 현재 섹션 찾기
         */
        getCurrentSection: function() {
            var self = this;

            // 스크롤 위치에 따라 현재 섹션 찾기
            $sections.each(function(i) {
                var $this = $(this);

                if($this.offset().top - IG.scrollTop + $this.outerHeight() > (IG.height / 2)) {
                    currentSection = i;
                    return false;
                } else if(i + 1 === $sections.length) {
                    currentSection = i;
                }
            });

            // 섹션 변경 시 이벤트 실행
            if(prevSection != currentSection) {
                self.changeSection.call(self, currentSection);
                prevSection = currentSection;
            }
        },

        /**
         * 섹션 변경 이벤트
         * @param current
         */
        changeSection: function(current) {
            void 0;

            var $current = $sections.eq(current);

            // 네비게이션에 현재 섹션 표시
            this.setNavItem(current);

            // 섹션별 callback 호출
            if(!$current.data('isActivated')) {
                if(typeof this.actions[$current.attr('id')] === 'function') {
                    this.actions[$current.attr('id')]($current);
                }

                $current.data('isActivated', true);
            }

            this.onChangeSection($current, current);
        },

        /**
         * 커버섹션 계속 버튼 동작
         */
        continueBtn: function() {
            var self = this;

            // 계속하기 버튼 클릭 시 2번째 섹션으로 이동
            $continueBtn.on('click', function() {
                self.gotoSection($sections.eq(1));
            });
        },

        scroll: function() {
            this.getCurrentSection();
        },

        resize: function() {
            this.scroll();
        },

        // 초기화
        init: function() {
            var self = this,
                hash = document.location.hash.replace(/^#_/, '#'),
                $target;

            // 페이지 네비게이션 만들기, 초기화
            this.nav();

            // 계속하기 버튼 동작
            this.continueBtn();

            IG.$win.on({
                'scroll.about': function() {
                    self.getCurrentSection();
                },
                'resize.about': function() {
                    self.scroll();
                },
                'load.about': function() {
                    self.resize();
                }
            });

            // hash 포함 접속 시 섹션으로 바로 이동
            if(hash) {
                $target = $(hash);

                if($target.length) {
                    this.gotoSection(hash);
                }
            }

           this.resize();
        }
    };

    return About;
});