/**
 * 샘표 소개 > 샘표 70년
 */
define(['ui', 'jquery', 'underscore', 'gsap', 'about', 'drawborder', 'inview'], function(IG, $, _, gsap, About) {
    'use strict';

    $(function() {
        var about = new About();

        /**
         * cover 애니메이션
         */
        (function() {
            var $cover = $('.cover'),
                $desc = $cover.find('.cover-desc'),
                $continue = $cover.find('.cover-continue'),
                $continueBtn = $continue.find('.cover-continue-btn'),

                tl = new TimelineMax({
                    paused: true,
                    delay: 0.5
                }),
                continueTl = new TimelineMax({
                    paused: true,
                    repeat: -1,
                    repeatDelay: 1.2
                });

            // viewport size 단위 미지원 시 수동으로 스타일 설정
            if(!Modernizr.cssvhunit && !Modernizr.cssvwunit) {
                IG.$win.on('resize.about', function() {
                    // 커버 섹션 높이
                    $cover.height(IG.height);
                }).resize();
            }

            // cover 타임라인
            if(!IG.isLegacyIE) { // 기본 동작
                // 계속하기 화살표
                continueTl
                    .to($continueBtn, 0.5, { yPercent: -20 })
                    .to($continueBtn, 0.5, { yPercent: 20, ease: Power1.easeOut })
                    .to($continueBtn, 0.5, { yPercent: 0 });

                tl
                    // 1번째 화면
                    .set($cover.find('.cover-content'), { visibility: 'visible' })
                    .from($cover.find('.cover-bg'), 1, { autoAlpha: 0 })
                    .fromTo($cover.find('.cover-h'), 3, { autoAlpha: 0 }, { autoAlpha: 1, ease: Power4.easeOut }, '-=0.5')
                    .to($cover.find('.cover-h'), 2, { autoAlpha: 0, ease: Power2.easeIn }, '-=2')

                    // 2번째 화면
                    .from($cover.find('.cover-year'), 2, { autoAlpha: 0, ease: Power2.easeOut });

                if($cover.find('.cover-desc > br').css('display') === 'none') {
                    $desc = $cover.find('.cover-desc');
                } else {
                    $desc.css('visibility', 'visible');
                    $desc = $cover.find('.cover-desc > span').css('visibility', 'hidden');
                }

                tl
                    .staggerFrom($desc, 2, { autoAlpha: 0, y: 20, ease: Power4.easeOut }, 0.15, '-=1')
                    .call(function() {
                        continueTl.play();
                    })
                    .from($continue, 1.5, { autoAlpha: 0, yPercent: -25, ease: Power3.easeOut }, '-=1');
            } else { // 구버전 IE 용 동작
                $desc.css('visibility', 'visible');
                $desc = $cover.find('.cover-desc > span').css('visibility', 'hidden');

                tl
                    .set($cover.find('.cover-content'), { visibility: 'visible' })
                    .fromTo($cover.find('.cover-h'), 3, { autoAlpha: 0 }, { autoAlpha: 1, ease: Power4.easeOut })
                    .to($cover.find('.cover-h'), 2, { autoAlpha: 0, ease: Power2.easeIn }, '-=1.5')

                    .fromTo($cover.find('.cover-year'), 2, { autoAlpha: 0 }, { autoAlpha: 1, ease: Power2.easeOut })

                    .staggerFromTo($desc, 2, { autoAlpha: 0, y: 20 }, { autoAlpha: 1, y: 0, ease: Power4.easeOut }, 0.15, '-=1')
                    .fromTo($continue, 1.5, { alpha: 0 }, { alpha: 0.5, ease: Power3.easeOut }, '-=1');
            }

            tl.play();
        })();

        /**
         * 섹션별 동작
         */
        (function() {
            // 샘표 소개 페이지 시작
            about.init();
        })();

        /**
         * modal layer 동작
         */
        (function () {
            var $imgLayer = $('#image-layer'),
                $largeImg = $('#large-image'),

                $videoLinks = $('.thumb .player'),
                $videoLayer = $('#video-layer'),
                $player = $videoLayer.find('.video-box-player'),
                $caption = $videoLayer.find('.video-box-caption'),
                $captionText = $videoLayer.find('.video-box-caption-text'),

                scroller;

            $.modal.defaults = {
                escapeClose: true,    // Allows the user to close the modal by pressing `ESC`
                clickClose: true,     // Allows the user to close the modal by clicking the overlay
                closeText: '닫기',    // Text content for the close <a> tag.
                closeClass: '',       // Add additional class(es) to the close <a> tag.
                showClose: true,      // Shows a (X) icon/link in the top-right corner
                modalClass: 'image-popup',  // CSS class added to the element being displayed in the modal.
                spinnerHtml: null,    // HTML appended to the default spinner during AJAX requests.
                showSpinner: true,    // Enable/disable the default spinner during AJAX requests.
                fadeDuration: 250,    // Number of milliseconds the fade transition takes (null means no transition)
                fadeDelay: 0.25       // Point during the overlay's fade-in that the modal begins to fade in (.5 = 50%, 1.5 = 150%, etc.)
            };

            if(IG.isLegacyIE) {
                $.modal.defaults.fadeDuration = false;
            }

            function initVideoLayer() {
                // 비디오 영역 캡션 스크롤바 초기화
                scroller = $caption.scroller({
                    target: $captionText
                });

                // 동영상 재생
                $videoLinks.on('click', function(event) {
                    void 0;

                    var $this = $(this),
                        url = $this.attr('href'),
                        $youtube = $('<a href=' + url + '></a>'),
                        caption = $this.siblings('.video-subtitles').html();

                    // 플레이어 초기화, 캡션 삽입
                    $player.empty();
                    $captionText.html(caption);
                    $player.append($youtube);
                    $youtube.ytiframe({
                        autoplay: 1,
                        onReady: function() {
                            void 0;

                            $videoLayer.modal();
                        }
                    });

                    event.preventDefault();
                });

                $videoLayer.on($.modal.OPEN, function() { // 영상 팝업 열 때 스크롤바 초기화
                    scroller.scroll(0);
                    scroller.update();
                }).on($.modal.AFTER_CLOSE, function() { // 영상 팝업 닫을 때 플레이어 제거
                    $player.empty();
                });
            }

            // 영상 플레이어에 hover 효과 적용
            $('.thumb .expand, .thumb .player').drawBorder();

            // 이미지 확대
            $('.thumb .expand').on('click', function(event) {
                var $this = $(this),
                    $img = $this.find('.thumb-img'),
                    src = $img.data('large') || $img.attr('src');

                $largeImg.attr('src', src);
                $imgLayer.modal();

                event.preventDefault();
            });

            // 모바일 환경 여부에 따라 동영상 재생 방법 설정
            if(IG.isMobile) {
                // 모바일 환경에서 현재 페이지에서 영상 재생
                $videoLinks.one('inview', { offset: 1 }, function() {
                    var $this = $(this),
                        $caption = $this.siblings('.video-subtitles');

                    $this.ytiframe({
                        onReady: function() {
                            $caption.show();
                        }
                    });
                });
            } else {
                initVideoLayer();
            }
        })();
    });

    var ContentNavDetail = (function() {
        function ContentNavDetail() {
            this.$contentNav = $('.content-fixed-nav'); // 컨텐츠 네비게이션
        }

        ContentNavDetail.prototype = {
            init: function() {
                // 컨텐츠 네비게이션 탐색
                this.setContentNav();
            },

            /**
             * 컨텐츠 네비게이션 탐색 기능
             */
            setContentNav: function() {
                var self = this,
                    $contentWrap = $('<div></div>'),
                    $items = this.$contentNav.find('.fixed-nav-item'),
                    navHeight = this.$contentNav.outerHeight(),
                    isFixed = false;

                function toggleFix() {
                    var $target = isFixed ? self.$contentNav.parent() : self.$contentNav,
                        criterion = $target.offset().top - IG.headerHeight;

                    if(criterion <= IG.scrollTop) {
                        self.$contentNav.css('top', IG.headerHeight);

                        if(isFixed) {
                            return;
                        }

                        $contentWrap.height(self.$contentNav.height());
                        self.$contentNav
                            .wrap($contentWrap)
                            .addClass('is-fixed');

                        isFixed = true;
                    } else {
                        if(!isFixed) {
                            return;
                        }

                        self.$contentNav
                            .unwrap()
                            .css('top', '')
                            .removeClass('is-fixed');

                        isFixed = false;
                    }
                }

                function traverse() {
                    var $target,
                        $link;

                    // 현재 대상 찾기
                    $items.each(function() {
                        var $this = $(this),
                            $_target = $($this.children('.fixed-nav-link').attr('href'));

                        if(IG.scrollTop >= $_target.offset().top - IG.headerHeight - navHeight) {
                            $target = $_target;
                            $link = $this;
                        }
                    });

                    $items.filter('.is-active').removeClass('is-active');

                    if($target) {
                        $link.addClass('is-active');
                    }
                }

                // 링크로 탐색
                $items.find('.fixed-nav-link').on('click', function(event) {
                    var $target = $($(this).attr('href')),
                        scrollTo = $target.offset().top - IG.headerHeight - navHeight + 3;

                    $('html, body').stop().animate({ scrollTop: scrollTo }, 1800, 'easeInOutExpo');

                    event.preventDefault();
                });

                // 현재 대상 찾기
                IG.$win.on({
                    scroll: function() {
                        toggleFix();
                        traverse();
                    },
                    resize: function() {
                        toggleFix();
                    }
                });

                setTimeout(function() {
                    toggleFix();
                    traverse();
                }, 0);

                $('.history-section').attr('data-offset', -navHeight);
            }
        };

        return ContentNavDetail;
    })();

    /**
     * ContentNavDetail 모듈 실행
     */
    if(!IG.extended.contentNav_detail) {
        $(function() {
            var contentNavDetail = new ContentNavDetail();

            contentNavDetail.init();
        });
    }

    return ContentNavDetail;
});