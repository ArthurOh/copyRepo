/**
 * Notify
 *
 * by Peter@iropke
 */
(function(root, factory) {
    'use strict';

    if(typeof define === 'function' && define.amd) {
        define(['jquery', 'tmpl'], factory);
    } else {
        root.Notify = factory(jQuery);
    }
})(this, function($) {
    // easeOutExpo easing 정의
    $.easing['easeOutExpo'] = function (x, t, b, c, d) {
        return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
    };

    /**
     * 알림 표시
     * @param {object} [opt] - 옵션
     * @constructor
     */
    function Notify(opt) {
        // 기본 옵션
        this.opt = $.extend({
            containerID: 'notify-container',
            maxNotifies: 3,         // 최대 표시 알림
            notify: {               // 메시지 기본 옵션
                message: '',        // 메시지 내용
                type: null,         // 메시지 타입 (색상)
                icon: 'message',    // 아이콘 이름
                delay: 2500         // 지연 시간
            }
        }, opt);

        // 메시지 컨테이너
        this.$container = $('<div id="' + this.opt.containerID + '" class="notify-container">');

        // 메시지 템플릿
        this._notifyTmpl =
            '{{if type}}<div class="notify notify-${type} is-show" role="alert">' +
            '{{else}}<div class="notify is-show" role="alert">{{/if}}' +
            '<span class="icon icon-${icon} notify-icon"></span>' +
            '${message}' +
            '<button type="button" class="notify-close">' +
            '<span class="icon icon-close"></span>' +
            '<span class="blind">닫기</span>' +
            '</button>' +
            '</div>';

        // 시작
        this._init();
    }

    Notify.prototype = {
        /**
         * 초기화
         * @private
         */
        _init: function() {
            var $container = $('#' + this.opt.containerID);

            if($container.length) {
                this.$container = $container;
            } else {
                this.$container.appendTo($('body'));
            }
        },

        /**
         * 알림 보이기
         * @param {object} [opt] 메시지 표시 옵션
         */
        show: function(opt) {
            var self = this,
                $notify;

            // 옵션 설정 및 알림 만들기
            opt = $.extend({}, this.opt.notify, opt);
            $notify = $.tmpl(this._notifyTmpl, opt);

            // 최대 알림 개수 초과할 경우 닫기
            if(this._notifies().length >= this.opt.maxNotifies) {
                this.hide(this._notifies().first(), true);
            }

            // 컨테이너에 알림 추가
            this.$container.append($notify.hide());

            // 알림 보여주기
            $notify.fadeIn(300, function() {
                // 시간 경과 후 닫기
                setTimeout(function() {
                    self.hide($notify);
                }, opt.delay);
            }).add('.notify-close').on('click', function() { // 알림 클릭 시 닫기
                self.hide($notify, true);
            });
        },

        /**
         * 알림 감추기
         * @param {jQuery} $notify - 알림 요소
         * @param {boolean} [isFast] - 빠르게 감추기 여부
         */
        hide: function($notify, isFast) {
            $notify.removeClass('is-show').stop().fadeTo(isFast ? 200 : 1000, 0).slideUp({
                duration: 300,
                easing: 'easeOutExpo',
                complete: function() {
                    $notify.remove();
                }
            });
        },

        /**
         * 현재 보여지고 있는 알림 찾기
         * @returns {jQuery}
         * @private
         */
        _notifies: function() {
            return this.$container.children('.is-show');
        }
    };

    return Notify;
});