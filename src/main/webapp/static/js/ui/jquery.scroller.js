/*!
 * scroller - 스크롤바 플러그인
 * @param opt
 * @returns {object}
 */
(function($) {

    $.fn.scroller = function(opt) {
        // 기본 옵션
        var defaults = {
                target: '',         // 스크롤 대상
                scrollSpeed: 500,   // 스크롤 속도
                scrollEase: Power3.easeOut, // 스크롤 모션 easing
                fadeDuration: 200,  // 스크롤바 감춤 속도
                fadeDelay: 800      // 스크롤바 감춤 딜레이
            },

            // 스크롤바 요소
            $_scroller = $('<div class="scroller"><div class="scroller-drag"></div></div>');

        // 커스텀 옵션 지정
        opt = $.extend({}, defaults, opt);

        var $this = $(this),
            $target = $this.find(opt.target),
            $scroller = $_scroller.clone().appendTo($this),
            $drag = $scroller.children(),

            $win = $(window),
            $doc = $(document),

            isHover = false,    // 마우스 호버 여부
            isScroll = false,   // 스크롤 가능 여부
            isDrag = false,     // 드래그 중 여부
            isTouch = false,

            hideTmr,            // 스크롤바 감춤 타이머

            height,             // 컨테이너 크기
            targetHeight,       // 스크롤 대상 크기
            scrollerHeight,     // 스크롤바 크기
            scrollMax,          // 스크롤 가능 범위
            dragHeight,         // 드래그 크기
            dragMax,            // 드래그 가능 범위

            scrollTop = 0,      // 현재 스크롤 위치
            scrollTopStart = 0,
            startY,             // 드래그 시작 마우스 좌표
            dragTop,            // 드래그 위치

            eventTs;

        // 스크롤 대상 움직임
        var tween = new TweenMax.to($target, opt.scrollSpeed / 1000, {
            top: 0,
            ease: opt.scrollEase
        });

        /**
         * 초기화
         */
        function init() {
            void 0;

            // 스크롤러 hover
            $scroller.hover(function() {
                isHover = true;
                $scroller.addClass('is-hover');
                show();
            }, function() {
                isHover = false;

                if(!isDrag) {
                    $scroller.removeClass('is-hover');
                    hide();
                }
            });

            $win.on('resize.scroller', update);

            update();
        }

        /**
         * 리사이징 시 업데이트
         */
        function update() {
            //console.log('[scroller] update');

            // 좌표 계산
            height = $this.height();
            targetHeight = $target.height();
            scrollerHeight = $scroller.height();
            scrollMax = targetHeight - height;
            dragHeight = scrollerHeight * height / targetHeight;
            dragMax = scrollerHeight - dragHeight;

            //console.log('[scroller] update() container:', $this, ', target: ', $target, container, 'height:', height, ', target height: ', targetHeight);

            if(height < targetHeight) { // 스크롤 필요
                // 현재 위치가 스크롤 범위보다 높을 떄 위치 제한
                if(scrollTop > scrollMax) {
                    scrollTop = scrollMax;
                    TweenLite.set($target, { y: -scrollTop });
                }

                // 드래그 위치 보정
                $drag.css({
                    height: dragHeight,
                    top: dragMax * (scrollTop / scrollMax)
                });

                // 스크롤바 보이기
                show();
                hide();

                if(isScroll) return;

                // 스크롤바 드래그
                $drag.on({
                    'mousedown.scroller': dragStart,
                    'mouseup.scroller': dragEnd
                });

                // 터치하여 스크롤
                $this.on('touchstart.scroller', touchStart);

                // 마우스 휠로 스크롤
                $this.on('mousewheel', wheel);

                isScroll = true;

            } else { // 스크롤 필요 없음
                if(!isScroll) return;

                unload();
            }
        }

        function unload(isDestroy) {
            // 드래그, 휠 이벤트 해제
            $drag.off('.scroller');
            $this.off('.scroller');
            $this.off('mousewheel');

            // 스크롤바 감춤
            clearTimeout(hideTmr);
            $scroller.stop(true).hide();

            // 스크롤 위치 초기화
            scrollTop = 0;
            TweenLite.set($target, { clearProps: 'y' });

            isScroll = false;

            if(isDestroy) {
                $win.off('resize.scroller');
                $scroller.remove();
            }
        }

        /**
         * 스크롤바 보이기 / 숨기기
         */
        function show() {
            clearTimeout(hideTmr);
            $scroller.stop(true).fadeIn(opt.fadeDuration / 2);
        }
        function hide() {
            clearTimeout(hideTmr);
            hideTmr = setTimeout(function() {
                $scroller.stop(true).fadeOut(opt.fadeDuration);
            }, opt.fadeDelay);
        }

        /**
         * 타겟 요소 스크롤
         * @param {Number}pos - 스크롤 위치
         * @param {Boolean}[isDrag] - 스크롤바 직접 드래그 여부
         */
        function scroll(pos, isDrag) {
            show();

            // 실제 가능한 위치로 지정
            scrollTop = Math.max(
                0, Math.min(pos, scrollMax)
            );

            // 드래그 위치 이동
            if(!isDrag) {
                $drag.css('top', dragMax * scrollTop / scrollMax);
            }

            // 스크롤 대상 이동
            tween.updateTo({ css: { y: -scrollTop } }, true);
        }

        /**
         * 드래그 시작
         * @param event
         */
        function dragStart(event) {
            eventTs = event.timeStamp;

            isDrag = true;

            // 드래그 이벤트 등록
            $doc.on('mousemove.scroller' + eventTs, dragMove);
            $doc.on('mouseup.scroller' + eventTs, dragEnd);
            $doc.on('mouseout.scroller' + eventTs, function(event) {
                var from = event.relatedTarget || event.toElement;

                if (!from || from.nodeName == "HTML") {
                    dragEnd();
                }
            });

            startY = event.pageY;
            dragTop = parseFloat($drag.css('top'));

            event.preventDefault();
        }

        /**
         * 스크롤러 드래그
         * @param event
         */
        function dragMove(event) {
            var diff = event.pageY - startY,
                dragTarget = dragTop + diff;

            dragTarget = Math.max(
                0, Math.min(dragTarget, dragMax)
            );

            $drag.css('top', dragTarget);

            scroll(scrollMax * dragTarget / dragMax, true);
        }

        /**
         * 드래그 끝
         */
        function dragEnd() {
            isDrag = false;

            if(!isHover) {
                $scroller.removeClass('is-hover');
                hide();
            }

            $doc.off('.scroller' + eventTs);
        }

        /**
         * 터치하여 스크롤
         */
        function touchStart(event) {
            isDrag = true;

            // 이벤트 등록
            $this.on({
                'touchmove.scroller': touchMove,
                'touchend.scroller': touchEnd
            });

            scrollTopStart = scrollTop;
            startY = event.originalEvent.touches[0].pageY;
        }

        function touchMove(event) {
            var diff = event.originalEvent.touches[0].pageY - startY;

            scroll(scrollTopStart - diff);

            event.preventDefault();
        }

        function touchEnd() {
            $this.off('touchmove.scroller');
            $this.off('touchend.scroller');

            $scroller.removeClass('is-hover');
            hide();
        }

        /**
         * 마우스 휠로 스크롤
         */
        function wheel(event) {
            var factor = event.deltaFactor < 10 ? 100 : event.deltaFactor,
                delta = event.deltaY * factor;

            scroll(scrollTop - delta);
            hide();

            event.preventDefault();
        }

        init();

        return {
            update: update,
            scroll: scroll,
            hide: hide,
            unload: function() {
                unload(true);
            }
        };
    };

})(jQuery);