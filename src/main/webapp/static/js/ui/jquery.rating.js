/**
 * Rating
 */
(function($) {
    $.fn.rating = function(option) {

        var option = $.extend({
            }, option),

            $doc = $(document);

        return this.each(function() {
            var $this = $(this),
                $star = $this.find('.rating-star');

            /**
             * 초기화
             */
            function init() {
                $star.on({
                    mouseenter: function() {
                        setRate($(this).index() + 1, true);
                    },
                    mouseleave: function() {
                        setRate(getVal());
                    }
                });
                $star.find('input').on('change', function() {
                    setRate($(this).parent().index() + 1);
                });

                setRate(getVal() || 5);
            }

            function getVal() {
                void 0;
                return $this.find('input:checked').parent().index() + 1;
            }
            function setRate(n, isHover) {
                n--;
                $star.removeClass('is-hover is-active');
                $star.slice(0, n + 1).addClass(isHover ? 'is-hover' : 'is-active');
            }

            init();
        });
    }
})(jQuery);
