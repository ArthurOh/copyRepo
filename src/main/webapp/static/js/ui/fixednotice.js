/**
 * 상단 고정노출 공지
 */
define(['ui', 'jquery'], function(IG, $) {
    'use strict';

    /**
     * 공지사항 영역만큼 상단에 여백 추가
     */
    function setPadding() {
        if(isClosing) {
            return;
        }

        IG.$wrap.css('padding-top', $notice.outerHeight());
    }

    /**
     * 공지사항 닫기
     */
    function close() {
        if(isClosing) {
            return;
        }

        isClosing = true;

        // 오늘하루 열지않기 체크 시 쿠키 설정
        if($check.prop('checked')) {
            IG.UI.setCookie('fixed-notice', true, 1);
        }

        // 닫기 애니메이션
        $notice.slideUp({
            duration: 400,
            easing: 'easeOutExpo',
            step: function(now, tween) {
                if(tween.prop !== 'height') {
                    return;
                }

                IG.$wrap.css('padding-top', now);
            },
            complete: function() {
                $notice.remove();
                IG.$wrap.css('padding-top', '');
                IG.$win.off('resize.notice');
                isClosing = false;
            }
        });
    }

    var $notice = $('#notice'),
        $close,
        $check,
        isClosing = false;

    $(function() {
        // 공지사항이 없을 경우 실행하지 않음
        if(!$notice.length) {
            return;
        }

        // 쿠키값 설정되어 있을 때 공지사항 요소 삭제 후 끝내기
        if(IG.UI.getCookie('fixed-notice')) {
            $notice.remove();

            return;
        }

        // 요소 설정
        $close = $notice.find('.notice-close');
        $check = $('#notice-check');

        // 이벤트 설정
        $close.on('click', close);
        $check.on('change', function() {
            var $this = $(this);

            if($this.prop('checked')) {
                close();
            }
        });

        // 창 리사이징 시 상단 여백 반영
        IG.$win.on('resize.notice', setPadding);

        // 공지사항 보이기
        $notice.show();

        // 여백 설정 바로 업데이트
        setPadding();
    });
});