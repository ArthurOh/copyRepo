define(['jquery'], function($) {
    $.fn.placeholder = function() {
        var input = document.createElement('input');

        if( 'placeholder' in input ) {
            return this;
        }

        var $wrapper = $('<div class="placeholder-wrapper"></div>'),
            $holder = $('<span></span>');

        $wrapper.css({
            position: 'relative',
            display: 'inline-block'
        });
        $holder.css({
            overflow: 'hidden',
            position: 'absolute',
            color: '#999',
            whiteSpace: 'nowrap',
            textOverflow: 'ellipsis'
        });

        return this.each(function(index, el){
            var $el = $(el),
                holder = $el.attr('placeholder'),
                $_wrapper = $wrapper.clone(),
                $_holder = $holder.clone().text(holder);

            if($el[0].tagName === 'TEXTAREA' || $el.hasClass('input-wide')) {
                $_wrapper.css('display', '');
            }

            $el.wrap($_wrapper);

            if($el.val() !== '') {
                $_holder.hide();
            }

            $_holder.insertBefore($el).css({
                top: $el.css('padding-top'),
                left: $el.css('padding-left'),
                width: $el.width(),
                fontSize: $el.css('font-size'),
                letterSpacing: $el.css('letter-spacing'),
                lineHeight: $el.css('line-height')
            });

            $_holder.on('click', function() {
                $_holder.hide();
                $el.focus();
            });

            $el.on({
                focus: function() {
                    $_holder.hide();
                },
                blur: function() {
                    if($el.val() === '') {
                        $_holder.show();
                    }
                },
                change: function() {
                    $_holder.toggle($el.val() === '');
                }
            });
        });
    }
});