/**
 * SmoothScroll
 */
define(['jquery', 'gsap', 'scrollto', 'wheel'], function($, gsap) {
    'use strict';

    var $win = $(window),
        isScrolling = false,
        scrollTo = 0;

    $win.on('mousewheel', function(event) {
        var factor = event.deltaFactor < 10 ? 100 : event.deltaFactor,
            delta = event.deltaY * factor;

        if(!isScrolling) {
            scrollTo = $win.scrollTop();
        } else {
            scrollTo = Math.max(0, scrollTo - delta);
        }

        TweenMax.to($win, 1, {
            scrollTo: {
                y: scrollTo,
                autoKill: true
            },
            ease: Power4.easeOut,
            overwrite: 'preexisting',
            onStart: function() {
                isScrolling = true;
            },
            onComplete: function() {
                void 0;
                isScrolling = false;
            }
        });

        event.preventDefault();
    });

    void 0;

});