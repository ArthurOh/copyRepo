define(['global', 'jquery', 'gsap'], function(IG, $) {
    'use strict';

    var $msg = $('#legacy-browser'),
        $closeBtn = $msg.find('.legacy-browser-close');

    function setUpdateMsg() {
        $msg.show();

        $closeBtn.on('click', function() {
            void 0;

            IG.UI.setCookie('legacy-msg', true, 1);

            $msg.slideUp();
        })
    }

    return function() {
        if(!$msg.length) {
            return false;
        }

        if(IG.UI.getCookie('legacy-msg')) {
            $('#legacy-browser').remove();

            return false;
        }

        setUpdateMsg();
    }
});