/**
 * custom dropdown (.selectbox)
 * @author: Alice Kim, alice@iropke.com
 * @last update: 2015.07.39
 */
define(['jquery'], function($) {
    'use strict';

    return function(selector) {
        var $select = $(selector ? selector : 'div.selectbox'),
            $options = $select.find('.selectbox-option'),
            $body = $(document),
            arrKey = [38, 40];

        $select.each(function() {
            var $box = $(this),
                $selector = $box.find('.selector'),
                $selected = $box.find('.selector-text'),
                $list = $box.find('.selectbox-option'),
                $items,
                selected = '',
                islink = $list.find('a').length,
                isselectonly = typeof $select.attr('data-select-only') !== typeof undefined && $select.attr('data-select-only') !== false,
                len = $list.find('li').length;

            function init() {
                var top = 0;

                // setting
                $box.data('is-open', false);
                $selector.attr('tabindex', 0);

                setAttr();
                setStyle();

                // events
                if ( islink ) {
                    $list.on('click', 'a', function(event) {
                        select(this);
                    });

                    $list.on('keydown', 'a', function(event){
                        var i = $(this).parent('li').data('index');

                        if ( event.keyCode == 9 || event.keyCode == 27 ) {
                            // tab or esc
                            afterSelect();
                            prevent(event);

                        } else if ( event.keyCode == 37 || event.keyCode == 38 ) {
                            // left or up arrow
                            if ( i >= 1 ) {
                                $items.eq( i - 1 ).focus();
                            }

                        } else if ( event.keyCode == 39 || event.keyCode == 40 ) {
                            // right or down arrow
                            if ( i + 1 <= len - 1 ) {
                                $items.eq( i + 1 ).focus();
                            }
                        }
                    });

                    $list.on('focus', 'a', function(event) {
                        $(this).parent().addClass('is-active');
                    });

                    $list.on('blur', 'a', function(event) {
                        $(this).parent().removeClass('is-active');
                    });
                } else {
                    $list.on('click', 'li', function(event) {
                        if ( $(this).hasClass('is-disabled') ) {
                            $(this).blur();
                        } else {
                            select(this);
                        }
                        // event.stopPropagation();
                    });
                    $list.on('keydown', 'li', function(event){
                        var $el = $(this),
                            i = $el.data('index');

                        if ( event.keyCode == 13 ) {
                            // enter
                            if ( !$el.hasClass('is-disabled') ) {
                                $(this).trigger('click');
                            }
                            prevent(event);

                        } else if ( event.keyCode == 9 || event.keyCode == 27 ) {
                            // tab or esc
                            afterSelect();
                            prevent(event);

                        } else if ( event.keyCode == 37 || event.keyCode == 38 ) {
                            // left or up arrow
                            if ( i >= 1 ) {
                                $items.eq( i - 1 ).focus();
                            }

                        } else if ( event.keyCode == 39 || event.keyCode == 40 ) {
                            // right or down arrow
                            if ( i + 1 <= len - 1 ) {
                                $items.eq( i + 1 ).focus();
                            }
                        }
                    });

                    $list.on('focus', 'li', function(event) {
                        $(this).addClass('is-active');
                    });

                    $list.on('blur', 'li', function(event) {
                        $(this).removeClass('is-active');
                    });
                }

                $selector.on('click keydown', function(event) {
                    if (event.type == 'click' || event.keyCode == 13) {
                        if ( $box.data('is-open') ) {
                            close();
                        } else {
                            open();
                            if ( event.keyCode == 13 ) {
                                $items.eq(0).focus();
                            }
                        }
                        event.preventDefault();
                    }
                });

                // cancel event bubble on selectbox
                $box.on('click', function(event){
                    event.stopPropagation();
                });

                // 선택된 아이템 처리
                if ($list.has('.is-current').length || $list.find(':checked').length) {
                    if (islink) {
                        $list.find('.is-current').addClass('is-active');
                        $selected.text($list.find('.is-current').text());
                    } else {
                        top = $(window).scrollTop();
                        $list.find(':checked').parent().click();
                        $list.find('.is-current').click();
                        $('html, body').scrollTop(top);
                        $selector.blur();
                    }
                }
            }

            function select(obj) {
                var $el = $(obj),
                    has_el = ( $el.find('.select-item').length ) ? true : false;

                if (!isselectonly) {
                    if ( has_el ) {
                        selected = $el.find('.select-item').html();
                        $selected.html( selected );
                    } else {
                        selected = $el.text();
                        $selected.text( selected );
                    }
                }

                if ( !islink ) {
                    $list.find('input').prop('checked', false);
                    $items.removeClass('is-current');

                    $el.find('input').prop('checked', true).trigger('change');
                    $el.addClass('is-current');
                }
                afterSelect();
            }

            function setAttr() {
                $items = ( islink ) ? $list.find('>a') : $list.find('li');
                len = $items.length;

                $items.each(function(i, el) {
                    $(el).data('index', i);
                });

                if ( !islink ) {
                    $items.attr('tabindex', 0).find('input[type=radio]').attr('tabindex', '-1').each(function(idx, el) {
                        var $el = $(el), $label, id;

                        if ( $el.attr('id') && $el.attr('id') !== '' ) return;

                        id = $el.attr('name') + (idx + 1);
                        $el.attr('id', id);

                        $label = $el.parent('label') || $el.siblings('label');
                        $label.length && $label.attr('for', id);
                    });
                }
            }

            function setStyle() {
                var $testItem, itemWidth, selectorWidth, selectorW;

                $testItem = ( islink ) ? $list.find('a') : $list.find('label');

                if ( !$select.hasClass('select--wide') ) {
                    itemWidth = $testItem.width() + 0;
                    selectorWidth = $selector.width() + 0;
                    selectorW = (selectorWidth < itemWidth) ? itemWidth : selectorWidth;
                    $selector.width(selectorW);
                }

                //$options.width('100%');
                $list.css('visibility', 'visible').hide();
            }

            function open() {
                if ( $box.data('is-open') || isDisabled($box) ) return;

                // li refresh 대비
                setAttr();

                allClose();
                $box.addClass('is-active').css('zIndex',99).data('is-open', true);
                $list.show();
                blockArrow();
            }

            function close() {
                $list.hide();
                $body.off('keydown.blockArrow');
                $box.removeClass('is-active').css('zIndex',1).data('is-open', false);
            }

            function afterSelect() {
                close();
                $selector.focus();
            }

            init();
        });

        $body.on('click', allClose);

        function allClose() {
            $body.off('keydown.blockArrow');
            $select.removeClass('is-active').css('zIndex', 'auto').data('is-open', false);
            $options.hide();
        }

        function prevent(event) {
            if( event.preventDefault ) {
                event.preventDefault();
                event.stopPropagation();
            } else {
                event.returnValue = false;
            }
        }

        function blockArrow() {
            $body.on('keydown.blockArrow', 'li', function(event) {
                var key = event.which;
                void 0;
                if( $.inArray(key, arrKey) > -1 ) {
                    event.preventDefault();
                    return false;
                }
            });
        }

        function isDisabled($box) {
            if ( $box.hasClass('is-disabled') && $box.data('disabled-msg') ) {
                if ( window.popupNotification ) {
                    popupNotification({
                        msg: $box.data('disabled-msg'),
                        timer: 3000
                    });
                } else {
                    void 0;
                }
            }
            return $box.hasClass('is-disabled');
        }
    }
});