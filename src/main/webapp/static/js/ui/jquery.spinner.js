/**
 * Spinner
 */
(function($) {
    $.fn.spinner = function(option) {

        var option = typeof option !== 'string' ? $.extend({}, option) : option,
            $doc = $(document);

        return this.each(function() {
            var $this = $(this),

                $num = $this.find('.spinner-num'), // 입력칸
                $btn = $this.find('.spinner-btn'), // 감소, 증가 버튼
                $dec = $this.find('.spinner-dec').data('amt', -1), // 감소 버튼
                $inc = $this.find('.spinner-inc').data('amt', 1), // 증가 버튼

                min, // 최솟값
                max, // 최댓값

                isMouseDown = false,
                changeWaitTmr,
                changeInt,
                isInChange = false,
                methods = {};

            /**
             * 초기화
             */
            function init() {
                val(clipVal(val()));

                // 이벤트 바인딩
                $num.on({
                    // inputbox 값 변경 시 검증
                    change: function() {
                        if(isInChange) {
                            isInChange = false;
                            return;
                        }

                        var clipped = clipVal(val());

                        if(val() !== clipped) {
                            val($(this).val());
                        }
                    },

                    // 상/하키로 값 변경
                    keydown: function(event) {
                        if(event.keyCode === 40 || event.keyCode === 38) {
                            var amt = event.keyCode === 40 ? -1 : 1,
                                initVal = val();

                            setTimeout(function() {
                                // 키보드로 값이 변경되는 것을 브라우저가 이미 지원할 경우
                                // 이벤트 제거
                                if(Math.abs(val() - initVal)) {
                                    $num.off('keydown');
                                } else {
                                    val(val() + amt);
                                }
                            }, 0);
                        }
                    }
                });

                $btn.on({
                    // 값 증가/감소
                    click: function() {
                        var amt = $(this).data('amt');

                        val(val() + amt);
                    },

                    // 마우스로 누르고 있으면 빠르게 값 증가/감소
                    mousedown: function() {
                        var amt = $(this).data('amt');

                        isMouseDown = true;

                        clearTimeout(changeWaitTmr);

                        changeWaitTmr = setTimeout(function() {
                            if(isMouseDown) {
                                changeInt = setInterval(function() {
                                    val(val() + amt);
                                }, 100);
                            }
                        }, 250);
                    }
                });

                // 마우스 동작 초기화
                $doc.on('mouseup', function() {
                    isMouseDown = false;
                    clearInterval(changeInt);
                });
            }

            /**
             * 값 설정
             * @param  {number} [tVal] - 설정할 값, 주어지지 않으면 기존 값 리턴
             * @returns {*}
             */
            function val(tVal, isIgnoreEvent) {
                if(typeof tVal === 'undefined') {
                    return $num.val() - 0;
                }

                $num.val(clipVal(tVal));

                if(!isIgnoreEvent) {
                    isInChange = true;
                    $num.change();
                }
            }

            /**
             * 현재값이 최솟값, 최댓값을 벗어난 상태일 경우 다시 설정
             * @param {number} [tVal] - 검사할 값, 주어지지 않으면 현재값으로 비교
             */
            function clipVal(tVal) {
                // 현재 최솟값, 최댓값 찾기
                if(typeof $num.attr('min') !== 'undefined') {
                    min = $num.attr('min') - 0
                }

                if(typeof $num.attr('max') !== 'undefined') {
                    max = $num.attr('max') - 0
                }

                // 최솟값이 설정되어 있을 경우 검증
                if(typeof min === 'number' && tVal < min) {
                    return min;
                }

                // 최댓값이 설정되어 있을 경우 검증
                if(typeof max === 'number' && tVal > max) {
                    return max;
                }

                return tVal;
            }

            methods = {
                disable: function() {
                    $this.addClass('is-disabled');
                    $num.prop('disabled', true);
                    $btn.prop('disabled', true);
                },
                enable: function() {
                    $this.removeClass('is-disabled');
                    $num.prop('disabled', false);
                    $btn.prop('disabled', false);
                }
            };

            if(typeof option === 'object') {
                init();
            } else if(typeof option === 'string') {
                if(option in methods) {
                    methods[option]();
                }
            }
        });
    }
})(jQuery);
