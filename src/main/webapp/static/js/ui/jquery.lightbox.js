/**
 * lightbox
 *
 * by Peter@iropke
 */
(function($) {

    "use strict";

    /**
     * jQuery utility로 등록
     */
    var lightbox = $.lightbox = function(opt) {
        if(this.constructor === lightbox) return;

        var _lightbox = new lightbox().init(opt);

        return _lightbox;
    };

    $.extend(lightbox, {
        methods: {
            /**
             * 초기화
             */
            init: function(opt) {
                this.opt = $.extend({
                    prefix: 'lightbox',     // 클래스 및 data 접두어

                    // elements
                    content: null,          // 팝업 컨텐츠 선택자
                    scrollable: null,       // 창 크기가 작을 경우 스크롤될 컨텐츠
                    root: $('body'),        // 스크롤바 제어를 위한 최상위 요소 지정

                    // switches
                    clone: true,            // 팝업 컨텐츠를 복사하여 표시할지 여부
                    closeByBG: true,        // 배경 클릭 시 팝업을 닫을지 여부
                    autoOpen: false,        // 초기화 할 때 자동으로 팝업 열기

                    // effect options
                    scaleStart: 0.75,       // scale 효과 배율
                    openSpeed: 400,         // 팝업 열기 효과 시간
                    closeSpeed: 250,        // 팝업 닫기 효과 시간

                    // events
                    onInit: null,           // 플러그인 초기화 이벤트
                    onBeforeOpen: null,     // 팝업을 열기 전 이벤트
                    onOpen: null,           // 팝업을 연 후 이벤트
                    onBeforeClose: null,    // 팝업을 닫기 전 이벤트
                    onClose: null           // 팝업을 닫은 후 이벤트
                }, opt);

                // if browser don't support css opacity, disable open/close effect
                if(!Modernizr.opacity) this.opt.openSpeed = this.opt.closeSpeed = 0;

                // this.$container = null;
                this.$_content = $(this.opt.content).eq(0);

                this.isOpen = false;
                this.scrollBarWidth = 0;

                this.$win = $(window);
                this.$containerTmpl = $('<div class="' + this.opt.prefix + '"><div class="' + this.opt.prefix + '-bg"></div></div>');

                this.execCallback('onInit');

                if(this.opt.autoOpen) this.open();

                return this;
            },

            /**
             * 이벤트 탑재
             */
            attach: function($target, opt) {
                var self = this;

                this.init(opt);
                self.opt.content = $target.attr('href') || $target.attr('data-' + self.opt.prefix);
                this.$_content = $(this.opt.content).eq(0);

                $target.on('click', function(event) {
                    if($(this).attr('data-' + self.opt.prefix + '-closebybg') == 'false') self.opt.closeByBG = false;

                    self.open();

                    event.preventDefault();
                    event.stopPropagation();
                });
            },

            /**
             * 콜백 이벤트 실행
             */
            execCallback: function(func) {
                if(typeof(this.opt[func]) === 'function') this.opt[func](this, this.$content);
            },

            /**
             * 팝업 위치를 화면 중앙으로 정렬
             */
            align: function() {
                this.$content.css({
                    'margin-left': this.$content.outerWidth() / -2,
                    'margin-top': this.$content.outerHeight() / -2
                });
            },

            /**
             * 스크롤 가능 컨텐츠 조정
             */
            scrollable: function() {
                var $scrollable = this.$content.find(this.opt.scrollable);

                if(!$scrollable.length) return false;

                $scrollable.css({
                    'height': '',
                    'min-height': '',
                    'overflow-y': ''
                });

                var height = this.$content.height(),
                    siblingsHeight = 0;

                $scrollable.siblings().each(function(idx, el) {
                    var $el = $(el);

                    if($el.css('position') !== 'static' && $el.css('position') !== 'relative') return true;

                    siblingsHeight += $(el).outerHeight(true);
                });

                $scrollable.css({
                    'height': height - siblingsHeight - parseInt($scrollable.css('padding-top')) - parseInt($scrollable.css('padding-bottom')),
                    'min-height': 0,
                    'overflow-y': 'auto'
                });
            },

            /**
             * 스크롤바 크기 구하기
             */
            getScrollBarWidth: function() {
                var $outer = $('<div>'),
                    $inner = $('<div>').appendTo($outer),

                    outerWidth,
                    innerWidth;

                $inner.css({
                    width: '100%',
                    height: '150px',
                    margin: 0,
                    padding: 0,
                    border: 0
                });

                $outer.css({
                    overflow: 'hidden',
                    visibillity: 'hidden',
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    width: '100px',
                    height: '100px',
                    margin: 0,
                    padding: 0,
                    border: 0
                });

                $outer.appendTo($('body'));
                outerWidth = $inner[0].offsetWidth;
                $outer.css('overflow', 'scroll');
                innerWidth = outerWidth == $inner[0].offsetWidth ? $outer[0].clientWidth : $inner[0].offsetWidth;

                $outer.remove();

                return outerWidth - innerWidth;
            },

            /**
             * 팝업 열기
             */
            open: function() {
                if(this.isOpen !== false) return false;

                this.execCallback('onBeforeOpen');

                var self = this,
                    rootWidth = Math.min(this.opt.root[0].offsetWidth, this.opt.root[0].clientWidth),
                    scrollTop = this.opt.root.scrollTop();

                // 스크롤바 감추기
                this.scrollBarWidth = 0;
                this.opt.root.css('overflow', 'hidden');
                this.scrollBarWidth = this.opt.root[0].offsetWidth - rootWidth;

                if(this.scrollBarWidth > 0) this.opt.root.css('padding-right', this.scrollBarWidth);

                // 컨테이너 만들기
                this.$container = this.$containerTmpl.clone();

                // 배경 클릭 시 닫기 적용
                if(this.opt.closeByBG) {
                    this.$container.find('.' + this.opt.prefix + '-bg').css('cursor', 'pointer');
                }

                this.$container.css('opacity', 0).appendTo(this.opt.root);

                // 팝업 컨텐츠 설정
                this.$content = this.$_content;

                if(this.opt.clone) {
                    this.$content = this.$content.clone(true).attr('id', this.opt.prefix + '-' + this.$content.attr('id'));
                } else {
                    // 컨텐츠를 복사하지 않을 때 원래 위치 기억
                    var $_prev = this.$content.prev();

                    // 바로 앞의 요소가 있을 경우 그것을 기억, 첫번째 자식일 경우 부모 요소를 기억
                    if($_prev.length) {
                        this.$content.data(this.opt.prefix + '-prev', $_prev[0]);
                    } else {
                        this.$content.data(this.opt.prefix + '-parent', this.$content.parent()[0]);
                    }
                }

                // 컨텐츠 초기 설정 후 컨테이너로 삽입
                this.$content = this.$content.addClass(this.opt.prefix + '-content')
                    .attr('tabindex', 0)
                    .css('transform', 'scale(' + this.opt.scaleStart + ')')
                    .appendTo(this.$container).focus();

                this.opt.root.scrollTop(scrollTop);

                // 스크롤 가능 컨텐츠 설정
                this.$win.on('resize', function() {
                    if(self.opt.scrollable) self.scrollable();
                    self.align();
                }).resize();

                // 팝업 열기 애니메이션 시작
                this.$container.animate({opacity: 1}, {
                    duration: self.opt.openSpeed,
                    easing: 'easeOutExpo',
                    step: function(now) {
                        self.$content.css('transform', 'scale(' + (self.opt.scaleStart + (now * (1 - self.opt.scaleStart))) + ')');
                    },
                    complete: function() {
                        self.$container.css('opacity', '');
                        self.$content.css('transform', '');
                        self.$container.css('filter', '');
                        self.execCallback('onOpen');

                        // 닫기 이벤트 바인딩
                        self.$container.on('click', function(event) {
                            var $target = $(event.target);

                            if(
                                ($target.is('.' + self.opt.prefix + '-bg') && self.opt.closeByBG) ||
                                $target.is('[data-' + self.opt.prefix + '-close]') ||
                                $target.parents('[data-' + self.opt.prefix + '-close]').length
                            ) {
                                self.close();
                            }
                        });

                        self.isOpen = true;
                    }
                });
            },

            /**
             * 팝업 닫기
             */
            close: function() {
                if(this.isOpen !== true) return false;

                var self = this;

                this.isOpen = null;
                this.execCallback('onBeforeClose');

                this.$container.animate({opacity: 0}, {
                    duration: self.opt.closeSpeed,
                    easing: 'easeInCubic',
                    step: function(now) {
                        self.$content.css('transform', 'scale(' + (self.opt.scaleStart + (now * (1 - self.opt.scaleStart))) + ')');
                    },
                    complete: function() {
                        if(!self.opt.clone) {
                            self.$content.removeClass(self.opt.prefix + '-content')
                                .removeAttr('tabindex')
                                .css({
                                    'margin-left': '',
                                    'margin-top': '',
                                    'transform': ''
                                });

                            if(self.$content.data(self.opt.prefix + '-prev')) {
                                $(self.$content.data(self.opt.prefix + '-prev')).after(self.$content)
                            } else {
                                $(self.$content.data(self.opt.prefix + '-parent')).prepend(self.$content);
                            }
                        }

                        self.$container.remove();
                        delete self.$container;
                        delete self.$content;

                        self.$win.off('resize');
                        self.opt.root.css('overflow', '');
                        if(self.scrollBarWidth > 0) self.opt.root.css('padding-right', '');

                        self.execCallback('onClose');

                        self.isOpen = false;
                    }
                });
            }
        }
    });

    /**
     * 메소드 설정
     */
    lightbox.prototype = $.extend({constructor: lightbox}, lightbox.methods);

    /**
     * jQuery 플러그인으로 등록
     */
    $.fn.lightbox = function(opt) {
        return this.each(function(){
            var _lightbox = new lightbox().attach($(this), opt);
        });
    };

}(jQuery));