/**
 * 메뉴 작동 모듈
 */
define(['global', 'jquery', 'underscore', 'gsap', 'menuaim', 'scroller'], function(IG, $, _) {
    /**
     * 모바일 메뉴
     */
    var Small = (function() {
        /**
         * 초기화
         * @constructor
         */
        function Small(current) {
            void 0;

            var self = this;

            // elements
            this.$navToggle = IG.$header.find('.nav-toggle');
            this.$navClose = IG.$sidenav.find('.sidenav-close');

            // search
            this.$search = IG.$sidenav.find('.sidenav-search');
            this.$searchOpen = IG.$sidenav.find('.sidenav-search-open');
            this.$searchClose = this.$search.find('.sidenav-search-close');
            this.$searchQuery = this.$search.find('.sidenav-search-query');

            // mypage
            this.$mypage = IG.$sidenav.find('.my');
            this.$mypageLink = IG.$sidenav.find('.sidenav-utility .menu-link-mypage');

            // depth 1
            this.$item = IG.$sidenav.find('.nav-item');
            this.$itemHasSub = this.$item.filter(':has(.nav-d2)'); // 하위메뉴 존재하는 아이템
            this.$a = IG.$sidenav.find('.nav-a');

            // depth 2
            this.$d2 = IG.$sidenav.find('.nav-d2');
            this.$itemD2 = IG.$sidenav.find('.nav-d2-item');
            this.$itemD2A = IG.$sidenav.find('.nav-d2-a');
            this.$itemD2HasSub = this.$itemD2.filter(':has(.nav-d3)'); // 하위메뉴 존재하는 아이템

            // depth 3
            this.$d3 = IG.$sidenav.find('.nav-d3');

            // values
            this.isOpen = false;
            this.isSearch = false;
            this.zIndex = 0;
            this.d2Width = this.$d2.outerWidth();
            this.scrollTop = 0;

            // 서브메뉴 존재하는 메뉴에 클래스 추가
            this.$itemHasSub.addClass('is-has-sub');
            this.$itemD2HasSub.addClass('is-has-sub');

            // 현재 메뉴 찾기
            if(typeof current === 'object') {
                this.$current = this.$item.eq(current[0]).addClass('is-current');
                typeof current[1] === 'number' && (this.$currentD2 = this.$current.find('.nav-d2-item').eq(current[1]).addClass('is-current'));
                typeof current[2] === 'number' && this.$currentD2.find('.nav-d3-item').eq(current[2]).addClass('is-current');
            } else {
                this.$current = this.$item.filter('.is-current').eq(0);
                this.$currentD2 = this.$itemD2.filter('.is-current').eq(0);
            }

            // d3 서브메뉴 위치 초기화
            this.$d3.each(function() {
                var $this = $(this),
                    height = $this.outerHeight();

                $this.data('height', height);

                TweenMax.set($this, {
                    'margin-top': -height
                });
            });

            // d2 서브메뉴 위치 초기화
            TweenMax.set(this.$d2, {
                display: 'none',
                x: this.d2Width
            });

            // d2 서브메뉴에 스크롤러 탑재
            this.$d2.each(function() {
                var $this = $(this);

                $this.data('scroller', $this.scroller({
                    target: '.nav-d2-inner'
                }));
            });

            // 메뉴 열기/닫기
            this.$navToggle.on('click', function(event) {
                self.toggleMenu.call(self, true);
                event.preventDefault();
            });
            this.$navClose.on('click', function(event) {
                self.toggleMenu.call(self, false);
                event.preventDefault();
            });

            // 검색 열기/닫기
            IG.$body.on('click', function() {
                if(self.$search.has($(event.target)).length) {
                    return;
                }

                self.toggleSearch.call(self, false);
            });

            this.$searchOpen.on('click', function(event) {
                self.toggleSearch.call(self);

                event.stopPropagation();
            });
            this.$searchClose.on('click', function(event) {
                self.toggleSearch.call(self);
                event.stopPropagation();
            });

            // 마이페이지 팝업 위치 초기화
            this.$mypage.css('bottom', -this.$mypage.outerHeight());

            // 마이페이지 팝업 열기/닫기
            IG.$body.on('click', function() {
                if(self.$mypage.has($(event.target)).length) {
                    return;
                }

                IG.$sidenav.removeClass('is-mypage');
            });
            this.$mypageLink.on('click', function(event) {
                self.toggleSearch.call(self, false);
                IG.$sidenav.toggleClass('is-mypage');
                event.preventDefault();
                event.stopPropagation();
            });

            // d1 선택
            this.$a.on('click', function(event) {
                var $item = $(this).parent();

                if(!$item.hasClass('is-has-sub')) {
                    self.toggleMenu(false);
                    return;
                }

                if(!$item.hasClass('is-active')) {
                    self.activateItem.call(self, $item);
                }

                if($item.hasClass('is-has-sub')) {
                    event.preventDefault();
                } else {
                    self.toggleMenu(false);
                }

                event.preventDefault();
            });

            // d2 선택
            this.$itemD2A.on('click', function(event) {
                var $item = $(this).parent(),
                    toggle = !$item.hasClass('is-active');

                if(!$item.hasClass('is-has-sub')) {
                    self.toggleMenu(false);
                    return;
                }

                if(!toggle && $item.is(self.$current)) {
                    return;
                }

                self.toggleD2Item.call(self, $item, toggle);

                if(!toggle && self.$currentD2.length) {
                    self.toggleD2Item.call(self, self.$currentD2, true);
                }

                event.preventDefault();
            });

            // 초기선택 메뉴 지정
            this.$current && this.activateItem(this.$current, true);
            this.$currentD2 && this.toggleD2Item(this.$currentD2, true);
        }

        /**
         * 메뉴 열기/닫기
         * @param {Boolean} toggle - 열기/닫기 여부
         */
        Small.prototype.toggleMenu = function(toggle) {
            var self = this,
                $activatedItem;

            this.isOpen = toggle;

            if(toggle) {
                // 메뉴 열 때 스크롤 방지
                $('html, body').on('touchmove.nav.small', function(event) {
                    event.preventDefault();
                });

                // 메뉴 밖 클릭 시 메뉴 닫기
                setTimeout(function() {
                    IG.$wrap.on('click.nav.small', function() {
                        self.toggleMenu(false);
                    });
                }, 1);

                // sidenav 영역으로 포커스 이동
                IG.$sidenav.focus();

                // 이미 열린 메뉴에 스크롤바가 있으면 스크롤바 업데이트
                $activatedItem = this.$item.filter('.is-active');

                if($activatedItem.length && $activatedItem.find('.nav-d2').data('scroller')) {
                    setTimeout(function() {
                        $activatedItem.find('.nav-d2').data('scroller').update();
                    }, 1);
                }
            } else {
                // 기존 이벤트 삭제
                $('html, body').off('touchmove.nav.small');
                IG.$wrap.off('click.nav.small');

                // sidenav 영역으로 포커스 이동
                this.$navToggle.focus();
            }

            // 클래스로 열기/닫기
            IG.$body.toggleClass('is-open', toggle);
        };

        /**
         * 검색 열기/닫기
        * @param {Boolean} [toggle] - 열기/닫기 여부
         */
        Small.prototype.toggleSearch = function(toggle) {
            var self = this;

            toggle = typeof toggle === 'undefined' ? !IG.$sidenav.hasClass('is-search') : toggle;

            if(toggle) {
                IG.$sidenav.removeClass('is-mypage');

                // iOS에서 fixed 요소 속의 폼 요소에게 포커스 부여 시 스크롤 위치가 잘못되는 버그가 있어 보정
                if(IG.ua.os.name === 'iOS') {
                    this.scrollTop = IG.$doc.scrollTop();
                    IG.$doc.scrollTop(0);
                }

                self.$searchQuery.val('').focus();
            }

            // 클래스로 열기/닫기
            IG.$sidenav.toggleClass('is-search', toggle);

            // iOS에서 검색 영역 닫을 때 스크롤 위치를 원래대로 복구
            if(IG.ua.os.name === 'iOS' && this.scrollTop > 0 && !toggle) {
                IG.$doc.scrollTop(this.scrollTop);
                this.scrollTop = 0;
            }
        };

        /**
         * 메뉴 활성
         * @param {jQuery} $item - 아이템 요소
         * @returns {boolean}
         */
        Small.prototype.activateItem = function($item) {
            var self = this,
                $activated = this.$item.filter('.is-active'),
                $sub = $item.children('.nav-d2');

            if($activated.length) {
                this.deactivateItem($activated);
            }

            $item.addClass('is-active');

            if(!$sub.length) {
                return;
            }

            this.zIndexTmr && clearTimeout(self.zIndexTmr);

            TweenMax.to($sub, 0.3, {
                display: 'block',
                x: 0,
                zIndex: ++this.zIndex,
                ease: Power4.easeOut,

                onComplete: function() {
                    // 스크롤러 업데이트
                    $sub.data('scroller').update();

                    // 서브메뉴 모션 완료 시 누적된 z-index 초기화
                    self.zIndexTmr = setTimeout(function() {
                        self.zIndex = 0;
                        $sub.css('z-index', ++self.zIndex);
                    }, 100);
                }
            });
        };

        /**
         * 메뉴 비활성
         * @param {jQuery} $item
         */
        Small.prototype.deactivateItem = function($item) {
            var $sub = $item.children('.nav-d2');

            $item.removeClass('is-active');

            if(!$sub.length) {
                return;
            }

            TweenMax.to($sub, 0.3, {
                display: 'none',
                x: this.d2Width,
                ease: Power4.easeOut,
                clearProps: 'zIndex'
            });
        };

        /**
         * D2 메뉴 활성/비활성
         * @param {jQuery} $item - 아이템 요소
         * @param {boolean} toggle - 활성/비활성 여부
         * @param {boolean} isFromActivated - 다른 메뉴를 비활성 시키는지 여부 (스크롤바 업데이트 최적화용)
         */
        Small.prototype.toggleD2Item = function($item, toggle, isFromActivated) {
            var $_sub = $item.find('.nav-d3'),
                $activatedItem = this.$itemD2.filter('.is-active');

            // 아이템 활성 클래스 toggle
            $item.toggleClass('is-active', toggle);

            if(!$item.is(this.$currentD2)) {
                this.$currentD2.toggleClass('is-current', !toggle);
            }

            if(!$_sub.length) {
                return;
            }

            // 메뉴 활성 시 활성된 다른 메뉴가 있을 때 비활성
            if(toggle && $activatedItem.length) {
                this.toggleD2Item($activatedItem, false, true);
            }

            // 서브메뉴 열기
            TweenMax.to($_sub, 0.5, {
                marginTop: toggle ? 0 : -$_sub.data('height'),
                ease: Power4.easeInOut,
                onUpdate: function() {
                    // 스크롤바 업데이트
                    if(isFromActivated) { // 현재 활성화 중인 메뉴에서만 업데이트 실행
                        return;
                    }

                    $item.parents('.nav-d2').data('scroller').update();
                }
            });
        };

        return Small;
    })();

    /**
     * 데스크탑 메뉴
     */
    var Large = (function() {
        /**
         * 초기화
         * @constructor
         */
        function Large(current) {
            void 0;

            var self = this;

            // constants
            this.OFFSET = 10; // 상하 움직임 정도 (px)
            this.DURATION = 0.3; // 모션 속도 (s)
            this.EXIT_DELAY = 200; // 메뉴 탈출 시 딜레이 (ms)

            this.HEADER_HEIGHT = 120;
            this.ITEM_PADDING = 15;
            this.COMPATED_HEADER_HEIGHT = 60;
            this.COMPATED_ITEM_PADDING = 21;

            // elements
            this.$menu = IG.$nav.children('.nav-list'); // 메뉴 목록
            this.$item = IG.$nav.find('.nav-item'); // 1 depth 아이템
            this.$a = IG.$nav.find('.nav-a'); // 1 depth 링크
            this.$itemHasSub = this.$item.filter(':has(.nav-sub)'); // 하위메뉴 존재하는 아이템
            this.$subA = this.$item.find('.nav-sub-a');
            this.$indicator = IG.$nav.find('.nav-indicator'); // 현재 선택 메뉴 표시
            this.$bg = IG.$header.find('.nav-bg'); // 하위메뉴 배경

            this.$logo = IG.$header.find('.logo');
            this.$lContent = IG.$header.find('.header-wrap .l-content');
            this.$utilityLeft = IG.$header.find('.utility-left');

            this.$utilityMypage = IG.$header.find('.utility-mypage');
            this.$mypage = IG.$header.find('.my');

            // search
            this.$search = IG.$header.find('.global-search');
            this.$searchQuery = this.$search.find('.global-search-query');
            this.$searchSubmit = this.$search.find('.global-search-submit');

            // values
            this.isCompact = false;
            this.exitTmr = setTimeout($.noop, 0); // 메뉴 탈출 시 딜레이 타이머
            this.tweenProp = { // 서브메뉴, 배경 tween시 속성
                sub: { // 서브메뉴 on/off
                    on: {
                        autoAlpha: 1,
                        y: 0,
                        ease: Power4.easeOut
                    },
                    off: {
                        autoAlpha: 0,
                        y: -this.OFFSET,
                        ease: Power4.easeOut
                    }
                },
                bg: { // 배경 on/off
                    on: {
                        autoAlpha: 1,
                        y: 0,
                        ease: Power4.easeOut
                    },
                    off: {
                        autoAlpha: 0,
                        y: -this.OFFSET,
                        ease: Power4.easeOut
                    }
                }
            };

            this.compactTl = new TimelineMax({ paused: true });
            this.expandTl = new TimelineMax({ paused: true });

            // 현재 메뉴 찾기
            if(typeof current === 'object') {
                this.$current = this.$item.eq(current[0]).addClass('is-current');
                typeof current[1] === 'number' && this.$current.find('.nav-sub-item').eq(Math.max(current[1] + (this.$current.data('offset') | 0), 0)).addClass('is-current');
            } else {
                this.$current = this.$item.filter('.is-current').eq(0);
            }

            // 2 depth 존재하는 메뉴에 클래스 추가
            this.$itemHasSub.addClass('is-has-sub');

            this.$sub = this.$itemHasSub.children('.nav-sub'); // 하위메뉴

            // 하위메뉴 초기 스타일 설정
            if(!IG.isLegacyIE) {
                TweenMax.set(this.$sub, { y: -this.OFFSET });
            }

            // 유틸리티 메뉴에서 사용된 :last-child 선택자를 지원하지 않는 브라우저 지원
            if(IG.isLegacyIE) {
                this.$utilityLeft.children().last().addClass('_last-child');
            }

            // header 접기
            this.compactTl
                .to(this.$logo, 0.2, { alpha: 0, x: -30, ease: Power2.easeIn })
                .to(this.$utilityLeft, 0.2, { alpha: 0, x: 30, ease: Power2.easeIn }, 0)

                .set(this.$logo, { x: 30, scale: 1.2, transformOrigin:'0% 50%' })
                .set(this.$utilityLeft, { x: -30 })
                .set(IG.$header, {className: '+=is-compact'} )

                .to(this.$logo, 0.7, { scale: 1, alpha: 1, x: 0, ease: Power3.easeOut })
                .to(this.$utilityLeft, 0.7, { alpha: 1, x: 0, ease: Power3.easeOut }, '-=0.69')

                .to(this.$lContent, 0.5, { height: this.COMPATED_HEADER_HEIGHT, ease: Power4.easeOut }, 0.2)
                .to(this.$a, 0.5, { paddingBottom: this.COMPATED_ITEM_PADDING, ease: Power4.easeOut }, 0.2);

            // header 열기
            this.expandTl
                .to(this.$logo, 0.2, { alpha: 0, x: 30, ease: Power2.easeIn }, 0.05)
                .to(this.$utilityLeft, 0.2, { alpha: 0, x: -30, ease: Power2.easeIn }, 0.05)

                .set(this.$logo, { x: -30, scale: 0.9, transformOrigin:'100% 50%' })
                .set(this.$utilityLeft, { x: 30 })
                .set(IG.$header, {className: '-=is-compact'} )

                .to(this.$logo, 0.7, { scale: 1, alpha: 1, x: 0, ease: Power3.easeOut, clearProps: 'all' })
                .to(this.$utilityLeft, 0.7, { alpha: 1, x: 0, ease: Power3.easeOut, clearProps: 'all' }, '-=0.69')

                .to(this.$lContent, 0.5, { height: this.HEADER_HEIGHT, ease: Power4.easeOut, clearProps: 'all' }, 0)
                .to(this.$a, 0.5, { paddingBottom: this.ITEM_PADDING, ease: Power4.easeOut, clearProps: 'all' }, 0);

            // menu aim 플러그인 실행
            this.$menu.menuAim({
                activate: function(item) { // 메뉴 활성
                    void 0;

                    IG.$nav.addClass('is-active');
                    toggleItem.call(self, $(item), true);
                },
                deactivate: function(item) { // 메뉴 비활성
                    toggleItem.call(self, $(item), false);
                },
                enter: function() { // GNB 진입
                    void 0;

                    IG.$nav.addClass('is-active');

                    // 마지막에 선택된 메뉴가 있을 때 해당 메뉴 보여주기
                    if(self.$lastItem) {
                        toggleItem.call(self, self.$lastItem, true);
                    }
                },
                exitMenu: function() { // GNB 탈출
                    void 0;

                    clearTimeout(self.exitTmr);

                    if(!self.$lastItem) {
                        return;
                    }

                    self.exitTmr = setTimeout(function() {
                        IG.$nav.removeClass('is-active');
                        toggleItem.call(self, self.$lastItem, false);
                    }, self.EXIT_DELAY);
                },
                submenuDirection: 'below'
            });

            // 메뉴 아이템 포커스 시 열기/닫기
            this.$a.on({
                click: function(event) {
                    var $item = $(this).parent();

                    if(IG.isMobile && $item.hasClass('is-has-sub')) {
                        event.preventDefault();
                    }
                },
                focusin: function() {
                    toggleItem.call(self, $(this).parent(), true);
                },
                focusout: function() {
                    toggleItem.call(self, $(this).parent(), false);
                }
            });
            this.$subA.on({
                focusin: function() {
                    toggleItem.call(self, $(this).parents('.nav-item'), true);
                },
                focusout: function() {
                    toggleItem.call(self, $(this).parents('.nav-item'), false);
                }
            });

            // 검색 열기/닫기
            this.$searchSubmit.on('click', function(event) {
                // 검색어가 있을 때 submit 되도록 진행
                if(self.$searchQuery.val()) {
                    return;
                }

                event.preventDefault();
                self.toggleSearch();
            });
            
            // 마이페이지 레이어
            IG.$body.on('click', function() {
                if(self.$mypage.has($(event.target)).length) {
                    return;
                }

                self.$utilityMypage.removeClass('is-show');
            });
            this.$utilityMypage.find('> a').on('click', function(event) {
                self.$utilityMypage.toggleClass('is-show');

                event.preventDefault();
                event.stopPropagation();
            });
        }

        Large.prototype.load = function() {
            var self = this;

            this.isCompact = false;

            // 스크롤 시 폴딩
            IG.$win.on('scroll.navLarge', function() {
                self.toggleCompact();
            });
            this.toggleCompact();

            void 0;
        };

        Large.prototype.unload = function() {
            IG.$win.off('scroll.navLarge');

            this.compactTl.kill();
            this.expandTl.kill();

            TweenMax.set(IG.$header, {className: '-=is-compact'});
            TweenMax.set(this.$lContent, { clearProps: 'all' });
            TweenMax.set(this.$logo, { clearProps: 'all' });

            void 0;
        };

        /**
         * 헤더 접기 / 열기
         */
        Large.prototype.toggleCompact = function(scrollTop) {
            scrollTop = !$.isNumeric(scrollTop) ? IG.scrollTop : scrollTop;

            if(scrollTop >= 150 !== this.isCompact) {
                this.isCompact = !this.isCompact;
                this[this.isCompact ? 'expandTl': 'compactTl'].kill();
                this[this.isCompact ? 'compactTl' : 'expandTl'].play(0);

                // 헤더 접기 / 열기 시 검색 닫기
                this.toggleSearch(false);
            }
        };

        /**
         * 검색 열기/닫기
         * @param {Boolean} [toggle] - 열기/닫기 여부
         */
        Large.prototype.toggleSearch = function(toggle) {
            var self = this;

            toggle = typeof toggle === 'boolean' ? toggle : !this.isSearch;
            this.isSearch = toggle;

            if(toggle) {
                this.$searchQuery.removeAttr('tabindex').focus();

                // 검색창 밖을 누르면 검색창 숨김
                setTimeout(function() {
                    IG.$wrap.on('click.nav.small.search', function(event) {
                        !self.$search.has(event.target).length && self.toggleSearch(false);
                    });
                }, 1);
            } else {
                // 검색창이 보이지 않는 상태에서 입력란에 포커스가 가지 않게 설정
                this.$searchQuery.attr('tabindex', '-1');

                // 기존 이벤트 해제
                IG.$wrap.off('click.nav.small.search');
            }

            IG.$header.toggleClass('is-search', toggle);
        };

        /**
         * 서브메뉴 위치를 부모 메뉴에 맞춤
         */
        function alignSub($sub) {
            var navCenter = $sub.width() / 2, // 메뉴 중간 위치
                $_item = $sub.parents('.nav-item').eq(0), // 부모 메뉴
                itemCenter = $_item.position().left + $_item.width() / 2, // 아이템 중간 위치
                diff = (navCenter - itemCenter)|0; // 부모-자식 차이 (int)

            $sub.css('left', -diff);
        }

        /**
         * 메뉴 아이템 토글
         * @param $item 부모메뉴 요소
         * @param toggle 활성/비활성 여부
         */
        function toggleItem($item, toggle) {
            var $_sub = $item.children('.nav-sub'),
                $_a = $item.children('.nav-a');

            // 메뉴 탈출 타이머 해제
            clearTimeout(this.exitTmr);

            // 마지막 아이템 설정
            this.$lastItem = $item;

            // 아이템 활성 클래스
            $item.toggleClass('is-active', toggle);
            this.$current.toggleClass('is-current', !toggle);

            // 서브메뉴 없을 때 배경 감추고 끝내기
            if(!$_sub.length) {
                toggleIndicator.call(this, false);
                toggleBG.call(this, false);

                return;
            }

            // 현재 메뉴 표시/감춤
            toggleIndicator.call(this,
                toggle,
                toggle ? ($_a.position().left + parseInt($_a.css('padding-left')))|0 : null,
                toggle ? $_a.width() : null
            );
            toggleBG.call(this, toggle);

            // 정렬이 필요한 서브메뉴 조정
            if(!$_sub.hasClass('nav-sub-full')) {
                alignSub($_sub);
            }

            // 서브메뉴 tween
            if(!IG.isLegacyIE) {
                TweenMax.to($_sub, this.DURATION, this.tweenProp.sub[toggle ? 'on' : 'off']);
            } else {
                $_sub.css('visibility', toggle ? 'visible' : '');
            }
        }

        /**
         * 서브메뉴 배경 보이기/감추기
         * @param toggle 활성/비활성 여부
         */
        function toggleBG(toggle) {
            // 배경 tween
            TweenMax.to(this.$bg, this.DURATION, this.tweenProp.bg[toggle ? 'on' : 'off']);
        }

        /**
         * 현재 메뉴 표시/감춤
         * @param {boolean} toggle
         * @param {number} [targetX]
         * @param {number} [targetWidth]
         */
        function toggleIndicator(toggle, targetX, targetWidth) {
            if(toggle) {
                TweenMax.to(this.$indicator, 0.6, {
                    autoAlpha: 1,
                    x: targetX,
                    width: targetWidth,
                    ease: Power4.easeOut
                });
            } else {
                TweenMax.to(this.$indicator, this.DURATION, { autoAlpha: 0 });
            }
        }

        return Large;
    })();

    function isActive(gsap) {
        return typeof gsap === 'object' && gsap.isActive();
    }

    /**
     * 초기화 함수 리턴
     */
    return {
        init: function() {
            void 0;

            var self = this;

            this.current = IG.$body.data('nav');

            // header 없을 때 끝내기
            if(!IG.$header.length) {
                void 0;

                return;
            }

            // 현재 메뉴 찾기
            if(this.current) {
                this.current = _.map((this.current + '').split(','), function(n) {
                    return parseInt(n) - 1;
                });

                void 0;
            }

            // 모바일/데스크탑 메뉴 객체 생성
            this.small = new Small(this.current);
            this.large = new Large(this.current);

            // 해상도에 따라 메뉴 스위칭
            IG.UI.switchLayout('nav', function() {
                return IG.size < IG.BP_LARGE;
            }, function(isSmall) {
                if(isSmall) {
                    self.large.unload();
                    IG.headerHeight = 50;
                } else {
                    self.small.toggleMenu(false);
                    self.large.load();
                    IG.headerHeight = 65;
                }
            });

            void 0;
        }
    };
});