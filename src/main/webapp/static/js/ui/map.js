/**
 * Google Maps 초기화
 */
define(['googlemaps'], function(google) {
    'use strict';

    return function(id, lat, lng, markerIcon) {
        void 0;

        id = ( id ) ? id : 'map-canvas';
        lat = ( lat ) ? lat : 0;
        lng = ( lng ) ? lng : 0;
        markerIcon = ( markerIcon ) ? markerIcon : '/static/images/a/marker.png';

        var myLatLng = new google.maps.LatLng(lat, lng),
            mapOptions = {
                zoom: 16,
                center: myLatLng
            },
            map = new google.maps.Map(document.getElementById(id), mapOptions),
            marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: markerIcon
            });

        return {
            myLatLng: myLatLng,
            map: map,
            marker: marker,
            resize: function() {
                google.maps.event.trigger(map, 'resize');
            }
        }
    }
});