/*
 * Split text
 * @author: alice@iropke.com
 * @version 0.1
 */
define(['jquery'], function($) {

    function SplitText($el, options) {
        this.$input = $el;
        this.bunches = [];

        if ( options ) {
            this.o = options;
        } else {
            this.o = {
                type: 'words'
            }
        }

        this.dataText = ( this.o.type == 'chars' ) ? this.$input.text() : this.$input.html();
        this.dataText = $.trim(this.dataText);

        if ( this.o.bunchClass != undefined ) {
            this.bunchClass = this.o.bunchClass;
        }
        this.init();
    }
    SplitText.prototype = {
        split: function() {
            var i = 0;
            if ( this.o.type == 'chars' ) {
                this.arrText = this.dataText.split('');
            } else {
                this.arrText = this.dataText.replace(/\t|\n/gi, '').split(' ');
            }
            return this.arrText;
        },
        append: function() {
            var i = 0, html = '';

            for ( ; i < this.arrText.length ; i++ ) {
                if ( this.arrText[i] ==  ' ') {
                    this.arrText[i] = '&nbsp;';
                }
                if ( this.arrText[i].indexOf('<br>') == 0 ) {
                    html += '<br><span>'+this.arrText[i].replace('<br>', '')+'</span>';
                } else {
                    html += '<span>'+this.arrText[i]+'</span>';
                }


                if ( this.o.type == 'words' ) {
                    html += ' ';
                }
            }

            this.$input.text('');
            $(html).appendTo(this.$input);
        },
        init: function() {
            var children = [];

            this.split();
            this.append();

            this.$input.find('>span').addClass(this.bunchClass).each(function(idx, el){
                children[idx] = el;
            });
            return this.bunches = children;
        }
    };

    return SplitText;
});
