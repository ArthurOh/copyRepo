/**
 * 각종 UI 관련 함수
 */
define(['global', 'jquery', 'underscore', 'gsap'], function(IG, $, _, gsap) {
    'use strict';

    return {
        /**
         * window resizing 시 특정 조건에 의해 함수 실행
         * @author: Peter Choi, peter@iropke.com
         * @param {String} namespace resize 이벤트 namespace
         * @param {Function} cond 조건을 판별할 함수
         * @param {Function} callback resizing 중 조건이 변경되었을 때, 실행할 함수
         */
        switchLayout: function(namespace, cond, callback) {
            var state = cond(); // 현재 결과

            /**
             * resizing 중 조건 판단
             */
            function chkBreakpoint() {
                var result = cond();

                if(result !== state) {
                    state = !state;

                    void 0;
                    callback(state);
                }
            }

            $(window).on('resize' + (namespace ? '.' + namespace : ''), function() {
                chkBreakpoint.call(null, state);
            });

            // 최초 실행
            callback(state);
        },

        /**
         * 스크롤바 크기 구하기
         * @returns {number}
         */
        getScrollbarWidth: function() {
            var $outer = $('<div>'),
                $inner = $('<div>').appendTo($outer),

                outerWidth,
                innerWidth;

            $inner.css({
                width: '100%',
                height: '150px',
                margin: 0,
                padding: 0,
                border: 0
            });

            $outer.css({
                overflow: 'hidden',
                visibillity: 'hidden',
                position: 'absolute',
                left: 0,
                top: 0,
                width: '100px',
                height: '100px',
                margin: 0,
                padding: 0,
                border: 0
            });

            $outer.appendTo($('body'));
            outerWidth = $inner[0].offsetWidth;
            $outer.css('overflow', 'scroll');
            innerWidth = outerWidth == $inner[0].offsetWidth ? $outer[0].clientWidth : $inner[0].offsetWidth;

            $outer.remove();

            return outerWidth - innerWidth;
        },

        /**
         * 쿠키 설정하기
         * @param name
         * @param value
         * @param days
         */
        setCookie: function(name, value, days){
            var expire = new Date(),
                cookies;

            expire.setDate(expire.getDate() + days);
            cookies = name + '=' + encodeURI(value) + '; path=/ ';

            if(typeof days != 'undefined') {
                cookies += ';expires=' + expire.toGMTString() + ';';
            }

            document.cookie = cookies;
        },

        /**
         * 쿠키값 가져오기
         * @param name
         * @returns {string}
         */
        getCookie: function(name) {
            var cookieData = document.cookie,
                start,
                value = '';

            name = name + '=';
            start = cookieData.indexOf(name);

            if(start != -1) {
                var end;

                start += name.length;
                end = cookieData.indexOf(';', start);

                if(end == -1) {
                    end = cookieData.length;
                }

                value = cookieData.substring(start, end);
            }

            return decodeURI(value);
        },

        commify: function(num) {
            var reg = /(^[+-]?\d+)(\d{3})/; // 정규식

            if(num !== 0 && !num) {
                return num;
            }

            num = num + '';

            while (reg.test(num))
                num = num.replace(reg, '$1' + ',' + '$2');

            return num;
        }
    };
});