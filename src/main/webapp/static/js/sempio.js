/**
 * 샘표 소개 > 샘표
 */
define(['ui', 'jquery', 'underscore', 'gsap', 'about', 'simpletab'], function(IG, $, _, gsap, About) {
    'use strict';

    function init() {
        var $tabs = $('#place-info')

        // 콘텐츠 탭 처리
        $tabs.simpleTab({
            activeClass: 'is-current',
            scroll: true,
            scrollOffset: -80
        });
    }
    $(function() {
        init();
    });
});