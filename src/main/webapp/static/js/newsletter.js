/**
 * 새소식 페이지
 */
define(['ui', 'jquery', 'underscore', 'gsap', 'masonry', 'drawborder'], function(IG, $, _, gsap, Masonry) {

    $(function() {
        //links rollover effect
        $('.block').drawBorder();
    });

    require(['jquery-bridget/jquery.bridget'], function(jQueryBridget) {
        jQueryBridget( 'masonry', Masonry, $ );

        var msnList = $('#newsletter-list').masonry({
            itemSelector: '.item',
            gutterWidth: 0,
            isAnimated: true,
            animationOptions: {
                duration: 300,
                queue: false
            }
        });

        msnList.imagesLoaded().progress(function() {
            msnList.masonry('layout');
            IG.contentHeight = $(document).height();
        });
    });

});