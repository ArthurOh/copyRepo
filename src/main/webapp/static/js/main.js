/**
 * 메인 페이지
 */
var isYTReady = false;

function onYouTubePlayerAPIReady() {
    window.isYTReady = true;
}

define(['ui', 'jquery', 'underscore', 'gsap', 'slick', 'scroller', 'drawborder', 'inview', 'ui/fixednotice', 'https://www.youtube.com/player_api'], function(IG, $) {
    'use strict';

    $(function() {
        /**
         * links rollover effect
         */
        (function() {
            $('.home .block').drawBorder();
        })();

        /**
         * menu block background image polyfill
         */
        (function() {
            if(!IG.isLegacyIE) {
                return;
            }

            var $bannerItem = $('.banner-item'),

                itemWidth = $bannerItem.outerWidth(),
                itemHeight = $bannerItem.outerHeight(),

                $bg = $('<span></span>').css({
                    position: 'absolute',
                    left: (itemHeight - itemWidth) / -2,
                    width: itemHeight,
                    top: 0,
                    bottom: 0
                });

            $bannerItem.each(function() {
                var $this = $(this);

                $bg.clone().css(
                    'filter',
                    "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" +
                    $this.css('background-image').slice(5, -2) +
                    "', sizingMethod='scale')"
                ).prependTo($this.css('background-image', 'none'));
            });
        })();

        /**
         * intro slider
         */
        (function() {
            var $intro = $('.intro'),
                $slider = $intro.find('.slider'),
                $slide = $intro.find('.slide'),
                $track,

                $video = $intro.find('.video-box'),
                $close = $video.find('.video-box-close'),
                $player = $video.find('.video-box-player'),
                $caption = $video.find('.video-box-caption'),
                $captionText = $video.find('.video-box-caption-text'),

                // 슬라이더 애니메이션 속도
                speed = 700,

                // 비디오 영역 ID
                videoID = '#' + $video.attr('id'),
                videoTween,
                scroller,

                // 이전, 다음 버튼
                prev = $intro.find('.slick-prev').remove()[0].outerHTML,
                next = $intro.find('.slick-next').remove()[0].outerHTML;

            /**
             * 슬라이드 비디오 초기화
             */
            function initVideo() {
                var youtubeURL = /^https:\/\/youtu\.be\/.+/;

                // 자세히 링크가 Youtube 주소일 경우 내부 팝업으로 열리도록 처리
                $slide.each(function() {
                    var $this = $(this),
                        $detail = $this.find('.slide-detail'),

                        href = $detail.attr('href'),
                        caption = $this.find('.slide-caption').remove().html();

                    if(!youtubeURL.test(href)) {
                        return true;
                    }

                    void 0;

                    // 링크 속성 변경, 클릭 시 비디오 열기
                    $detail
                        .attr('href', videoID)
                        .removeAttr('target')
                        .removeAttr('title')
                        .on('click', function(event) {
                            openVideo(href, caption);
                            event.preventDefault();
                        });
                });

                // 비디오 영역 초기화
                scroller = $caption.scroller({
                    target: $captionText
                });

                // 비디오 닫기 버튼
                $close.on('click', function() {
                    $intro.removeClass('is-video-open');

                    videoTween = TweenMax.to($video, 0.6, {
                        height: 0,
                        ease: Power4.easeOut,
                        clearProps: 'all',
                        onComplete: function() {
                            $player.empty();
                            $video.removeClass('is-show');
                        }
                    });
                });
            }

            /**
             * 비디오 열기
             * @param {String} url - Youtube URL
             * @param {String} caption
             */
            function openVideo(url, caption) {
                void 0;

                var $youtube = $('<a href=' + url + '>' + url + '</a>');

                function open() {
                    void 0;

                    var popupHeight = $video.height();

                    $intro.addClass('is-video-open');

                    scroller.scroll(0);
                    scroller.update();

                    videoTween = TweenMax.fromTo($video, 0.5, {
                        height: 0
                    }, {
                        height: popupHeight,
                        visibility: 'visible',
                        ease: Power4.easeOut,
                        clearProps: 'all'
                    });
                }

                // 애니메이션 진행 시 종료
                if(typeof videoTween === 'object' && videoTween.isActive()) {
                    return;
                }

                // 플레이어 초기화, 캡션 삽입
                $video.css('visibility', 'hidden').addClass('is-show');
                $player.empty();
                $captionText.html(caption);
                $player.append($youtube);
                $youtube.ytiframe({
                    autoplay: 1,
                    onReady: open
                });

                open();
            }

            /**
             * 슬라이더 초기화 이벤트
             * @param slick
             */
            function sliderInit(slick) {
                var $target = $(slick.target),
                    $current = $target.find('.slick-current'),
                    $overlay = $('<div class="slide-overlay">'),

                    tl = new TimelineMax({ paused: true }),
                    targetWidth = $target.width(),
                    YTReadyInv;

                // 최신버전 IE에서 슬라이더 작동 시 비디오 영역이 슬라이더 영역 밖으로 나와보이는 문제 해결
                if(IG.isIEEdge) {
                    $track = $target.find('.slick-track');
                    $track.css('opacity', 0.99);
                }

                // 초기화 시 스타일링 위해 페이징 감싸기
                $target.find('.slick-dots').wrap('<div class="slick-paging">');

                if(IG.isLegacyIE) {
                    $target.find('.slide').each(function() {
                        var $this = $(this);

                        $this.css(
                            'filter',
                            'progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'' +
                            $this.data('large') +
                            '\', sizingMethod=\'scale\')'
                        );
                    });

                    return;
                }

                // 사이즈에 따라 슬라이더 배경 적용
                IG.UI.switchLayout('mainSliderBG', function() {
                    return IG.size < 2;
                }, function(isSmall) {
                    $target.find('.slide').each(function() {
                        var $this = $(this);

                        $this.css(
                            'background-image',
                            'url(' + $this.data(isSmall ? 'small' : 'large') + ')'
                        );
                    });
                });

                // 비디오 배경 적용
                if(!IG.isMobile) {
                    YTReadyInv = setInterval(function() {
                        void 0;

                        if(!isYTReady) {
                            return;
                        }

                        clearInterval(YTReadyInv);

                        applyVideoBG();
                    }, 10);
                }

                tl
                    .to($target, 0.5, { autoAlpha: 1 })
                    .from($current.find('.slide-sub'), 1, { autoAlpha: 0, y: Math.min(targetWidth / 20, 30), ease: Power4.easeOut }, '-=0.2')
                    .from($current.find('.slide-h'), 1, { autoAlpha: 0, y: Math.min(targetWidth / 20, 30), ease: Power4.easeOut }, '-=0.9')
                    .from($current.find('.slide-icon'), 1, { autoAlpha: 0, ease: Power4.easeInOut }, '-=0.7')
                    .play();


                $('.intro').css('visibility', 'visible');

                $target.find('.slick-list').append($overlay);

                void 0;
            }

            /**
             * 슬라이더 비디오 배경 설정
             */
            function applyVideoBG() {
                var youtubeURL = /^https:\/\/youtu\.be\/(.+)/,
                    RATIO = 16 / 9,
                    LOGO_HEIGHT = 45;

                $slider.find('.slide:not(.slick-cloned)').each(function() {
                    var $this = $(this),
                        $video = $this.find('.slide-video'),
                        url = $this.data('video'),
                        videoID,
                        player;

                    function resize() {
                        var width = $this.width(),
                            height = $this.height(),

                            playerSize;

                        void 0;

                        if(width / RATIO < height) {
                            playerSize = Math.ceil(height * RATIO);

                            $video
                                .width(playerSize)
                                .height(height + LOGO_HEIGHT * 2)
                                .css({
                                    left: (width - playerSize) / 2,
                                    top: -LOGO_HEIGHT
                                });
                        } else {
                            playerSize = Math.ceil(width / RATIO);

                            $video
                                .width(width)
                                .height(playerSize + LOGO_HEIGHT * 2)
                                .css({
                                    left: 0,
                                    top: (height - (playerSize + LOGO_HEIGHT * 2)) / 2
                                });
                        }
                    }

                    if(!url) {
                        return;
                    }

                    videoID = youtubeURL.exec(url);
                    videoID = videoID[1];

                    $video.append('<div id="' + videoID + '">').css('opacity', 0);

                    player = new YT.Player(videoID, {
                        videoId: videoID,
                        playerVars: {
                            autoplay: 1,
                            controls: 0,
                            disablekb: 1,
                            enablejsapi: 1,
                            loop: 1,
                            playlist: videoID,
                            showinfo: 0,
                            autohide: 1,
                            hl: 'ko',
                            iv_load_policy: 3,
                            rel: 0
                        },
                        events: {
                            onReady: function() {
                                player.mute();
                                player.setPlaybackQuality('highres');
                            },
                            onStateChange: function(state) {
                                void 0;

                                if(state.data === YT.PlayerState.PLAYING) {
                                    $video.fadeTo(500, 1);
                                    player.removeEventListener('onStateChange');
                                }
                            }
                        }
                    });

                    $slider.on('setPosition', resize);
                    resize();
                });
            }

            // 비디오 초기화
            initVideo();

            // 최신버전 IE에서 슬라이더 작동 시 비디오 오버레이 영역 렌더링이 잘못되는 문제 해결
            if(IG.isIEEdge) {
                $slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                    var $slide = $(slick.$slides[nextSlide]),
                        $slideVideo = $slide.find('.slide-video');

                    if($slideVideo.length && $slideVideo.find('iframe').length) {
                        $slideVideo.addClass('is-refresh');

                        setTimeout(function() {
                            $slideVideo.removeClass('is-refresh');
                        }, speed);
                    }
                });
            }

            // slick 실행
            $slider.on('init', sliderInit).slick({
                autoplay: true,
                speed: speed,
                easing: 'easeInOutQuart',
                dots: true,
                prevArrow: prev,
                nextArrow: next
            });
        })();

        /**
         * news ticker
         */
        (function() {
            var $news = $('.news-latest-ticker'),

                SPEED = 2500,   // 항목 넘김 속도
                DURATION = 500; // 애니메이션 속도

            $news.each(function(i) {
                var $el = $(this),
                    $list = $el.find('.news-latest-list'),
                    $item = $list.find('>li'),

                    index = 0,
                    tmr;

                /**
                 * 재생
                 */
                function play() {
                    setTimeout(function() {
                        if(tmr) {
                            clearInterval(tmr);
                        }

                        tmr = setInterval(rolling, SPEED + DURATION * 2);
                    }, SPEED + i * DURATION);
                }

                /**
                 * 정지
                 */
                function stop() {
                    clearInterval(tmr);
                }

                /**
                 * 다음 항목 넘기기
                 */
                function rolling() {
                    index++;

                    TweenMax.to($list, DURATION / 1000, {
                        y: -index * $item.height(),
                        onComplete: function() {
                            if(index >= $item.length) {
                                index = 0;
                                TweenMax.set($list, { y: 0 });
                            }
                        }
                    });
                }

                // 항목을 넘길 필요 없을 때는 실행하지 않음
                if($item.length < 2) {
                    return;
                }

                // 끊김 없이 넘기기 위해 첫번째 항목을 마지막에 복사
                $item.eq(0).clone().appendTo($list);

                // 마우스 올릴 때 일시정지, 포커스로 스크롤이 꼬이지 않도록 방지
                $el.on({
                    mouseenter: stop,
                    mouseleave: play,
                    scroll: function(event) {
                        this.scrollTop = 0;
                        event.preventDefault();
                    }
                });

                // 시작
                play();
            });
        })();
    });
});