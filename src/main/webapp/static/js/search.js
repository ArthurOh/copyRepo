/**
 * 통합검색 결과
 */
define(['ui', 'jquery', 'masonry', 'simpletab'], function(IG, $, Masonry) {

    $(function() {
        // newsletter list
        require(['jquery-bridget/jquery.bridget'], function(jQueryBridget) {
            jQueryBridget( 'masonry', Masonry, $ );

            var newsList = $('#search-newsletter-list').masonry({
                itemSelector: '.item',
                gutterWidth: 0,
                isAnimated: true,
                animationOptions: {
                    duration: 300,
                    queue: false
                }
            });
            newsList.imagesLoaded().progress(function() {
                newsList.masonry('layout');
                IG.contentHeight = $(document).height();
            });
            
            var recipeList = $('#search-recipe-list').masonry({
                itemSelector: '.item',
                gutterWidth: 0,
                isAnimated: true,
                animationOptions: {
                    duration: 300,
                    queue: false
                }
            });
            recipeList.imagesLoaded().progress(function() {
                recipeList.masonry('layout');
                IG.contentHeight = $(document).height();
            });
        });
    });
});
