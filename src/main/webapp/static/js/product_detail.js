/**
 * 제품 상세
 */
define(['ui', 'jquery', 'gsap', 'scrollto', 'slick'], function(IG, $) {
    'use strict';

    // Recipes 슬라이더
    $(function() {
        var $section = $('.carousel'),
            slickOpts = {
                recommended: {
                    responsive: [
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 2
                            }
                        }, {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 3
                            }
                        }
                    ]
                }
            };

        $section.each(function() {
            var $this = $(this),
                $list = $this.find('.carousel-list'),
                prev = $this.find('.carousel-prev').remove()[0].outerHTML,
                next = $this.find('.carousel-next').remove()[0].outerHTML,
                type = $this.attr('data-type');

            if(!$list.children().length) {
                return true;
            }

            $list.slick($.extend({
                slidesToShow: 1,
                prevArrow: prev,
                nextArrow: next,
                easing: 'easeOutQuart',
                cssEase: 'cubic-bezier(0.15, 0.8, 0.5, 1)',
                mobileFirst: true
            }, slickOpts[type]));
        });
    });

    // 컨텐츠 내 sticky 메뉴 & SNS 공유 메뉴
    var ProductDetail = (function() {
        function ProductDetail() {
            this.$contentNav = $('.content-nav'); // 컨텐츠 네비게이션
        }

        ProductDetail.prototype = {
            init: function() {
                this.setContentNav(); // 컨텐츠 네비게이션 탐색
                this.toggleShareLayer(); // SNS 공유 메뉴 열기/닫기
            },

            /**
             * 컨텐츠 네비게이션 탐색 기능
             */
            setContentNav: function() {
                var self = this,
                    $contentWrap = $('<div></div>'),
                    $items = this.$contentNav.find('.nav-item'),
                    navHeight = this.$contentNav.outerHeight(),
                    isFixed = false;

                function toggleFix() {
                    var $target = isFixed ? self.$contentNav.parent() : self.$contentNav,
                        criterion = $target.offset().top - IG.headerHeight;

                    if(criterion <= IG.scrollTop) {
                        self.$contentNav.css('top', IG.headerHeight);

                        if(isFixed) {
                           return;
                        }

                        $contentWrap.height(self.$contentNav.height());
                        self.$contentNav
                            .wrap($contentWrap)
                            .addClass('is-fixed');

                        isFixed = true;
                    } else {
                        if(!isFixed) {
                            return;
                        }

                        self.$contentNav
                            .unwrap()
                            .removeClass('is-fixed');

                        isFixed = false;
                    }
                }

                function traverse() {
                    var $target,
                        $link;

                    // 현재 대상 찾기
                    $items.each(function() {
                        var $this = $(this),
                            $_target = $($this.children('.nav-link').attr('href'));

                        if(IG.scrollTop >= $_target.offset().top - IG.headerHeight - navHeight) {
                            $target = $_target;
                            $link = $this;
                        }
                    });

                    $items.filter('.is-active').removeClass('is-active');

                    if($link) {
                        $link.addClass('is-active');
                    }
                }

                // 링크로 탐색
                $items.find('.nav-link').on('click', function(event) {
                    void 0;

                    var $target = $($(this).attr('href'));

                    IG.$doc.stop().scrollTop($target.offset().top - IG.headerHeight - navHeight + 2);

                    event.preventDefault();
                });

                // 현재 대상 찾기
                IG.$win.on({
                    scroll: function() {
                        toggleFix();
                        traverse();
                    },
                    resize: function() {
                        toggleFix();
                    }
                });

                setTimeout(function() {
                    toggleFix();
                    traverse();
                }, 0);
            },

            /**
             * SNS 공유 메뉴 열기/닫기
             */
            toggleShareLayer: function() {
                var $sns = $('.summary-sns'),
                    $toggle = $sns.find('.summary-sns-btn'),
                    $layer = $sns.find('.social'),
                    isShow = false,

                    hideProps = {
                        autoAlpha: 0,
                        scale: 0.6
                    };

                TweenMax.set($layer, hideProps);

                $toggle.on('click', function(event) {
                    if(!isShow) {
                        TweenMax.to($layer, 0.6, {
                            autoAlpha: 1,
                            scale: 1,
                            ease: Elastic.easeOut.config(1.1, 0.4)
                        });
                    } else {
                        TweenMax.to($layer, 0.2, $.extend({}, hideProps, { ease: Power3.easeIn }));
                    }

                    isShow = !isShow;
                    event.stopPropagation();
                });
                $layer.on('click', function(event) {
                    event.stopPropagation();
                });

                IG.$body.on('click.share', function() {
                    TweenMax.to($layer, 0.2, $.extend({}, hideProps, { ease: Power3.easeIn }));
                    isShow = false;
                });
            }
        };

        return ProductDetail;
    })();

    /**
     * ProductDetail 모듈 실행
     */
    if(!IG.extended.product_detail) {
        $(function() {
            var productDetail = new ProductDetail();

            productDetail.init();
        });
    }

    return ProductDetail;
});