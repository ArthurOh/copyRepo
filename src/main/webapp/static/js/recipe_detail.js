/**
 * 제품 상세
 */
define(['ui', 'jquery', 'slick'], function(IG, $) {
    'use strict';

    // Recipes 슬라이더
    $(function() {
        var $section = $('.carousel'),
            slickOpts = {
                recommended: {
                    responsive: [
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2
                            }
                        }, {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3
                            }
                        }
                    ]
                },
                related: {
                    responsive: [
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2
                            }
                        }
                    ]
                }
            };

        $section.each(function() {
            var $this = $(this),
                $list = $this.find('.carousel-list'),
                prev = $this.find('.carousel-prev').remove()[0].outerHTML,
                next = $this.find('.carousel-next').remove()[0].outerHTML,
                type = $this.attr('data-type');

            if(!$list.children().length) {
                return true;
            }

            $list.slick($.extend({
                slidesToShow: 1,
                prevArrow: prev,
                nextArrow: next,
                easing: 'easeOutQuart',
                cssEase: 'cubic-bezier(0.15, 0.8, 0.5, 1)',
                mobileFirst: true
            }, slickOpts[type]));
        });
    });
});