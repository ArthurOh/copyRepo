@import() {
    com.sempio.en.code.*
    com.sempio.en.entity.*
    com.sempio.en.command.*
    com.sempio.en.util.*
    org.apache.commons.lang3.*
}

@extends(layout.admin.main)
@args {
    List<IntNewsletterApply> list
    Paging paging
    IntNewsletterApplySearchForm form
}

@section('title') { Manage newsletter applicants : Sempio Multilingual Admin }

<div class="box box-transparent">
    <div class="box-header">
        <div class="row">
            <div class="col-xs-12">
                <form id="searchForm" class="form-horizontal" action="/admin/newsletter-apply/list/0">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Country</label>
                        <div class="col-sm-10">
                            <label class="checkbox-inline">
                                <input type="radio" name="applyLanguageType" value="" @if (StringUtils.isEmpty(form.getApplyLanguageType())) { checked }> All
                            </label>
                            <label class="checkbox-inline">
                                <input type="radio" name="applyLanguageType" value="en"  @if (StringUtils.equals(form.getApplyLanguageType(), "en")) { checked }> English
                            </label>
                            <label class="checkbox-inline">
                                <input type="radio" name="applyLanguageType" value="kr" @if (StringUtils.equals(form.getApplyLanguageType(), "kr")) { checked }> Korean
                            </label>
                            <label class="checkbox-inline">
                                <input type="radio" name="applyLanguageType" value="en_kr" @if (StringUtils.equals(form.getApplyLanguageType(), "en_kr")) { checked }> English + Korean
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Use (Y/N)</label>
                        <div class="col-sm-10">
                            <label class="checkbox-inline">
                                <input type="radio" name="state" value="" @if (StringUtils.isEmpty(form.getState())) { checked }> All
                            </label>
                            <label class="checkbox-inline">
                                <input type="radio" name="state" value="on" @if (StringUtils.equals(form.getState(),"on")) { checked }> Y(In use)
                            </label>
                            <label class="checkbox-inline">
                                <input type="radio" name="state" value="off" @if (StringUtils.equals(form.getState(),"off")) { checked }> N(Not in use)
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Searching Keword</label>
                        <div class="col-sm-2">
                            <label class="checkbox-inline">
                            	<input type="radio" name="qType" value="name" @if (StringUtils.isBlank(form.getqType()) || StringUtils.equals(form.getqType(), "name")) { checked }> Name
                            </label>
                            <label class="checkbox-inline">
                                <input type="radio" name="qType" value="email" @if (StringUtils.equals(form.getqType(), "email")) { checked }> E-mail
                            </label>
                        </div>
                        <div class="col-sm-8">
                            <input type="text" name="q" class="form-control" value="@form.getQ()" placeholder="Input Searching Keyword">
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">Search</button>
                        <a href="/admin/newsletter-apply/list" class="btn btn-flat">View All</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.box-header -->

<div class="box">
    <form id="go-list-form" method="post" data-bind-func="validateChecks">
        <div class="box-body">
	        <div class="row">
	            <div class="col-xs-6">
	            <p>@{
	            	    Integer totalCount = paging.getTotalCount();
	            	    Integer currentPage = paging.getCurrentPage() + 1;
	            	    Integer lastPage = paging.getLastPage() + 1;
	            	}Total @totalCount &nbsp;|&nbsp;@currentPage/@lastPage Page</p>
	            </div>
	        </div>
            <table class="table table-bordered table-hover">
                <colgroup>
                    <col width="2%" />
                    <col width="15%" />
                    <col width="20%" />
                    <col width="15%" />
                    <col width="15%" />
                    <col width="10%" />
                    <col width="15%" />
                    <col width="8%" />
                </colgroup>
                <thead>
                    <tr>
                        <th>
                           <input type="checkbox" class="cbr" name="idsToggle" data-bind-func="toggleCheck">
                        </th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Language</th>
                        <th>Country</th>
                        <th>Inflow route</th>
                        <th>Application date</th>
                        <th>Use (Y/N)</th>
                    </tr>
                </thead>
                <tbody>
                @for (IntNewsletterApply item : list) {
                    <tr>
                        <td>
                            <input value="@item.id@" type="checkbox" class="cbr" name="ids">
                        </td>
                        <td>
                        	@item.getName()
                        </td>
                        <td>
                           <a href="/admin/newsletter-apply/edit/@item.getId()?qs=@BaseUtil.encodeBase64(form.queryString((currentPage-1)))">@item.getEmail()</a>
                        </td>
                        <td>
                            @{
                                String [] types = item.getApplyLanguage();
                                List<String> languageList = new ArrayList();
                                if (types != null && types.length > 0) {
                                    if (ArrayUtils.contains(types, "en")) {
                                        languageList.add("English");
                                    }
                                    if (ArrayUtils.contains(types, "kr")) {
                                        languageList.add("Korean");
                                    }
                                }
                                p(StringUtils.join(languageList, ", "));
                            }
                        </td>
                        <td>@item.getCountry()</td>
                        <td>
                            @if (item.getLanguage() != null) {
                                @item.getLanguage().name().toUpperCase()
                            } else {
                                none                            
                            }
                        </td>
                        <td>@item.getCreateAt().format("yyyy-MM-dd")</td>
                        <td>
                            @if (item.getState() == State.on) {
                                <span class="label label-success">Y</span>
                            } else if (item.getState() == State.off) {
                                <span class="label label-waring">N</span>
                            }
                        </td>
					</tr>
                }
                </tbody>
            </table>
            <div class="row">
                <div class="pagination pagination-sm no-margin col-md-12 text-center">
                    @include(admin.paging_inc.rythm)
                </div>
            </div>
        </div>

        <!-- /.box-body -->

        <div class="box-footer clearfix">
            <div class="row">
                <div class="col-md-6">
                    <input value="newsletter-apply" type="hidden" name="type">
                    <input value="@form.queryString()" type="hidden" name="queryString">
                    <button type="submit" class="btn btn-sm btn-danger sp-danger-action" data-form-action="/admin/op/admin/newsletter-apply/list/@paging.getCurrentPage()/delete">Delete selected contents</button>
                    <button type="submit" class="btn btn-sm btn-default" data-form-action="/admin/op/admin/newsletter-apply/list/@paging.getCurrentPage()/on">In use selected contents</button>
                    <button type="submit" class="btn btn-sm" data-form-action="/admin/op/admin/newsletter-apply/list/@paging.getCurrentPage()/off">Not in use selected contents</button>
                </div>
                <div class="col-md-6 text-right">
                    <a href="/admin/newsletter-apply/edit" class="btn btn-flat btn-default">Register</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <button type="button" class="btn btn-flat btn-primary" id="excelBtn">Download the applicant list</button>
                </div>            
            </div>
        </div>
    </form>
</div>

@section('last') {
    <script>
        $(function(){
            nav('operation', 'newsletter-apply');
            $('#excelBtn').on('click', function(){
                var url = '/admin/newsletter-apply/excel?' + $('#searchForm').serialize();
                $.post(url, function(data){
                    if (data !== undefined && data.code === 'success') {
                        location.href = '/uploadfiles/files/'+data.path;
                    }
                });
            });
        });
    </script>
}